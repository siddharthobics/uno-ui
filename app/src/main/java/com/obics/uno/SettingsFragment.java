package com.obics.uno;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.LocationManager;
//import android.media.session.PlaybackState;
import android.provider.Settings;
//import android.support.design.widget.TextInputEditText;
import android.support.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Group;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
/*import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;*/
import android.support.v7.widget.SwitchCompat;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
//import android.util.Log;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import com.obics.uno.config.ConfigData;
import com.obics.uno.dialog.DelAccDialog;
import com.obics.uno.dialog.EnterFeedbackDlg;
import com.obics.uno.dialog.PickImageDialog;
import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
import com.obics.uno.utils.BitmapUtils;
import com.obics.uno.utils.FormatUtils;
import com.obics.uno.utils.UnicodeLimitTextWatcher;
//import com.obics.uno.utils.CustomTextInputLayout;
//import com.rengwuxian.materialedittext.MaterialEditText;
//import com.obics.uno.utils.ToolDotProgress;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class SettingsFragment extends Fragment {
    /**
     * The SETTINGS fragment
     */
    //private RecyclerView mBlockedRecycler;
    private AppCompatImageView mProfileImg;
    private EditText mEdtZipCode;
    private EditText mEdtDispName, mEdtAboutMe, mEdtWhereFrom;
    private String mZipcode, mDisplayName, mAboutMe, mWhereFrom;
    private String mCurrentPhotoPath = "", userDetail = "";
    private SharedPreferences preferences;
    private static final String FRAGMENT_DIALOG_TAG = "frgmnt_settings_dlg";
    private static final String BTN_TAG_COLLAPSED = "Collapsed";
    private static final String BTN_TAG_EXPANDED = "Expanded";
    private SwitchCompat mSwitchLoc;
    public final static int REQUEST_LOCATION = 199;
    public final static int REQUEST_CODE_LOCATION = 1;
    public final static int REQUEST_CODE_PERMISSION_STOR = 124;
    public final static int ZIP_MAX_LENGTH = 5;
    private static final int REQUEST_CAMERA = 90;
    private static final int REQUEST_GALLERY = 91;
    private static final int THUMBNAIL_DIMENS = 100;

    private static final int TYPE_ABOUT_ME = 165;
    private static final int TYPE_WHERE_FROM = 167;

    private Group location_grp, details_grp;
    private TextView mLocationHeadTxt, mUniqUser, mDetailsHeader, mPhoneNo;
    static double oldLatitude = 0, oldLongitude = 0;
    private AppCompatImageButton mExpLocBtn, mExpDetailsBtn;// mExpBlockBtn;
    private String mUploadImgData = "";
    private boolean isTouched = false;
    private View mLocationCard;
    private ViewGroup mDetailsCard;// mBlockedCard;
    private SettingsFragmentListener settingsFragmentListener;
    private FloatingActionButton uploadImg;

    public void setSettingsFragmentListener(SettingsFragmentListener mListener) {
        this.settingsFragmentListener = mListener;
    }

    public interface SettingsFragmentListener {
        void enableLocation();

        void removeLocationUpdates();

        void closeFragment();

    }

    @Override
    public void onStart() {
        isTouched = false;
        super.onStart();
    }

    private void updateZipCode() {
        isTouched = false;
        String zipStr = mEdtZipCode.getText().toString().trim();
        if (zipStr.isEmpty()) {
            isTouched = true;
            try {
                resetLocationDisplay();
            } catch (Exception e) {
            }
        } else {
            if (zipStr.length() >= ZIP_MAX_LENGTH) {
                if (!mZipcode.equalsIgnoreCase(zipStr))
                    requestUpdateZipcode(zipStr, getActivity().getApplicationContext());
            } else {
                if (isAdded())
                    Toast.makeText(getActivity().getApplicationContext(), R.string.invalid_zip_entered, Toast.LENGTH_SHORT).show();
                try {
                    resetLocationDisplay();
                } catch (Exception e) {
                }
            }
        }
    }

    @Override
    public void onStop() {
        isTouched = false;
        if (!mSwitchLoc.isChecked()) {
            updateZipCode();
        }
        String strdisp = mEdtDispName.getText().toString().trim();
        String strabout = mEdtAboutMe.getText().toString().trim();
        String strwhrfrom = mEdtWhereFrom.getText().toString().trim();

        if (!mDisplayName.equals(StringEscapeUtils.escapeJava(strdisp)) && !strdisp.isEmpty())
            updateDisplayName(strdisp, UnoApplication.getAppContext());
        if (!mAboutMe.equals(StringEscapeUtils.escapeJava(strabout)))
            updateUserDetails(strabout, getActivity().getApplicationContext(), TYPE_ABOUT_ME);
        if (!mWhereFrom.equals(StringEscapeUtils.escapeJava(strwhrfrom)))
            updateUserDetails(strwhrfrom, getActivity().getApplicationContext(), TYPE_WHERE_FROM);
//099090


        super.onStop();
    }

    public SettingsFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        initializeLocation();
        View view = inflater.inflate(R.layout.settings_page, container, false);
        return init(view);
    }

    private void showProfilePic(boolean loadAgain) {
        final String filename = preferences.getString(ConfigData.PREF_PROFILE_PIC, "");
        if (!TextUtils.isEmpty(filename) && (filename.contains(ConfigData.COMPARE_JPG.toUpperCase()) || filename.contains(ConfigData.COMPARE_JPG.toLowerCase()))) {
            // uploadImg.setText("Change Photo");
            if (loadAgain)
                Glide
                        .with(this)
                        .load(Uri.parse(filename))
                        .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(mProfileImg);
            //todo uncomment all the mProfileImg.setOnClickListener(new View.OnClickListener() === for zoom
            setProfileImgZoom(Uri.parse(filename).toString());
            /*mProfileImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(UnoApplication.getAppContext(), PhotoViewActivity.class);
                    i.putExtra(ConfigData.INTENT_EXTRA_DISP_PHOTO, Uri.parse(filename).toString());
                    startActivity(i);
                }
            });*/
        } else {
            //uploadImg.setText("Upload Photo");
            mProfileImg.setImageResource(R.drawable.ic_avatar);
        }


    }

    private View init(View view) {
        mProfileImg = view.findViewById(R.id.profile_pic);
        /*mBlockedRecycler = view.findViewById(R.id.recycler_view);
        mBlockedRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        mBlockedRecycler.setItemAnimator(new DefaultItemAnimator());
        mBlockedRecycler.setNestedScrollingEnabled(true);*/
        mEdtZipCode = view.findViewById(R.id.zipcode);
        mEdtZipCode.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    updateZipCode();
                }
                return false;
            }
        });
        mEdtDispName = view.findViewById(R.id.editusrname);
        mEdtAboutMe = view.findViewById(R.id.editaboutme);
        mEdtWhereFrom = view.findViewById(R.id.editwhere_from);
        mEdtDispName.addTextChangedListener(new UnicodeLimitTextWatcher(mEdtDispName, 15));
        mEdtAboutMe.addTextChangedListener(new UnicodeLimitTextWatcher(mEdtAboutMe, 90));
        mEdtWhereFrom.addTextChangedListener(new UnicodeLimitTextWatcher(mEdtWhereFrom, 45));

        mEdtAboutMe.setMaxLines(3); // Or specify a lower value if you want
        mEdtAboutMe.setHorizontallyScrolling(false);
        mEdtWhereFrom.setMaxLines(3);
        mEdtWhereFrom.setHorizontallyScrolling(false);
        mEdtWhereFrom.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String strwhrfrom = mEdtWhereFrom.getText().toString().trim();
                    if (!mWhereFrom.equals(strwhrfrom) && !strwhrfrom.isEmpty())
                        updateUserDetails(strwhrfrom, getActivity().getApplicationContext(), TYPE_WHERE_FROM);
                }
                return false;
            }
        });
        mEdtAboutMe.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String strabout = mEdtAboutMe.getText().toString().trim();
                    if (!mAboutMe.equals(strabout) && !strabout.isEmpty())
                        updateUserDetails(strabout, getActivity().getApplicationContext(), TYPE_ABOUT_ME);
                }
                return false;
            }
        });
        mEdtDispName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String strdisp = mEdtDispName.getText().toString().trim();
                    if (!mDisplayName.equals(strdisp) && !strdisp.isEmpty())
                        updateDisplayName(strdisp, getActivity().getApplicationContext());
                }
                return false;
            }
        });
        mUniqUser = view.findViewById(R.id.uniqusrname);
        mPhoneNo = view.findViewById(R.id.edt_phoneno);
        mPhoneNo.setText("+" + preferences.getString(ConfigData.PREF_PHONE_NUMBER, ""));
        String username = preferences.getString(ConfigData.PREF_USERNAME, "");
        mUniqUser.setText(username);
        mDisplayName = preferences.getString(ConfigData.PREF_DISPLAY_NAME, "");

        mAboutMe = preferences.getString(ConfigData.PREF_ABOUT_ME, "");
        mWhereFrom = preferences.getString(ConfigData.PREF_WHERE_FROM, "");

        if (!TextUtils.isEmpty(mAboutMe))
            mEdtAboutMe.setText(StringEscapeUtils.unescapeJava(mAboutMe));
        if (!TextUtils.isEmpty(mWhereFrom))
            mEdtWhereFrom.setText(StringEscapeUtils.unescapeJava(mWhereFrom));
        if (!TextUtils.isEmpty(mDisplayName))
            mEdtDispName.setText(StringEscapeUtils.unescapeJava(mDisplayName));
        /*if (mDisplayName.isEmpty())
            mEdtDispName.setText(username);
        else
            mEdtDispName.setText(mDisplayName);*/

        mEdtDispName.setSelection(mEdtDispName.getText().length());
        //mEdtDispName.setError("This name will be on your public profile");
        //mLocProgress = view.findViewById(R.id.loc_progress);
        mSwitchLoc = view.findViewById(R.id.locswitch);
        FloatingActionButton mFabLocation = view.findViewById(R.id.fabloc1);
        mFabLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
            }
        });
        View logoutBtn = view.findViewById(R.id.card6);
        View feedbackBtn = view.findViewById(R.id.card5);
        View deleteAcctBtn = view.findViewById(R.id.card4);

        View termsBtn = view.findViewById(R.id.terms_btn);
        View privacyBtn = view.findViewById(R.id.priv_btn);
        termsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAdded()) {
                    Intent intent = new Intent(getContext(), WebViewActivity.class);
                    intent.putExtra(WebViewActivity.INTENT_EXTRA_DISP_HTML, WebViewActivity.TERMS_PAGE);
                    startActivity(intent);
                }
            }
        });
        privacyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAdded()) {
                    Intent intent = new Intent(getContext(), WebViewActivity.class);
                    intent.putExtra(WebViewActivity.INTENT_EXTRA_DISP_HTML, WebViewActivity.PRIVACY_PAGE);
                    startActivity(intent);
                }
            }
        });

        mEdtZipCode.addTextChangedListener(mLocSwitchWatcher);
        String zip = preferences.getString(ConfigData.PREF_ZIPCODE, "");
        if (!TextUtils.isEmpty(zip)) {
            int zipcode = Integer.parseInt(zip);
            mZipcode = String.format("%05d", zipcode);
        } else {
            mZipcode = "";
        }
        mSwitchLoc.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                isTouched = true;
                view.requestFocus();
                //view.performClick();
                return false;
            }
        });
        mSwitchLoc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isTouched) {
                    isTouched = false;
                    if (isChecked) {
                        getLocation();
                    } else {
                    }
                }
                if (isChecked) {
                    mEdtZipCode.setText("");
                } else {
                    mEdtZipCode.requestFocus();
                }
            }
        });
        String latitude = "", longitude = "";
        if (preferences.getBoolean(ConfigData.PREF_USE_LOCATION, true)) {
            mSwitchLoc.setChecked(true);
        } else {
            mSwitchLoc.setChecked(false);
            mEdtZipCode.setText(mZipcode);
            latitude = preferences.getString(ConfigData.ZIPCODE_LATITUDE, "");
            longitude = preferences.getString(ConfigData.ZIPCODE_LONGITUDE, "");
            try {
                oldLatitude = Double.parseDouble(latitude);
                oldLongitude = Double.parseDouble(longitude);
            } catch (Exception e) {

            }
        }
        if (oldLatitude == 0 && oldLongitude == 0)
            getLocation();

        feedbackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFeedbackDlg();
            }
        });
        deleteAcctBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDelAcctDialog();
            }
        });
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLogoutDialog();
            }
        });
        uploadImg = view.findViewById(R.id.upload_photo);
        uploadImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkStoragePermission();
            }
        });
        showProfilePic(true);
        /////////////////GROUP BLOCKED////
        /*mExpBlockBtn = view.findViewById(R.id.blocked_btn);
        mExpBlockBtn.setTag(BTN_TAG_COLLAPSED);
        mExpBlockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleBlockedVw();
            }
        });
        mBlockedCard = view.findViewById(R.id.card3);
        mBlockedCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleBlockedVw();
            }
        });*/
        /////////////////GROUP LOCATION////
        mExpLocBtn = view.findViewById(R.id.location_btn);
        mExpDetailsBtn = view.findViewById(R.id.dtls_btn);
        location_grp = view.findViewById(R.id.location_group);
        details_grp = view.findViewById(R.id.dtls_group);
        mLocationHeadTxt = view.findViewById(R.id.location_header);
        mDetailsHeader = view.findViewById(R.id.dtls_header);
        mExpLocBtn.setTag(BTN_TAG_COLLAPSED);
        mExpLocBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleLocationVw();
            }
        });
        mExpDetailsBtn.setTag(BTN_TAG_COLLAPSED);
        mExpDetailsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleDetailsVw();
            }
        });
        mLocationCard = view.findViewById(R.id.card2);
        mLocationCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleLocationVw();
            }
        });
        mDetailsCard = view.findViewById(R.id.card3);
        mDetailsCard.requestDisallowInterceptTouchEvent(false);
        mDetailsCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleDetailsVw();
            }
        });

        ////////////////////////////////////////
        return view;
    }

    private void checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                showPicDialog();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_STOR);
            }
        } else {
            showPicDialog();
        }
    }

    private TextWatcher mLocSwitchWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            if (s.length() > 0)
                mSwitchLoc.setChecked(false);
        }
    };

    private void showFeedbackDlg() {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_DIALOG_TAG);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        EnterFeedbackDlg feedbackDlg = new EnterFeedbackDlg();
        feedbackDlg.show(manager, FRAGMENT_DIALOG_TAG);
    }

    private void toggleDetailsVw() {
        if (mExpDetailsBtn.getTag().toString().contains(BTN_TAG_COLLAPSED)) {
            mExpDetailsBtn.setTag(BTN_TAG_EXPANDED);
            if (getContext() != null)
                mExpDetailsBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_small_arr_black_up));
            mDetailsHeader.setVisibility(View.INVISIBLE);
            details_grp.setVisibility(ConstraintLayout.VISIBLE);
        } else {
            mExpDetailsBtn.setTag(BTN_TAG_COLLAPSED);
            if (getContext() != null)
                mExpDetailsBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_small_arr_black_down));
            mDetailsHeader.setVisibility(View.VISIBLE);
            details_grp.setVisibility(ConstraintLayout.GONE);
        }
    }

    private void toggleLocationVw() {
        if (mExpLocBtn.getTag().toString().contains(BTN_TAG_COLLAPSED)) {
            mExpLocBtn.setTag(BTN_TAG_EXPANDED);
            if (getContext() != null)
                mExpLocBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_small_arr_black_up));
            mLocationHeadTxt.setVisibility(View.INVISIBLE);
            location_grp.setVisibility(ConstraintLayout.VISIBLE);
        } else {
            mExpLocBtn.setTag(BTN_TAG_COLLAPSED);
            if (getContext() != null)
                mExpLocBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_small_arr_black_down));
            mLocationHeadTxt.setVisibility(View.VISIBLE);
            location_grp.setVisibility(ConstraintLayout.GONE);
        }
    }

    /*private void toggleBlockedVw() {
        if (mExpBlockBtn.getTag().toString().contains(BTN_TAG_COLLAPSED)) {
            mExpBlockBtn.setTag(BTN_TAG_EXPANDED);
            if (getContext() != null)
                mExpBlockBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_small_arr_black_up));
            mBlockedRecycler.setVisibility(ConstraintLayout.VISIBLE);
            mBlockedRecycler.setAdapter(new BlockedUsrAdptr());
        } else {
            mExpBlockBtn.setTag(BTN_TAG_COLLAPSED);
            if (getContext() != null)
                mExpBlockBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_small_arr_black_down));
            mBlockedRecycler.setVisibility(ConstraintLayout.GONE);
        }
    }*/

    ////////
    private void showPicDialog() {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_DIALOG_TAG);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        PickImageDialog imageDialog = new PickImageDialog();
        imageDialog.setPickImgListener(new PickImageDialog.PickImgListener() {
            @Override
            public void clickCamera() {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePicture.resolveActivity(getContext().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                    }
                    if (photoFile != null) {
                        Uri photoURI;
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                            photoURI = FileProvider.getUriForFile(getContext(),
                                    ConfigData.UNO_FILE_PROVIDER,
                                    photoFile);
                        } else {
                            photoURI = Uri.fromFile(photoFile);
                        }
                        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(takePicture, REQUEST_CAMERA);
                    }
                }
            }

            @Override
            public void removePhoto() {
                final String filename = preferences.getString(ConfigData.PREF_PROFILE_PIC, "");
                if (filename != null && (filename.contains(ConfigData.COMPARE_JPG.toUpperCase()) || filename.contains(ConfigData.COMPARE_JPG.toLowerCase()))) {
                    uploadProfileRequest(UnoApplication.getAppContext(), true);
                }
            }

            @Override
            public void clickGallery() {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                if (pickPhoto.resolveActivity(getContext().getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {

                    }
                    if (photoFile != null) {
                        startActivityForResult(pickPhoto, REQUEST_GALLERY);//one can be replaced with any action code
                    }
                }

            }
        });
        imageDialog.show(manager, FRAGMENT_DIALOG_TAG);
    }

    private void showDelAcctDialog() {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_DIALOG_TAG);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        DelAccDialog delAccDialog = new DelAccDialog();
        delAccDialog.setmConfrmDelLstnr(new DelAccDialog.DelDlgListener() {
            @Override
            public void logOut() {
                if (isAdded())
                    try {
                        if (settingsFragmentListener != null)
                            settingsFragmentListener.closeFragment();
                    } catch (Exception e) {

                    }
            }
        });
        Bundle args = new Bundle();
        args.putInt(DelAccDialog.DELETE_DLG_TYPE, DelAccDialog.DELETE_ACCOUNT_TYPE);
        delAccDialog.setArguments(args);

        delAccDialog.show(manager, FRAGMENT_DIALOG_TAG);
    }

    private void showLogoutDialog() {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_DIALOG_TAG);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        DelAccDialog delAccDialog = new DelAccDialog();
        delAccDialog.setmConfrmDelLstnr(new DelAccDialog.DelDlgListener() {
            @Override
            public void logOut() {
                if (isAdded())
                    try {
                        if (settingsFragmentListener != null)
                            settingsFragmentListener.closeFragment();
                    } catch (Exception e) {

                    }
            }
        });
        Bundle args = new Bundle();
        args.putInt(DelAccDialog.DELETE_DLG_TYPE, DelAccDialog.LOGOUT_TYPE);
        delAccDialog.setArguments(args);
        delAccDialog.show(manager, FRAGMENT_DIALOG_TAG);
    }

    private int getLocAccuracy() {
        int locationMode = 0;
        String locationProviders;
        if (isAdded()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                try {
                    locationMode = Settings.Secure.getInt(getContext().getContentResolver(), Settings.Secure.LOCATION_MODE);
                } catch (Settings.SettingNotFoundException e) {
                    e.printStackTrace();
                }
                if (locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY)
                    locationMode = ConfigData.LOC_HIGH_ACCURACY;
                else if (locationMode == Settings.Secure.LOCATION_MODE_OFF)
                    locationMode = ConfigData.LOC_TURNED_OFF;

            } else {
                locationProviders = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
                if (locationProviders.contains(LocationManager.GPS_PROVIDER) && locationProviders.contains(LocationManager.NETWORK_PROVIDER)) {
                    locationMode = ConfigData.LOC_HIGH_ACCURACY;
                } else if (TextUtils.isEmpty(locationProviders)) {
                    locationMode = ConfigData.LOC_TURNED_OFF;
                }
            }
        }
        return locationMode;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                int locationState = getLocAccuracy();
                if (locationState != ConfigData.LOC_TURNED_OFF) {
                    if (locationState == ConfigData.LOC_HIGH_ACCURACY) {
                        getLocation();
                    } else {
                        resetLocationDisplay();
                        if (mAccuracyCount == 0) {
                            if (settingsFragmentListener != null)
                                settingsFragmentListener.enableLocation();
                            Toast.makeText(UnoApplication.getAppContext(), R.string.loc_error1, Toast.LENGTH_SHORT).show();
                            mAccuracyCount++;
                        } else {
                            Toast.makeText(UnoApplication.getAppContext(), R.string.loc_error2, Toast.LENGTH_SHORT).show();
                            mAccuracyCount = 0;
                        }
                    }
                } else {
                    Toast.makeText(UnoApplication.getAppContext(), R.string.loc_error3, Toast.LENGTH_SHORT).show();
                    resetLocationDisplay();
                }
                break;
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {
                    processCameraPic();
                }
                break;
            case REQUEST_GALLERY:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    processGalleryPic(selectedImage);
                }
                break;
        }

    }

    private void processGalleryPic(Uri selectedImage) {
        try {
            int attribOrient = BitmapUtils.getPicOrientation(UnoApplication.getAppContext(), selectedImage);
            InputStream ims = getContext().getContentResolver().openInputStream(selectedImage);
            Bitmap bitmap = BitmapUtils.decodeSampledBitmapFromStream(ims, ConfigData.JPEG_REQUIRED_DIMENSION, ConfigData.JPEG_REQUIRED_DIMENSION);
            switch (attribOrient) {
                case 90:
                    bitmap = BitmapUtils.rotate(bitmap, 90);
                    break;
                case 270:
                    bitmap = BitmapUtils.rotate(bitmap, 270);
                    break;
                case 180:
                    bitmap = BitmapUtils.rotate(bitmap, 180);
                    break;
                default:
                    break;
            }
            Glide
                    .with(this)
                    .load(bitmap)
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(mProfileImg);
            FileOutputStream out = new FileOutputStream(mCurrentPhotoPath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, ConfigData.JPEG_QUALITY, out);
            if (mCurrentPhotoPath != null)
                uploadProfileRequest(UnoApplication.getAppContext(), false);
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
        }
    }


    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        //File image =new File(storageDir,"ChannelTempImg.jpg");
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void processCameraPic() {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / ConfigData.JPEG_REQUIRED_DIMENSION, photoH / ConfigData.JPEG_REQUIRED_DIMENSION);
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        try {
            ExifInterface exif = new ExifInterface(mCurrentPhotoPath);
            //for some samsung devices
            int attribOrient = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            switch (attribOrient) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = BitmapUtils.rotate(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = BitmapUtils.rotate(bitmap, 270);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = BitmapUtils.rotate(bitmap, 180);
                    break;
                default:
                    break;
            }
            FileOutputStream out = new FileOutputStream(mCurrentPhotoPath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, ConfigData.JPEG_QUALITY, out);
            // galleryAddPic();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Glide
                .with(this)
                .load(bitmap)
                .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(mProfileImg);

        if (mCurrentPhotoPath != null)
            uploadProfileRequest(UnoApplication.getAppContext(), false);

    }


    public void uploadProfileRequest(final Context con, final boolean remove) {
        if (remove)
            mUploadImgData = "";
        else {
            if (!TextUtils.isEmpty(mCurrentPhotoPath)) {
                //todo uncomment all the mProfileImg.setOnClickListener(new View.OnClickListener() === for zoom
                setProfileImgZoom(Uri.fromFile(new File(mCurrentPhotoPath)).toString());
                /*mProfileImg.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(UnoApplication.getAppContext(), PhotoViewActivity.class);
                        i.putExtra(ConfigData.INTENT_EXTRA_DISP_PHOTO, Uri.fromFile(new File(mCurrentPhotoPath)).toString());
                        startActivity(i);
                    }
                });*/
                mUploadImgData = Base64.encodeToString(BitmapUtils.getBytesFromBitmap(mCurrentPhotoPath), Base64.DEFAULT);
            }
        }
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConfigData.BASE_URL + "/profile/updateProfileImg",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            boolean status = jsonObject.optBoolean("status");
                            if (status) {
                                if (remove) {
                                    ConfigData.PROFILE_PIC = "";
                                    Toast.makeText(con, "Profile image removed!", Toast.LENGTH_SHORT).show();
                                    //todo uncomment all the mProfileImg.setOnClickListener(new View.OnClickListener() === for zoom
                                    //mProfileImg.setOnClickListener(null);
                                    setProfileImgZoom(null);
                                } else {
                                    ConfigData.PROFILE_PIC = jsonObject.optString("profile");
                                    Toast.makeText(con, R.string.img_uploaded, Toast.LENGTH_SHORT).show();
                                }
                                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(con);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString(ConfigData.PREF_PROFILE_PIC, ConfigData.PROFILE_PIC);
                                editor.apply();
                                if (isAdded())
                                    showProfilePic(remove);
                            } else {
                                Toast.makeText(con, R.string.err_img_upload, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String errString = NetUtil.getMessage(error);
                        if (!errString.isEmpty())
                            Toast.makeText(con, errString, Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("userId", "" + ConfigData.USER_ID);
                params.put("imageData", mUploadImgData);
                return params;
            }
        };
        VolleySingleton.getInstance().addToRequestQueue(stringRequest, true);
    }

  /*  private String getTruncatedStr(String str, int length) {
        if (TextUtils.isEmpty(str)) {
            return "";
        } else {
            String str2 = StringEscapeUtils.escapeJava(str);
            if (str2.length() > length) {
                str2 = str2.substring(0, length);
                int endIndex = str2.lastIndexOf("\\u");
                if (endIndex != -1) {
                    if (length - endIndex < 7) {
                        str2 = str2.substring(0, endIndex);
                    }
                }
                return str2;
            } else {
                return str2;
            }

        }
    }*/

    private void updateUserDetails(final String userDetl, final Context con, final int type) {
        JSONObject jsonBody = null;
        userDetail = "";
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);
            if (type == TYPE_ABOUT_ME) {
                userDetail = FormatUtils.getTruncatedStr(userDetl, 90);
                jsonBody.put("aboutMe", userDetail);
            } else {
                userDetail = FormatUtils.getTruncatedStr(userDetl, 45);
                jsonBody.put("whereFrom", userDetail);
            }
        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/user/updateUserDetails", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.trim().equalsIgnoreCase("true")) {
                            SharedPreferences.Editor editor = preferences.edit();
                            if (type == TYPE_ABOUT_ME) {
                                mEdtAboutMe.setText(StringEscapeUtils.unescapeJava(userDetail));
                                mEdtAboutMe.setSelection(mEdtAboutMe.getText().length());
                                mAboutMe = userDetail;
                                editor.putString(ConfigData.PREF_ABOUT_ME, userDetail);
                            } else {
                                mEdtWhereFrom.setText(StringEscapeUtils.unescapeJava(userDetail));
                                mEdtWhereFrom.setSelection(mEdtWhereFrom.getText().length());
                                mWhereFrom = userDetail;
                                editor.putString(ConfigData.PREF_WHERE_FROM, userDetail);
                            }
                            editor.apply();
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(con, "User detail updated", Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(con, errString, Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    public void updateDisplayName(final String dispName, final Context con) {
        final String displayName = FormatUtils.getTruncatedStr(dispName, 15);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);
            jsonBody.put("displayName", displayName);
        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/user/updateDisplayName", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.trim().equalsIgnoreCase("true")) {
                            mDisplayName = displayName;
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString(ConfigData.PREF_DISPLAY_NAME, displayName);
                            editor.apply();
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (mEdtDispName != null) {
                                        mEdtDispName.setText(StringEscapeUtils.unescapeJava(displayName));
                                        mEdtDispName.setSelection(mEdtDispName.getText().length());
                                    }
                                    Toast.makeText(con, R.string.disp_name_updated, Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(con, errString, Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    public void requestUpdateZipcode(final String zipcode, final Context con) {
        int zip = 0;
        if (!TextUtils.isEmpty(zipcode) && TextUtils.isDigitsOnly(zipcode)) {
            zip = Integer.parseInt(zipcode);
        }
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"userId\": " + ConfigData.USER_ID + ", \"zipCode\": " + zip + "}");
        } catch (JSONException e) {

        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/user/updateUserZipcode", jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        boolean stat = response.optBoolean("status");

                        if (stat) {
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(con, R.string.zip_updated, Toast.LENGTH_SHORT).show();
                                }
                            });
                            mZipcode = zipcode;
                            if (isAdded() && settingsFragmentListener != null)
                                settingsFragmentListener.removeLocationUpdates();
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putString(ConfigData.PREF_ZIPCODE, zipcode);
                            String ziplat = response.optString("latitude");
                            String ziplong = response.optString("longtitude");
                            if (ziplat != null && ziplong != null) {
                                editor.putString(ConfigData.ZIPCODE_LATITUDE, ziplat);
                                editor.putString(ConfigData.ZIPCODE_LONGITUDE, ziplong);
                            }
                            try {
                                oldLatitude = Double.parseDouble(ziplat);
                                oldLongitude = Double.parseDouble(ziplong);
                            } catch (Exception e) {

                            }
                            editor.putBoolean(ConfigData.PREF_USE_LOCATION, false);
                            editor.apply();
                            mSwitchLoc.setChecked(false);
                            mEdtZipCode.setText(mZipcode);
                            Intent pushNotification = new Intent(ConfigData.LOCATION_CHANGED);
                            pushNotification.putExtra("ziplocation", true);
                            LocalBroadcastManager.getInstance(UnoApplication.getAppContext()).sendBroadcast(pushNotification);
                        } else {

                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        resetLocationDisplay();
                                    } catch (Exception e) {
                                    }
                                    Toast.makeText(con, R.string.invalid_zip_entered, Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(con, errString, Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private void resetLocationDisplay() {
        boolean bget = preferences.getBoolean(ConfigData.PREF_USE_LOCATION, true);
        if (bget) {
            mSwitchLoc.setChecked(true);
            mEdtZipCode.setText("");
        } else {
            mSwitchLoc.setChecked(false);
            mEdtZipCode.setText(mZipcode);
        }
    }

    private void initializeLocation() {
        String latitude = preferences.getString(ConfigData.PREF_LATITUDE, 0 + "");
        String longitude = preferences.getString(ConfigData.PREF_LONGITUDE, 0 + "");
        try {
            oldLatitude = Double.parseDouble(latitude);
            oldLongitude = Double.parseDouble(longitude);
        } catch (NumberFormatException ne) {
            oldLatitude = 0;
            oldLongitude = 0;
        }
    }

    int mAccuracyCount = 0;

    private void getLocation() {
        mAccuracyCount = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (settingsFragmentListener != null)
                    settingsFragmentListener.enableLocation();
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
            }
        } else {
            if (settingsFragmentListener != null)
                settingsFragmentListener.enableLocation();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults != null)
            if (grantResults.length > 0)
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        getLocation();
                    } else if (permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showPicDialog();
                    }
                } else {
                    if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        Toast.makeText(UnoApplication.getAppContext(), "Please allow location access\nto use this app", Toast.LENGTH_SHORT).show();
                        resetLocationDisplay();
                    }
                }
    }

    private void setProfileImgZoom(final String path) {
        if (TextUtils.isEmpty(path))
            mProfileImg.setOnClickListener(null);
        else
            mProfileImg.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(UnoApplication.getAppContext(), PhotoViewActivity.class);
                    i.putExtra(ConfigData.INTENT_EXTRA_DISP_PHOTO, path);
                    startActivity(i);
                }
            });
    }

    //////////////////////////
    public void updateLocUi(boolean updated) {
        try {
            if (updated)
                mZipcode = "";
            mSwitchLoc.setChecked(updated);
        } catch (Exception e) {
        }
    }
////////////////////
}


////END////
