package com.obics.uno.interfaces;

public interface PagerFrgmntLstnr {

    void decrReceiveCount(int count);

    void decrSendCount(int count);

    void showReceiveCount(int count);

    void showSendCount(int count);

    void setPlaceholderButtons(int leftResource, int rightResource);

    void showLogin();

    void showSettings();

    void logOut();


}
