package com.obics.uno.utils;

public interface OtpListener {
    void onOtpEntered(String otp);
}