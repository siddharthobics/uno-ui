package com.obics.uno.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseMgr extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "UnoDatabase";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "cattable";

    public DatabaseMgr(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE "
                + TABLE_NAME
                + "(id INTEGER PRIMARY KEY,catid INTEGER, json TEXT,marksent TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        // Creating tables again
        onCreate(db);
    }

    private void insert(int categoryId, String json) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("catid", categoryId);
        values.put("json", json);
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    private void updateCategory(int categoryId, String json) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues args = new ContentValues();
        args.put("json", json);
        db.update(TABLE_NAME, args, "catid=" + categoryId, null);
        db.close();
    }

    public void insertOrUpdate(int categoryId, int intVal) {
        insertOrUpdate(categoryId, String.valueOf(intVal));
    }

    public void insertOrUpdate(int categoryId, String json) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(false, TABLE_NAME, new String[]{"id", "catid"}, "catid=?",
                new String[]{"" + categoryId}, null, null, "id desc", "2");
        if (cursor.getCount() > 0) {
            updateCategory(categoryId, json);
        } else {
            insert(categoryId, json);
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        db.close();
    }

    public String fetchCategory(int categoryId) {
        String s1 = null;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.query(false, TABLE_NAME, new String[]{"id", "catid", "json"}, "catid=?",
                new String[]{"" + categoryId}, null, null, "id desc", "1");
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            s1 = cursor.getString(2);
        }
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
        db.close();
        return s1;
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);
        db.close();
    }
}

