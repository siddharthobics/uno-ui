package com.obics.uno.utils;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.NonNull;
//import android.text.format.DateUtils;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONObject;

//import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
//import java.util.Locale;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created
 */

public class FormatUtils {
    //private static final Pattern patternMiles = Pattern.compile("[0-9]*((\\.[0-9]{0,1})?)||(\\.)?");

    private static final Pattern patternMiles = Pattern.compile("[0-9]*((\\.[0-9]?)?)");
    private static final Pattern patternPassword = Pattern.compile("\\b[a-zA-Z0-9\\-._@$#%]{6,}\\b");
    private static final Pattern patternUserName = Pattern.compile("\\b[a-zA-Z][a-zA-Z0-9\\-._]{2,}\\b");
    private static final String GMT_TIMEZONE = "UTC";
    private static final String HARDCODED_TIMEZONE = "GMT-7:00";
    private static final String HARDCODED_REFRESH_TIME = "08:00";

    private static String convertToLocalTime(String dtc) {
        String str = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            sdf.setTimeZone(TimeZone.getTimeZone(GMT_TIMEZONE));
            Date parsed = sdf.parse(dtc);
            str = convertToLATime(parsed);
        } catch (ParseException pe) {

        }
        return str;
    }

    public static String convertToLATime(Date date) {
        TimeZone tz = TimeZone.getTimeZone(HARDCODED_TIMEZONE);
        SimpleDateFormat destFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        destFormat.setTimeZone(tz);
        return destFormat.format(date);
    }

    public static boolean isWithinTimeLimit(String timeer, long base) {
        String converted = convertToLocalTime(timeer);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        Calendar msgtime = Calendar.getInstance();
        try {
            msgtime.setTimeInMillis(sdf.parse(converted).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(base);
        if (isSameDay(now, msgtime)) {
            if (now.get(Calendar.HOUR_OF_DAY) < 8) {
                return msgtime.get(Calendar.HOUR_OF_DAY) < 8;
            } else if (now.get(Calendar.HOUR_OF_DAY) >= 8) {
                return msgtime.get(Calendar.HOUR_OF_DAY) >= 8;
            }
        } else if (isYesterday(now, msgtime)) {
            if (now.get(Calendar.HOUR_OF_DAY) < 8) {
                return msgtime.get(Calendar.HOUR_OF_DAY) > 8;
            }
        }
        return false;
    }

    public static boolean isSameDay(Calendar now, Calendar lastDate) {
        return (now.get(Calendar.DAY_OF_YEAR) == lastDate.get(Calendar.DAY_OF_YEAR)) && (now.get(Calendar.YEAR) == lastDate.get(Calendar.YEAR));
    }

    public static boolean isYesterday(Calendar now, Calendar lastDate) {
        if (now.get(Calendar.YEAR) == lastDate.get(Calendar.YEAR)) {
            return (now.get(Calendar.DAY_OF_YEAR) - lastDate.get(Calendar.DAY_OF_YEAR) == 1);
        } else if (now.get(Calendar.YEAR) - lastDate.get(Calendar.YEAR) == 1) {
            return now.get(Calendar.DAY_OF_YEAR) == 1 && (lastDate.get(Calendar.DAY_OF_YEAR) >= 365);
        }
        return false;
    }

    public static int[] getHourDifference(long currentMillis) {
        String endTime = HARDCODED_REFRESH_TIME;
        Calendar time = Calendar.getInstance();
        time.setTimeInMillis(currentMillis);
        int nowHour = time.get(Calendar.HOUR_OF_DAY);
        int nowMin = time.get(Calendar.MINUTE);
        Matcher m = Pattern.compile("(\\d{2}):(\\d{2})").matcher(endTime);
        if (!m.matches())
            throw new IllegalArgumentException("Invalid time format: " + endTime);
        int endHour = Integer.parseInt(m.group(1));
        int endMin = Integer.parseInt(m.group(2));
        if (endHour >= 24 || endMin >= 60)
            throw new IllegalArgumentException("Invalid time format: " + endTime);
        int minutesLeft = endHour * 60 + endMin - (nowHour * 60 + nowMin);
        if (minutesLeft < 0)
            minutesLeft += 24 * 60; // Time passed, so time until 'end' tomorrow
        int diff[] = new int[2];
        diff[0] = minutesLeft / 60;
        diff[1] = minutesLeft % 60;
        return diff;
    }


    public static String formatChatTime(String time) {
        String str = "";
        try {
            TimeZone tz = TimeZone.getTimeZone(HARDCODED_TIMEZONE);
            Calendar thenCal = new GregorianCalendar();
            thenCal.setTimeZone(tz);
            thenCal.setTimeInMillis(Long.parseLong(time));
            Date parsed = thenCal.getTime();
            SimpleDateFormat destFormat = new SimpleDateFormat("MMM dd", Locale.US);
            destFormat.setTimeZone(tz);
            str = destFormat.format(parsed);
        } catch (Exception pe) {

        }
        return str;
    }

    // Format expected "2017-06-16 00:22:57.0"
    public static String formatSendTime(String time) {
        String str = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            sdf.setTimeZone(TimeZone.getTimeZone(GMT_TIMEZONE));
            Date parsed = sdf.parse(time);
            TimeZone tz = TimeZone.getTimeZone(HARDCODED_TIMEZONE);
            SimpleDateFormat destFormat = new SimpleDateFormat("MMM dd", Locale.US);
            destFormat.setTimeZone(tz);
            str = destFormat.format(parsed);
        } catch (ParseException pe) {

        }
        return str;
    }

    /**
     * Converting dp to pixel
     */
    public static int dpToPx(@NonNull Context context, int dp) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


    public static boolean isMilesValid(String s) {
        Matcher matcherDot = patternMiles.matcher(s);
        return matcherDot.matches();
    }


    public static boolean isValidNameText(String s) {
        Matcher matcherName = patternUserName.matcher(s);
        return matcherName.matches();
    }


    public static boolean isValidPassword(String s) {
        Matcher matcherName = patternPassword.matcher(s);
        return matcherName.matches();
    }

    public static int spToPx(Context context, float sp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }

    public static String firebaseDataToJson(String strn) {
        String retStr = null;
        try {
            String str = strn;
            str = str.replace("{", " ");
            str = str.replace("}", " ");
            String[] splitStrs = str.split(",");
            JSONObject jsonObject = new JSONObject();
            for (String s : splitStrs) {
                String[] keyValue = s.split("=");
                if (keyValue.length > 1)
                    jsonObject.put(keyValue[0].trim(), keyValue[1].trim());
            }
            retStr = jsonObject.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return retStr;
    }

    public static String getTruncatedStr(String str, int length) {
        if (TextUtils.isEmpty(str)) {
            return "";
        } else {
            String str2 = StringEscapeUtils.escapeJava(str);
            if (str2.length() > length) {
                str2 = str2.substring(0, length);
                //Log.d("MISHUI0", " ***  " + str2);
                if (str2.endsWith("\\\\"))
                    str2 = str2.substring(0, length - 2);
                else if (str2.endsWith("\\"))
                    str2 = str2.substring(0, length - 1);


                int endIndex = str2.lastIndexOf("\\u");
                if (endIndex != -1) {
                    if (length - endIndex < 7) {
                        str2 = str2.substring(0, endIndex);
                        //Log.d("MISHUI1", "   " + str2);
                        int lastIndex = str2.lastIndexOf("\\u");
                        if (endIndex - lastIndex <= 7) {
                            String str3 = str2.substring(lastIndex);
                            //Log.d("MISHUI2", " --  " + str3);

                            if (Character.isHighSurrogate(StringEscapeUtils.unescapeJava(str3).charAt(0)))
                                str2 = str2.substring(0, lastIndex);
                            //Log.d("MISHUI3", " ----  " + str2);

                        }
                    }
                }
                return str2;
            } else {
                return str2;
            }

        }
    }

    public static String escapeJavaString(String st) {
        StringBuilder builder = new StringBuilder();
        try {
            for (int i = 0; i < st.length(); i++) {
                char c = st.charAt(i);
                int code = (int) c;

                if (code > 255 && !Character.isSpaceChar(c) && !Character.isWhitespace(c)) {
                    String unicode = String.valueOf(c);
                    if (c > 0xfff) {
                        unicode = "\\\\u" + Integer.toHexString(c);
                    } else if (c > 0xff) {
                        unicode = "\\\\u0" + Integer.toHexString(c);
                    } else if (c > 0x7f) {
                        unicode = "\\\\u00" + Integer.toHexString(c);
                    }
                    //if(!(code >= 0 && code <= 255)){
                    // unicode = "\\u"+Integer.toHexString(c);
                    //}
                    builder.append(unicode);
                } else {
                    builder.append(c);
                }
            }
            Log.i("Unicode Block", builder.toString());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return builder.toString();
    }

   /* public static String getTruncatedSendStr(String str, int length) {
        if (TextUtils.isEmpty(str)) {
            return "";
        } else {
            String str2 = StringEscapeUtils.escapeJava(str);
            if (str2.length() > length) {
                str2 = str2.substring(0, length);
                //Log.d("MISHUI0", " ***  " + str2);
                if (str2.endsWith("\\\\"))
                    str2 = str2.substring(0, length - 2);
                else if (str2.endsWith("\\"))
                    str2 = str2.substring(0, length - 1);


                int endIndex = str2.lastIndexOf("\\u");
                if (endIndex != -1) {
                    if (length - endIndex < 7) {
                        str2 = str2.substring(0, endIndex);
                        //Log.d("MISHUI1", "   " + str2);
                        int lastIndex = str2.lastIndexOf("\\u");
                        if (endIndex - lastIndex <= 7) {
                            String str3 = str2.substring(lastIndex);
                            //Log.d("MISHUI2", " --  " + str3);

                            if (Character.isHighSurrogate(StringEscapeUtils.unescapeJava(str3).charAt(0)))
                                str2 = str2.substring(0, lastIndex);
                            //Log.d("MISHUI3", " ----  " + str2);

                        }
                    }
                }
                return str2;
            } else {
                return str2;
            }

        }
    }*/

}

