package com.obics.uno.utils;

import android.support.v4.view.ViewPager;
//import android.util.Log;
import android.view.View;
import android.view.ViewGroup;


public class IntroPageTransformer implements ViewPager.PageTransformer {

    @Override
    public void transformPage(View view, float position) {
        if (view instanceof ViewGroup) {
            if (position > -1 && position < 1) {
                float childWidth = ((ViewGroup) view).getChildAt(0).getWidth();
                ((ViewGroup) view).getChildAt(0).setTranslationX((position * childWidth * 0.2f));

                int tag = (int) view.getTag();
                float childWidth2 = ((ViewGroup) view).getChildAt(1).getWidth();
                if (tag > 0) {
                    if (position < 0) {
                        ((ViewGroup) view).getChildAt(1).setTranslationX((int) ((float) (childWidth2) * position));
                    } else if (position > 0) {
                        ((ViewGroup) view).getChildAt(1).setTranslationX(-(int) ((float) (childWidth2) * -position));
                    } else {
                        ((ViewGroup) view).getChildAt(1).setTranslationX(0);
                    }
                } else {
                    ((ViewGroup) view).getChildAt(2).setTranslationX((position * childWidth * 0.4f));
                }
                //}
                // ((ViewGroup) view).getChildAt(1).setTranslationX(-(position * childWidth2 * 1.44f));
                // }
                //  else
                //    ((ViewGroup) view).getChildAt(1).setTranslationX((position * childWidth2 * 1.2f));
            }


            /*for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                if (position > -1 && position < 1) {
                    float childWidth = ((ViewGroup) view).getChildAt(i).getWidth();
                    ((ViewGroup) view).getChildAt(i).setTranslationX(-(position * childWidth * 0.5f));
                }
            }*/
        }
    }
}