package com.obics.uno.utils;

import android.text.Editable;
import android.text.TextWatcher;
//import android.view.KeyEvent;
//import android.view.View;
import android.widget.EditText;

public class PhoneNumberTextWatcher implements TextWatcher {

    private static final int MAX_PHONE_LENGTH = 10;
    private EditText edTxt;
    // private boolean isDelete;
    private String oldtext = "";

    public PhoneNumberTextWatcher(EditText edTxtPhone) {
        this.edTxt = edTxtPhone;
        /*edTxt.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    isDelete = true;
                }
                return false;
            }
        });*/
    }

    private boolean backspacingFlag = false;
    private int cursorComplement;
    //private String oldtext = "";

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        oldtext = s.toString().replaceAll("[^\\d]", "");
        cursorComplement = edTxt.getSelectionStart();
        backspacingFlag = false;
        if (count > after)
            backspacingFlag = true;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        try {
            String string = editable.toString();
            String phone = string.replaceAll("[^\\d]", "");
            if (backspacingFlag) {
                cursorComplement--;
            } else {
                boolean replaced = false;
                cursorComplement++;
                if (phone.length() > MAX_PHONE_LENGTH) {
                    phone = oldtext;
                    replaced = true;
                }
                if (phone.length() == 0) {
                    editable.clear();
                } else {
                    String ans = phone;
                    //ans = phone.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "$1-$2-$3");
                    if (cursorComplement == string.length() || phone.length() == MAX_PHONE_LENGTH) {
                        if (phone.length() >= 6) {
                            ans = phone.substring(0, 3) + "-" + phone.substring(3, 6) + "-" + phone.substring(6);
                        } else if (phone.length() >= 3) {
                            ans = phone.substring(0, 3) + "-" + phone.substring(3);
                        }
                        if (!replaced) {
                            int diff = getDifference(string, ans);
                            cursorComplement += diff;
                            //Log.d("MISHU**2", cursorComplement + " " + string.length() + " " + diff);
                        }
                        try {
                            edTxt.removeTextChangedListener(this);
                            edTxt.setText(ans);
                            edTxt.setSelection(cursorComplement);
                            edTxt.addTextChangedListener(this);
                        } catch (Exception e) {
                        }
                    }
                }

            }
        } catch (Exception e) {

        }
    }

    private int getDifference(String initial, String finalstr) {
        int occurrences1 = 0, occurrences2 = 0;
        for (char c : initial.toCharArray()) {
            if (c == '-') {
                occurrences1++;
            }
        }
        for (char c : finalstr.toCharArray()) {
            if (c == '-') {
                occurrences2++;
            }
        }
        return occurrences2 - occurrences1;
    }

}