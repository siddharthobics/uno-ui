package com.obics.uno.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import org.apache.commons.lang.StringEscapeUtils;


public class UnicodeLimitTextWatcher implements TextWatcher {

    private EditText edTxt;
    private int mLimit;
    private String temp = "";
    private boolean backspacingFlag;

    public UnicodeLimitTextWatcher(EditText edTxtPhone, int limit) {
        this.edTxt = edTxtPhone;
        mLimit = limit;
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        temp = s.toString();
        backspacingFlag = false;
        if (count > after)
            backspacingFlag = true;
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        //Log.d("MISHUKA", " *** " + backspacingFlag);
        if (!backspacingFlag) {
            if (StringEscapeUtils.escapeJava(temp).length() >= mLimit) {
                edTxt.removeTextChangedListener(this);
                edTxt.setText(temp);
                edTxt.setSelection(temp.length());
                edTxt.addTextChangedListener(this);
            }
        }

    }

}