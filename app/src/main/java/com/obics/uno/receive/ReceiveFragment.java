package com.obics.uno.receive;

import android.content.Context;
//import android.content.SharedPreferences;
import android.os.Bundle;
//import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
//import android.util.Log;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
//import android.widget.Toast;

//import com.obics.uno.MainActivity;
import com.obics.uno.R;
import com.obics.uno.config.ConfigData;
import com.obics.uno.interfaces.PagerFrgmntLstnr;
//import com.obics.uno.utils.DatabaseMgr;

import org.json.JSONException;
import org.json.JSONObject;

public class ReceiveFragment extends Fragment {
    /**
     * The receive main container fragment
     */
    //private PagerFrgmntLstnr mListener;
    // private static final String RECV_FRAGMENT_COMMON_TAG = "fragment_recv_tag";
    private static final String RECV_FRAGMENT_HOME_TAG = "fragment_recv_home_tag";
    private static final String RECV_FRAGMENT_HIST_TAG = "fragment_recv_hist_tag";
    private static final String RECV_FRAGMENT_CHAT_TAG = "fragment_recv_chat_tag";

    private static final String USER_KEYWORD_ID = "userKeywordId";
    private static final String USER_KEYWORD = "userKeyword";

    private static final String MSG_MASTER_ID = "msgMasterId";
    private static final String HISTORY_ID = "historyId";
    private static final String HISTORY_KEY = "historyKey";
    private static final String CHAT_WITH_USER = "chatwith";
    private static final String ARG_RADIUS = "userRadius";
    private static final String CHAT_USR_ID = "chat_usr_id";
    private static final String CHAT_PROFILE_PIC = "chat_profile_pic";

    String redirData = "";

    public ReceiveFragment() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            PagerFrgmntLstnr mListener = (PagerFrgmntLstnr) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement PageFrgmntListener");
        }
        //this.mListener = context;
    }

    /*public void setRecvFragmentListener(PagerFrgmntLstnr mListener) {
        this.mListener = mListener;
    }*/

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        try {
            getChildFragmentManager().findFragmentById(R.id.constraintLayout).setMenuVisibility(menuVisible);
        } catch (IllegalStateException | NullPointerException e) {

        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        try {
            getChildFragmentManager().findFragmentById(R.id.constraintLayout).setUserVisibleHint(isVisibleToUser);
        } catch (IllegalStateException | NullPointerException e) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_main_recv, container, false);
        init();
        return rootView;
    }

    private void init() {
        if (redirData.isEmpty())
            showReceiveHomeFragment(false);
        else if (redirData.equals(ConfigData.REDIRECT_DATA_RECV)) {
            FragRecvHome fragRecvChat = (FragRecvHome) getChildFragmentManager().findFragmentByTag(RECV_FRAGMENT_HOME_TAG);
            if (fragRecvChat == null) {
                showReceiveHomeFragment(false);
            }
        } else
            setupRedirect(redirData);

    }

    public void setupRedirectData(String str) {
        redirData = str;
        if (isAdded())
            init();
    }

    private void setupRedirect(String title) {
        if (!title.isEmpty()) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(title);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String datatype = jsonObject.optString("dataType");
            if (datatype.contains("SC")) {
                String message_master_id = jsonObject.optString("message_master_id");
                String userKeywordId = jsonObject.optString("userKeywordId");
                String chat_user_id = jsonObject.optString("chat_user_id");
                String keywordName = jsonObject.optString("keywordName");
                String chat_uniquser_name = jsonObject.optString("chat_user_name");
                String chat_user_name = jsonObject.optString("displayName");
                String radius = jsonObject.optString("radius");
                String aboutMe = jsonObject.optString("aboutMe");
                String whereFrom = jsonObject.optString("whereFrom");
                showReceiveChatFragment(Integer.parseInt(message_master_id), chat_user_name, ConfigData.REDIRECT_HOME, keywordName, radius, Integer.parseInt(chat_user_id), "", aboutMe, whereFrom, chat_uniquser_name);
            } else if (datatype.contains("RH")) {
                String keywordName = jsonObject.optString("keywordName");
                String userKeywordId = jsonObject.optString("userKeywordId");
                String radius = jsonObject.optString("radius");
                showReceiveHistFragment(keywordName, Integer.parseInt(userKeywordId), radius);
            }
        }
    }

    private void showReceiveChatFragment(int id, String str, int histId, String histKey, String radius, int chatUsrId, String chatpic, String aboutme, String wherefrom, String uniqusr) {
        FragRecvChat fragRecvChat = new FragRecvChat();
        Bundle args = new Bundle();
        args.putInt(MSG_MASTER_ID, id);
        args.putInt(HISTORY_ID, histId);
        args.putString(HISTORY_KEY, histKey);
        args.putString(CHAT_WITH_USER, str);
        args.putString(ARG_RADIUS, radius);
        args.putString(CHAT_PROFILE_PIC, chatpic);
        args.putString(ConfigData.INTENT_ABOUT_ME, aboutme);
        args.putString(ConfigData.INTENT_WHERE_FROM, wherefrom);
        args.putString("chatwithuniqusr", uniqusr);
        args.putInt(CHAT_USR_ID, chatUsrId);

        fragRecvChat.setArguments(args);
        fragRecvChat.setRecvFragmentListener(new FragRecvChat.FragRecvChatListener() {

            @Override
            public void showHomePage() {
                showReceiveHomeFragment(true);
            }

            @Override
            public void showHistPage(int id, String str, String radius) {
                showReceiveHistFragment(str, id, radius);
            }


        });
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.constraintLayout, fragRecvChat, RECV_FRAGMENT_CHAT_TAG).commit();
    }

    private void showReceiveHistFragment(String keyword, int userKeywordId, final String radius) {
        FragRecvHistory fragRecvHistory = new FragRecvHistory();
        Bundle args = new Bundle();
        args.putInt(USER_KEYWORD_ID, userKeywordId);
        args.putString(USER_KEYWORD, keyword);
        args.putString(ARG_RADIUS, radius);

        fragRecvHistory.setArguments(args);
        fragRecvHistory.setRecvFragmentListener(new FragRecvHistory.FragRecvHistListener() {


            @Override
            public void showChatPage(int id, String str, int histId, String histkey, String radius, int chatUsrId, String chatpic, String aboutme, String wherefrom, String uniqusr) {
                showReceiveChatFragment(id, str, histId, histkey, radius, chatUsrId, chatpic, aboutme, wherefrom, uniqusr);
            }

            @Override
            public void showRecvHome() {
                showReceiveHomeFragment(true);
            }

        });
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.constraintLayout, fragRecvHistory, RECV_FRAGMENT_HIST_TAG).commit();
    }

    private void showReceiveHomeFragment(boolean launchFromNavi) {
        FragRecvHome fragRecvHome = new FragRecvHome();
        fragRecvHome.setFragRecvHomeListener(new FragRecvHome.FragRecvHomeListener() {
            @Override
            public void showRecvItemsPage(String str, int userKeywordId, String radius) {
                showReceiveHistFragment(str, userKeywordId, radius);
            }

            @Override
            public void showRecvChatPage(int id, String str, int histid, String histkey, int chatUserId, String chatpic, String aboutme, String wherefrom, String uniqusr) {
                showReceiveChatFragment(id, str, histid, histkey, "0", chatUserId, chatpic, aboutme, wherefrom, uniqusr);

            }


        });
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.constraintLayout, fragRecvHome, RECV_FRAGMENT_HOME_TAG).commit();
        //if (!launchFromNavi)
        //  fragRecvHome.setUserVisibleHint(true);
    }

    ////END////
    public void updateReceiveHome(boolean showProgress, int msg_masterid) {
        try {
            if (isAdded()) {
                if (showProgress)
                    setupRedirectData("");
                FragRecvHome fragRecvHome = (FragRecvHome) getChildFragmentManager().findFragmentByTag(RECV_FRAGMENT_HOME_TAG);
                if (fragRecvHome != null) {
                    fragRecvHome.requestRecvHomeList(true, false);
                }
                FragRecvHistory fragRecvHistory = (FragRecvHistory) getChildFragmentManager().findFragmentByTag(RECV_FRAGMENT_HIST_TAG);
                if (fragRecvHistory != null) {
                    fragRecvHistory.refreshPage(false, msg_masterid);
                }
                FragRecvChat fragRecvChat = (FragRecvChat) getChildFragmentManager().findFragmentByTag(RECV_FRAGMENT_CHAT_TAG);
                if (fragRecvChat != null) {
                    fragRecvChat.refreshChat(msg_masterid);
                }
            }
        } catch (IllegalStateException | NullPointerException e) {

        }
    }

}