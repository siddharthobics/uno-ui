package com.obics.uno.receive;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.ContextThemeWrapper;


import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
//import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
//import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
//import com.google.gson.JsonObject;
//import com.obics.uno.MainActivity;
import com.obics.uno.R;
import com.obics.uno.UnoApplication;
import com.obics.uno.config.ConfigData;
import com.obics.uno.dialog.BlockConfirmDialog;
import com.obics.uno.dialog.EnterFeedbackDlg;
import com.obics.uno.interfaces.PagerFrgmntLstnr;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
//import com.obics.uno.utils.DatabaseMgr;
import com.obics.uno.dialog.ProfileDialogFragment;
import com.obics.uno.utils.FormatUtils;
import com.obics.uno.utils.ToolDotProgress;
import com.obics.uno.utils.UnicodeLimitTextWatcher;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class FragRecvChat extends Fragment {
    /**
     * The receive fragment
     */
    private PagerFrgmntLstnr mMainListener;
    private RecyclerView mRecyclerView;
    private FragRecvChatListener mListener;
    private ToolDotProgress mProgress;
    private RecvChatAdapter mRecvMsgAdapter;
    private ReqChatRspns mRecvChatRspns = new ReqChatRspns();
    private TextView mHeadingText, mTimeTxt, mHeaderTopTxt;
    private EditText mChatInput;
    private AppCompatImageView mSendButton;
    private static final String FRAGMENT_REPORT = "fragment_report";
    private static final String FRAGMENT_BLOCK = "fragment_block";

    private static final String CHAT_USR_ID = "chat_usr_id";
    private String mHistoryKey = "", mChatWithUser = "", mUsrRadius = "", mChatPic = "", mAboutme = "", mWherefrom = "", mChatUniqUsr = "";
    private int mHistoryId = ConfigData.REDIRECT_HOME, mMsgMasterId, mChatUserId, mRootHeight = 0;
    private View rootView;
    private Handler handler;
    private AppCompatImageButton mTopLeftBtn, mTopRightBtn, topimg;
    private static final String MSG_MASTER_ID = "msgMasterId";
    private static final String HISTORY_ID = "historyId";
    private static final String HISTORY_KEY = "historyKey";
    private static final String CHAT_WITH_USER = "chatwith";
    private static final String ARG_RADIUS = "userRadius";
    private static final String REQ_TAG_RECV_CHAT = "requestrecvchat";
    private static final String CHAT_PROFILE_PIC = "chat_profile_pic";
    private static final String FRAGMENT_BOTTOM_DLG_TAG = "bottom_dialog_tag";
    private boolean fcmUploaded = false;
    private String fcmKey = "", msg = "";
    private boolean isVisible = true, isBlocked = true, isAdminPage = false;

    private Runnable runnable = new Runnable() {
        public void run() {
            requestRecvChatContent(mMsgMasterId, false, false);
            handler.postDelayed(this, ConfigData.REFRESH_TIME_CHAT);
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mMainListener = (PagerFrgmntLstnr) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement PageFrgmntListener");
        }
    }

    private TextWatcher mChatTxtWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mSendButton.setEnabled(s.length() > 0);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public interface FragRecvChatListener {
        void showHomePage();

        void showHistPage(int id, String str, String radius);

    }

    public FragRecvChat() {

    }

    public void setRecvFragmentListener(FragRecvChatListener mListener) {
        this.mListener = mListener;
        if (mMainListener != null)
            mMainListener.setPlaceholderButtons(R.drawable.ic_left_arr, R.drawable.ic_menu_dot);
    }


    private void setupExternalButtons() {
        mHeadingText.setText("#" + mHistoryKey);
        if (mHistoryId != ConfigData.REDIRECT_HOME) {
            mTopLeftBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isAdded())
                        hideSoftKeyboard(getContext(), mChatInput);
                    if (mListener != null)
                        mListener.showHistPage(mHistoryId, mHistoryKey, mUsrRadius);
                }
            });
        } else {
            mTopLeftBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isAdded())
                        hideSoftKeyboard(getContext(), mChatInput);
                    if (mListener != null)
                        mListener.showHomePage();
                }
            });
        }
        mTopRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(v, "hello");
            }
        });
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            if (mRecyclerView != null)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
            if (mMainListener != null)
                mMainListener.setPlaceholderButtons(R.drawable.ic_left_arr, R.drawable.ic_menu_dot);
        } else {
            if (mRecyclerView != null)
                mRecyclerView.setBackgroundColor(Color.parseColor(ConfigData.COLOR_DARK_TRANSPARENCY));
        }

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            showTopButtons(true);
            if (!isVisible)
                requestRecvChatContent(mMsgMasterId, true, false);
        } else {
            showTopButtons(false);
        }
        isVisible = isVisibleToUser;

    }

    private void showTopButtons(boolean show) {
        int visible = (show ? ConstraintLayout.VISIBLE : ConstraintLayout.GONE);
        if (mTopLeftBtn != null && mTopRightBtn != null) {
            mTopLeftBtn.setVisibility(visible);
            mTopRightBtn.setVisibility(visible);
        }
        if (mRecyclerView != null) {
            if (show)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        topimg = rootView.findViewById(R.id.media_image);
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                //r will be populated with the coordinates of your view that area still visible.
                rootView.getWindowVisibleDisplayFrame(r);
                //constants for spacing and top bar
                int rootHeight = rootView.getHeight() + FormatUtils.dpToPx(UnoApplication.getAppContext(), 41) + 2;
                if (rootHeight > mRootHeight)
                    mRootHeight = rootHeight;
                int heightDiff = rootView.getRootView().getHeight() - (r.bottom - r.top);
                if (heightDiff > ConfigData.KBOARD_HEIGHT) { // if more than 100 pixels, its probably a keyboard...
                    //topimg.setVisibility(ConstraintLayout.GONE);
                    if (mRecvChatRspns != null)
                        if (mRecvChatRspns.getConversations() != null)
                            mRecyclerView.smoothScrollToPosition(mRecvChatRspns.getConversations().size());
                } else {
                    //mRootHeight = rootView.getHeight() + FormatUtils.dpToPx(UnoApplication.getAppContext(), 41) + 2;
                    //topimg.setVisibility(ConstraintLayout.VISIBLE);
                }
            }
        });
        mHistoryKey = getArguments().getString(HISTORY_KEY);
        mChatWithUser = getArguments().getString(CHAT_WITH_USER);
        mAboutme = getArguments().getString(ConfigData.INTENT_ABOUT_ME);
        mWherefrom = getArguments().getString(ConfigData.INTENT_WHERE_FROM);
        mChatUniqUsr = getArguments().getString("chatwithuniqusr");
        mChatPic = getArguments().getString(CHAT_PROFILE_PIC);
        if (mChatPic == null)
            mChatPic = "";
        mUsrRadius = getArguments().getString(ARG_RADIUS);
        mHistoryId = getArguments().getInt(HISTORY_ID);
        mMsgMasterId = getArguments().getInt(MSG_MASTER_ID);
        mChatUserId = getArguments().getInt(CHAT_USR_ID);
        mHeadingText = rootView.findViewById(R.id.topbartext);
        mHeaderTopTxt = rootView.findViewById(R.id.chat_top_name);
        mTimeTxt = rootView.findViewById(R.id.chat_top_txt);
        mTopLeftBtn = rootView.findViewById(R.id.topbarleftbtn);
        mTopRightBtn = rootView.findViewById(R.id.topbarrightbtn);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        mProgress = rootView.findViewById(R.id.progressBar);
        mProgress.changeStartColor(ContextCompat.getColor(getContext(), R.color.inboxDark));
        mProgress.changeEndColor(ContextCompat.getColor(getContext(), R.color.inBtnDarker));
        mProgress.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(mChatWithUser))
            mHeaderTopTxt.setText(StringEscapeUtils.unescapeJava(mChatWithUser));
        //else
        //  requestDisplayName(mChatUserId);


        View topbg = rootView.findViewById(R.id.topbarbase);
        topbg.setBackgroundResource(R.drawable.topbarinbox);
        // mHeadingText.setBackgroundResource(R.drawable.topbarinbox);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        topimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAdded() && !isBlocked) {
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    Fragment frag = manager.findFragmentByTag(FRAGMENT_BOTTOM_DLG_TAG);
                    if (frag != null) {
                        manager.beginTransaction().remove(frag).commitAllowingStateLoss();
                    }
                    ProfileDialogFragment bottomSheetDialog = new ProfileDialogFragment();
                    Bundle args = new Bundle();
                    args.putInt("screenheight", mRootHeight);
                    args.putString("profilepic", mChatPic);
                    args.putString("dispname", mChatWithUser);
                    args.putString("chatwithuniqusr", mChatUniqUsr);
                    args.putString(ConfigData.INTENT_ABOUT_ME, mAboutme);
                    args.putString(ConfigData.INTENT_WHERE_FROM, mWherefrom);
                    bottomSheetDialog.setArguments(args);
                    bottomSheetDialog.show(manager, FRAGMENT_BOTTOM_DLG_TAG);
                }
            }
        });

        mChatInput = rootView.findViewById(R.id.chatInput);
        mSendButton = rootView.findViewById(R.id.send_save_btn);
        if (isAdded())
            mSendButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_chat_recv));
        mSendButton.setEnabled(false);
        setupExternalButtons();
        if (isAdded()) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());
            fcmUploaded = preferences.getBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, false);
            if (fcmUploaded)
                fcmKey = preferences.getString(ConfigData.PREF_FCM_TOKEN, "");
        }
        mRecvMsgAdapter = new RecvChatAdapter(getContext(), mRecvChatRspns);
        mRecyclerView.setAdapter(mRecvMsgAdapter);
        mChatInput.addTextChangedListener(mChatTxtWatcher);
        mChatInput.addTextChangedListener(new UnicodeLimitTextWatcher(mChatInput,1000));

        if (ConfigData.UNO_NEWS_MSG_MASTER_ID == mMsgMasterId) {
            isAdminPage = true;
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(UnoApplication.getAppContext());
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(ConfigData.NEWS_FIRST_TIME, false);
            editor.apply();
            Glide
                    .with(getContext())
                    .load(R.mipmap.ic_knob_round)
                    .apply(RequestOptions.circleCropTransform()).into(topimg);
            requestAdminMsgMaster(mChatUserId);

        } else {
            requestRecvChatContent(mMsgMasterId, true, true);
            mSendButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mSendButton.setEnabled(false);
                    requestChatUpdate(mMsgMasterId);
                }
            });
            if (!mChatPic.isEmpty())
                Glide
                        .with(getContext())
                        .asBitmap()
                        .load(Uri.parse(mChatPic))
                        .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(topimg);
        }


        handler = new Handler();
        return rootView;

    }

    @Override
    public void onStart() {
        super.onStart();
        handler.postDelayed(runnable, ConfigData.REFRESH_TIME_CHAT);
    }

    protected void hideSoftKeyboard(final Context context, EditText input) {
        WeakReference<EditText> tedit = new WeakReference<>(input);
        tedit.get().setInputType(0);
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(tedit.get().getWindowToken(), 0);
        } catch (NullPointerException npe) {
        }
    }

    @Override
    public void onStop() {
        handler.removeCallbacks(runnable);
        RecvChatAdapter.recvChatCounter = 0;
        super.onStop();
    }

    private void enableInput(boolean enable) {
        mSendButton.setEnabled(enable);

    }

    private void requestRecvChatContent(int msgMasterId, final boolean showProgress, final boolean scroll) {
        if (!isVisible && !showProgress)
            return;
        if (isAdded())
            VolleySingleton.getInstance().cancelAll(REQ_TAG_RECV_CHAT);
        if (showProgress)
            mProgress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);
            jsonBody.put("messageMasterId", msgMasterId);
        } catch (JSONException e) {

        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/message/getConversations", jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgress.setVisibility(View.GONE);
                        mRecvChatRspns = new Gson().fromJson(response.toString(), ReqChatRspns.class);
                        checkDuplicateLogin(mRecvChatRspns.getUserAppId());
                        isBlocked = mRecvChatRspns.getIsBlocked();
                        if (isBlocked) {
                            //Toast.makeText(getContext().getApplicationContext(), "User blocked", Toast.LENGTH_SHORT).show();
                            mChatInput.setEnabled(false);
                            mTimeTxt.setVisibility(View.VISIBLE);
                            mTimeTxt.setText("You have blocked this user");
                            mTimeTxt.setPadding(12, 32, 12, 2);
                        } else {
                            int unreadcnt = 0;
                            int listsize = mRecvChatRspns.getConversations().size();
                            for (int u = listsize - 1; u >= 0; u--) {
                                String str = mRecvChatRspns.getConversations().get(u).getReadStatus();
                                int userid = mRecvChatRspns.getConversations().get(u).getUserId();
                                if (str.contains("unRead") && userid != ConfigData.USER_ID) {
                                    unreadcnt++;
                                }
                            }
                            if (mMainListener != null)
                                mMainListener.decrReceiveCount(unreadcnt);

                            if (mChatPic.isEmpty() && listsize >= 1) {
                                String url = mRecvChatRspns.getConversations().get(0).getProfileImg();
                                if (isAdded())
                                    if (url != null)
                                        Glide
                                                .with(getContext())
                                                .asBitmap()
                                                .load(Uri.parse(url))
                                                .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(topimg);
                            }
                            mTimeTxt.setVisibility(View.VISIBLE);
                            mTimeTxt.setPadding(2, 2, 2, 2);

                            mTimeTxt.setText(FormatUtils.formatChatTime(mRecvChatRspns.getConversations().get(0).getInsertDate()));
                            mRecvMsgAdapter.setNewData(mRecvChatRspns);
                            mRecvMsgAdapter.notifyDataSetChanged();
                            if (scroll)
                                mRecyclerView.scrollToPosition(listsize - 1);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //enableInput(true);
                mProgress.setVisibility(View.GONE);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    showToast(R.string.no_internet_conn);
            }
        });
        req.setTag(REQ_TAG_RECV_CHAT);
        VolleySingleton.getInstance().addToRequestQueue(req, true);
    }

    private void requestChatUpdate(final int msgMasterId) {
        msg = "";
        msg = FormatUtils.getTruncatedStr(mChatInput.getText().toString().trim(), 1000);

        /*msg = StringEscapeUtils.escapeJava(mChatInput.getText().toString().trim());
        if (msg.length() > 1000)
            msg = msg.substring(0, 1000);*/
        //final String msg = mChatInput.getText().toString().trim();
        mChatInput.setText("");
        if (msg.isEmpty())
            return;
        mProgress.setVisibility(View.VISIBLE);
        enableInput(false);

        JSONObject jsonBody = null;

        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);
            jsonBody.put("messageMasterId", msgMasterId);
            jsonBody.put("message", msg);

            //jsonBody = new JSONObject("{\"userId\": \"" + ConfigData.USER_ID + "\", \"messageMasterId\": \"" + msgMasterId + "\", \"message\": \"" + msg + "\"}");
        } catch (JSONException e) {

        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/message/addConversation", jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        enableInput(true);
                        mProgress.setVisibility(View.GONE);
                        String stat = response.optString("status");
                        if (stat.equals("true")) {
                            mChatInput.setText("");
                            //todo update the text here
                            ReqChatRspns.Conversation conversation = new ReqChatRspns().new Conversation();
                            conversation.setContent(msg);
                            conversation.setFileName("");
                            conversation.setConversationStatus(true);
                            conversation.setUserId(ConfigData.USER_ID);
                            if (mRecvChatRspns.getConversations() != null)
                                mRecvChatRspns.getConversations().add(conversation);
                            mRecvMsgAdapter.setNewData(mRecvChatRspns);
                            mRecvMsgAdapter.notifyDataSetChanged();

                            requestRecvChatContent(msgMasterId, true, true);
                        } else {
                            if (isAdded())
                                Toast.makeText(getContext().getApplicationContext(), "  \"" + mChatWithUser + "\" is not available  ", Toast.LENGTH_SHORT).show();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                enableInput(true);
                mProgress.setVisibility(View.GONE);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    showToast(R.string.no_internet_conn);
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, true);
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view, String link) {
        // inflate menu
        Context wrapper = new ContextThemeWrapper(getContext(), R.style.CustomPopup);
        PopupMenu popup = new PopupMenu(wrapper, view);
        MenuInflater inflater = popup.getMenuInflater();
        if (isBlocked || isAdminPage) {
            inflater.inflate(R.menu.menu_unblok, popup.getMenu());
        } else {
            inflater.inflate(R.menu.menu_main, popup.getMenu());
        }
        popup.setOnMenuItemClickListener(new CategoryMenuClickListener(link));
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class CategoryMenuClickListener implements PopupMenu.OnMenuItemClickListener {
        String strLink = "";

        public CategoryMenuClickListener(String link) {
            strLink = link;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.block_user:
                    showBlockDlg();

                    return true;
                case R.id.report:
                    showFeedbackDlg();
                    //requestReportUser();
                    return true;
                default:
            }
            return false;
        }
    }

    ////END////
    private void showBlockDlg() {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_BLOCK);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        BlockConfirmDialog blockConfirmDialog = new BlockConfirmDialog();
        blockConfirmDialog.setBlockLstnr(new BlockConfirmDialog.BlockListener() {
            @Override
            public void userBlocked() {
                isBlocked = true;
                mChatInput.setEnabled(false);
                mRecyclerView.setAdapter(null);
                mRecyclerView.removeAllViewsInLayout();
                mTimeTxt.setVisibility(View.VISIBLE);
                mTimeTxt.setText("You have blocked this user");
                mTimeTxt.setPadding(12, 32, 12, 2);
            }
        });
        Bundle args = new Bundle();

        args.putString(BlockConfirmDialog.USER_NAME, mChatUniqUsr);
        args.putInt(BlockConfirmDialog.USER_ID, mChatUserId);
        blockConfirmDialog.setArguments(args);
        blockConfirmDialog.show(manager, FRAGMENT_BLOCK);
    }

    private void showToast(int resid) {
        if (isAdded())
            Toast.makeText(getContext().getApplicationContext(), resid, Toast.LENGTH_SHORT).show();

    }

    public void refreshChat(int msgMasterId) {
        if (mMsgMasterId == msgMasterId)
            requestRecvChatContent(mMsgMasterId, false, true);

    }

    private void checkDuplicateLogin(String appid) {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID)
            if (fcmUploaded && appid != null)
                if (!fcmKey.isEmpty())
                    if (!appid.trim().equals(fcmKey.trim())) {
                        mRecyclerView.removeAllViews();
                        if (mMainListener != null)
                            mMainListener.logOut();
                    }
    }

    private void showFeedbackDlg() {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_REPORT);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        EnterFeedbackDlg feedbackDlg = new EnterFeedbackDlg();
        Bundle args = new Bundle();
        args.putInt(EnterFeedbackDlg.USER_ID, mChatUserId);
        feedbackDlg.setArguments(args);
        feedbackDlg.show(manager, FRAGMENT_REPORT);
    }

    @Override
    public void onResume() {
        super.onResume();
        mChatInput.setFocusableInTouchMode(true);
        mChatInput.requestFocus();
        //getView().setFocusableInTouchMode(true);
        mChatInput.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (mHistoryId != ConfigData.REDIRECT_HOME) {
                            if (mListener != null)
                                mListener.showHistPage(mHistoryId, mHistoryKey, mUsrRadius);
                        } else {
                            if (mListener != null)
                                mListener.showHomePage();
                        }
                        return true;
                    }
                }
                return false;
            }
        });
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (mHistoryId != ConfigData.REDIRECT_HOME) {
                            if (mListener != null)
                                mListener.showHistPage(mHistoryId, mHistoryKey, mUsrRadius);
                        } else {
                            if (mListener != null)
                                mListener.showHomePage();
                        }
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public void requestAdminMsgMaster(int userId) {
        JSONObject jsonBody = null;
        mProgress.setVisibility(View.VISIBLE);

        try {
            jsonBody = new JSONObject("{\"userKeywordId\": \"" + userId + "\",\"userId\":" + ConfigData.USER_ID + "}");

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                    ConfigData.BASE_URL + "/message/getMatchingInUser", jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            mProgress.setVisibility(View.GONE);

                            RecvHistRspns mRecvHistRspns = new Gson().fromJson(response.toString(), RecvHistRspns.class);
                            if (mRecvHistRspns.getStatus()) {
                                mChatUserId = 0;
                                mMsgMasterId = mRecvHistRspns.getMatchingUsers().get(0).getMessageMasterId();
                                requestRecvChatContent(mMsgMasterId, true, true);
                                mSendButton.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mSendButton.setEnabled(false);
                                        requestChatUpdate(mMsgMasterId);
                                    }
                                });
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgress.setVisibility(View.GONE);

                    String errString = NetUtil.getMessage(error);
                    if (!errString.isEmpty())
                        showToast(R.string.no_internet_conn);
                }
            });

            VolleySingleton.getInstance().addToRequestQueue(req, true);
        } catch (JSONException | IllegalStateException | NullPointerException e) {

        }
    }

    /*public void requestDisplayName(int userId) {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"userKeywordId\": \"-100\",\"userId\":" + userId + "}");

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                    ConfigData.BASE_URL + "/message/getMatchingInUser", jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            RecvHistRspns mRecvHistRspns = new Gson().fromJson(response.toString(), RecvHistRspns.class);
                            mHeaderTopTxt.setText(mRecvHistRspns.getDisplayName());
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String errString = NetUtil.getMessage(error);
                    if (!errString.isEmpty())
                        showToast(R.string.no_internet_conn);
                }
            });

            VolleySingleton.getInstance().addToRequestQueue(req, true);
        } catch (JSONException | IllegalStateException | NullPointerException e) {

        }
    }*/


}