package com.obics.uno.receive;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
//import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
//import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

//import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
//import com.obics.uno.MainActivity;
import com.obics.uno.MapActivity;
import com.obics.uno.R;
import com.obics.uno.config.ConfigData;
import com.obics.uno.dialog.ConfirmDeleteDialog;
import com.obics.uno.dialog.DelAccDialog;
import com.obics.uno.interfaces.PagerFrgmntLstnr;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
//import com.obics.uno.utils.DatabaseMgr;
import com.obics.uno.utils.FormatUtils;
import com.obics.uno.utils.ToolDotProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class FragRecvHistory extends Fragment {
    /**
     * The receive fragment
     */
    private PagerFrgmntLstnr mMainListener;

    private RecyclerView mRecyclerView;
    private FragRecvHistListener mListener;
    private ToolDotProgress mProgress;
    private RecvHistAdapter mRecvHistAdapter;
    private RecvHistRspns mRecvHistRspns = new RecvHistRspns();
    private TextView mHeadingText, mErrorText, mUpdateBtnTxt;
    private boolean fcmUploaded = false;
    private String fcmKey = "";
    private static final int RECV_MAP_REQUEST = 233;
    //private static int UPDATE_MAP_RESULT = 493;
    private int mUserKeywordId = -1;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View updateView, mErrIncrRadiusbg;
    private String mRadius = "", mKeyword = "";//prevTxt = "";
    //private static final double MAX_MILES = 30.0;
    private double miles = 0;// initialMiles = 0;
    private static final String FRAGMENT_DIALOG_TAG = "fragment_recv_hist";
    public static final String ARG_RADIUS = "userRadius";
    public static final String ARG_USER_KEYWORD = "userKeyword";
    public static final String ARG_USER_KEYWORD_ID = "userKeywordId";
    private boolean isVisible = true;
    private AppCompatImageButton mTopLeftBtn, mTopRightBtn;
    private AppCompatButton mErrIncrRadius;

    private Handler handler;
    // private DatabaseMgr datastore;

    Runnable runnable = new Runnable() {
        public void run() {
            requestRecvHistList(mUserKeywordId, false, false);
            handler.postDelayed(this, ConfigData.REFRESH_TIME);
        }
    };

    interface FragRecvHistListener {
        void showChatPage(int id, String str, int histid, String histkey, String radius, int chatUserId, String chatpic, String aboutme, String wherefrom, String uniqusr);

        void showRecvHome();

        //void setPlaceholders();

        //void logOut();

        //void updateReadCount(int readcount);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //Log.d("UNOLOGK1", context + "  **  ");
        try {
            mMainListener = (PagerFrgmntLstnr) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement PageFrgmntListener");
        }
        //this.mListener = context;
    }

    public FragRecvHistory() {

    }

    private void setupExternalButtons() {
        mHeadingText.setText("#" + mKeyword);
        mUpdateBtnTxt.setText("UPDATE RADIUS " + miles + " mi.");

        mTopLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.showRecvHome();
            }
        });
        mTopRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleHomeDelete();
            }
        });
    }

    public void setRecvFragmentListener(FragRecvHistListener mListener) {
        this.mListener = mListener;
        if (mMainListener != null)
            mMainListener.setPlaceholderButtons(R.drawable.ic_left_arr, R.drawable.ic_del1);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            if (mRecyclerView != null)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
            if (mMainListener != null)
                mMainListener.setPlaceholderButtons(R.drawable.ic_left_arr, R.drawable.ic_del1);
        } else {
            if (mRecyclerView != null)
                mRecyclerView.setBackgroundColor(Color.parseColor(ConfigData.COLOR_DARK_TRANSPARENCY));
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (mRecvHistAdapter != null) {
            resetDelete();
        }
        if (isVisibleToUser) {
            showTopButtons(true);
            if (!isVisible)
                refreshPage(false, mUserKeywordId);
        } else {
            showTopButtons(false);
            mRadius = String.format(Locale.US, "%.1f", Math.round(miles * 10.0) / 10.0);
        }
        isVisible = isVisibleToUser;

    }

    private void resetDelete() {
        //mRecyclerView.scrollToPosition(0);
        if (mRecvHistAdapter.getShowDelete()) {
            mRecvHistAdapter.setShowDelete(false);
            mRecvHistAdapter.notifyDataSetChanged();
        }
    }

    private void showTopButtons(boolean show) {
        int visible = (show ? ConstraintLayout.VISIBLE : ConstraintLayout.GONE);
        if (mTopLeftBtn != null && mTopRightBtn != null) {
            mTopLeftBtn.setVisibility(visible);
            mTopRightBtn.setVisibility(visible);
            if (show)
                mTopRightBtn.setImageResource(R.drawable.ic_del1);
        }
        if (mRecyclerView != null) {
            if (show)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //datastore = new DatabaseMgr(getContext());
        View rootView = inflater.inflate(R.layout.frag_recv_hist, container, false);
        mHeadingText = rootView.findViewById(R.id.topbar_toptext);
        mUpdateBtnTxt = rootView.findViewById(R.id.updRadiusTxt);
        updateView = rootView.findViewById(R.id.updRadius);
        mErrorText = rootView.findViewById(R.id.back_text);
        mErrIncrRadius = rootView.findViewById(R.id.increase_radius);
        mErrIncrRadiusbg = rootView.findViewById(R.id.increase_rad_bg);

        mErrorText.setVisibility(View.GONE);
        mErrIncrRadius.setVisibility(ConstraintLayout.GONE);
        mErrIncrRadiusbg.setVisibility(ConstraintLayout.GONE);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        mProgress = rootView.findViewById(R.id.progressBar);

        mProgress.setVisibility(View.GONE);
        swipeRefreshLayout = rootView.findViewById(R.id.simpleSwipeRefreshLayout);
        mTopLeftBtn = rootView.findViewById(R.id.topbarleftbtn);
        mTopRightBtn = rootView.findViewById(R.id.topbarrightbtn);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //get args
        mRadius = (getArguments().getString(ARG_RADIUS));
        mKeyword = (getArguments().getString(ARG_USER_KEYWORD));
        mUserKeywordId = getArguments().getInt(ARG_USER_KEYWORD_ID);
        if (mKeyword.equalsIgnoreCase(ConfigData.CHANNEL_NEWS_NAME)) {
            updateView.setVisibility(ConstraintLayout.GONE);
            mUpdateBtnTxt.setVisibility(ConstraintLayout.GONE);
            mErrIncrRadius.setVisibility(ConstraintLayout.GONE);
            mErrIncrRadiusbg.setVisibility(ConstraintLayout.GONE);
            mErrorText.setVisibility(View.GONE);
        }
        mErrIncrRadius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDelete();
                Intent i = new Intent(getActivity(), MapActivity.class);
                i.putExtra(ConfigData.INTENT_MAP_SCREEN_TYPE, MapActivity.UPDATE_RADIUS_SCREEN);
                i.putExtra(ARG_RADIUS, mRadius);
                i.putExtra(ARG_USER_KEYWORD, mKeyword);
                i.putExtra(ARG_USER_KEYWORD_ID, mUserKeywordId + "");
                startActivityForResult(i, RECV_MAP_REQUEST);
                try {
                    getActivity().overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_top);//R.anim.slide_from_bottom
                } catch (NullPointerException | IllegalStateException npe) {
                    npe.printStackTrace();
                }
            }
        });
        updateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetDelete();
                Intent i = new Intent(getActivity(), MapActivity.class);
                i.putExtra(ConfigData.INTENT_MAP_SCREEN_TYPE, MapActivity.UPDATE_RADIUS_SCREEN);
                i.putExtra(ARG_RADIUS, mRadius);
                i.putExtra(ARG_USER_KEYWORD, mKeyword);
                i.putExtra(ARG_USER_KEYWORD_ID, mUserKeywordId + "");
                startActivityForResult(i, RECV_MAP_REQUEST);
                try {
                    getActivity().overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_top);//R.anim.slide_from_bottom
                } catch (NullPointerException | IllegalStateException npe) {
                    npe.printStackTrace();
                }
            }
        });
        updateView.setOnTouchListener(btnlistner);
        try {
            miles = Double.parseDouble(mRadius);
            //initialMiles = miles;
        } catch (NumberFormatException nem) {

        }
        setupExternalButtons();
        if (isAdded()) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());
            fcmUploaded = preferences.getBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, false);
            if (fcmUploaded)
                fcmKey = preferences.getString(ConfigData.PREF_FCM_TOKEN, "");
        }
        mRecvHistRspns.setMatchingUsers(new ArrayList<RecvHistRspns.MatchingUser>());
        mRecvHistAdapter = new RecvHistAdapter(getContext(), mRecvHistRspns, new RecvHistAdapter.ReceiveHistListener() {
            @Override
            public void itemClicked(int id, String itemname, int chatUsrId, String chatpic, String aboutme, String wherefrom, String uniqusr) {
                mRadius = String.format(Locale.US, "%.1f", Math.round(miles * 10.0) / 10.0);
                if (mListener != null)
                    mListener.showChatPage(id, itemname, mUserKeywordId, mKeyword, mRadius, chatUsrId, chatpic, aboutme, wherefrom,uniqusr);
            }

            @Override
            public void showDeleteIconState(boolean delete) {
                if (getContext() != null && isVisible && mListener != null)
                    if (delete)
                        mTopRightBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_del2));
                    else
                        mTopRightBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_del1));
            }

            @Override
            public void deleteItem(int id, String keyword, int matchcount) {
                showConfirmDelete(id, keyword, matchcount);
            }
        });
        mRecyclerView.setAdapter(mRecvHistAdapter);
        //requestRecvHistList(mUserKeywordId, true);
        swipeRefreshLayout.setNestedScrollingEnabled(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestRecvHistList(mUserKeywordId, false, true);
            }
        });
        handler = new Handler();
        return rootView;
    }

   /* public void refreshHistPage() {
        requestRecvHistList(mUserKeywordId, false);
    }*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mProgress.setVisibility(View.GONE);
        if (requestCode == RECV_MAP_REQUEST && resultCode == MapActivity.UPDATE_MAP_RESULT) {
            mRadius = data.getStringExtra(ARG_RADIUS);
            if (mRadius != null) {
                if (mRadius.isEmpty())
                    mRadius = "0.0";
            } else {
                mRadius = "0.0";
            }
            miles = Double.parseDouble(mRadius);
            mUpdateBtnTxt.setText("UPDATE RADIUS " + mRadius + " mi.");

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onStart() {
        super.onStart();
        handler.postDelayed(runnable, ConfigData.REFRESH_TIME);
    }

    ////////
    public void toggleHomeDelete() {
        mRecvHistAdapter.toggleDelete();
        mRecvHistAdapter.notifyDataSetChanged();
    }

    @Override
    public void onStop() {
        handler.removeCallbacks(runnable);
        super.onStop();

    }

    private void showConfirmDelete(final int id, final String keyword, final int matchMsgCount) {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_DIALOG_TAG);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        ConfirmDeleteDialog deleteDialog = new ConfirmDeleteDialog();

        deleteDialog.setmConfrmDelLstnr(new ConfirmDeleteDialog.ConfirmDelDlgListener() {
            @Override
            public void clickPositive() {
                requestDeleteItem(id, keyword, matchMsgCount);
            }

            @Override
            public void clickNegative() {
                resetDelete();
            }
        });
        Bundle args = new Bundle();
        args.putInt(ConfirmDeleteDialog.CONF_DEL_DLG_TYPE, ConfirmDeleteDialog.REPLY_TYPE);
        deleteDialog.setArguments(args);
        deleteDialog.show(manager, FRAGMENT_DIALOG_TAG);
    }

    public void requestDeleteItem(int id, String keyword, final int matchMsgCount) {
        mProgress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"messageMasterId\": \"" + keyword + "\", \"userKeywordId\": \"" + id + "\", \"userOutKeywordId\": \"" + id + "\", \"userId\": " + ConfigData.USER_ID + "}");

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                    ConfigData.BASE_URL + "/message/deleteConversation", jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            boolean status = response.optBoolean("status");
                            if (status) {
                                //Log.d("KISHUP", response.toString());
                                requestRecvHistList(mUserKeywordId, true, true);
                                if (mMainListener != null)
                                    mMainListener.decrReceiveCount(matchMsgCount);
                                mProgress.setVisibility(View.GONE);
                            } else {
                                showToast(R.string.delete_failed);
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgress.setVisibility(View.GONE);
                    String errString = NetUtil.getMessage(error);
                    if (!errString.isEmpty())
                        showToast(R.string.no_internet_conn);
                }
            });
            VolleySingleton.getInstance().addToRequestQueue(req, false);
        } catch (JSONException | IllegalStateException | NullPointerException e) {

        }
    }

    private void checkDuplicateLogin(String appid) {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID)
            if (fcmUploaded && appid != null)
                if (!fcmKey.isEmpty())
                    if (!appid.trim().equals(fcmKey.trim())) {
                        mRecyclerView.removeAllViews();
                        if (mMainListener != null)
                            mMainListener.logOut();
                    }
    }

    public void requestRecvHistList(int userKeywordId, final boolean showProgress, final boolean resetDelete) {
        if (showProgress)
            mProgress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"userKeywordId\": \"" + userKeywordId + "\",\"userId\":" + ConfigData.USER_ID + "}");

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                    ConfigData.BASE_URL + "/message/getMatchingInUser", jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            mRecvHistRspns = new Gson().fromJson(response.toString(), RecvHistRspns.class);
                            //checkDuplicateLogin("22333fake");
                            checkDuplicateLogin(mRecvHistRspns.getUserAppId());
                            if (resetDelete)
                                mRecvHistAdapter.setShowDelete(false);
                            mRecvHistAdapter.setNewData(mRecvHistRspns);
                            mRecvHistAdapter.notifyDataSetChanged();
                            mErrorText.setVisibility(View.GONE);
                            mErrIncrRadius.setVisibility(ConstraintLayout.GONE);
                            mErrIncrRadiusbg.setVisibility(ConstraintLayout.GONE);

                            mProgress.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);

                            if (!mRecvHistRspns.getStatus()) {
                                if (getContext() != null) {
                                    if (isVisible && mListener != null)
                                        mTopRightBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_del1));
                                    //int padding = FormatUtils.dpToPx(getContext(), 6);
                                    if (miles >= 30) {
                                        if (isAdded())
                                            if (!mKeyword.equalsIgnoreCase(ConfigData.CHANNEL_NEWS_NAME)) {
                                                mErrorText.setVisibility(View.VISIBLE);
                                                mErrorText.setText(R.string.recv_hist_error_1);//"No one has replied to your \"" + mKeyword + "\" ping");
                                                mErrIncrRadius.setVisibility(ConstraintLayout.GONE);
                                                mErrIncrRadiusbg.setVisibility(ConstraintLayout.VISIBLE);
                                                //mErrorText.setPadding(padding, padding, padding, padding);
                                            }

                                    } else {
                                        if (isAdded()) {
                                            if (!mKeyword.equalsIgnoreCase(ConfigData.CHANNEL_NEWS_NAME)) {
                                                mErrorText.setVisibility(View.VISIBLE);
                                                // mErrorText.setPadding(padding, padding, padding, 0);
                                                mErrorText.setText(R.string.recv_hist_error_1);//("No one in your area has pinged for \"" + mKeyword + "\". Try increasing your radius.");
                                                mErrIncrRadius.setVisibility(ConstraintLayout.VISIBLE);
                                                mErrIncrRadiusbg.setVisibility(ConstraintLayout.VISIBLE);
                                            }


                                        }
                                    }
                                }
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgress.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                    String errString = NetUtil.getMessage(error);
                    if (!errString.isEmpty())
                        showToast(R.string.no_internet_conn);
                }
            });

            VolleySingleton.getInstance().addToRequestQueue(req, true);
        } catch (JSONException | IllegalStateException | NullPointerException e) {

        }
    }

    private void showToast(int resid) {
        if (isAdded())
            Toast.makeText(getContext().getApplicationContext(), resid, Toast.LENGTH_SHORT).show();
    }

    public void refreshPage(boolean resetDelete, int userKeywordId) {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID)
            if (mUserKeywordId != -1 && mUserKeywordId == userKeywordId)
                requestRecvHistList(mUserKeywordId, true, resetDelete);
    }

    private View.OnTouchListener btnlistner = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mUpdateBtnTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_small_arr_black_up, 0);
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    mUpdateBtnTxt.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_small_arr_white_up, 0);

                    break;
            }
            return false;
        }
    };

    ////END////
    @Override
    public void onResume() {
        super.onResume();
        refreshPage(true, mUserKeywordId);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (mListener != null)
                            mListener.showRecvHome();
                        return true;
                    }
                }
                return false;
            }
        });
    }


}