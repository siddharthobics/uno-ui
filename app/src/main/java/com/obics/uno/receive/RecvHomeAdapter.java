package com.obics.uno.receive;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.obics.uno.R;
import com.obics.uno.UnoApplication;
import com.obics.uno.config.ConfigData;

import java.util.Locale;

public class RecvHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private RecvHomeRspns mRecvHomeItmLst;
    ReceiveHomeListener mListener;
    boolean showDelete = false;

    interface ReceiveHomeListener {
        void newsItemClicked(int userKeywordId);

        void itemClicked(String itemname, int userKeywordId, String userRadius);

        void showDeleteIconState(boolean delete);

        void deleteItem(int id, String keyword);

    }

    public void toggleDelete() {
        if (getItemCount() > 0) {
            showDelete = !showDelete;
            if (mListener != null)
                mListener.showDeleteIconState(showDelete);
        }
    }

    public void setShowDelete(boolean showDelete) {
        if (getItemCount() > 0) {
            this.showDelete = showDelete;
            if (mListener != null)
                mListener.showDeleteIconState(showDelete);
        }
    }

    public boolean getShowDelete() {
        return showDelete;
    }

    public class RecvHomeHolder extends RecyclerView.ViewHolder {
        public TextView username, userradius, msgcount;
        public View indicator, milestxt;
        String itemname = "", createdBy = "";
        int userKeywordId = 0, keywordId = 0;
        String userRadius = "";
        CheckBox radioButton;

        public RecvHomeHolder(View view) {
            super(view);
            username = view.findViewById(R.id.sender_text);
            userradius = view.findViewById(R.id.miles_text);
            indicator = view.findViewById(R.id.msgcount);
            msgcount = view.findViewById(R.id.repcount_text);
            milestxt = view.findViewById(R.id.sub_text);

            radioButton = view.findViewById(R.id.delbtn);
            view.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (createdBy != null && mListener != null) {
                        if (createdBy.equalsIgnoreCase(ConfigData.ADMIN_KEYWORD)) {
                            mListener.newsItemClicked(userKeywordId);
                        } else
                            mListener.itemClicked(itemname, userKeywordId, userRadius);
                    } else {
                        mListener.itemClicked(itemname, userKeywordId, userRadius);
                    }
                }
            });
        }
    }


    public RecvHomeAdapter(RecvHomeRspns rspnsArrayList, ReceiveHomeListener mListener) {

        this.mRecvHomeItmLst = rspnsArrayList;
        this.mListener = mListener;
        //setHasStableIds(true);

    }

    public void setNewData(RecvHomeRspns rspnsArrayList) {

        this.mRecvHomeItmLst = rspnsArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recv_home_card, parent, false);
        return new RecvHomeHolder(itemView);

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final RecvHomeHolder catItemHolder = (RecvHomeHolder) holder;
        final KeywordList newsItem = mRecvHomeItmLst.getKeywordList().get(position);
        String keyname = newsItem.getKeyword().getKeywordName();
        if (keyname != null)
            catItemHolder.itemname = newsItem.getKeyword().getKeywordName();
        catItemHolder.keywordId = newsItem.getKeyword().getKeywordId();
        catItemHolder.createdBy = newsItem.getKeyword().getCreatedBy();
        catItemHolder.username.setText(catItemHolder.itemname);
        catItemHolder.userKeywordId = newsItem.getUserKeywordId();
        catItemHolder.userRadius = newsItem.getKiloMeter() + "";
        String miles = String.format(Locale.US, "%.1f", newsItem.getKiloMeter() + 0.0f);
        catItemHolder.userradius.setText(miles);
        int mcount = newsItem.getMatchingCount();
        if (mcount > 0) {
            String pluraltext = mcount > 1 ? "broadcasts" : "broadcast";
            catItemHolder.msgcount.setText("(" + mcount + " " + pluraltext + ")");
        } else {
            /*if (catItemHolder.createdBy != null) {
                if (catItemHolder.createdBy.equalsIgnoreCase(ConfigData.ADMIN_KEYWORD))
                    catItemHolder.msgcount.setText("(1 broadcast)");
                else
                    catItemHolder.msgcount.setText("(0 broadcasts)");
            } else*/
            catItemHolder.msgcount.setText("(0 broadcasts)");
        }
        int count = newsItem.getMatchingMessageCount();
        if (count > 0) {
            catItemHolder.indicator.setVisibility(View.VISIBLE);
            //catItemHolder.username.setTypeface(catItemHolder.username.getTypeface(), Typeface.BOLD);

        } else {
            catItemHolder.indicator.setVisibility(View.INVISIBLE);
            //catItemHolder.username.setTypeface(catItemHolder.username.getTypeface(), Typeface.NORMAL);

        }
        if (catItemHolder.createdBy != null) {
            if (catItemHolder.createdBy.equalsIgnoreCase(ConfigData.ADMIN_KEYWORD)) {
                //catItemHolder.itemname = ConfigData.CHANNEL_NEWS_NAME;
                //catItemHolder.username.setText(ConfigData.CHANNEL_NEWS_NAME);
                catItemHolder.msgcount.setText("(1 broadcast)");
                if (count <= 0) {
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(UnoApplication.getAppContext());
                    boolean first_time = preferences.getBoolean(ConfigData.NEWS_FIRST_TIME, true);
                    if (first_time)
                        catItemHolder.indicator.setVisibility(View.VISIBLE);
                    else
                        catItemHolder.indicator.setVisibility(View.INVISIBLE);
                }
            }
        }


        catItemHolder.userradius.setVisibility(ConstraintLayout.VISIBLE);
        catItemHolder.milestxt.setVisibility(ConstraintLayout.VISIBLE);
        if (catItemHolder.createdBy != null && catItemHolder.createdBy.equalsIgnoreCase(ConfigData.ADMIN_KEYWORD)) {
            catItemHolder.userradius.setVisibility(ConstraintLayout.GONE);
            catItemHolder.milestxt.setVisibility(ConstraintLayout.GONE);
            catItemHolder.radioButton.setEnabled(false);
            catItemHolder.radioButton.setVisibility(View.INVISIBLE);
        } else {
            if (showDelete) {
                catItemHolder.radioButton.setChecked(false);
                catItemHolder.radioButton.setVisibility(View.VISIBLE);
            /*if (mListener != null)
                mListener.showDeleteIconState(true);*/
            } else {
                catItemHolder.radioButton.setVisibility(View.INVISIBLE);
            /*if (mListener != null)
                mListener.showDeleteIconState(false);*/
            }
        }

        catItemHolder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //todo add ids on delete pressed
                    if (mListener != null)
                        mListener.deleteItem(newsItem.getUserKeywordId(), catItemHolder.itemname);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mRecvHomeItmLst.getKeywordList() != null) {
            int count = 0;
            int mcnt = 0;
            for (KeywordList kk : mRecvHomeItmLst.getKeywordList()) {
                if (kk.getKeyword().getCreatedBy() != null) {
                    if (kk.getKeyword().getCreatedBy().equalsIgnoreCase(ConfigData.ADMIN_KEYWORD))
                        mcnt++;
                    else
                        count++;
                } else
                    count++;
            }
            if (count > ConfigData.MAX_CHANNELS)
                count = ConfigData.MAX_CHANNELS;
            return count + mcnt;
        } else
            return 0;
    }
///////////END OF CATEGORY ADAPTER/////////////////
}
