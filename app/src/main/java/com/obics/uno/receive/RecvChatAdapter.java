package com.obics.uno.receive;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
//import android.support.v4.content.ContextCompat;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
//import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.obics.uno.PhotoViewActivity;
import com.obics.uno.R;
import com.obics.uno.config.ConfigData;
import com.obics.uno.utils.FormatUtils;
import com.obics.uno.utils.WrapWidthTextView;

import org.apache.commons.lang.StringEscapeUtils;

public class RecvChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ReqChatRspns recvChatRspns;
    //private RecvChatListener mListener;
    private Context mContext;
    private final static int TYPE_HEADER = 10;
    private final static int TYPE_ME = 11;
    private final static int TYPE_YOU = 12;
    static int recvChatCounter = 0;
    int paddin, paddingVertical;

    public void setNewData(ReqChatRspns recvChatRspns) {
        this.recvChatRspns = recvChatRspns;
        recvChatCounter = 0;
    }

    public interface RecvChatListener {
        void itemClicked(String itemname);

        //void readCountUpdate(int read);
    }

    public class RecvChatHolder extends RecyclerView.ViewHolder {
        WrapWidthTextView userName;
        AppCompatImageView chatpic;
        ConstraintLayout container;
        Guideline guideline;

        public RecvChatHolder(View view) {
            super(view);
            userName = view.findViewById(R.id.dialog_desc);
            chatpic = view.findViewById(R.id.media_image);
            container = view.findViewById(R.id.constraintLayout);
            guideline = view.findViewById(R.id.guideline1);
        }
    }

    public RecvChatAdapter(Context con, ReqChatRspns newsItemList) {
        this.mContext = con;
        this.recvChatRspns = newsItemList;
        paddin = FormatUtils.dpToPx(mContext, 6);
        paddingVertical = FormatUtils.dpToPx(mContext, 8);
        //this.mListener = mListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        switch (viewType) {
            case TYPE_HEADER:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.recv_chat_header, parent, false);
                break;
            case TYPE_YOU:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_in, parent, false);
                break;
            case TYPE_ME:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_out_recv, parent, false);
                break;
            default:
                break;
        }
        /*if (viewType == TYPE_HEADER) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recv_chat_header, parent, false);
        } else if (viewType == TYPE_YOU) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_in, parent, false);
        } else if (viewType == TYPE_ME) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_out_recv, parent, false);
        }*/
        return new RecvChatHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final RecvChatHolder recvChatHolder = (RecvChatHolder) holder;
            /*String str = recvChatRspns.getConversations().get(position).getReadStatus();
            if (str.contains("unRead")) {
                recvChatCounter++;
            }
            if (position == recvChatRspns.getConversations().size() - 1) {
                if (mListener != null)
                    mListener.readCountUpdate(recvChatCounter);

            }*/
        if (position == 0) {
            String desc = recvChatRspns.getConversations().get(0).getContent();
            //int id = recvChatRspns.getConversations().get(0).getMessageMasterId();
            //if (id == ConfigData.UNO_NEWS_MSG_MASTER_ID) {
            //recvChatHolder.userName.setGravity(Gravity.CENTER);
            if (desc != null) {
                String msg = StringEscapeUtils.unescapeJava(desc);

                recvChatHolder.userName.setText(Html.fromHtml(msg));
            }
            //}
            //else if (desc != null)
            //  recvChatHolder.userName.setText(desc);

            final String filename = recvChatRspns.getConversations().get(0).getFileName();
            if (filename != null && (filename.contains(ConfigData.COMPARE_JPG.toUpperCase()) || filename.contains(ConfigData.COMPARE_JPG.toLowerCase()))) {
                recvChatHolder.chatpic.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent(mContext, PhotoViewActivity.class);
                        i.putExtra(ConfigData.INTENT_EXTRA_DISP_PHOTO, filename);
                        mContext.startActivity(i);
                    }
                });
                Glide
                        .with(mContext)
                        .load(Uri.parse(filename))
                        .apply(RequestOptions.centerCropTransform().placeholder(R.drawable.ic_img_plchold)).into(recvChatHolder.chatpic);
            } else {
                recvChatHolder.chatpic.setVisibility(ConstraintLayout.GONE);
                recvChatHolder.guideline.setGuidelinePercent(0);
                /*ConstraintLayout.LayoutParams clpi = (ConstraintLayout.LayoutParams) recvChatHolder.chatpic.getLayoutParams();
                clpi.endToStart = recvChatHolder.container.getLeft();
                clpi.width=0;
                clpi.height=0;
                recvChatHolder.userName.setLayoutParams(clpi);*/
                ConstraintLayout.LayoutParams clp = (ConstraintLayout.LayoutParams) recvChatHolder.userName.getLayoutParams();
                clp.width = ConstraintLayout.LayoutParams.WRAP_CONTENT;
                clp.rightMargin = 1;
                //int leftpad = (int) Math.round(paddin * 2);
                recvChatHolder.userName.setPadding(paddin * 2, paddingVertical, paddin * 3, paddingVertical);

                recvChatHolder.userName.setLayoutParams(clp);
                RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) recvChatHolder.container.getLayoutParams();
                lp.width = RecyclerView.LayoutParams.WRAP_CONTENT;
                lp.rightMargin = FormatUtils.dpToPx(mContext, 80);
                recvChatHolder.container.setLayoutParams(lp);
                //recvChatHolder.chatpic.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_img_plchold));
            }

        } else {
            String desc = recvChatRspns.getConversations().get(position).getContent();
            if (desc != null) {
                String msg = StringEscapeUtils.unescapeJava(desc);
                recvChatHolder.userName.setText(msg);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (recvChatRspns.getStatus())
            return recvChatRspns.getConversations().size();
        else
            return 0;

    }

    @Override
    public int getItemViewType(int position) {
        int type;
        if (position == 0)
            type = TYPE_HEADER;
        else {
            int id = recvChatRspns.getConversations().get(position).getUserId();
            if (id == ConfigData.USER_ID)
                type = TYPE_ME;
            else
                type = TYPE_YOU;
        }
        return type;
    }
}
