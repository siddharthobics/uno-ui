package com.obics.uno.receive;

import java.util.ArrayList;

public class RecvHomeRspns {

    private ArrayList<KeywordList> keywordList;

    public ArrayList<KeywordList> getKeywordList() {
        return this.keywordList;
    }

    public void setKeywordList(ArrayList<KeywordList> keywordList) {
        this.keywordList = keywordList;
    }

    private int totalNotification;

    public int getTotalNotification() {
        return this.totalNotification;
    }

    public void setTotalNotification(int totalNotification) {
        this.totalNotification = totalNotification;
    }

    private String userAppId;

    public String getUserAppId() {
        return this.userAppId;
    }

    public void setUserAppId(String userAppId) {
        this.userAppId = userAppId;
    }

    private int todayKeywordCount;

    public int getTodayKeywordCount() {
        return this.todayKeywordCount;
    }

    public void setTodayKeywordCount(int todayKeywordCount) {
        this.todayKeywordCount = todayKeywordCount;
    }
}
