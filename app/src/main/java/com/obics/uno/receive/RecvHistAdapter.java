package com.obics.uno.receive;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.content.ContextCompat;

import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.obics.uno.R;
import com.obics.uno.config.ConfigData;
import com.obics.uno.utils.FormatUtils;

import org.apache.commons.lang.StringEscapeUtils;

public class RecvHistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private RecvHistRspns newsItemList;
    ReceiveHistListener mListener;
    Context mContext;
    boolean showDelete = false;

    interface ReceiveHistListener {
        void itemClicked(int id, String itemname, int chatUserId, String chatpic, String aboutme, String wherefrom, String uniqusr);

        void showDeleteIconState(boolean delete);

        void deleteItem(int id, String keyword, int matchCount);
    }

    public void setNewData(RecvHistRspns rspnsArrayList) {
        this.newsItemList = rspnsArrayList;
    }

    public void toggleDelete() {
        if (getItemCount() > 0) {
            showDelete = !showDelete;
            if (mListener != null)
                mListener.showDeleteIconState(showDelete);
        }
    }

    public void setShowDelete(boolean showDelete) {
        if (getItemCount() > 0) {
            this.showDelete = showDelete;
            if (mListener != null)
                mListener.showDeleteIconState(showDelete);
        }
    }

    public boolean getShowDelete() {
        return showDelete;
    }

    public class RecvHistHolder extends RecyclerView.ViewHolder {
        AppCompatImageView img1;
        String itemname = "", chatpic = "", aboutme = "", wherefrom = "", uniqusr = "";
        TextView userName, userDesc, timeTxt, inlay_txt;
        CheckBox radioButton;
        int mesgMasterId;
        int chatUserId;

        RecvHistHolder(View view) {
            super(view);
            img1 = view.findViewById(R.id.media_image);
            userName = view.findViewById(R.id.primary_text);
            userDesc = view.findViewById(R.id.sub_text);
            timeTxt = view.findViewById(R.id.time_text);
            inlay_txt = view.findViewById(R.id.inlay_txt);
            radioButton = view.findViewById(R.id.delbtn);
            view.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    //if (showDelete)
                    // radioButton.toggle();
                    //else
                    if (mListener != null)
                        mListener.itemClicked(mesgMasterId, itemname, chatUserId, chatpic, aboutme, wherefrom, uniqusr);
                }
            });
        }
    }

    public RecvHistAdapter(Context con, RecvHistRspns newsItemList, ReceiveHistListener mListener) {
        this.mContext = con;
        this.newsItemList = newsItemList;
        this.mListener = mListener;
        //setHasStableIds(true);

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_itm_card, parent, false);
        return new RecvHistHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final RecvHistHolder recvHistHolder = (RecvHistHolder) holder;
        String profileImg = newsItemList.getMatchingUsers().get(position).getProfileImg();
        String desc = newsItemList.getMatchingUsers().get(position).getMessageDescription();
        String keyname = newsItemList.getMatchingUsers().get(position).getKeywordName();

        recvHistHolder.mesgMasterId = newsItemList.getMatchingUsers().get(position).getMessageMasterId();
        recvHistHolder.uniqusr = newsItemList.getMatchingUsers().get(position).getUserName();
        String name = newsItemList.getMatchingUsers().get(position).getDisplayName();
        recvHistHolder.aboutme = newsItemList.getMatchingUsers().get(position).getAboutMe();
        recvHistHolder.wherefrom = newsItemList.getMatchingUsers().get(position).getWhereFrom();


        recvHistHolder.chatpic = profileImg;
        if (name != null)
            recvHistHolder.itemname = name;
        recvHistHolder.userName.setText(StringEscapeUtils.unescapeJava(recvHistHolder.itemname));
        recvHistHolder.timeTxt.setText(FormatUtils.formatChatTime(newsItemList.getMatchingUsers().get(position).getInsertDate() + ""));
        final int msgcnt = newsItemList.getMatchingUsers().get(position).getMatchingMessageCount();
        recvHistHolder.chatUserId = newsItemList.getMatchingUsers().get(position).getUserId();

        String status = newsItemList.getMatchingUsers().get(position).getStatus();
        if (msgcnt > 0) {
            recvHistHolder.inlay_txt.setVisibility(View.VISIBLE);
            if (status.contains("new".toLowerCase())) {
                recvHistHolder.inlay_txt.setText("New");
            } else {
                recvHistHolder.inlay_txt.setText("" + msgcnt);
            }
            int pixel = FormatUtils.dpToPx(mContext, 2);
            recvHistHolder.img1.setPadding(pixel, pixel, pixel, pixel);
            recvHistHolder.img1.setBackgroundResource(R.drawable.picbg);
            recvHistHolder.img1.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_avatar));
            if (mContext != null)
                recvHistHolder.userName.setTypeface(ResourcesCompat.getFont(mContext, R.font.roboto_bold));
            //recvHistHolder.userName.setTypeface(recvHistHolder.userName.getTypeface(), Typeface.BOLD);
            recvHistHolder.userDesc.setTextColor(Color.BLACK);
        } else {
            recvHistHolder.inlay_txt.setVisibility(View.GONE);
            recvHistHolder.img1.setPadding(0, 0, 0, 0);
            recvHistHolder.img1.setBackgroundResource(R.drawable.ic_avatar);
            if (mContext != null)
                recvHistHolder.userName.setTypeface(ResourcesCompat.getFont(mContext, R.font.roboto_regular));
            //recvHistHolder.userName.setTypeface(recvHistHolder.userName.getTypeface(), Typeface.NORMAL);
            recvHistHolder.userDesc.setTextColor(Color.parseColor("#b1b4bb"));
            //recvHistHolder.userDesc.setTypeface(recvHistHolder.userDesc.getTypeface(), Typeface.NORMAL);

        }
        // get
        if (profileImg != null)
            Glide
                    .with(mContext)
                    .asBitmap()
                    .load(Uri.parse(profileImg))
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(recvHistHolder.img1);
        if (keyname != null && (keyname.equalsIgnoreCase(ConfigData.CHANNEL_NEWS_NAME))) {
            if (desc != null)
                recvHistHolder.userDesc.setText(Html.fromHtml(desc));
            recvHistHolder.radioButton.setEnabled(false);
            recvHistHolder.radioButton.setVisibility(View.INVISIBLE);

        } else {
            if (desc != null)
                recvHistHolder.userDesc.setText(StringEscapeUtils.unescapeJava(desc));
            recvHistHolder.radioButton.setEnabled(true);
            if (showDelete) {
                recvHistHolder.radioButton.setChecked(false);
                recvHistHolder.radioButton.setVisibility(View.VISIBLE);
           /* if (mListener != null)
                mListener.showDeleteIconState(true);*/

            } else {
                recvHistHolder.radioButton.setVisibility(View.INVISIBLE);
           /* if (mListener != null)
                mListener.showDeleteIconState(false);*/
            }
        }

        recvHistHolder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {


                    if (mListener != null)
                        mListener.deleteItem(newsItemList.getMatchingUsers().get(position).getKeywordId(), recvHistHolder.mesgMasterId + "", msgcnt);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (newsItemList.getStatus())
            return newsItemList.getMatchingUsers().size();
        else
            return 0;

    }
///////////END OF CATEGORY ADAPTER/////////////////
}
