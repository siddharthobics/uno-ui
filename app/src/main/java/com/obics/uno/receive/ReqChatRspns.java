package com.obics.uno.receive;

import java.util.ArrayList;

public class ReqChatRspns {

    public class Conversation {
        private String readStatus;

        public String getReadStatus() {
            return this.readStatus;
        }

        public void setReadStatus(String readStatus) {
            this.readStatus = readStatus;
        }

        private String fileName;

        public String getFileName() {
            return this.fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        private int messageMasterId;

        public int getMessageMasterId() {
            return this.messageMasterId;
        }

        public void setMessageMasterId(int messageMasterId) {
            this.messageMasterId = messageMasterId;
        }

        private String insertDate;

        public String getInsertDate() {
            return this.insertDate;
        }

        public void setInsertDate(String insertDate) {
            this.insertDate = insertDate;
        }

        private int messageId;

        public int getMessageId() {
            return this.messageId;
        }

        public void setMessageId(int messageId) {
            this.messageId = messageId;
        }

        private String messageDescription;

        public String getMessageDescription() {
            return this.messageDescription;
        }

        public void setMessageDescription(String messageDescription) {
            this.messageDescription = messageDescription;
        }

        private String userName;

        public String getUserName() {
            return this.userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        private String profileImg;

        public String getProfileImg() {
            return this.profileImg;
        }

        public void setProfileImg(String profileImg) {
            this.profileImg = profileImg;
        }

        private int userId;

        public int getUserId() {
            return this.userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        private boolean conversationStatus;

        public boolean getConversationStatus() {
            return this.conversationStatus;
        }

        public void setConversationStatus(boolean conversationStatus) {
            this.conversationStatus = conversationStatus;
        }

        private String content;

        public String getContent() {
            return this.content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }


    private ArrayList<Conversation> conversations;

    public ArrayList<Conversation> getConversations() {
        return this.conversations;
    }

    public void setConversations(ArrayList<Conversation> conversations) {
        this.conversations = conversations;
    }

    private String userAppId;

    public String getUserAppId() {
        return this.userAppId;
    }

    public void setUserAppId(String userAppId) {
        this.userAppId = userAppId;
    }

    private boolean status;

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    private boolean isBlocked;

    public boolean getIsBlocked() {
        return this.isBlocked;
    }

    public void setIsBlocked(boolean isBlocked) {
        this.isBlocked = isBlocked;
    }
}
