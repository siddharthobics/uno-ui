package com.obics.uno.receive;

public class KeywordList {

    public class Zipcode {

        private String zipcode;

        public String getZipcode() {
            return this.zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        private String latitude;

        public String getLatitude() {
            return this.latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        private String longitude;

        public String getLongitude() {
            return this.longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        private String placeName;

        public String getPlaceName() {
            return this.placeName;
        }

        public void setPlaceName(String placeName) {
            this.placeName = placeName;
        }

        private String state;

        public String getState() {
            return this.state;
        }

        public void setState(String state) {
            this.state = state;
        }

        private String stateAbbreviation;

        public String getStateAbbreviation() {
            return this.stateAbbreviation;
        }

        public void setStateAbbreviation(String stateAbbreviation) {
            this.stateAbbreviation = stateAbbreviation;
        }

        private String county;

        public String getCounty() {
            return this.county;
        }

        public void setCounty(String county) {
            this.county = county;
        }
    }

    public class User {
        private int userId;

        public int getUserId() {
            return this.userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        private String userName;

        public String getUserName() {
            return this.userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        private String latitude;

        public String getLatitude() {
            return this.latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        private String longtitude;

        public String getLongtitude() {
            return this.longtitude;
        }

        public void setLongtitude(String longtitude) {
            this.longtitude = longtitude;
        }

        private String emailId;

        public String getEmailId() {
            return this.emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        private String password;

        public String getPassword() {
            return this.password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        private String firstName;

        public String getFirstName() {
            return this.firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        private String lastName;

        public String getLastName() {
            return this.lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        private Zipcode zipcode;

        public Zipcode getZipcode() {
            return this.zipcode;
        }

        public void setZipcode(Zipcode zipcode) {
            this.zipcode = zipcode;
        }

        private String phoneNumber;

        public String getPhoneNumber() {
            return this.phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        private String profileImg;

        public String getProfileImg() {
            return this.profileImg;
        }

        public void setProfileImg(String profileImg) {
            this.profileImg = profileImg;
        }

        private String userAppId;

        public String getUserAppId() {
            return this.userAppId;
        }

        public void setUserAppId(String userAppId) {
            this.userAppId = userAppId;
        }
    }

    public class Keyword {
        private int keywordId;

        public int getKeywordId() {
            return this.keywordId;
        }

        public void setKeywordId(int keywordId) {
            this.keywordId = keywordId;
        }

        private String keywordName;

        public String getKeywordName() {
            return this.keywordName;
        }

        public void setKeywordName(String keywordName) {
            this.keywordName = keywordName;
        }

        private String createdBy;

        public String getCreatedBy() {
            return this.createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }
    }

    private int userKeywordId;

    public int getUserKeywordId() {
        return this.userKeywordId;
    }

    public void setUserKeywordId(int userKeywordId) {
        this.userKeywordId = userKeywordId;
    }

    private User user;

    public User getUser() {
        return this.user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    private Keyword keyword;

    public Keyword getKeyword() {
        return this.keyword;
    }

    public void setKeyword(Keyword keyword) {
        this.keyword = keyword;
    }

    private String insertedTime;

    public String getInsertedTime() {
        return this.insertedTime;
    }

    public void setInsertedTime(String insertedTime) {
        this.insertedTime = insertedTime;
    }

    private float kiloMeter;

    public float getKiloMeter() {
        return this.kiloMeter;
    }

    public void setKiloMeter(float kiloMeter) {
        this.kiloMeter = kiloMeter;
    }

    private int matchingCount;

    public int getMatchingCount() {
        return this.matchingCount;
    }

    public void setMatchingCount(int matchingCount) {
        this.matchingCount = matchingCount;
    }

    private int matchingMessageCount;

    public int getMatchingMessageCount() {
        return this.matchingMessageCount;
    }

    public void setMatchingMessageCount(int matchingMessageCount) {
        this.matchingMessageCount = matchingMessageCount;
    }

    /*@Override
    public boolean equals(Object v) {
        boolean retVal = false;
        if (v instanceof KeywordList) {
            KeywordList ptr = (KeywordList) v;
            retVal = ptr.userKeywordId == this.userKeywordId;
        }
        return retVal;
    }*/
}