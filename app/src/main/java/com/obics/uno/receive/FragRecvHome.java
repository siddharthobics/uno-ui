package com.obics.uno.receive;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;


import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
//import android.util.Log;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
//import com.obics.uno.MainActivity;
import com.obics.uno.dialog.ConfirmDeleteDialog;
import com.obics.uno.MapActivity;
import com.obics.uno.R;
import com.obics.uno.config.ConfigData;
import com.obics.uno.interfaces.PagerFrgmntLstnr;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
import com.obics.uno.utils.DatabaseMgr;
import com.obics.uno.utils.ToolDotProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragRecvHome extends Fragment {
    /**
     * The receive fragment
     */
    private PagerFrgmntLstnr mMainListener;
    private RecyclerView mRecyclerView;
    private FragRecvHomeListener mListener;
    private ToolDotProgress mProgress;
    private RecvHomeAdapter mRecvHomeAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecvHomeRspns mRecvHomeRspns = new RecvHomeRspns();
    private boolean fcmUploaded = false;
    private String fcmKey = "";
    private static final int RECV_MAP_REQUEST = 230;
    //private static final int RECV_MAP_RESULT = 455;
    //private static final int ConfigData.MAX_CHANNELS = 20;
    private static int mTagCount = 0;
    private DatabaseMgr datastore;
    private TextView  mFaabText, mLimCntTxt, mToolTxt;
    private AppCompatImageView mTooltip;
    private boolean isVisible = true;
    private static final String FRAGMENT_DIALOG_TAG = "fragment_recv_home";
    private Handler handler;
    //private ToolDotProgress dprog;
    private View fab;
    private AppCompatImageButton mTopLeftBtn, mTopRightBtn;
    private Runnable runnable = new Runnable() {
        public void run() {
            requestRecvHomeList(false, false);
            handler.postDelayed(this, ConfigData.REFRESH_TIME);
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //Log.d("UNOLOGK", context + "  **  ");
        try {
            mMainListener = (PagerFrgmntLstnr) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement PageFrgmntListener");
        }
        //this.mListener = context;
    }

    @Override
    public void onStop() {
        handler.removeCallbacks(runnable);
        super.onStop();
    }

    interface FragRecvHomeListener {
        void showRecvItemsPage(String str, int userKeywordId, String radius);

        void showRecvChatPage(int id, String str, int histid, String histkey, int chatUserId, String chatpic, String aboutme, String wherefrom, String uniqusr);

        //void showLogin();

        //void logOut();

        //void showSettings();

        //void setPlaceholders();

        //void showRecvCount(int count);
    }

    public FragRecvHome() {

    }

    private void setupExternalButtons() {
        mTopLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConfigData.USER_ID == -1) {
                    if (mMainListener != null)
                        mMainListener.showLogin();
                } else {
                    if (mMainListener != null)
                        mMainListener.showSettings();
                }
            }
        });
        mTopRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleHomeDelete();
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        refreshPage(true);
        if (mTagCount < ConfigData.MAX_CHANNELS - 1)
            fab.setEnabled(true);

    }

    public void setFragRecvHomeListener(FragRecvHomeListener mListener) {
        this.mListener = mListener;
        if (mMainListener != null)
            mMainListener.setPlaceholderButtons(R.drawable.ic_set1, R.drawable.ic_del1);
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        if (menuVisible) {
            if (mRecyclerView != null)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
            if (mMainListener != null)
                mMainListener.setPlaceholderButtons(R.drawable.ic_set1, R.drawable.ic_del1);

        } else {
            if (mRecyclerView != null)
                // mRecyclerView.setForeground(new ColorDrawable(Color.parseColor(ConfigData.COLOR_DARK_TRANSPARENCY)));
                mRecyclerView.setBackgroundColor(Color.parseColor(ConfigData.COLOR_DARK_TRANSPARENCY));
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (mRecvHomeAdapter != null) {
            resetDelete();
        }
        if (isVisibleToUser) {
            showTopButtons(true);
            if (!isVisible)
                refreshPage(false);
        } else {
            showTopButtons(false);
        }
        isVisible = isVisibleToUser;

    }

    private void showTopButtons(boolean show) {
  /*      if(!show)
            Log.d("mailog", "getta333444");
else
            Log.d("mailog", "getta333");*/


        int visible = (show ? ConstraintLayout.VISIBLE : ConstraintLayout.GONE);
        if (mTopLeftBtn != null && mTopRightBtn != null) {
            mTopLeftBtn.setVisibility(visible);
            mTopRightBtn.setVisibility(visible);
            if (show)
                mTopRightBtn.setImageResource(R.drawable.ic_del1);
        }
        if (mRecyclerView != null) {
            if (show)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        datastore = new DatabaseMgr(getContext());
        View rootView = inflater.inflate(R.layout.fragment_recv, container, false);
        //mHeadingText = rootView.findViewById(R.id.topbartext);
        //mHeadingText.setText(R.string.recv_home_heading);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        mProgress = rootView.findViewById(R.id.progressBar);
        mProgress.setVisibility(View.GONE);
        swipeRefreshLayout = rootView.findViewById(R.id.simpleSwipeRefreshLayout);
        mTooltip = rootView.findViewById(R.id.retooltip);
        mToolTxt = rootView.findViewById(R.id.retooltiptxt);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mTopLeftBtn = rootView.findViewById(R.id.topbarleftbtn);
        mTopRightBtn = rootView.findViewById(R.id.topbarrightbtn);

        fab = rootView.findViewById(R.id.fabre);
        mFaabText = rootView.findViewById(R.id.fabretxt);
        mLimCntTxt = rootView.findViewById(R.id.fab_count_txt);
        fab.setOnTouchListener(btnlistner);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConfigData.USER_ID == -1) {
                    if (mMainListener != null)
                        mMainListener.showLogin();
                } else {

                    resetDelete();
                    fab.setEnabled(false);
                    Intent i = new Intent(getActivity(), MapActivity.class);
                    i.putExtra(ConfigData.INTENT_MAP_SCREEN_TYPE, MapActivity.RECEIVE_MAP_SCREEN);
                    startActivityForResult(i, RECV_MAP_REQUEST);
                    try {
                        getActivity().overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_top);//R.anim.slide_from_bottom
                    } catch (NullPointerException npe) {
                        npe.printStackTrace();
                    }
                }
            }
        });
        setupExternalButtons();
        if (isAdded()) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());
            fcmUploaded = preferences.getBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, false);
            if (fcmUploaded)
                fcmKey = preferences.getString(ConfigData.PREF_FCM_TOKEN, "");
        }
        mRecvHomeAdapter = new RecvHomeAdapter(mRecvHomeRspns, new RecvHomeAdapter.ReceiveHomeListener() {
            @Override
            public void newsItemClicked(int userKeyId) {
                if (mListener != null)
                    mListener.showRecvChatPage(ConfigData.UNO_NEWS_MSG_MASTER_ID, ConfigData.ADMIN_KEYWORD, ConfigData.REDIRECT_HOME, ConfigData.CHANNEL_NEWS_NAME, userKeyId, ConfigData.ADMIN_KEYWORD, "Knob Admin", "Seattle", "Admin");
            }

            @Override
            public void itemClicked(String itemname, int userKeywordId, String radius) {
                if (mListener != null)
                    mListener.showRecvItemsPage(itemname, userKeywordId, radius);
            }

            @Override
            public void showDeleteIconState(boolean delete) {
                if (getContext() != null && isVisible && mListener != null)
                    if (delete)
                        mTopRightBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_del2));
                    else
                        mTopRightBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_del1));
            }

            @Override
            public void deleteItem(int id, String keyword) {
                showConfirmDelete(id, keyword);
            }

        });
        mRecyclerView.setAdapter(mRecvHomeAdapter);
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID) {
            String json = datastore.fetchCategory(ConfigData.DB_CATEG_RECV_HOME);
            populateReceiveHome(json, false, false);
            // requestRecvHomeList(true);
        }
        swipeRefreshLayout.setNestedScrollingEnabled(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestRecvHomeList(false, true);
            }
        });


        handler = new Handler();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        handler.postDelayed(runnable, ConfigData.REFRESH_TIME);
    }

    ////////
    public void toggleHomeDelete() {
        mRecvHomeAdapter.toggleDelete();
        mRecvHomeAdapter.notifyDataSetChanged();
    }

    private void checkDuplicateLogin(String appid) {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID)
            if (fcmUploaded && appid != null)
                if (!fcmKey.isEmpty())
                    if (!appid.trim().equals(fcmKey.trim())) {
                        mRecyclerView.removeAllViews();
                        if (mMainListener != null)
                            mMainListener.logOut();
                    }
    }

    private int getSize(ArrayList<KeywordList> arrayList) {
        int count = 0;
        if (arrayList != null)
            for (KeywordList kk : arrayList) {
                if (kk.getKeyword().getCreatedBy() != null) {
                    if (!kk.getKeyword().getCreatedBy().equalsIgnoreCase(ConfigData.ADMIN_KEYWORD))
                        count++;
                } else
                    count++;
            }
        return count;
    }

    private void populateReceiveHome(String json, boolean live, boolean showProgress) {
        if (json != null) {
            if (live) {
                datastore.insertOrUpdate(ConfigData.DB_CATEG_RECV_HOME, json);
            }
            try {
                mRecvHomeRspns = new Gson().fromJson(json, RecvHomeRspns.class);
            } catch (Exception e) {
                datastore.insertOrUpdate(ConfigData.DB_CATEG_RECV_HOME, "");
                return;
            }
            int newsIndx = -1;
            // remove the channel news item
            for (KeywordList keywordList : mRecvHomeRspns.getKeywordList()) {
                if (keywordList.getKeyword().getCreatedBy() != null) {
                    if (keywordList.getKeyword().getCreatedBy().equalsIgnoreCase(ConfigData.ADMIN_KEYWORD)) {
                        newsIndx = mRecvHomeRspns.getKeywordList().indexOf(keywordList);
                        break;
                    }
                }
            }
            if (newsIndx > 0) {
                KeywordList keywordList = mRecvHomeRspns.getKeywordList().get(newsIndx);
                mRecvHomeRspns.getKeywordList().remove(newsIndx);
                mRecvHomeRspns.getKeywordList().add(0, keywordList);
            }
            //checkDuplicateLogin("22333fake");
            checkDuplicateLogin(mRecvHomeRspns.getUserAppId());
            int tagcount = getSize(mRecvHomeRspns.getKeywordList());
            showRemainingTags(tagcount);
            //showRemainingTags(mRecvHomeRspns.getKeywordList().size());
            if (isAdded() && live) {
                int totalRecvNotif = mRecvHomeRspns.getTotalNotification();
                if (mMainListener != null)
                    mMainListener.showReceiveCount(totalRecvNotif);
                storeNotificationCount(totalRecvNotif);
            }
            if (mRecvHomeRspns.getKeywordList() != null) {
                if (tagcount == 0) {
                    mTooltip.setVisibility(ConstraintLayout.VISIBLE);
                    mToolTxt.setVisibility(ConstraintLayout.VISIBLE);
                    if (isVisible && mListener != null)
                        if (getContext() != null)
                            mTopRightBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_del1));
                } else {
                    mTooltip.setVisibility(ConstraintLayout.GONE);
                    mToolTxt.setVisibility(ConstraintLayout.GONE);
                    //fallback
                    /*if (mRecvHomeRspns.getUserAppId() == null) {
                        checkDuplicateLogin(mRecvHomeRspns.getKeywordList().get(0).getUser().getUserAppId());
                    } else {
                        if (mRecvHomeRspns.getUserAppId().isEmpty())
                            checkDuplicateLogin(mRecvHomeRspns.getKeywordList().get(0).getUser().getUserAppId());
                    }*/
                }
            } else {
                mTooltip.setVisibility(ConstraintLayout.GONE);
                mToolTxt.setVisibility(ConstraintLayout.GONE);
            }
            if (showProgress)
                mRecvHomeAdapter.setShowDelete(false);
            mRecvHomeAdapter.setNewData(mRecvHomeRspns);
            mRecvHomeAdapter.notifyDataSetChanged();

            mProgress.setVisibility(View.GONE);
        }
    }

    public void requestRecvHomeList(final boolean showProgress, final boolean resetDelete) {
        if (showProgress) {
            mProgress.setVisibility(View.VISIBLE);
        }
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"userId\":" + ConfigData.USER_ID + "}");

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                    ConfigData.BASE_URL + "/keyword/list", jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            populateReceiveHome(response.toString(), true, resetDelete);
                            mProgress.setVisibility(View.GONE);
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgress.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                    String errString = NetUtil.getMessage(error);
                    if (!errString.isEmpty())
                        showToast(R.string.no_internet_conn);
                }
            });
            VolleySingleton.getInstance().addToRequestQueue(req, false);
        } catch (JSONException | IllegalStateException | NullPointerException e) {

        }
    }


    private void showRemainingTags(int tagCount) {
        if (tagCount > ConfigData.MAX_CHANNELS)
            tagCount = ConfigData.MAX_CHANNELS;
        mTagCount = tagCount;
        if (ConfigData.MAX_CHANNELS - tagCount > 0) {
            //show in text box
            mFaabText.setText(R.string.add_new_tag_btn);
            fab.setBackgroundResource(R.drawable.in_btm_btn_bg);
            fab.setOnTouchListener(btnlistner);
            mFaabText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            mFaabText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_small_arr_white_up, 0);
            fab.setEnabled(true);
            if (tagCount > 0) {
                mLimCntTxt.setVisibility(ConstraintLayout.VISIBLE);
                int difcount = ConfigData.MAX_CHANNELS - tagCount;
                if (isAdded())
                    mLimCntTxt.setText(Html.fromHtml("You can add <b>" + difcount + "</b>" + getResources().getQuantityString(R.plurals.add_new_tag_limit, difcount)));
            } else {
                mLimCntTxt.setVisibility(ConstraintLayout.GONE);

            }
        } else {
            fab.setOnTouchListener(null);
            mLimCntTxt.setVisibility(ConstraintLayout.GONE);
            mFaabText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            mFaabText.setText(R.string.add_new_tag_limit_reach);
            fab.setBackgroundResource(R.color.settingDark);
            mFaabText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            fab.setEnabled(false);
        }
    }

    public void requestDeleteItem(int id, String keyword) {
        mProgress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id\": \"" + id + "\", \"keyword\": \"" + keyword + "\",\"userId\":" + ConfigData.USER_ID + "}");
        } catch (JSONException e) {

        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/keyword/delete", jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        populateReceiveHome(response.toString(), true, true);
                        //TODO SIDDHARTHO
                        //requestRecvHomeList(false);
                        mProgress.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    showToast(R.string.no_internet_conn);
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private void showConfirmDelete(final int id, final String keyword) {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_DIALOG_TAG);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        ConfirmDeleteDialog deleteDialog = new ConfirmDeleteDialog();
        deleteDialog.setmConfrmDelLstnr(new ConfirmDeleteDialog.ConfirmDelDlgListener() {
            @Override
            public void clickPositive() {
                // removeFrag();
                requestDeleteItem(id, keyword);
            }

            @Override
            public void clickNegative() {
                // removeFrag();
                resetDelete();
            }
        });
        Bundle args = new Bundle();
        args.putInt(ConfirmDeleteDialog.CONF_DEL_DLG_TYPE, ConfirmDeleteDialog.TAG_TYPE);
        deleteDialog.setArguments(args);
        deleteDialog.show(manager, FRAGMENT_DIALOG_TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mProgress.setVisibility(View.GONE);
        if (requestCode == RECV_MAP_REQUEST && resultCode == MapActivity.RECV_MAP_RESULT) {
            //
            try {
                String json = datastore.fetchCategory(ConfigData.DB_CATEG_RECV_HOME);
                populateReceiveHome(json, false, true);
            } catch (Exception err) {
                requestRecvHomeList(true, true);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void resetDelete() {
        if (mRecvHomeAdapter.getShowDelete()) {
            mRecvHomeAdapter.setShowDelete(false);
            mRecvHomeAdapter.notifyDataSetChanged();
        }
    }

    private void showToast(int resid) {
        if (isAdded())
            Toast.makeText(getContext().getApplicationContext(), resid, Toast.LENGTH_SHORT).show();
    }

    private void storeNotificationCount(int recvcount) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(ConfigData.PREF_RECV_NOTIF_COUNT, recvcount);
        editor.apply();
    }

    private void refreshPage(boolean resetDelete) {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID)
            requestRecvHomeList(true, resetDelete);
    }

    private View.OnTouchListener btnlistner = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mFaabText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_small_arr_black_up, 0);
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    mFaabText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_small_arr_white_up, 0);
                    break;
            }
            return false;
        }
    };
    /*private void refreshPage() {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID)
            requestRecvHomeList(true);
    }*/
    ////END////
}