package com.obics.uno;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.text.emoji.EmojiCompat;
import android.support.text.emoji.bundled.BundledEmojiCompatConfig;

import com.obics.uno.service.UnoFirebaseMessagingService;

public class UnoApplication extends Application
        implements Application.ActivityLifecycleCallbacks {

    private static Context mAppContext;

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(this);
        this.setAppContext(getApplicationContext());
        EmojiCompat.Config config = new BundledEmojiCompatConfig(this);
        EmojiCompat.init(config);
    }

    public static Context getAppContext() {
        return mAppContext;
    }

    public void setAppContext(Context mappContext) {
        mAppContext = mappContext;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
        setState(activity.getLocalClassName(), false);
    }

    /////////////////////////////////
    @Override
    public void onActivityPaused(Activity activity) {
        setState(activity.getLocalClassName(), true);
    }

    @Override
    public void onActivityStopped(Activity activity) {
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }

    private void setState(String actname, boolean isbg) {
        if (actname.equals(MainActivity.class.getSimpleName()))
            UnoFirebaseMessagingService.isMainActBackground = isbg;
        if (actname.equals(MapActivity.class.getSimpleName()))
            UnoFirebaseMessagingService.isMapActBackground = isbg;
        if (actname.equals(PhotoViewActivity.class.getSimpleName()))
            UnoFirebaseMessagingService.isPicActBackground = isbg;
    }

}