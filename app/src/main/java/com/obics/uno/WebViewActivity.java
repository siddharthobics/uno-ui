package com.obics.uno;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class WebViewActivity extends AppCompatActivity {

    public static final int TERMS_PAGE = 876;
    public static final int PRIVACY_PAGE = 877;
    public static final String INTENT_EXTRA_DISP_HTML = "webdispurl";
    private static final String ENCODING = "ISO-8859-1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        int urlType = getIntent().getIntExtra(INTENT_EXTRA_DISP_HTML, 0);
        WebView mWebView = findViewById(R.id.web_view);
        FloatingActionButton mCloseBtn = findViewById(R.id.close_btn);
        mCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        switch (urlType) {
            case TERMS_PAGE:
                mWebView.loadDataWithBaseURL("file:///android_res/", readTextFromResource(R.raw.terms), "text/html", ENCODING, null);

                //mWebView.loadData(readTextFromResource(R.raw.terms), "text/html", ENCODING);
                break;
            case PRIVACY_PAGE:
                mWebView.loadDataWithBaseURL("file:///android_res/", readTextFromResource(R.raw.privacy), "text/html", ENCODING, null);

                //mWebView.loadData(readTextFromResource(R.raw.privacy), "text/html", ENCODING);
                break;
            default:
                finish();
                break;
        }
    }

    private String readTextFromResource(int resourceID) {
        BufferedInputStream raw = new BufferedInputStream(getResources().openRawResource(resourceID));
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        int i;
        try {
            i = raw.read();
            while (i != -1) {
                stream.write(i);
                i = raw.read();
            }
            raw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream.toString();
    }

}
