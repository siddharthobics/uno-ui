package com.obics.uno.dialog;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
//import android.widget.Button;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
//import android.widget.Toast;

import com.obics.uno.R;
import com.obics.uno.utils.FormatUtils;

public class EnterPasswordDlg extends DialogFragment {
    private EnterPasswdLstnr enterPasswdLstnr;
    private TextView mTopTextView, mErrView;
    public static final String ARG_DLG_TYPE = "passwd_dialogtype";
    private static final String HEADING_SET_PASSWORD = "Set password";
    private static final String HEADING_RESET_PASSWORD = "Reset password";
    public static final int TYPE_SET_PASSWD = 135;
    public static final int TYPE_RESET_PASSWD = 139;
    int dlgType;
    private EditText mPasswordField;
    private static final String MSG_VALID_PASSWD = "must be a minimum of 6 characters\n" +
            "only letters, numbers, '-', '.', '@', '$', '#', '%', and '_' may be used";

    public void setEnterPasswdLstnr(EnterPasswdLstnr enterPasswdLstnr) {
        this.enterPasswdLstnr = enterPasswdLstnr;
    }

    public EnterPasswordDlg() {
    }

    public interface EnterPasswdLstnr {
        void clickPositive(String password);

    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getArguments() != null)
            dlgType = getArguments().getInt(ARG_DLG_TYPE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.password_dlg, null);
        mPasswordField = view.findViewById(R.id.username_edit);
        mPasswordField.addTextChangedListener(passwdWatcher);
        mPasswordField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    onSubmit();
                }
                return false;
            }
        });
        mTopTextView = view.findViewById(R.id.heading_txt);
        mErrView = view.findViewById(R.id.invalid_passwd);

        AppCompatButton btn = view.findViewById(R.id.signup_btn);
        if (dlgType == TYPE_SET_PASSWD) {
            mTopTextView.setText(HEADING_SET_PASSWORD);
        } else if (dlgType == TYPE_RESET_PASSWD) {
            mTopTextView.setText(HEADING_RESET_PASSWORD);
        }

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmit();
            }
        });

        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    private void onSubmit() {
        final String passwo = mPasswordField.getText().toString();
        if (FormatUtils.isValidPassword(passwo)) {
            enterPasswdLstnr.clickPositive(passwo);
            getDialog().dismiss();
        } else {
            mErrView.setVisibility(ConstraintLayout.VISIBLE);
            mErrView.setText(MSG_VALID_PASSWD);
            //Toast.makeText(getContext().getApplicationContext(), "Password should be atleast 6 characters, " +
            //      "only alphanumeric and . - _ @ $ # % allowed!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    private TextWatcher passwdWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mErrView.setVisibility(ConstraintLayout.GONE);
            // btn.setEnabled(mUsername.getText().length() > 0 && mPassword.getText().length() > 0);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
}