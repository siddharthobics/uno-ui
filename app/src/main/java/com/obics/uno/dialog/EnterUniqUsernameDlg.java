package com.obics.uno.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.obics.uno.R;
import com.obics.uno.WebViewActivity;
import com.obics.uno.config.ConfigData;
import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
import com.obics.uno.utils.FormatUtils;
import com.obics.uno.utils.ToolDotProgress;

import org.json.JSONException;
import org.json.JSONObject;

public class EnterUniqUsernameDlg extends DialogFragment {

    private EnterUniqUsernameLstnr enterUniqUsernameLstnr;
    private TextView errView;
    private ToolDotProgress mProgress;
    private EditText mUniqueUserField;
    private AppCompatButton btn_sgnup, btn_terms, btn_privacy;
    private static final String MSG_VALID_USERNAME = "must begin with a letter\nmust be a minimum of 3 characters\n" +
            "only letters, numbers, '-', '.', and '_' may be used";
    // private static final String MSG_INVALID_USERNAME = "username not valid";
    private static final String MSG_USERNAME_EXIST = "username already exists!";

    public void setEnterUniqUsernameLstnr(EnterUniqUsernameLstnr enterUniqUsernameLstnr) {
        this.enterUniqUsernameLstnr = enterUniqUsernameLstnr;
    }

    public EnterUniqUsernameDlg() {
    }

    public interface EnterUniqUsernameLstnr {
        void userNameVerified(String username);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // getArguments().
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.username_dlg, null);
        mUniqueUserField = view.findViewById(R.id.username_input);
        mUniqueUserField.addTextChangedListener(usernameWatcher);
        mProgress = view.findViewById(R.id.mini_progress);
        errView = view.findViewById(R.id.invalid_usrname);
        btn_sgnup = view.findViewById(R.id.signup_btn);
        btn_terms = view.findViewById(R.id.terms_btn);
        btn_privacy = view.findViewById(R.id.privacy_btn);
        btn_sgnup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkExistingName();
            }
        });
        mUniqueUserField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    checkExistingName();
                }
                return false;
            }
        });
        btn_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAdded()) {
                    Intent intent = new Intent(getContext(), WebViewActivity.class);
                    intent.putExtra(WebViewActivity.INTENT_EXTRA_DISP_HTML, WebViewActivity.PRIVACY_PAGE);
                    startActivity(intent);
                }
            }
        });
        btn_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAdded()) {
                    Intent intent = new Intent(getContext(), WebViewActivity.class);
                    intent.putExtra(WebViewActivity.INTENT_EXTRA_DISP_HTML, WebViewActivity.TERMS_PAGE);
                    startActivity(intent);
                }
            }
        });

        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    public void checkExistingName() {
        errView.setVisibility(View.INVISIBLE);
        btn_sgnup.setEnabled(false);
        final String username = mUniqueUserField.getText().toString().trim();
        if (FormatUtils.isValidNameText(username)) {
            errView.setVisibility(View.INVISIBLE);
        } else {
            errView.setVisibility(View.VISIBLE);
            errView.setText(MSG_VALID_USERNAME);
            btn_sgnup.setEnabled(true);
            //Toast.makeText(getContext().getApplicationContext(), MSG_VALID_USERNAME, Toast.LENGTH_SHORT).show();
            return;
        }
        mProgress.setVisibility(View.VISIBLE);

        JSONObject jsonBody = null;
        mUniqueUserField.setEnabled(false);
        try {
            //jsonBody = new JSONObject("{ \"userName\":\"" + username + "\"}");
            jsonBody = new JSONObject();
            jsonBody.put("userName", username);
        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/user/userExists", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgress.setVisibility(View.GONE);
                        mUniqueUserField.setEnabled(true);
                        if (response.trim().equalsIgnoreCase("true")) {
                            errView.setVisibility(View.VISIBLE);
                            errView.setText(MSG_USERNAME_EXIST);
                            btn_sgnup.setEnabled(true);

                        } else if (response.trim().equalsIgnoreCase("false")) {
                            //todo save the data
                            errView.setVisibility(View.INVISIBLE);
                            enterUniqUsernameLstnr.userNameVerified(username);
                            //showEnterPasswordDlg();
                            getDialog().dismiss();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                btn_sgnup.setEnabled(true);
                mProgress.setVisibility(View.GONE);
                mUniqueUserField.setEnabled(true);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    if (isAdded())
                        Toast.makeText(getContext().getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private TextWatcher usernameWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            errView.setVisibility(ConstraintLayout.GONE);

            // btn.setEnabled(mUsername.getText().length() > 0 && mPassword.getText().length() > 0);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
}