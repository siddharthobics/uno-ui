package com.obics.uno.dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.graphics.ColorUtils;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.obics.uno.PhotoViewActivity;
import com.obics.uno.R;
import com.obics.uno.UnoApplication;
import com.obics.uno.config.ConfigData;

import org.apache.commons.lang.StringEscapeUtils;

//@SuppressLint("ValidFragment")
public final class ProfileDialogFragment extends BottomSheetDialogFragment {
    int rootheight = 0;
    String profilepic = "", display_name = "", aboutme = "", wherefrom = "", uniqusr = "";

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        if (getArguments() != null) {
            profilepic = getArguments().getString("profilepic");
            display_name = getArguments().getString("dispname");
            aboutme = getArguments().getString(ConfigData.INTENT_ABOUT_ME);
            wherefrom = getArguments().getString(ConfigData.INTENT_WHERE_FROM);
            uniqusr = getArguments().getString("chatwithuniqusr");
            rootheight = getArguments().getInt("screenheight");
        }
        super.onActivityCreated(savedInstanceState);
        assert getView() != null;
        View parent = (View) getView().getParent();
        parent.setBackgroundColor(Color.TRANSPARENT);
        View space_container = parent.findViewById(R.id.b_container);
        final View spec = parent.findViewById(R.id.bottom_sheet_space);
        final View cont = parent.findViewById(R.id.profile_card);
        TextView uniqname = parent.findViewById(R.id.uniqusernam);
        uniqname.setText(uniqusr);
        TextView name = parent.findViewById(R.id.uniqusrname);
        if (!TextUtils.isEmpty(display_name))
            name.setText(StringEscapeUtils.unescapeJava(display_name));
        TextView abt = parent.findViewById(R.id.editlyt_aboutme);
        if (!TextUtils.isEmpty(aboutme))
            abt.setText(StringEscapeUtils.unescapeJava(aboutme));
        TextView whr = parent.findViewById(R.id.editlyt_wherefrom);
        if (!TextUtils.isEmpty(wherefrom))
            whr.setText(StringEscapeUtils.unescapeJava(wherefrom));
        AppCompatImageView profilepicvw = parent.findViewById(R.id.profile_image);
        if (!TextUtils.isEmpty(profilepic)) {
            if ((profilepic.contains(ConfigData.COMPARE_JPG.toUpperCase()) || profilepic.contains(ConfigData.COMPARE_JPG.toLowerCase()))) {
                Glide
                        .with(getActivity())
                        .load(Uri.parse(profilepic))
                        .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(profilepicvw);
                profilepicvw.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(UnoApplication.getAppContext(), PhotoViewActivity.class);
                        i.putExtra(ConfigData.INTENT_EXTRA_DISP_PHOTO, Uri.parse(profilepic).toString());
                        startActivity(i);
                    }
                });
            }
        }
        cont.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ConstraintLayout.LayoutParams clp = (ConstraintLayout.LayoutParams) spec.getLayoutParams();
                clp.height = ((rootheight / 2) - (cont.getHeight() / 2));
                spec.setLayoutParams(clp);
                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                cont.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                //else
                //  cont.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

        space_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.custom_bottom_sheet, container, false);
        CardView cardView = view.findViewById(R.id.profile_card);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
            cardView.setRadius(0f);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //disable dismiss on click
            }
        });
        getDialog().setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialogInterface) {
                View parent = (View) getView().getParent();
                final BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(parent);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                bottomSheetBehavior.setSkipCollapsed(true);
                bottomSheetBehavior.setFitToContents(true);
                bottomSheetBehavior.setHideable(true);
                bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                    @Override
                    public void onStateChanged(@NonNull View bottomSheet, int newState) {
                        switch (newState) {
                            case BottomSheetBehavior.STATE_HIDDEN:
                            case BottomSheetBehavior.STATE_COLLAPSED:
                                getDialog().dismiss();
                                break;
                        }

                    }

                    @Override
                    public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                        // Called when the bottom sheet is being dragged
                    }

                });
            }
        });
        getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int visibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //Log.d("MISHU&&&",getActivity().getWindow().getNavigationBarColor()+"   -- ");
                try {
                    if (ColorUtils.calculateLuminance(getActivity().getWindow().getNavigationBarColor()) > 0.5)
                        visibility |= View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR;
                } catch (Exception e) {

                }
            }
            getDialog().getWindow().getDecorView().setSystemUiVisibility(visibility);
            getDialog().getWindow().setStatusBarColor(Color.TRANSPARENT);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getDialog().getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getDialog().getWindow().setStatusBarColor(Color.parseColor(ConfigData.COLOR_DARK_TRANSPARENCY));
        }

        return view;
    }

}