package com.obics.uno.dialog;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
//import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.obics.uno.MainActivity;
import com.obics.uno.config.ConfigData;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.obics.uno.R;
import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
import com.obics.uno.utils.NoMenuEditText;
import com.obics.uno.utils.PhoneNumberTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;


import java.lang.ref.WeakReference;

import static android.app.Activity.RESULT_OK;

public class EnterPhoneDlg extends DialogFragment {
    private int RESOLVE_HINT = 995;
    private int verifyType;
    private NoMenuEditText mPhoneField, mCountryCodeField;
    //private EditText mCountryCodeField;
    private EnterPhnDlgLstnr enterPhnDlgLstnr;
    public static final String ARG_DIALOG_TYPE = "arg_verify_dlg_type";
    public static final String COUNTRY_CODE = "country_code";
    String country_code = "";
    public static final int TYPE_SIGNUP = 11;
    public static final int TYPE_RESET = 12;
    private AppCompatButton send_sms, forgot_pass;
    private GoogleApiClient googleApiClient;
    private static final String MSG_ENTER_CC = "please enter a valid country code !";
    private static final String MSG_ENTER_PHONE = "please enter a valid phone number !";
    private static final String MSG_PHONE_NOT_EXIST = "phone number is not registered !";
    private static final String MSG_PHONE_EXISTS = "phone number is already registered !";
    private PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    static int countryCode = 0;
    TextView error_txt;

    public void setEnterPhnDlgLstnr(EnterPhnDlgLstnr enterPhnDlgLstnr) {
        this.enterPhnDlgLstnr = enterPhnDlgLstnr;
    }

    public EnterPhoneDlg() {
    }

    public interface EnterPhnDlgLstnr {
        void clickSendSMS(String str);

        void clickResetPasswd(String str);

        void goBack();

        //void clickCall();
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    /*@Override
    public void onResume() {
        if (mCountryCodeField != null)
            if (mCountryCodeField.getText().toString().isEmpty())
                setCountryCodeField(countryCode + "");
        super.onResume();
    }*/

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        verifyType = getArguments().getInt(ARG_DIALOG_TYPE);
        country_code = getArguments().getString(COUNTRY_CODE);
        countryCode = phoneUtil.getCountryCodeForRegion(country_code);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.enter_phone_dlg, null);
        mPhoneField = view.findViewById(R.id.phone_input);
        mPhoneField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    verifyFone();
                }
                return false;
            }
        });
        mPhoneField.addTextChangedListener(new PhoneNumberTextWatcher(mPhoneField));
        mPhoneField.addTextChangedListener(phoneWatcher);
        error_txt = view.findViewById(R.id.error_msg);
        error_txt.setVisibility(ConstraintLayout.GONE);
        mCountryCodeField = view.findViewById(R.id.phone_code);
        setCountryCodeField(countryCode + "");
        mCountryCodeField.addTextChangedListener(phoneWatcher);
        AppCompatButton cancelBtn = view.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                if (enterPhnDlgLstnr != null)
                    enterPhnDlgLstnr.goBack();
                getDialog().dismiss();
            }
        });
        forgot_pass = view.findViewById(R.id.forgot_pass_btn);
        forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPhoneField.getText() != null)
                    if (!mCountryCodeField.getText().toString().isEmpty() && !mPhoneField.getText().toString().isEmpty()) {
                        String phone = mPhoneField.getText().toString().trim().replaceAll("[^\\d]", "");
                        String phoneno = "+" + mCountryCodeField.getText().toString().trim() + phone;
                        if (enterPhnDlgLstnr != null)
                            enterPhnDlgLstnr.clickResetPasswd(phoneno);
                    }
            }
        });
        send_sms = view.findViewById(R.id.send_sms_btn);
        send_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyFone();
            }
        });
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        requestHint();
        return dialog;
    }

    private void verifyFone() {
        String countrycode = "", phoneno = "";
        countrycode = mCountryCodeField.getText().toString().trim();
        if (mPhoneField.getText() != null)
            //{
            phoneno = mPhoneField.getText().toString().trim().replaceAll("[^\\d]", "");

        //}
        //phoneno = mPhoneField.getText().toString().trim();
        if (!countrycode.isEmpty()) {
            if (!phoneno.isEmpty()) {//&& phoneno.length() >= 10
                String phonenum = "+" + countrycode + phoneno;
                if (isPhoneNumberValid(phonenum)) {
                    requestPhoneVerify(phonenum, verifyType);
                } else {
                    error_txt.setVisibility(ConstraintLayout.VISIBLE);
                    error_txt.setText(MSG_ENTER_PHONE);
                    send_sms.setEnabled(false);
                }
            } else {
                error_txt.setVisibility(ConstraintLayout.VISIBLE);
                error_txt.setText(MSG_ENTER_PHONE);
                send_sms.setEnabled(false);
                //Toast.makeText(getContext().getApplicationContext(), MSG_ENTER_PHONE, Toast.LENGTH_SHORT).show();
            }
        } else {
            error_txt.setVisibility(ConstraintLayout.VISIBLE);
            error_txt.setText(MSG_ENTER_CC);
            send_sms.setEnabled(false);

            //Toast.makeText(getContext().getApplicationContext(), MSG_ENTER_CC, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESOLVE_HINT) {
            if (resultCode == RESULT_OK) {
                Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
                displayFormattedPhone(credential.getId());
            }
        }
    }

    private void displayFormattedPhone(String phonenumb) {
        try {
            if (!TextUtils.isEmpty(phonenumb)) {
                String numberOnly = phonenumb.replaceAll("[^0-9]", "");
                String phonenum = "";
                if (numberOnly.length() > 10) {
                    // phone must begin with '+'
                    String phoneno = "+" + numberOnly;
                    Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneno, "");
                    int countryCode = numberProto.getCountryCode();
                    long phone = numberProto.getNationalNumber();
                    setCountryCodeField("" + countryCode);
                    phonenum = phone + "";
                } else {
                    phonenum = numberOnly;
                }
                if (!phonenum.isEmpty() && phonenum.length() <= 10)
                    mPhoneField.setText(phonenum);
                if (mPhoneField.getText() != null)
                    if (mPhoneField.getText().length() > 0 && mPhoneField.getText().length() <= 12) {
                        mPhoneField.setSelection(mPhoneField.getText().length());
                    }
            }
        } catch (NumberParseException | IndexOutOfBoundsException e) {
            //System.err.println("NumberParseException was thrown: " + e.toString());
        }
    }


    private void requestHint() {
        //displayFormattedPhone("+91234556666");
        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Auth.CREDENTIALS_API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        HintRequest hintRequest = new HintRequest.Builder()
                                .setPhoneNumberIdentifierSupported(true)
                                .build();
                        PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                                googleApiClient, hintRequest);
                        try {
                            startIntentSenderForResult(intent.getIntentSender(),
                                    RESOLVE_HINT, null, 0, 0, 0, null);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .build();
        googleApiClient.connect();

    }

    private boolean isPhoneNumberValid(String phoneNumber) {
        try {
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, null);//country code??
            return phoneUtil.isValidNumber(numberProto);
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }
        return false;
    }

    //////////
    private void requestPhoneVerify(final String mPhoneNumber, final int verifyType) {
        send_sms.setEnabled(false);
        mCountryCodeField.setEnabled(false);
        mPhoneField.setEnabled(false);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put("phoneNumber", mPhoneNumber);
        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/user/phoneExists", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.trim().equalsIgnoreCase("true")) {
                            if (verifyType == TYPE_SIGNUP) {
                                send_sms.setEnabled(true);
                                mCountryCodeField.setEnabled(true);
                                mPhoneField.setEnabled(true);
                                //
                                error_txt.setVisibility(ConstraintLayout.VISIBLE);
                                error_txt.setText(MSG_PHONE_EXISTS);
                                forgot_pass.setVisibility(ConstraintLayout.VISIBLE);
                                send_sms.setEnabled(false);
                                //Toast.makeText(getContext().getApplicationContext(), MSG_PHONE_EXISTS, Toast.LENGTH_SHORT).show();
                            } else {
                                if (enterPhnDlgLstnr != null)
                                    enterPhnDlgLstnr.clickResetPasswd(mPhoneNumber);
                                if (getDialog() != null)
                                    getDialog().dismiss();
                            }
                        } else if (response.trim().equalsIgnoreCase("false")) {
                            //todo save the data
                            if (verifyType == TYPE_SIGNUP) {
                                if (enterPhnDlgLstnr != null)
                                    enterPhnDlgLstnr.clickSendSMS(mPhoneNumber);
                                if (getDialog() != null)
                                    getDialog().dismiss();
                            } else {
                                send_sms.setEnabled(true);
                                mCountryCodeField.setEnabled(true);
                                mPhoneField.setEnabled(true);
                                error_txt.setVisibility(ConstraintLayout.VISIBLE);
                                error_txt.setText(MSG_PHONE_NOT_EXIST);
                                send_sms.setEnabled(false);
                                //Toast.makeText(getContext().getApplicationContext(), MSG_PHONE_NOT_EXIST, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                send_sms.setEnabled(true);
                mCountryCodeField.setEnabled(true);
                mPhoneField.setEnabled(true);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    if (isAdded())
                        Toast.makeText(getContext().getApplicationContext(), errString, Toast.LENGTH_SHORT).show();

            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private TextWatcher phoneWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (mPhoneField.getText() == null) {
                send_sms.setEnabled(false);
                forgot_pass.setEnabled(false);
            } else {
                if (mPhoneField.getText().toString().isEmpty()) {
                    send_sms.setEnabled(false);
                    forgot_pass.setEnabled(false);
                } else {
                    send_sms.setEnabled(true);
                    forgot_pass.setEnabled(true);
                }
            }
            error_txt.setVisibility(ConstraintLayout.GONE);
            forgot_pass.setVisibility(ConstraintLayout.GONE);
            // btn.setEnabled(mUsername.getText().length() > 0 && mPassword.getText().length() > 0);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    @Override
    public void onResume() {
        if (mCountryCodeField != null)
            if (mCountryCodeField.getText().toString().isEmpty())
                setCountryCodeField(countryCode + "");
        super.onResume();
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(android.content.DialogInterface dialog,
                                 int keyCode, android.view.KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    if (enterPhnDlgLstnr != null)
                        enterPhnDlgLstnr.goBack();
                    getDialog().dismiss();
                    return true;
                } else return false;
            }
        });
    }

    private void setCountryCodeField(String ss) {
        mCountryCodeField.setText("");
        if (ss != null)
            if (ss.length() > 0) {
                if (ss.length() < 4) {
                    mCountryCodeField.setText(ss);
                } else {
                    mCountryCodeField.setText(ss.substring(0, 3));
                }
            }
    }

    private void hideSoftKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            // WeakReference<View> tview;
            WeakReference<View> tview = new WeakReference<>(getDialog().getCurrentFocus());
            if (tview.get() == null) {
                tview = new WeakReference<>(new View(getDialog().getContext()));
            }
            imm.hideSoftInputFromWindow(tview.get().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }
    ////////
}