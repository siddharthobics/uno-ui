package com.obics.uno.dialog;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.obics.uno.R;
import com.obics.uno.UnoApplication;
import com.obics.uno.send.HotTagRspns;

public class HotTagAdptr extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private HotTagRspns hotTagRspns;
    HotTagAdptLstnr mListener;
    int size = 0, mType;

    interface HotTagAdptLstnr {
        void itemClicked(String itemname);
    }

    public class HotTagHolder extends RecyclerView.ViewHolder {
        TextView tag_text, usrCount;
        String str;
        View btm_vw;

        public HotTagHolder(View view) {
            super(view);
            tag_text = view.findViewById(R.id.tag_txt);
            usrCount = view.findViewById(R.id.tag_usr);
            btm_vw = view;
            view.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mListener.itemClicked(str);
                }
            });
        }
    }


    public HotTagAdptr(HotTagRspns hotTagRspns, HotTagAdptLstnr mListener, int type) {
        this.hotTagRspns = hotTagRspns;
        this.mListener = mListener;
        mType = type;
    }

    public void setNewData(HotTagRspns newsItemList) {
        this.hotTagRspns = newsItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hot_tag_card, parent, false);
        return new HotTagHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final HotTagHolder catItemHolder = (HotTagHolder) holder;
        catItemHolder.str = hotTagRspns.getHotTags().get(position).getTag();
        int users = hotTagRspns.getHotTags().get(position).getUsers();
        String userStr = "user";
        if (users > 1) userStr = "users";
        catItemHolder.usrCount.setText("(" + users + " " + userStr + ")");
        catItemHolder.tag_text.setText("#" + catItemHolder.str);
        if (mType == HotTagDlg.TYPE_CHANNEL_HOT) {
            catItemHolder.tag_text.setTextColor(ContextCompat.getColorStateList(UnoApplication.getAppContext(), R.color.hot_channel_btn_color_lst));
            catItemHolder.usrCount.setTextColor(ContextCompat.getColorStateList(UnoApplication.getAppContext(), R.color.hot_channel_usr_color_lst));
        }
        if (position == size - 1)
            catItemHolder.btm_vw.setBackgroundColor(Color.WHITE);
    }

    @Override
    public int getItemCount() {
        size = hotTagRspns.getHotTags().size();
        if (size > 10)
            size = 10;
        return size;
    }
///////////END OF hot tag ADAPTER/////////////////
}
