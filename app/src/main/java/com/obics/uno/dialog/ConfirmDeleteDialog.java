package com.obics.uno.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.obics.uno.R;

public class ConfirmDeleteDialog extends DialogFragment {

    private ConfirmDelDlgListener mConfrmDelLstnr;
    public static final String CONF_DEL_DLG_TYPE = "conf_del_dlg_type ";
    public static final int TAG_TYPE = 137;
    public static final int MSG_TYPE = 138;
    public static final int REPLY_TYPE = 139;
    int dlgType = 0;

    public ConfirmDeleteDialog() {
    }

    public interface ConfirmDelDlgListener {
        void clickPositive();

        void clickNegative();
    }

    public void setmConfrmDelLstnr(ConfirmDelDlgListener mConfrmDelLstnr) {
        this.mConfrmDelLstnr = mConfrmDelLstnr;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getArguments() != null)
            dlgType = getArguments().getInt(CONF_DEL_DLG_TYPE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.delete_dialog, null);
        TextView header_txt = view.findViewById(R.id.header_text);
        TextView sub_txt = view.findViewById(R.id.dialog_desc);

        switch (dlgType) {
            case TAG_TYPE:
                header_txt.setText(R.string.delete_tag);
                sub_txt.setText(R.string.are_you_sure_to_delete_tag);
                break;
            case REPLY_TYPE:
                header_txt.setText(R.string.delete_reply);
                sub_txt.setText(R.string.are_you_sure_to_delete_reply);
                break;
            case MSG_TYPE:
            default:
                break;
        }

        AppCompatButton delBtn = view.findViewById(R.id.delet_btn);
        delBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mConfrmDelLstnr != null)
                    mConfrmDelLstnr.clickPositive();
                getDialog().dismiss();
            }
        });
        AppCompatButton cancelBtn = view.findViewById(R.id.cancel_btn);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mConfrmDelLstnr != null)
                    mConfrmDelLstnr.clickNegative();
                getDialog().dismiss();

            }
        });
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (mConfrmDelLstnr != null)
                    mConfrmDelLstnr.clickNegative();
                getDialog().dismiss();

            }
        });
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (mConfrmDelLstnr != null)
                    mConfrmDelLstnr.clickNegative();
            }
        });
        return dialog;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(android.content.DialogInterface dialog,
                                 int keyCode, android.view.KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    if (mConfrmDelLstnr != null)
                        mConfrmDelLstnr.clickNegative();
                    getDialog().dismiss();
                    return true;
                } else return false;
            }
        });
    }
}