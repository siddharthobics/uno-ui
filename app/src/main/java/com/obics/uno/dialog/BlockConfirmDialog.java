package com.obics.uno.dialog;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.obics.uno.R;
import com.obics.uno.UnoApplication;
import com.obics.uno.config.ConfigData;
import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

public class BlockConfirmDialog extends DialogFragment {


    private int mUserId = 0;
    private String mUserName = "";
    public static final String USER_ID = "block_user_id";
    public static final String USER_NAME = "block_user_name";
    private BlockListener blockListener;


    public interface BlockListener {
        void userBlocked();
    }

    public void setBlockLstnr(BlockListener mConfrmDelLstnr) {
        this.blockListener = mConfrmDelLstnr;
    }


    public BlockConfirmDialog() {
    }


    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getArguments() != null) {
            mUserId = getArguments().getInt(USER_ID);
            mUserName = getArguments().getString(USER_NAME);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout

        View v = inflater.inflate(R.layout.block_dialog, null);
        TextView mMsgField = v.findViewById(R.id.dialog_desc);
        mMsgField.setText("Are you sure you want\nto block '" + mUserName + "'");
        AppCompatButton btn = v.findViewById(R.id.cancel_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();

            }
        });
        AppCompatButton btn2 = v.findViewById(R.id.delet_btn);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestBlockUser(UnoApplication.getAppContext());
            }
        });

        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return d;
    }

    public void requestBlockUser(final Context context) {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);
            jsonBody.put("userBlockingId", mUserId);

        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/admin/blockedUser/add", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.trim().equalsIgnoreCase("true")) {
                            //hideSoftKeyboard(context, mUniqueUserField);
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (context != null)
                                        Toast.makeText(context, "   User blocked!   ", Toast.LENGTH_SHORT).show();
                                    if (isAdded() && blockListener != null)
                                        blockListener.userBlocked();
                                }
                            });
                            if (getDialog() != null)
                                getDialog().dismiss();
                        } else {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    if (context != null)
                        Toast.makeText(context, errString, Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, true);
    }

}