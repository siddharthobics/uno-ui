package com.obics.uno.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
//import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.google.gson.Gson;
//import com.obics.uno.MainActivity;
import com.obics.uno.MapActivity;
import com.obics.uno.R;
//import com.obics.uno.config.ConfigData;
import com.obics.uno.send.HotTagRspns;
import com.obics.uno.utils.FormatUtils;

public class HotTagDlg extends DialogFragment {

    private HotTagDlgLstnr hotTagDlgLstnr;
    private RecyclerView mRecyclerView;
    public static final String ARG_HOT_TAG_MILES = "hottagmiles";
    public static final String ARG_HOT_TAG_TYPE = "hottagdlgtype";
    public static final int TYPE_BROADCAST_HOT = 71;
    public static final int TYPE_CHANNEL_HOT = 72;

    public void setHotTagDlgLstnr(HotTagDlgLstnr hotTagDlgLstnr) {
        this.hotTagDlgLstnr = hotTagDlgLstnr;
    }

    public HotTagDlg() {
    }

    public interface HotTagDlgLstnr {
        void clickPositive(String str);

        void dismiss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String responseJson = getArguments().getString(ARG_HOT_TAG_MILES);
        int type = getArguments().getInt(ARG_HOT_TAG_TYPE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.hot_tag_dlg, null);
        View topView = view.findViewById(R.id.heading_txt);
        switch (type) {
            case TYPE_CHANNEL_HOT:
                topView.setBackgroundResource(R.drawable.map_recv_header_bg);
                break;
            case TYPE_BROADCAST_HOT:
                break;
        }
        final HotTagRspns hotTagRspns = new Gson().fromJson(responseJson, HotTagRspns.class);
        TextView heading = view.findViewById(R.id.heading_toptxt);
        //if (hotTagRspns.getHotTags() != null)
        if (hotTagRspns.getHotTags().size() < 2)
            heading.setText("Top Channel");
        mRecyclerView = view.findViewById(R.id.hot_tag_recycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        ViewTreeObserver treeObserver = mRecyclerView.getViewTreeObserver();
        treeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                mRecyclerView.getWindowVisibleDisplayFrame(rect);
                int mRootHeight = mRecyclerView.getRootView().getHeight();
                int heightDiff = (rect.bottom - rect.top) - mRootHeight;
                //Log.d("MISHU", heightDiff + "  " + mRootHeight);
                int diff = FormatUtils.dpToPx(getActivity(), 8);
                if (heightDiff <= diff) {
                    ConstraintLayout.LayoutParams clp = (ConstraintLayout.LayoutParams) mRecyclerView.getLayoutParams();
                    clp.height = mRootHeight - (FormatUtils.dpToPx(getActivity(), 60));
                    mRecyclerView.setLayoutParams(clp);
                }
                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                    mRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                //else
                  //  mRecyclerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
        mRecyclerView.setAdapter(new HotTagAdptr(hotTagRspns, new HotTagAdptr.HotTagAdptLstnr() {
            @Override
            public void itemClicked(String itemname) {
                if (hotTagDlgLstnr != null)
                    hotTagDlgLstnr.clickPositive(itemname);
                getDialog().dismiss();
            }
        }, type));
        View closeBtn = view.findViewById(R.id.close_btn);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hotTagDlgLstnr != null)
                    hotTagDlgLstnr.dismiss();
                getDialog().dismiss();
            }
        });
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationBottomTop;
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (hotTagDlgLstnr != null)
                    hotTagDlgLstnr.dismiss();
            }
        });
        return dialog;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog,
                                 int keyCode, android.view.KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    if (hotTagDlgLstnr != null)
                        hotTagDlgLstnr.dismiss();
                    getDialog().dismiss();
                    return true;
                } else return false;
            }
        });
    }
}
