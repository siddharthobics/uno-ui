package com.obics.uno.dialog;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.obics.uno.R;

public class NotificationEnableDlg extends DialogFragment {
    public static final String NOTIF_DLG_TYPE = "notif_dlg_type ";
    public static final int NOTIF_APP = 1;
    public static final int NOTIF_CATEG = 2;
    private int dlgType;

    EnableNotifnListener enableNotifnListener;

    public void setEnableNotifnListener(EnableNotifnListener enableNotifnListener) {
        this.enableNotifnListener = enableNotifnListener;
    }

    public NotificationEnableDlg() {
    }

    public interface EnableNotifnListener {

        void enableNotifications();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getArguments() != null)
            dlgType = getArguments().getInt(NOTIF_DLG_TYPE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View v = inflater.inflate(R.layout.enabl_notificatn, null);
        final TextView mHeaderField = v.findViewById(R.id.dialog_desc);
        switch (dlgType) {
            case NOTIF_APP:
                mHeaderField.setText(R.string.push_notif_app);
                break;
            case NOTIF_CATEG:
                mHeaderField.setText(R.string.push_notif_channel);
                break;
            default:
                break;
        }
        AppCompatButton btn = v.findViewById(R.id.cam_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        AppCompatButton btn2 = v.findViewById(R.id.gallery_btn);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                if (enableNotifnListener != null)
                    enableNotifnListener.enableNotifications();
            }
        });
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return d;
    }


}