package com.obics.uno.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.obics.uno.MainActivity;
import com.obics.uno.R;
import com.obics.uno.config.ConfigData;
import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
import com.obics.uno.utils.DatabaseMgr;
import com.obics.uno.utils.ToolDotProgress;

import org.json.JSONException;
import org.json.JSONObject;

public class DelAccDialog extends DialogFragment {
    private int dlgType;
    ToolDotProgress dotProgress;
    private AppCompatButton mActiveBtn;
    public static final String DELETE_DLG_TYPE = "del_dialog_type ";
    public static final int DELETE_ACCOUNT_TYPE = 113;
    public static final int LOGOUT_TYPE = 114;

    public interface DelDlgListener {
        void logOut();
    }

    private DelDlgListener mConfrmDelLstnr;

    public void setmConfrmDelLstnr(DelDlgListener mConfrmDelLstnr) {
        this.mConfrmDelLstnr = mConfrmDelLstnr;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getArguments() != null)
            dlgType = getArguments().getInt(DELETE_DLG_TYPE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.del_acc_dialog, null);
        TextView desc = view.findViewById(R.id.dialog_desc);
        mActiveBtn = view.findViewById(R.id.del_btn);
        AppCompatButton btn2 = view.findViewById(R.id.cancel_btn);
        TextView headingTxt = view.findViewById(R.id.heading_txt);
        dotProgress = view.findViewById(R.id.mini_progress);
        if (dlgType == DELETE_ACCOUNT_TYPE) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            String phoneno = preferences.getString(ConfigData.PREF_PHONE_NUMBER, "");
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            String pnE164 = "+" + phoneno;
            try {
                Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneno, "");
                pnE164 = phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164);
            } catch (NumberParseException e) {
                // System.err.println("NumberParseException was thrown: " + e.toString());
            }
            if (isAdded())
                desc.setText(getString(R.string.delete_account_message, pnE164));
        } else if (dlgType == LOGOUT_TYPE) {
            headingTxt.setText(R.string.confirm_logout);
            desc.setText(R.string.logout_confirm_text);
            mActiveBtn.setText(R.string.logoutBtnTxt);
        }
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        mActiveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mActiveBtn.setEnabled(false);
                requestClearFcmTokenUpdate();
            }
        });

        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    private void requestClearFcmTokenUpdate() {
        //Log.d("FireMSAK", "lucky");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, false);
        editor.apply();
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);
            jsonBody.put("userAppId", "");
        } catch (JSONException e) {

        }
        dotProgress.setVisibility(ConstraintLayout.VISIBLE);
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/notify/updateappid", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dotProgress.setVisibility(ConstraintLayout.GONE);
                        if (response.trim().equalsIgnoreCase("true")) {
                            //Log.d("FireTUDOY", "fcm deleted success");
                            if (dlgType == DELETE_ACCOUNT_TYPE) {
                                deleteAccount();
                            } else if (dlgType == LOGOUT_TYPE) {
                                if (getActivity() != null)
                                    logOut(getActivity());
                            }
                            //deleteAccount();
                        } else if (response.trim().equalsIgnoreCase("false")) {
                            if (mActiveBtn != null)
                                mActiveBtn.setEnabled(true);
                            if (isAdded())
                                if (dlgType == DELETE_ACCOUNT_TYPE)
                                    showToast(R.string.unable_to_delete_account);
                                else if (dlgType == LOGOUT_TYPE)
                                    showToast(R.string.unable_to_logout);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dotProgress.setVisibility(ConstraintLayout.GONE);

                String errString = NetUtil.getMessage(error);
                if (mActiveBtn != null)
                    mActiveBtn.setEnabled(true);
                if (!errString.isEmpty())
                    showToast(R.string.no_internet_conn);
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private void deleteAccount() {
        if (ConfigData.USER_ID == ConfigData.INVALID_USER_ID)
            return;
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);

            dotProgress.setVisibility(ConstraintLayout.VISIBLE);

            JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                    ConfigData.BASE_URL + "/user/deleteAccount", jsonBody,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            dotProgress.setVisibility(ConstraintLayout.GONE);

                            if (response.trim().equalsIgnoreCase("true")) {
                                if (getActivity() != null) {
                                    logOut(getActivity());
                                }
                            } else if (response.trim().equalsIgnoreCase("false")) {
                                if (mActiveBtn != null)
                                    mActiveBtn.setEnabled(true);
                                showToast(R.string.unable_to_delete_account);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    dotProgress.setVisibility(ConstraintLayout.GONE);

                    String errString = NetUtil.getMessage(error);
                    if (mActiveBtn != null)
                        mActiveBtn.setEnabled(true);
                    if (!errString.isEmpty())
                        showToast(R.string.no_internet_conn);
                }
            });
            VolleySingleton.getInstance().addToRequestQueue(req, false);
        } catch (JSONException | IllegalStateException | NullPointerException e) {

        }
    }

    @NonNull
    private void logOut(final Activity activity) {
        DatabaseMgr databaseMgr = new DatabaseMgr(activity);
        databaseMgr.deleteAll();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(ConfigData.PREF_IS_LOGGED, false);
        editor.putBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, false);
        editor.putInt(ConfigData.PREF_USER_ID, ConfigData.INVALID_USER_ID);
        editor.putString(ConfigData.PREF_PROFILE_PIC, "");
        editor.putString(ConfigData.PREF_USERNAME, "");
        editor.putString(ConfigData.PREF_PHONE_NUMBER, "");
        editor.putString(ConfigData.PREF_DISPLAY_NAME, "");
        editor.putString(ConfigData.LOGIN_LATITUDE, "");
        editor.putString(ConfigData.LOGIN_LONGITUDE, "");
        editor.putString(ConfigData.PREF_ZIPCODE, "");
        editor.putString(ConfigData.ZIPCODE_LATITUDE, "");
        editor.putString(ConfigData.ZIPCODE_LONGITUDE, "");
        editor.putString(ConfigData.PREF_LATITUDE, "");
        editor.putString(ConfigData.PREF_LONGITUDE, "");
        editor.putString(ConfigData.PREF_WHERE_FROM, "");
        editor.putString(ConfigData.PREF_ABOUT_ME, "");
        editor.putInt(ConfigData.PREF_RECV_NOTIF_COUNT, 0);
        editor.putInt(ConfigData.PREF_SEND_NOTIF_COUNT, 0);
        editor.apply();
        ConfigData.USER_ID = ConfigData.INVALID_USER_ID;
        if (isAdded()) {
            if (mConfrmDelLstnr != null)
                mConfrmDelLstnr.logOut();
            if (getActivity() != null) {
                MainActivity act = (MainActivity) getActivity();
                act.actRecreate();
            }
            getDialog().dismiss();
        }
    }

    private void showToast(int resid) {
        try {
            if (getContext() != null)
                Toast.makeText(getContext().getApplicationContext(), resid, Toast.LENGTH_SHORT).show();
        } catch (IllegalStateException | NullPointerException ilnpe) {

        }
    }

}