package com.obics.uno.dialog;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.obics.uno.MainActivity;
import com.obics.uno.R;
//import com.obics.uno.UnoLogin;
import com.obics.uno.UnoApplication;
import com.obics.uno.config.ConfigData;
import com.obics.uno.config.LoginRspns;
import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
import com.obics.uno.utils.ToolDotProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;

public class LoginDlg extends DialogFragment {

    private LoginLstnr loginLstnr;
    private EditText mUsername, mPassword;
    private static final String LOGIN_ERROR = "username or password incorrect !";
    private static final String LOGIN_URL = ConfigData.BASE_URL + "/user/login";
    public static final String AUTOLOG_USER = "autologin_user";
    public static final String AUTOLOG_PASS = "autologin_passwd";
    public static final String COUNTRY_CODE = "country_code";
    private PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
    private TextView error_txt;
    private int countrycode = 0;
    private ToolDotProgress mProgress;
    private AppCompatButton btn;

    public void setLoginLstnr(LoginLstnr loginLstnr) {
        this.loginLstnr = loginLstnr;
    }

    public LoginDlg() {
    }

    public interface LoginLstnr {
        void clickLogin();

        void clickForgotPassword();

        void goBack();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        String autouser = "";
        String autopass = "";
        String countrcode = "";

        if (getArguments() != null) {
            autouser = getArguments().getString(AUTOLOG_USER, "");
            autopass = getArguments().getString(AUTOLOG_PASS, "");
            countrcode = getArguments().getString(COUNTRY_CODE, "");
        }
        countrycode = phoneUtil.getCountryCodeForRegion(countrcode);

        if (!autouser.isEmpty() && !autopass.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            View view = inflater.inflate(R.layout.common_info_dlg, null);
            TextView tv = view.findViewById(R.id.bottom_txt);
            mProgress = view.findViewById(R.id.progressBar);
            tv.setText(R.string.logging_in);
            builder.setView(view);
            requestLogin(autouser, autopass);
            Dialog dialog = builder.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);
            return dialog;

        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            View view = inflater.inflate(R.layout.login_dlg, null);
            mProgress = view.findViewById(R.id.progressBar);
            error_txt = view.findViewById(R.id.error_msg);
            mUsername = view.findViewById(R.id.login_edit_usr);
            mPassword = view.findViewById(R.id.login_passwd_edit);
            mUsername.addTextChangedListener(loginWatcher);
            mPassword.addTextChangedListener(loginWatcher);
            btn = view.findViewById(R.id.login_btn);
            btn.setEnabled(false);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String username = mUsername.getText().toString().trim();
                    try {
                        new BigInteger(username);
                        String tempphone = "+" + countrycode + username;
                        if (isPhoneNumberValid(tempphone))
                            username = tempphone;

                    } catch (
                            NumberFormatException e)

                    {
                        //System.out.println(input1 + " is not a valid integer number");
                    }

                    //if (mPassword.getText().toString().trim().isEmpty())
                    //    Toast.makeText(getContext().getApplicationContext(), "   Password cannot be blank!   ", Toast.LENGTH_SHORT).show();
                    //if (mUsername.getText().toString().trim().isEmpty())
                    //    Toast.makeText(getContext().getApplicationContext(), "   Phone number cannot be blank!   ", Toast.LENGTH_SHORT).show();
                    requestLogin(username, mPassword.getText().

                            toString());
                    if (loginLstnr != null)
                        loginLstnr.clickLogin();
                }
            });
            AppCompatButton btn2 = view.findViewById(R.id.forgot_btn);
            btn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (loginLstnr != null)
                        loginLstnr.clickForgotPassword();
                }
            });
            builder.setView(view);

            Dialog dialog = builder.create();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCanceledOnTouchOutside(false);

            return dialog;
        }
        //return null;
    }

    private boolean isPhoneNumberValid(String phoneNumber) {
        try {
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneNumber, null);//country code??
            return phoneUtil.isValidNumber(numberProto);
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }
        return false;
    }

    private void requestLogin(final String username, final String password) {
        mProgress.setVisibility(View.VISIBLE);
        String text = username;
        String formatted = getFormattedPhone(text);
        if (!formatted.isEmpty())
            text = formatted;
        JSONObject jsonBody = null;
        try {
            //jsonBody = new JSONObject("{\"userName\": \"" + text + "\", \"password\": \"" + mPassword.getText() + "\"}");
            jsonBody = new JSONObject();
            jsonBody.put("userName", text);
            jsonBody.put("password", password);
        } catch (JSONException e) {

        }
        if (btn != null)
            btn.setEnabled(false);
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST, LOGIN_URL, jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (btn != null)
                            btn.setEnabled(true);

                        if (response.isEmpty()) {
                            error_txt.setVisibility(ConstraintLayout.VISIBLE);
                            error_txt.setText(LOGIN_ERROR);
                            //Toast.makeText(getContext().getApplicationContext(), LOGIN_ERROR, Toast.LENGTH_SHORT).show();
                            //mUsername.setText("");
                            //mPassword.setText("");
                            mProgress.setVisibility(View.GONE);
                        } else {
                            LoginRspns rspns = new Gson().fromJson(response, LoginRspns.class);
                            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(UnoApplication.getAppContext());
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putBoolean(ConfigData.PREF_IS_LOGGED, true);
                            editor.putInt(ConfigData.PREF_USER_ID, rspns.getUserId());
                            editor.putString(ConfigData.PREF_PROFILE_PIC, rspns.getProfileImg());
                            editor.putString(ConfigData.PREF_USERNAME, rspns.getUserName());
                            editor.putString(ConfigData.PREF_PHONE_NUMBER, rspns.getPhoneNumber());

                            String aboutme = rspns.getAboutMe();
                            if (!TextUtils.isEmpty(aboutme))
                                editor.putString(ConfigData.PREF_ABOUT_ME, aboutme);

                            String whereFrom = rspns.getWhereFrom();
                            if (!TextUtils.isEmpty(whereFrom))
                                editor.putString(ConfigData.PREF_WHERE_FROM, whereFrom);

                            String dispnam = rspns.getDisplayName();
                            if (TextUtils.isEmpty(dispnam))
                                dispnam = rspns.getUserName();

                            editor.putString(ConfigData.PREF_DISPLAY_NAME, dispnam);
                            editor.putString(ConfigData.LOGIN_LATITUDE, rspns.getLatitude());
                            editor.putString(ConfigData.LOGIN_LONGITUDE, rspns.getLongtitude());
                            //setup latitude and longitude and zipcode
                            if (rspns.getZipcode() != null) {
                                // USE *****zipcode***** FROM LOGIN INFO
                                editor.putBoolean(ConfigData.PREF_USE_LOCATION, false);
                                String zipcode = "", ziplat = "", ziplong = "";
                                if (rspns.getZipcode() != null) {
                                    zipcode = rspns.getZipcode().getZipcode();
                                    ziplat = rspns.getZipcode().getLatitude();
                                    ziplong = rspns.getZipcode().getLongitude();
                                    if (zipcode == null)
                                        zipcode = "";
                                }
                                editor.putString(ConfigData.PREF_ZIPCODE, zipcode);
                                editor.putString(ConfigData.ZIPCODE_LATITUDE, ziplat);
                                editor.putString(ConfigData.ZIPCODE_LONGITUDE, ziplong);
                            } else {
                                // USE LATITUDE FROM LOGIN INFO
                                editor.putBoolean(ConfigData.PREF_USE_LOCATION, true);
                                editor.putBoolean(ConfigData.NEWS_FIRST_TIME, true);

                                editor.putString(ConfigData.PREF_LATITUDE, rspns.getLatitude());
                                editor.putString(ConfigData.PREF_LONGITUDE, rspns.getLongtitude());
                            }
                            //setup latitude and longitude and zipcod end/////////
                            editor.apply();
                            getDialog().dismiss();
                            if (getActivity() != null) {
                                MainActivity act = (MainActivity) getActivity();
                                act.actRecreate();
                            }
                            /*getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                }
                            });*/
                        }
                        ///TODO PARSE REQUEST
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (btn != null)
                    btn.setEnabled(true);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    if (isAdded())
                        Toast.makeText(getContext().getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
                mProgress.setVisibility(View.GONE);
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        //getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    private String getFormattedPhone(String phoneno) {
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        String disphoneno = "";
        try {
            // phone must begin with '+'
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(phoneno, "");
            int countryCode = numberProto.getCountryCode();
            long phone = numberProto.getNationalNumber();
            disphoneno = "" + countryCode + phone;
        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }
        return disphoneno;
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(android.content.DialogInterface dialog,
                                 int keyCode, android.view.KeyEvent event) {
                if ((keyCode == android.view.KeyEvent.KEYCODE_BACK)) {
                    if (loginLstnr != null)
                        loginLstnr.goBack();
                    getDialog().dismiss();
                    return true;
                } else return false;
            }
        });
    }

    private TextWatcher loginWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            error_txt.setVisibility(ConstraintLayout.GONE);
            btn.setEnabled(mUsername.getText().length() > 0 && mPassword.getText().length() > 0);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
}