package com.obics.uno.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
//import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.text.Editable;
//import android.text.InputType;
import android.text.TextWatcher;
//import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
//import android.widget.Button;
import android.widget.EditText;
//import android.widget.ImageButton;
//import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.obics.uno.R;
import com.obics.uno.UnoApplication;
import com.obics.uno.config.ConfigData;
import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

public class EnterFeedbackDlg extends DialogFragment {
    private TextView countView;
    private EditText mFeedbackField;
    private AppCompatButton mFeedbackBtn;
    private int mUserId = 0;
    public static final String USER_ID = "report_user_id";

    public EnterFeedbackDlg() {
    }

    @Override
    public void onStart() {
        super.onStart();
        // showSoftKeyboard(getContext().getApplicationContext(), mFeedbackField);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if (getArguments() != null) {
            mUserId = getArguments().getInt(USER_ID);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.user_feedback_dlg, null);
        countView = view.findViewById(R.id.wcount);
        mFeedbackBtn = view.findViewById(R.id.feedbk_submit_btn);
        mFeedbackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmit();
            }
        });
        AppCompatImageButton close = view.findViewById(R.id.close_btn);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard(UnoApplication.getAppContext(), mFeedbackField);
                getDialog().dismiss();
            }
        });
        mFeedbackField = view.findViewById(R.id.username_input);
        mFeedbackField.addTextChangedListener(mTagDescTxtWatcher);
        mFeedbackField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if (view.getId() == R.id.username_input) {
                    view.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            view.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });
        mFeedbackField.setMaxLines(4); // Or specify a lower value if you want
        mFeedbackField.setHorizontallyScrolling(false);
        mFeedbackField.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    onSubmit();
                }
                return false;
            }
        });
        builder.setView(view);

        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    private void onSubmit() {
        String str = mFeedbackField.getText().toString().trim();
        if (str.length() > 3)
            requestReportUser(UnoApplication.getAppContext(), mFeedbackField.getText().toString().trim());
        else if (isAdded())
            Toast.makeText(UnoApplication.getAppContext(), "   Please enter something...   ", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    private TextWatcher mTagDescTxtWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            int len = 0;
            if (s != null)
                len = s.length();
            countView.setText("" + (1000 - len));

        }
    };

    public void requestReportUser(final Context context, String report) {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);
            jsonBody.put("reportedUserId", mUserId);
            jsonBody.put("userDescription", report);

        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/admin/reportUser", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.trim().equalsIgnoreCase("true")) {
                            hideSoftKeyboard(context, mFeedbackField);
                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (context != null)
                                        Toast.makeText(context, "   Feedback submitted!   ", Toast.LENGTH_SHORT).show();
                                }
                            });
                            if (getDialog() != null)
                                getDialog().dismiss();
                        } else {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    if (context != null)
                        Toast.makeText(context, errString, Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, true);
    }

    protected void hideSoftKeyboard(final Context context, EditText input) {
        WeakReference<EditText> tedit = new WeakReference<>(input);
        tedit.get().setInputType(0);
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(tedit.get().getWindowToken(), 0);
        } catch (NullPointerException npe) {
        }
    }
   /* protected void showSoftKeyboard(final Context context, EditText input) {
        input.setInputType(0);
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInputFromInputMethod(input.getWindowToken(), 0);
    }*/
}