package com.obics.uno.dialog;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.obics.uno.R;

public class LoginSignupDlg extends DialogFragment {

    private LoginSignupLstnr loginSignupLstnr;

    public void setLoginSignupLstnr(LoginSignupLstnr loginSignupLstnr) {
        this.loginSignupLstnr = loginSignupLstnr;
    }

    public LoginSignupDlg() {
    }
    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
       // getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }
    public interface LoginSignupLstnr {
        void clickLogin();

        void clickSignup();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.login_signup_dlg, null);
        AppCompatButton login_btn = view.findViewById(R.id.login_btn);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginSignupLstnr != null)
                    loginSignupLstnr.clickLogin();
            }
        });
        AppCompatButton signup_btn = view.findViewById(R.id.signup_btn);
        signup_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginSignupLstnr != null)
                    loginSignupLstnr.clickSignup();
            }
        });
        builder.setView(view);
        Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }
}