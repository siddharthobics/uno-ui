package com.obics.uno.dialog;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.obics.uno.R;
import com.obics.uno.utils.OtpListener;
import com.obics.uno.utils.OtpView;

public class SmsVerifyDlg extends DialogFragment {

    private SmsVerifyDlgLstnr smsVerifyDlgLstnr;
    private OtpView otpView;
    private TextView invalidtxt, mMesgTxt, mTimerTxt;
    private String phoneno = "", smscode = "";
    //private ProgressBar mProgress;
    public static final String ARG_PHONE_NUMBER = "arg_phone_number";
    public static final String ARG_SMS_CODE = "arg_sms_code";
    private static final String ARG_USR_MSG = "Enter the 6-digit verification code\nwe sent to ";
    private CountDownTimer countDownTimer;
    private ProgressBar timer_prog;
    AppCompatButton btn;

    public void setSmsVerifyDlgLstnr(SmsVerifyDlgLstnr smsVerifyDlgLstnr) {
        this.smsVerifyDlgLstnr = smsVerifyDlgLstnr;
    }

    public SmsVerifyDlg() {
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    @Override
    public void onDestroy() {
        if (countDownTimer != null)
            countDownTimer.cancel();
        super.onDestroy();
    }

    public interface SmsVerifyDlgLstnr {
        void otpEntered(String str);

        void resendOtp(String str);
    }

    public void setPinTxt(String pin) {
        //otpView.setOTP(pin);
        if (isAdded()) {
            otpView.setDummyOTP(pin);
            mMesgTxt.append("\n");
            mMesgTxt.append(Html.fromHtml("<font color=\"#967cc3\"><b>Verifying ...</b></font>"));
        }
    }

    public void setInvalidtxt(boolean visible) {
        if (isAdded()) {
            mMesgTxt.setText(ARG_USR_MSG + phoneno);
            if (visible) {
                //mMesgTxt.setText(ARG_USR_MSG + phoneno);
                invalidtxt.setVisibility(ConstraintLayout.VISIBLE);
                otpView.clearOTP();
            } else
                invalidtxt.setVisibility(ConstraintLayout.GONE);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        phoneno = getArguments().getString(ARG_PHONE_NUMBER);
        smscode = getArguments().getString(ARG_SMS_CODE);
        countDownTimer = new CountDownTimer(60000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int remaining = (int) millisUntilFinished / 1000;
                mTimerTxt.setText(remaining + "s");
                timer_prog.setProgress(remaining);
            }

            @Override
            public void onFinish() {
                btn.setEnabled(true);
                mMesgTxt.setText(ARG_USR_MSG + phoneno);
                mTimerTxt.setText("0s");
                mTimerTxt.setVisibility(ConstraintLayout.GONE);
            }
        };
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        View view = inflater.inflate(R.layout.entr_pin_dlg, null);
        invalidtxt = view.findViewById(R.id.invalid_otp);
        mMesgTxt = view.findViewById(R.id.dialog_desc);
        mMesgTxt.setText(ARG_USR_MSG + phoneno);
        mTimerTxt = view.findViewById(R.id.timer_txt);
        otpView = view.findViewById(R.id.otp_view);
        otpView.setListener(new OtpListener() {
            @Override
            public void onOtpEntered(String otp) {
                if (smsVerifyDlgLstnr != null) {
                    smsVerifyDlgLstnr.otpEntered(otpView.getOTP().trim());
                    mMesgTxt.append("\n");
                    mMesgTxt.append(Html.fromHtml("<font color=\"#967cc3\"><b>Verifying ...</b></font>"));
                }
            }
        });
        btn = view.findViewById(R.id.resend_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (countDownTimer != null)
                    countDownTimer.start();
                btn.setEnabled(false);
                mTimerTxt.setVisibility(ConstraintLayout.VISIBLE);
                if (smsVerifyDlgLstnr != null)
                    smsVerifyDlgLstnr.resendOtp(phoneno);
            }
        });
        timer_prog = view.findViewById(R.id.mini_progress);
        builder.setView(view);
        Dialog dialog = builder.create();
        btn.setEnabled(false);
        countDownTimer.start();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }


}