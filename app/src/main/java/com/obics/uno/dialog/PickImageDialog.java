package com.obics.uno.dialog;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.view.LayoutInflater;
import android.view.View;

import com.obics.uno.R;

public class PickImageDialog extends DialogFragment {


    PickImgListener pickImgListener;

    public void setPickImgListener(PickImgListener pickImgListener) {
        this.pickImgListener = pickImgListener;
    }

    public PickImageDialog() {
    }

    public interface PickImgListener {
        void clickCamera();

        void removePhoto();

        void clickGallery();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        try {
            FragmentTransaction ft = manager.beginTransaction();
            ft.add(this, tag);
            ft.commitNowAllowingStateLoss();
        } catch (IllegalStateException e) {
            //Log.d("ABSDIALOGFRAG", "Exception", e);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // getArguments().
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout

        View v = inflater.inflate(R.layout.pick_image_dialog, null);
        //final EditText mPasswordField = v.findViewById(R.id.phone_input);
        AppCompatButton btn = v.findViewById(R.id.cam_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                if (pickImgListener != null)
                    pickImgListener.clickCamera();
            }
        });
        AppCompatButton btn2 = v.findViewById(R.id.gallery_btn);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                if (pickImgListener != null)
                    pickImgListener.clickGallery();
            }
        });
        AppCompatImageButton btn3 = v.findViewById(R.id.remove_pic);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                if (pickImgListener != null)
                    pickImgListener.removePhoto();
            }
        });
        builder.setView(v);
        Dialog d = builder.create();
        d.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return d;
    }


}