package com.obics.uno.map;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.obics.uno.R;

public class CenteredMapFragment extends Fragment implements OnMapReadyCallback {

    CenteredMapView centeredMapView;
    Bundle bundle;
    GoogleMap map;
    OnMapReadyCallback mapReadyCallback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);
        centeredMapView = v.findViewById(R.id.mapView);
        centeredMapView.onCreate(bundle);
        centeredMapView.onResume();
        centeredMapView.getMapAsync(this);
        MapsInitializer.initialize(getActivity());
        return v;
    }

    public GoogleMap getMap() {
        return map;
    }

    @Override
    public void onResume() {
        super.onResume();
        centeredMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        centeredMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        centeredMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        centeredMapView.onLowMemory();
    }

    public void setMapReadyCallback(OnMapReadyCallback mapReadyCallback) {
        this.mapReadyCallback = mapReadyCallback;
    }

    public void setMapCenter(LatLng center) {
        centeredMapView.setCenter(center);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        centeredMapView.init(googleMap);
        mapReadyCallback.onMapReady(googleMap);
    }
}