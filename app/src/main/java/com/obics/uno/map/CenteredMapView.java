package com.obics.uno.map;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;

public class CenteredMapView extends MapView {
    private int fingers = 0;
    private boolean isMapAvailable = false;
    private GoogleMap googleMap;
    private long lastZoomTime = 0;
    private float lastSpan = -1;
    private static final int DRAG_ZOOM_TIME = 50;
    private static final int DOUBLE_TAP_ZOOM_TIME = 400;
    private static final int ZOOM_THRESHOLD = 9;
    private static final int ZOOM_FACTOR = 8;

    private LatLng mapCenter = new LatLng(0, 0);

    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetector gestureDetector;

    public CenteredMapView(Context context) {
        super(context);
    }

    public CenteredMapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CenteredMapView(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style);
    }

    public void setCenter(LatLng center) {
        mapCenter = center;
    }

    public void init(GoogleMap map) {
        googleMap = map;
        if (map != null) {
            isMapAvailable = true;
        } else {
            isMapAvailable = false;
        }
        disableScrolling();
        scaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleGestureDetector.OnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                if (lastSpan == -1) {
                    lastSpan = detector.getCurrentSpan();
                } else if (detector.getEventTime() - lastZoomTime >= DRAG_ZOOM_TIME) {
                    lastZoomTime = detector.getEventTime();
                    float zoomIncrVal = getZoomValue(detector.getCurrentSpan(), lastSpan);
                    float camZoomVal = googleMap.getCameraPosition().zoom;
                    float zoomVal;
                    if (camZoomVal > ZOOM_THRESHOLD)
                        zoomVal = camZoomVal + zoomIncrVal * (camZoomVal / ZOOM_FACTOR);
                    else
                        zoomVal = camZoomVal + zoomIncrVal * 2;
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mapCenter, zoomVal), DRAG_ZOOM_TIME, null);
                    lastSpan = detector.getCurrentSpan();
                }
                return false;
            }

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                lastSpan = -1;
                return true;
            }

            @Override
            public void onScaleEnd(ScaleGestureDetector detector) {
                lastSpan = -1;

            }
        });
        gestureDetector = new GestureDetector(getContext(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onDoubleTapEvent(MotionEvent e) {
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(mapCenter));
                googleMap.animateCamera(CameraUpdateFactory.zoomIn(), DOUBLE_TAP_ZOOM_TIME, null);
                return true;
            }
        });
    }

    private float getZoomValue(float currentSpan, float lastSpan) {
        double value = (Math.log(currentSpan / lastSpan) / Math.log(2));
        return (float) value;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
      /*  if (isMapAvailable) {
            boolean retVal = scaleGestureDetector.onTouchEvent(ev);
            retVal = gestureDetector.onTouchEvent(ev) || retVal;
            return retVal || super.dispatchTouchEvent(ev);
        } else
            return false;*/


        switch (ev.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_POINTER_DOWN:
                fingers = fingers + 1;
                break;
            case MotionEvent.ACTION_POINTER_UP:
                fingers = fingers - 1;
                break;
            case MotionEvent.ACTION_UP:
                fingers = 0;
                break;
            case MotionEvent.ACTION_DOWN:
                fingers = 1;
                break;
        }
        if (isMapAvailable) {
            if (fingers > 1) {
                return scaleGestureDetector.onTouchEvent(ev);
            } else {
                gestureDetector.onTouchEvent(ev);
            }
            return super.dispatchTouchEvent(ev);
        } else
            return false;
    }

    private void disableScrolling() {
        if (googleMap != null) {
            googleMap.setIndoorEnabled(false);
            googleMap.getUiSettings().setAllGesturesEnabled(false);
        }
    }
}