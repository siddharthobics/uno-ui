package com.obics.uno.netutils;

import android.os.Build;
import android.content.Context;


import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;

import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NetUtil {

    public static String getMessage(Object error) {
        /*if (error instanceof TimeoutError) {
            return "Connection Timeout Error";
        } else if (isServerProblem(error)) {
            return "Server Error";

        } else */
        if (isNetworkProblem(error)) {
            return "No Internet Connection!";
        }
        return "";
    }

    private static boolean isServerProblem(Object error) {
        return (error instanceof ServerError || error instanceof AuthFailureError);
    }

    private static boolean isNetworkProblem(Object error) {
        return (error instanceof NetworkError || error instanceof NoConnectionError);
    }

}
