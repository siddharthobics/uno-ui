package com.obics.uno;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
//import android.location.Address;
//import android.location.Geocoder;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.location.Location;
//import android.location.LocationManager;
import android.location.LocationManager;
import android.support.media.ExifInterface;
//import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;


import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.CardView;
import android.text.Editable;
//import android.text.InputFilter;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.DisplayMetrics;
//import android.util.Log;
//import android.util.Log;
import android.util.Log;
import android.view.DisplayCutout;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ProgressBar;
//import android.widget.RelativeLayout;
//import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
//import com.androidadvance.topsnackbar.TSnackbar;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.common.api.ApiException;
//import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.obics.uno.config.ConfigData;
import com.obics.uno.dialog.HotTagDlg;
import com.obics.uno.dialog.PickImageDialog;
import com.obics.uno.map.CenteredMapFragment;
import com.obics.uno.netutils.JsonToJsonArrayRequest;
import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.StringToJsonRequest;
import com.obics.uno.netutils.VolleySingleton;
import com.obics.uno.receive.FragRecvHistory;
import com.obics.uno.send.HotTagRspns;
import com.obics.uno.service.UnoFirebaseMessagingService;
import com.obics.uno.topsnackbar.TSnackbar;
import com.obics.uno.utils.BitmapUtils;
import com.obics.uno.utils.DatabaseMgr;
import com.obics.uno.utils.FormatUtils;
import com.obics.uno.utils.ToolDotProgress;
import com.obics.uno.utils.UnicodeLimitTextWatcher;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
//import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    public static final int RECV_MAP_RESULT = 455;
    public static final int SEND_MAP_RESULT = 485;
    public static final int UPDATE_MAP_RESULT = 493;
    public static final int MAP_LOAD_DELAY_MILLIS = 100;
    private static final int ERROR_NOT_AVAILABLE = 991;
    private static final int ERROR_MSG_ALREADY_AVAILABLE = 992;
    private static final int ERROR_TAG_ALREADY_AVAILABLE = 993;


    private static final int REQUEST_CAMERA = 90;
    private static final int REQUEST_GALLERY = 91;
    private static final int THUMBNAIL_DIMENS = 100;
    private static final int MAP_ANIM_DURATION = 400;

    private static final String MAP_DIALOG_TAG = "map_dialog";
    private static final String FRAGMENT_HOT_TAG = "fragment_hot_tag";
    private static final String TAG_AUTO_MATCHING_COUNT = "AutoMatchingCount";
    private static final String TAG_AUTO_SEARCH = "AutoSearch";
    private static final String URL_ADD_KEYWORD = ConfigData.BASE_URL + "/keyword/add";
    private static final String URL_ADD_SEND_KEYWORD = ConfigData.BASE_URL + "/keyword/addOut";
    private static final String URL_SEARCH_KEYWORD = ConfigData.BASE_URL + "/keyword/searchKeyword";

    //private static final String SEND_BLUE = "#fff09aa5";
    //private static final String SEND_BLUE_ALPHA = "#65f09aa5";
    //private static final String RECV_PINK = "#fff09aa5";
    //private static final String RECV_PINK_ALPHA = "#50f09aa5";
    private static final String PROVIDER_NAME = "com.obics.uno.fileprovider";
    private static final String URL_UPDATE_LOCATION = ConfigData.BASE_URL + "/user/updateUserLocation";
    private static final int REQUEST_CODE_PERMISSION_LOC = 23;
    private static final int REQUEST_CODE_PERMISSION_STOR = 21;
    private static final String COLOR_UPDATE_RADIUS = "#efefef";

    private static final String URL_REQUEST_MATCHCOUNT = ConfigData.BASE_URL + "/keyword/matchCount/addOut";

    private LocationRequest locationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    private SharedPreferences preferences;
    private static final double MAX_MILES = 30.0;
    private static final double TILE_SIZE_FT_AT_0_ZOOM = 513592.62;
    private static final int FT_IN_MILE = 5280;
    private static final float METERS_IN_MILE = 1609.34f;
    public static final int RECEIVE_MAP_SCREEN = 0;
    public static final int SEND_MAP_SCREEN = 1;
    public static final int UPDATE_RADIUS_SCREEN = 2;

    private CardView picCard, cardView;
    private View tagDivider, mSendSaveBtn, mFixedCircleVw;
    private GoogleMap mMap = null;
    private ProgressBar mProgress;
    private ToolDotProgress mTextProgress;
    private AutoCompleteTextView mTagText;
    private EditText mMilesTxt, mTagDescTxt;
    private double mRadiusMiles = 0, oldLatitude = 0, oldLongitude = 0;
    private TextView mHeaderTxt, mErrorTxt, mTagSendBtnTxt, countView, matchesTxt;
    private LatLng mMapCenter;
    private float mDisplayDensity, mDiameter;
    private int notchTopOffset = 0, notchBottomOffset = 0, mRootHeight = 0, heightDiff = 0;
    private AppCompatImageView mCenterPic, mUploadPic;//mSendSaveBtn mFixedCircleVw
    //private int mCircleStrokeColor = 0, mCircleFillColor = 0;
    private AppCompatImageButton mAddPicBtn, mDelPicBtn;
    private ArrayAdapter<String> mAutoCompAdapter;
    private ArrayList<String> mAutoCompList;
    private String mCurrentPhotoPath = "", mUploadImgData = "", mPrevTxt = "", mKeyword = "", mKeywordId = "";
    private boolean flagZooming = false, flagShowExtCircle = true, flagGestureZoom = false, flagDevZoom = false, flaginit = false, isKeyboardShown = false, isTyped = false;
    private FloatingActionButton mCurrLocBtn, mHotTagBtn;
    private CenteredMapFragment mapFragment;
    private final static int REQUEST_LOCATION = 199;

    private DatabaseMgr datastore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_placeholder_map);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        initPlaceholderLayout();
        setupNotifyBroadcast();
    }

    private BroadcastReceiver mRegistrationBroadcastReceiver = null;

    private void setupNotifyBroadcast() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ConfigData.NOTIFICATION_COUNT)) {
                    findViewById(R.id.new_message).setVisibility(ConstraintLayout.VISIBLE);
                } else if (intent.getAction().equals(ConfigData.LOCATION_CHANGED)) {
                    //update map location here
                    setupLocation();
                    mapFragment.setMapCenter(mMapCenter);
                    setInitialZoom();
                }
            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRegistrationBroadcastReceiver != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRegistrationBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(ConfigData.NOTIFICATION_COUNT));
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(ConfigData.LOCATION_CHANGED));
        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        try {
            notificationManager.cancel(UnoFirebaseMessagingService.uniqueid);
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
        if (mMilesTxt != null)
            mMilesTxt.setEnabled(true);

    }

    private void initPlaceholderLayout() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                window.setStatusBarColor(Color.TRANSPARENT);
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.parseColor(ConfigData.COLOR_DARK_TRANSPARENCY));
            }
        }
        cardView = findViewById(R.id.header_input);
        //initialize the dummy views
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            //cardView.setRadius(0f);
        }
        int mapScreenType = getIntent().getIntExtra(ConfigData.INTENT_MAP_SCREEN_TYPE, RECEIVE_MAP_SCREEN);
        TextView mHeaderTxt = findViewById(R.id.add_tag_header);
        TextView mDescTxt = findViewById(R.id.add_tag_desc);
        TextView tagText = findViewById(R.id.add_tag_input);
        TextView tagSendBtnTxt = findViewById(R.id.bottom_btn_txt);
        // View sendSaveBtn = findViewById(R.id.bottom_btn);//send_save_btn
        //View hottagbtn = findViewById(R.id.img_header);
        //View centImgView = findViewById(R.id.centImgView);
        //View circleVw = findViewById(R.id.circleView);
        switch (mapScreenType) {
            case RECEIVE_MAP_SCREEN:
                mHeaderTxt.setText(R.string.recv_map_heading);
                mHeaderTxt.setBackgroundResource(R.drawable.map_recv_header_bg);
                tagSendBtnTxt.setBackgroundColor(Color.parseColor("#b4b4b4"));

                tagText.setHint(R.string.add_a_tag);
                mDescTxt.setVisibility(ConstraintLayout.GONE);
                //hottagbtn.setVisibility(ConstraintLayout.GONE);
                //sendSaveBtn.setEnabled(false);
                tagSendBtnTxt.setText(R.string.recv_map_footer);
                //centImgView.setBackgroundResource(R.drawable.receive_circle);
                //circleVw.setBackgroundResource(R.drawable.receive_circle);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    cardView.setForeground(ContextCompat.getDrawable(MapActivity.this, R.drawable.map_recv_card_outline));
                //circleVw.setImageDrawable(ContextCompat.getDrawable(MapActivity.this, R.drawable.receive_circle));
                break;
            case SEND_MAP_SCREEN:
                mHeaderTxt.setText(R.string.send_map_heading);
                tagSendBtnTxt.setText(R.string.send_map_footer);
                tagSendBtnTxt.setBackgroundColor(Color.parseColor("#b4b4b4"));
                mHeaderTxt.setBackgroundResource(R.drawable.map_send_header_bg);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    cardView.setForeground(ContextCompat.getDrawable(MapActivity.this, R.drawable.map_send_card_outline));
                //sendSaveBtn.setEnabled(false);
                //tagSendBtnTxt.setText(ConfigData.INTENT_DATA_SEND);
                break;
            case UPDATE_RADIUS_SCREEN:
                mHeaderTxt.setText(R.string.update_radius_header);
                mDescTxt.setVisibility(ConstraintLayout.GONE);
                mHeaderTxt.setBackgroundResource(R.drawable.map_recv_header_bg);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    cardView.setForeground(ContextCompat.getDrawable(MapActivity.this, R.drawable.map_recv_card_outline));
                //hottagbtn.setVisibility(ConstraintLayout.GONE);
                tagSendBtnTxt.setText(R.string.update_map_footer);
                //centImgView.setBackgroundResource(R.drawable.receive_circle);
                tagText.setText(getIntent().getStringExtra(FragRecvHistory.ARG_USER_KEYWORD));
                //circleVw.setBackgroundResource(R.drawable.receive_circle);
                //circleVw.setImageDrawable(ContextCompat.getDrawable(MapActivity.this, R.drawable.receive_circle));
                tagText.setTypeface(tagText.getTypeface(), Typeface.ITALIC);
                tagText.setTextColor(Color.parseColor("#ee979899"));
                tagText.setBackgroundColor(Color.parseColor(COLOR_UPDATE_RADIUS));
                //tagText.setGravity(Gravity.CENTER);
                TextView milesTxt = findViewById(R.id.centMiles);
                milesTxt.setText(getIntent().getStringExtra(FragRecvHistory.ARG_RADIUS));
                break;
        }
    }

    private void initMapViews() {
        setContentView(R.layout.activity_map);
        datastore = new DatabaseMgr(MapActivity.this);
        mFixedCircleVw = findViewById(R.id.circleView);
        ViewTreeObserver treeObserver = mFixedCircleVw.getViewTreeObserver();
        treeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                Rect rect = new Rect();
                mFixedCircleVw.getWindowVisibleDisplayFrame(rect);
                mRootHeight = mFixedCircleVw.getRootView().getHeight();
                heightDiff = mRootHeight - (rect.bottom - rect.top);
                isKeyboardShown = heightDiff > ConfigData.KBOARD_HEIGHT;
                if (!flaginit) {
                    flaginit = true;
                    mDiameter = mFixedCircleVw.getMeasuredWidth();
                    if (mMap != null)
                        setInitialZoom();
                    setupPositionAdjustment();
                }
                //do not delete
                        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                            mFixedCircleVw.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        else
                            mFixedCircleVw.getViewTreeObserver().removeGlobalOnLayoutListener(this);*/

            }
        });
        //views
        mProgress = findViewById(R.id.progressBar);
        mTextProgress = findViewById(R.id.txt_progress);
        mAddPicBtn = findViewById(R.id.add_tag_pic);
        mDelPicBtn = findViewById(R.id.del_img_btn);
        mCenterPic = findViewById(R.id.centImgView);
        mHotTagBtn = findViewById(R.id.img_header);
        picCard = findViewById(R.id.upload_pic_card);
        tagDivider = findViewById(R.id.add_tag_space);
        cardView = findViewById(R.id.header_input);
        matchesTxt = findViewById(R.id.txt_matches);

        //if (notchTopOffset > 0) {
        adjustUiForNotchDisplay(notchTopOffset, notchBottomOffset);

        //}
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            //cardView.setRadius(0f);
            picCard.setRadius(0f);
        }

        mDisplayDensity = getResources().getDisplayMetrics().densityDpi / (DisplayMetrics.DENSITY_DEFAULT + 0.0f);
        setupLocation();
        mRadiusMiles = MAX_MILES;
        //////////
        mapFragment = (CenteredMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.setMapReadyCallback(MapActivity.this);
        mapFragment.setMapCenter(mMapCenter);

        mUploadPic = findViewById(R.id.upload_pic);
        AppCompatImageButton closeBtn = findViewById(R.id.close_btn);
        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeMapActivity();

            }
        });
        mDelPicBtn.setVisibility(View.GONE);
        picCard.setVisibility(ConstraintLayout.GONE);
        mUploadPic.setVisibility(View.GONE);
        mProgress.setVisibility(View.GONE);
        mDelPicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePic();
            }
        });
        mUploadPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MapActivity.this, PhotoViewActivity.class);
                i.putExtra(ConfigData.INTENT_EXTRA_DISP_PHOTO, Uri.fromFile(new File(mCurrentPhotoPath)).toString());
                startActivity(i);
            }
        });
        mMilesTxt = findViewById(R.id.centMiles);
        mMilesTxt.setEnabled(true);
        mTagText = findViewById(R.id.add_tag_input);
        mAutoCompList = new ArrayList<>();
        mAutoCompAdapter =
                new ArrayAdapter<>(MapActivity.this, android.R.layout.simple_list_item_1, mAutoCompList);
        mTagText.setAdapter(mAutoCompAdapter);
        mTagText.setThreshold(0);
        mTagText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mTagText.getText().toString().trim().isEmpty())
                    mTagText.showDropDown();
            }
        });
        mAddPicBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isStoragePermissionGranted())
                    showPicDialog();
            }
        });
        mCurrLocBtn = findViewById(R.id.fablocmap);
        mCurrLocBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
            }
        });
        mTagDescTxt = findViewById(R.id.add_tag_desc);
        mSendSaveBtn = findViewById(R.id.bottom_btn);//send_save_btn
        mHeaderTxt = findViewById(R.id.add_tag_header);
        countView = findViewById(R.id.wcount);
        mErrorTxt = findViewById(R.id.error_msg);
        mTagSendBtnTxt = findViewById(R.id.bottom_btn_txt);
        mMilesTxt.addTextChangedListener(mMilesTxtWatcher);
        Glide
                .with(MapActivity.this)
                .asBitmap()
                .load(Uri.parse(ConfigData.PROFILE_PIC))
                .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(mCenterPic);

        int mapScreenType = getIntent().getIntExtra(ConfigData.INTENT_MAP_SCREEN_TYPE, RECEIVE_MAP_SCREEN);
        switch (mapScreenType) {
            case RECEIVE_MAP_SCREEN:
                configureRecv();
                break;
            case SEND_MAP_SCREEN:
                configureSend();
                break;
            case UPDATE_RADIUS_SCREEN:
                configureUpdate();
                break;
        }
    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initMapViews();
            }
        }, MAP_LOAD_DELAY_MILLIS);
    }

    private class CamMoveCancelListener implements GoogleMap.OnCameraIdleListener {
        @Override
        public void onCameraIdle() {
            flagGestureZoom = false;
            flagDevZoom = false;
            mMilesTxt.setEnabled(true);
            if (!TextUtils.isEmpty(mMilesTxt.getText()))
                mMilesTxt.setSelection(mMilesTxt.getText().length());

            isTyped = false;

        }
    }

    private class CamMoveStartedListener implements GoogleMap.OnCameraMoveStartedListener {
        @Override
        public void onCameraMoveStarted(int reason) {
            if (!isTyped) {
                hideSoftKeyboard();
                mMilesTxt.setEnabled(false);
            }
            switch (reason) {
                case REASON_API_ANIMATION:
                    break;
                case REASON_DEVELOPER_ANIMATION:
                    if (flagDevZoom)
                        flagGestureZoom = true;
                    break;
                case REASON_GESTURE:
                    break;

            }
        }
    }

    private void updateCircle() {
        mMap.clear();
        CircleOptions circ = new CircleOptions()
                .center(mMapCenter)
                .radius(MAX_MILES * METERS_IN_MILE)
                .strokeWidth(FormatUtils.dpToPx(MapActivity.this, 2))
                .strokeColor(ContextCompat.getColor(MapActivity.this, R.color.settingDark))
                .fillColor(ContextCompat.getColor(MapActivity.this, R.color.mapCircleAlpha));
        mMap.addCircle(circ);
    }

    /**
     * See http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Resolution_and_Scale
     */
    private class CamMoveListener implements GoogleMap.OnCameraMoveListener {
        public void onCameraMove() {
            if (!flagGestureZoom) {
                final double resolution = TILE_SIZE_FT_AT_0_ZOOM / mDisplayDensity * Math.cos(mMapCenter.latitude * Math.PI / 180) / Math.pow(2, mMap.getCameraPosition().zoom);
                double radius = (mDiameter * resolution) / (FT_IN_MILE * 2);
                if (radius > MAX_MILES) {
                    radius = MAX_MILES;
                    if (flagShowExtCircle) {
                        updateCircle();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mFixedCircleVw.setVisibility(View.GONE);
                            }
                        }, 50);
                        flagShowExtCircle = false;
                    }
                } else {
                    mFixedCircleVw.setVisibility(View.VISIBLE);
                    mMap.clear();
                    flagShowExtCircle = true;
                }
                flagZooming = true;
                mRadiusMiles = Math.round(radius * 10.0) / 10.0;

                mMilesTxt.setText(String.format(Locale.US, "%.1f", mRadiusMiles));
               /* try {
                    mRadiusMiles = Double.parseDouble(mMilesTxt.getText().toString());
                } catch (NumberFormatException ne) {

                }*/
            }
        }
    }

    /**
     * See http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Resolution_and_Scale
     */
    private float getZoomLevel(double miles, float diameter) {
        double resolution = (miles * FT_IN_MILE * 2) / diameter;
        double scale = TILE_SIZE_FT_AT_0_ZOOM / mDisplayDensity * Math.cos(mMapCenter.latitude * Math.PI / 180) / resolution;
        return (float) (Math.log(scale) / Math.log(2));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //styleMap();
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            public void onMapLoaded() {
                findViewById(R.id.loading_txt).setVisibility(ConstraintLayout.GONE);
            }
        });
        mMap.setOnCameraMoveListener(new CamMoveListener());
        mMap.setOnCameraMoveStartedListener(new CamMoveStartedListener());
        mMap.setOnCameraIdleListener(new CamMoveCancelListener());
        if (mDiameter > 0)
            setInitialZoom();
        if (oldLatitude == 0 && oldLongitude == 0)
            getLocation();
        if (mMilesTxt != null)
            mMilesTxt.setEnabled(true);
    }

    private void setInitialZoom() {
        float zoomLevel = getZoomLevel(mRadiusMiles, mDiameter);
        if (mMap != null)
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMapCenter, zoomLevel));
    }

    public void requestUpdateMiles(String mUserKeywordId, String mKeyword) {
        //if (initialMiles == miles)
        //  return;
        mProgress.setVisibility(View.VISIBLE);
        String miles = "0.0";
        if (mMilesTxt.getText().length() > 0) {
            miles = mMilesTxt.getText().toString();
        }
        mSendSaveBtn.setEnabled(false);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"userKeywordId\": \"" + mUserKeywordId + "\", \"keyword\": \"" + mKeyword + "\", \"kilometer\": \"" + miles + "\", \"userId\": \"" + ConfigData.USER_ID + "\"}");
        } catch (JSONException e) {
        }
        ////Log.d("TUDO**", jsonBody.toString());

        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/keyword/update", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        mProgress.setVisibility(View.GONE);
                        mSendSaveBtn.setEnabled(true);

                        if (response.equalsIgnoreCase("true")) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showBigToast(R.string.map_radius_updated);
                                    Intent i = getIntent().putExtra(FragRecvHistory.ARG_RADIUS, mMilesTxt.getText().toString());
                                    setResult(UPDATE_MAP_RESULT, i);
                                    closeMapActivity();
                                }
                            });
                        } else {
                            Toast.makeText(getApplicationContext(), "  Update failed !  ", Toast.LENGTH_SHORT).show();
                        }
                        ////Log.d("TUDO", response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);
                mSendSaveBtn.setEnabled(true);

                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
                closeMapActivity();

            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private void addTagRequest() {
        mProgress.setVisibility(View.VISIBLE);
        mSendSaveBtn.setEnabled(false);
        String miles = "0.0";
        if (mMilesTxt.getText().length() > 0) {
            miles = mMilesTxt.getText().toString();
        }
        final String msg = mTagText.getText().toString().trim();
        //String msg2 = msg;
        //if (msg2.equalsIgnoreCase(ConfigData.CHANNEL_NEWS_NAME))
        //    msg2 = "Uno News";
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"keyword\": \"" + msg + "\", \"kilometer\": \"" + miles + "\", \"userId\": \"" + ConfigData.USER_ID + "\"}");
        } catch (JSONException e) {

        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST, URL_ADD_KEYWORD, jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        mProgress.setVisibility(View.GONE);
                        mSendSaveBtn.setEnabled(true);

                        // try {
                        boolean status = response.optBoolean("status");
                        //final int bdcount = response.optInt("todayKeywordCount");
                        if (status)
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showBigToast(R.string.map_tag_saved);
                                    //Intent i = getIntent();
                                    //i.putExtra(RECV_BROADCAST_COUNT, bdcount);
                                    //i.putExtra("JSONEXTRA", response.toString());
                                    datastore.insertOrUpdate(ConfigData.DB_CATEG_RECV_HOME, response.toString());

                                    setResult(RECV_MAP_RESULT);
                                    closeMapActivity();
                                }
                            });
                        else
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showErrorText(msg, ERROR_TAG_ALREADY_AVAILABLE);

                                    //Toast.makeText(getApplicationContext(), R.string.map_tag_already_exists, Toast.LENGTH_SHORT).show();

                                }
                            });
                        /*} catch (JSONException e) {

                        }*/
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);
                mSendSaveBtn.setEnabled(true);

                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
                closeMapActivity();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }


    private void addSendTagRequest() {
        mProgress.setVisibility(View.VISIBLE);
        mSendSaveBtn.setEnabled(false);
        mUploadImgData = "";
        final String msg = mTagText.getText().toString().trim();
        byte[] photoBytes = null;
        if (mCurrentPhotoPath != null)
            if (!mCurrentPhotoPath.isEmpty())
                photoBytes = BitmapUtils.getBytesFromBitmap(mCurrentPhotoPath);
        if (photoBytes != null)
            mUploadImgData = Base64.encodeToString(photoBytes, Base64.DEFAULT);
        StringToJsonRequest stringRequest = new StringToJsonRequest(Request.Method.POST, URL_ADD_SEND_KEYWORD,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        mProgress.setVisibility(View.GONE);
                        mSendSaveBtn.setEnabled(true);
                        ////Log.d("DETA@@", response.toString());
                        // try {
                        //final JSONObject jsonObject = new JSONObject(response);
                        boolean status = response.optBoolean("status");
                        final String str = response.optString("keywordList");

                        if (status)
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showBigToast(R.string.map_msg_sent);
                                    datastore.insertOrUpdate(ConfigData.DB_CATEG_SEND_HOME, str);
                                    setResult(SEND_MAP_RESULT);
                                    closeMapActivity();
                                }
                            });
                        else
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    showErrorText(msg, ERROR_MSG_ALREADY_AVAILABLE);
                                    //Toast.makeText(getApplicationContext(), R.string.map_tag_already_exists, Toast.LENGTH_SHORT).show();

                                }
                            });

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mProgress.setVisibility(View.GONE);
                        mSendSaveBtn.setEnabled(true);

                        String errString = NetUtil.getMessage(error);
                        if (!errString.isEmpty())
                            Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
                        closeMapActivity();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                String miles = "0.0";
                if (mMilesTxt.getText().length() > 0) {
                    miles = mMilesTxt.getText().toString();
                }
                Map<String, String> params = new HashMap<>();
                String bdtext = FormatUtils.getTruncatedStr(mTagDescTxt.getText().toString().trim(), 1000);
                bdtext=bdtext.replace("\\u","\\\\u");
                //String bdtext=FormatUtils.escapeJavaString(mTagDescTxt.getText().toString().trim());
                //if (bdtext.length() > 1000)
                //  bdtext = bdtext.substring(0, 1000);
                params.put("keywordData", "{\"keyword\":\"" + mTagText.getText().toString().trim() + "\",\"kilometer\":\"" + miles + "\",\"message\":\"" + bdtext + "\",\"userId\":" + ConfigData.USER_ID + "}");
                params.put("imageData", mUploadImgData);
                return params;
            }
        };
        VolleySingleton.getInstance().addToRequestQueue(stringRequest, true);
    }

    private TextWatcher mTagDescTxtWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (mTagText.getText().toString().trim().length() > 0 && mTagDescTxt.getText().toString().trim().length() > 0)
                enableSendBtn();
            else
                mSendSaveBtn.setEnabled(false);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    private TextWatcher mTagDescCountWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            int len = 0;
            if (s != null)
                len = s.length();
            countView.setText("" + (1000 - len));
        }
    };

    private void enableSendBtn() {
        if (mAutoCompList.contains(mTagText.getText().toString().trim())) {
            if (mTagDescTxt.getText().toString().trim().length() > 0)
                mSendSaveBtn.setEnabled(true);
        }

    }

    private TextWatcher mAutoCompWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence ss, int start, int count, int after) {
            mSendSaveBtn.setEnabled(false);
            enableSendBtn();
        }

        @Override
        public void onTextChanged(CharSequence ss, int start, int before, int count) {
            if (mTagText.getText().toString().trim().length() == 0) {
                mAutoCompList.clear();
                mSendSaveBtn.setEnabled(false);
                mTextProgress.setVisibility(View.GONE);
                matchesTxt.setVisibility(View.INVISIBLE);
                matchesTxt.setText("");
                VolleySingleton.getInstance().cancelAll(TAG_AUTO_SEARCH);
                mTagText.dismissDropDown();
                mAutoCompAdapter.notifyDataSetChanged();
                hideErrorText();
            } else {
                requestAutoCompleteEntries(mTagText.getText().toString().trim());
            }
        }

        @Override
        public void afterTextChanged(Editable ss) {
            mSendSaveBtn.setEnabled(false);
            enableSendBtn();
        }
    };

    private void requestAutoCompleteEntries(final String ss) {
        mTextProgress.setVisibility(View.VISIBLE);
        matchesTxt.setVisibility(View.INVISIBLE);
        matchesTxt.setText("");
        VolleySingleton.getInstance().cancelAll(TAG_AUTO_SEARCH);
        String miles = "0.0";
        if (mMilesTxt.getText().length() > 0) {
            miles = mMilesTxt.getText().toString();
        }
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"keyword\": \"" + ss + "\", \"kilometer\": \"" + miles + "\",\"userId\":" + ConfigData.USER_ID + "}");
        } catch (JSONException e) {

        }
        JsonToJsonArrayRequest req = new JsonToJsonArrayRequest(Request.Method.POST,
                URL_SEARCH_KEYWORD, jsonBody,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        ////Log.d("TAG_UNOUM", response.toString());
                        mTextProgress.setVisibility(View.GONE);
                        Type listType = new TypeToken<List<String>>() {
                        }.getType();
                        mAutoCompList = new Gson().fromJson(response.toString(), listType);
                        if (mAutoCompList.size() > 0) {
                            hideErrorText();
                            enableSendBtn();
                            requestMatchingCount(ss);
                            mAutoCompAdapter =
                                    new ArrayAdapter<>(MapActivity.this, R.layout.autocompitem, mAutoCompList);
                            mTagText.setAdapter(mAutoCompAdapter);
                            boolean valid = false;
                            for (int i = 0; i < mAutoCompList.size(); i++) {
                                if (mTagText.getText().toString().trim().equals(mAutoCompList.get(i).trim())) {
                                    valid = true;
                                    break;
                                }
                            }
                            if (!valid) {
                                mTagText.showDropDown();
                                hideErrorText();
                            }

                            /*
                            if (mAutoCompList.size() == 1) {
                                if (!mTagText.getText().toString().trim().equals(mAutoCompList.get(0).trim()))//contains
                                //if (!mAutoCompList.get(0).trim().contains(mTagText.getText().toString().trim()))//contains
                                    mTagText.showDropDown();
                            } else
                                mTagText.showDropDown();*/

                            //if (ss.length() > 0 && ss.length() <= 2)
                        } else {
                            showErrorText(ss, ERROR_NOT_AVAILABLE);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mTextProgress.setVisibility(View.GONE);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
                //else
                //  showErrorText(ss);
            }
        });
        req.setTag(TAG_AUTO_SEARCH);
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }


    private TextWatcher mTagTxtWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mSendSaveBtn.setEnabled(s.length() > 0);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    private TextWatcher mMilesTxtWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            mPrevTxt = s.toString();
            isTyped = true;
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            isTyped = true;
            if (flagZooming) {
            } else {
                if (s.length() != 0) {
                    if (FormatUtils.isMilesValid(s.toString())) {
                        try {
                            mRadiusMiles = Double.parseDouble(s.toString());
                            if (mRadiusMiles > MAX_MILES) {
                                mRadiusMiles = MAX_MILES;
                                mMilesTxt.setText("" + MAX_MILES);
                                mMilesTxt.setSelection(mMilesTxt.getText().length());
                            }
                        } catch (NumberFormatException ne) {

                        }
                    } else {
                        mMilesTxt.setText(mPrevTxt);
                        mMilesTxt.setSelection(mMilesTxt.getText().length());
                    }
                } else {
                    mRadiusMiles = 0.0;
                    mFixedCircleVw.setVisibility(View.VISIBLE);
                    if (mMap != null)
                        mMap.clear();
                    flagShowExtCircle = true;
                }
                float zoomLevel = getZoomLevel(mRadiusMiles, mDiameter);
                flagDevZoom = true;
                if (mMap != null) {
                    mMap.clear();
                    mFixedCircleVw.setVisibility(View.VISIBLE);
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mMapCenter, zoomLevel), MAP_ANIM_DURATION, null);
                }
            }
            flagZooming = false;
        }

        @Override
        public void afterTextChanged(Editable s) {
            isTyped = true;
        }
    };


    private void configureUpdate() {
        configureRecv();
        mHotTagBtn.hide();
        mMilesTxt.setText(getIntent().getStringExtra(FragRecvHistory.ARG_RADIUS));
        mKeyword = getIntent().getStringExtra(FragRecvHistory.ARG_USER_KEYWORD);
        mKeywordId = getIntent().getStringExtra(FragRecvHistory.ARG_USER_KEYWORD_ID);
        mTagSendBtnTxt.setText(R.string.update_map_footer);
        mTagText.removeTextChangedListener(mTagTxtWatcher);
        mTagText.setText(mKeyword);
        mTagText.setTypeface(mTagText.getTypeface(), Typeface.ITALIC);
        mTagText.setTextColor(Color.parseColor("#ee979899"));
        mTagText.setBackgroundColor(Color.parseColor(COLOR_UPDATE_RADIUS));
        cardView.setCardBackgroundColor(Color.parseColor(COLOR_UPDATE_RADIUS));
        mTagText.setEnabled(false);
        //mTagText.setGravity(Gravity.CENTER);
        mSendSaveBtn.setEnabled(true);
        mHeaderTxt.setText(R.string.update_radius_header);
        mSendSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mSendSaveBtn.requestFocus();
                requestUpdateMiles(mKeywordId, mKeyword);
            }
        });
        mMilesTxt.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mMilesTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    requestUpdateMiles(mKeywordId, mKeyword);
                }
                return false;
            }
        });
        mMilesTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    requestUpdateMiles(mKeywordId, mKeyword);
                }
                return false;
            }
        });

        //mMilesTxt.requestFocus();

    }

    private void configureSend() {
        mHotTagBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getHotTags(HotTagDlg.TYPE_BROADCAST_HOT);
                mHotTagBtn.setEnabled(false);
            }
        });
        mHeaderTxt.setText(R.string.send_map_heading);
        mHeaderTxt.setBackgroundResource(R.drawable.map_send_header_bg);
        mHotTagBtn.show();
        mAddPicBtn.setVisibility(View.VISIBLE);
        mTagDescTxt.setVisibility(View.VISIBLE);
        mTagDescTxt.setMaxLines(4); // Or specify a lower value if you want
        mTagDescTxt.setHorizontallyScrolling(false);
        //mSendSaveBtn.setImageDrawable(ContextCompat.getDrawable(MapActivity.this, R.drawable.ic_tag_send));
        mSendSaveBtn.setEnabled(false);
        mTagText.setHint(R.string.add_a_tag_to_receive_and_view_messages);
        mTagDescTxt.addTextChangedListener(mTagDescTxtWatcher);
        mTagDescTxt.addTextChangedListener(new UnicodeLimitTextWatcher(mTagDescTxt, 1000));
        mTagDescTxt.addTextChangedListener(mTagDescCountWatcher);
        mTagText.addTextChangedListener(mTagDescTxtWatcher);
        mTagText.addTextChangedListener(mAutoCompWatcher);
        mMilesTxt.addTextChangedListener(mAutoCompWatcher);
        mTagText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        //mTagText.removeTextChangedListener(mTagTxtWatcher);
        mSendSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSendTagRequest();
            }
        });
        mSendSaveBtn.setBackgroundResource(R.drawable.map_send_bottom_bg);
        mTagSendBtnTxt.setText(R.string.send_map_footer);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            cardView.setForeground(ContextCompat.getDrawable(MapActivity.this, R.drawable.map_send_card_outline));

    }

    private void configureRecv() {
        mHotTagBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getHotTags(HotTagDlg.TYPE_CHANNEL_HOT);
                mHotTagBtn.setEnabled(false);
            }
        });
        mHeaderTxt.setText(R.string.recv_map_heading);
        mHeaderTxt.setBackgroundResource(R.drawable.map_recv_header_bg);
        mHotTagBtn.show();
        mAddPicBtn.setVisibility(View.GONE);
        mTagDescTxt.setVisibility(View.GONE);
        tagDivider.setVisibility(View.GONE);
        countView.setVisibility(View.GONE);
        mTagText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mTagText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (mTagText.getText().length() > 0)
                        addTagRequest();
                }
                return false;
            }
        });
        mTagText.addTextChangedListener(mTagTxtWatcher);
        //mMilesTxt.removeTextChangedListener(mAutoCompWatcher);
        //mMilesTxt.setBackgroundResource(R.drawable.editxt_map_recv);
        mTagText.setHint(R.string.add_a_tag);
        //mSendSaveBtn.setImageDrawable(ContextCompat.getDrawable(MapActivity.this, R.drawable.ic_tag_save));
        //mSendSaveBtn.setVisibility(View.VISIBLE);
        mSendSaveBtn.setEnabled(false);
        mSendSaveBtn.setBackgroundResource(R.drawable.map_recv_bottom_bg);

        mSendSaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addTagRequest();
            }
        });
        mTagSendBtnTxt.setText(R.string.recv_map_footer);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            cardView.setForeground(ContextCompat.getDrawable(MapActivity.this, R.drawable.map_recv_card_outline));

    }


    private void showPicDialog() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment frag = manager.findFragmentByTag(MAP_DIALOG_TAG);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        PickImageDialog imageDialog = new PickImageDialog();
        imageDialog.setPickImgListener(new PickImageDialog.PickImgListener() {
            @Override
            public void clickCamera() {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePicture.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    if (photoFile != null) {
                        Uri photoURI;
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                            photoURI = FileProvider.getUriForFile(MapActivity.this, PROVIDER_NAME, photoFile);
                        } else {
                            photoURI = Uri.fromFile(photoFile);
                        }
                        takePicture.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        MapActivity.this.startActivityForResult(takePicture, REQUEST_CAMERA);
                    }
                }
            }

            @Override
            public void removePhoto() {
                removePic();
            }

            @Override
            public void clickGallery() {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                if (pickPhoto.resolveActivity(getPackageManager()) != null) {
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    } catch (IOException ex) {

                    }
                    if (photoFile != null) {
                        startActivityForResult(pickPhoto, REQUEST_GALLERY);//one can be replaced with any action code
                    }
                }


              /*  Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                MapActivity.this.startActivityForResult(pickPhoto, REQUEST_GALLERY);//one can be replaced with any action code*/
            }
        });
        imageDialog.show(manager, MAP_DIALOG_TAG);
    }

    private void removePic() {
        mDelPicBtn.setVisibility(View.GONE);
        picCard.setVisibility(ConstraintLayout.GONE);
        mUploadPic.setVisibility(View.GONE);
        mCurrentPhotoPath = "";
        mUploadImgData = "";
    }

    private void getHotTags(final int type) {
        mProgress.setVisibility(ConstraintLayout.VISIBLE);
        String url = "/keyword/getHotTag";
        if (type == HotTagDlg.TYPE_CHANNEL_HOT)
            url = "/keyword/channel/getHotTag";
        /*else
            url = "/keyword/getHotTag";*/
        JSONObject jsonBody = null;
        //String hotmiles=String.format(Locale.US, "%.1f", Math.round(mRadiusMiles * 10.0) / 10.0);
        try {
            jsonBody = new JSONObject("{ \"userId\":\"" + ConfigData.USER_ID + "\",\"kilometer\": \"" + mRadiusMiles + "\"}");
        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + url, jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        final HotTagRspns hotTagRspns = new Gson().fromJson(response, HotTagRspns.class);
                        if (hotTagRspns != null && hotTagRspns.getHotTags() != null)
                            if (hotTagRspns.getHotTags().size() != 0) {
                                showHotTagDlg(response, type);
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.no_hot_tags, Toast.LENGTH_SHORT).show();
                                mHotTagBtn.setEnabled(true);
                            }
                        else
                            mHotTagBtn.setEnabled(true);
                        mProgress.setVisibility(View.GONE);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);
                mHotTagBtn.setEnabled(true);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
            }
        });

        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private void showHotTagDlg(final String response, final int type) {
        if (!isFinishing()) {
            FragmentManager manager = getSupportFragmentManager();
            Fragment frag = manager.findFragmentByTag(FRAGMENT_HOT_TAG);
            if (frag != null) {
                manager.beginTransaction().remove(frag).commit();
            }
            HotTagDlg hotTagDialog = new HotTagDlg();
            Bundle args = new Bundle();
            args.putInt(HotTagDlg.ARG_HOT_TAG_TYPE, type);

            args.putString(HotTagDlg.ARG_HOT_TAG_MILES, response);
            hotTagDialog.setArguments(args);
            hotTagDialog.setHotTagDlgLstnr(new HotTagDlg.HotTagDlgLstnr() {
                @Override
                public void clickPositive(String str) {
                    mTagText.setText(str);
                    mHotTagBtn.setEnabled(true);
                }

                @Override
                public void dismiss() {
                    mHotTagBtn.setEnabled(true);
                }
            });
            hotTagDialog.show(getSupportFragmentManager(), FRAGMENT_HOT_TAG);
        }
    }

    private int getLocAccuracy() {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            if (locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY)
                locationMode = ConfigData.LOC_HIGH_ACCURACY;
            else if (locationMode == Settings.Secure.LOCATION_MODE_OFF)
                locationMode = ConfigData.LOC_TURNED_OFF;

        } else {
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (locationProviders.contains(LocationManager.GPS_PROVIDER) && locationProviders.contains(LocationManager.NETWORK_PROVIDER)) {
                locationMode = ConfigData.LOC_HIGH_ACCURACY;
            } else if (TextUtils.isEmpty(locationProviders)) {
                locationMode = ConfigData.LOC_TURNED_OFF;
            }
        }
        return locationMode;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case REQUEST_LOCATION:
                int locationState = getLocAccuracy();
                if (locationState != ConfigData.LOC_TURNED_OFF) {
                    if (locationState == ConfigData.LOC_HIGH_ACCURACY) {
                        getLocation();
                    } else {
                        if (mAccuracyCount == 0) {
                            enableLoc();
                            Toast.makeText(getApplicationContext(), R.string.loc_error1, Toast.LENGTH_SHORT).show();
                            mAccuracyCount++;
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.loc_error2, Toast.LENGTH_SHORT).show();
                            mAccuracyCount = 0;
                        }
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.loc_error3, Toast.LENGTH_SHORT).show();
                }
                break;
            case REQUEST_CAMERA:
                if (resultCode == RESULT_OK) {
                    mDelPicBtn.setVisibility(View.VISIBLE);
                    mUploadPic.setVisibility(View.VISIBLE);
                    picCard.setVisibility(ConstraintLayout.VISIBLE);
                    processCameraPic();
                }
                break;
            case REQUEST_GALLERY:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    mDelPicBtn.setVisibility(View.VISIBLE);
                    mUploadPic.setVisibility(View.VISIBLE);
                    picCard.setVisibility(ConstraintLayout.VISIBLE);
                    processGalleryPic(selectedImage);
                }
                    /*try {
                        mCurrentPhotoPath = BitmapUtils.getRealPathFromURI(MapActivity.this, selectedImage);
                        InputStream ims = getContentResolver().openInputStream(selectedImage);
                        mUploadPic.setImageBitmap(BitmapUtils.decodeSampledBitmapFromStream(ims, THUMBNAIL_DIMENS, THUMBNAIL_DIMENS));
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }*/
                break;
        }
    }


/////

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        //File image =new File(storageDir,"ChannelTempImg.jpg");
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    /*private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }*/

    private void processCameraPic() {
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        int scaleFactor = Math.min(photoW / ConfigData.JPEG_REQUIRED_DIMENSION, photoH / ConfigData.JPEG_REQUIRED_DIMENSION);
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;
        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        try {
            ExifInterface exif = new ExifInterface(mCurrentPhotoPath);
            //for some samsung devices
            int attribOrient = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            switch (attribOrient) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    bitmap = BitmapUtils.rotate(bitmap, 90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    bitmap = BitmapUtils.rotate(bitmap, 270);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    bitmap = BitmapUtils.rotate(bitmap, 180);
                    break;
                default:
                    break;
            }
            FileOutputStream out = new FileOutputStream(mCurrentPhotoPath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, ConfigData.JPEG_QUALITY, out);
            //galleryAddPic();
        } catch (OutOfMemoryError | NullPointerException | IOException e) {
            e.printStackTrace();
        }
        mUploadPic.setImageBitmap(bitmap);
    }

    /////
    private boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_PERMISSION_STOR);
                return false;
            }
        } else {
            return true;
        }
    }

    private void showErrorText(String message, int type) {
        mErrorTxt.setVisibility(View.VISIBLE);
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) mErrorTxt.getLayoutParams();
        params.height = ConstraintLayout.LayoutParams.WRAP_CONTENT;
        mErrorTxt.setLayoutParams(params);
        mTagText.dismissDropDown();
        mSendSaveBtn.setEnabled(false);
        switch (type) {
            case ERROR_NOT_AVAILABLE:
                Double errorMiles = 0.0;
                try {
                    errorMiles = Double.parseDouble(mMilesTxt.getText().toString());
                } catch (NumberFormatException ne) {
                }
                if (errorMiles >= 30.0) {
                    // mErrorTxt.setText("\"#" + message + "\" channel has not been created in your area. Please select a different channel.");
                    mErrorTxt.setText(getString(R.string.map_channel_error4, message));
                } else {
                    // mErrorTxt.setText("\"#" + message + "\" channel has not been created in your area. Try increasing your radius or select a different channel.");
                    mErrorTxt.setText(getString(R.string.map_channel_error5, message));
                }
                break;
            case ERROR_MSG_ALREADY_AVAILABLE:
                //mErrorTxt.setText("You'll have to delete the previous \"" + message + "\" broadcast before sending another one on this channel.");
                mErrorTxt.setText(getString(R.string.map_channel_error2, message));
                break;
            case ERROR_TAG_ALREADY_AVAILABLE:
                mErrorTxt.setText(R.string.map_channel_error1);

                break;
        }
    }

    private void hideErrorText() {
        mErrorTxt.setVisibility(View.INVISIBLE);
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) mErrorTxt.getLayoutParams();
        params.height = 0;
        mErrorTxt.setLayoutParams(params);
    }

    public void requestMatchingCount(final String ss) {
        VolleySingleton.getInstance().cancelAll(TAG_AUTO_MATCHING_COUNT);
        String miles = "0.0";
        if (mMilesTxt.getText().length() > 0) {
            miles = mMilesTxt.getText().toString();
        }
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"keyword\": \"" + ss + "\",\"message\": \"\", \"kilometer\": \"" + miles + "\",\"userId\":" + ConfigData.USER_ID + "}");
        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST, URL_REQUEST_MATCHCOUNT, jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            int i = Integer.parseInt(response);
                            if (i > 0) {
                                matchesTxt.setVisibility(View.VISIBLE);
                                matchesTxt.setText("" + i + getResources().getQuantityString(R.plurals.matching_users, i) + "");

                                TSnackbar snackbar = TSnackbar
                                        .make(findViewById(R.id.header_input), "", Snackbar.LENGTH_LONG);
                                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) snackbar.getView().getLayoutParams();
                                params.gravity = Gravity.CENTER_HORIZONTAL;
                                params.topMargin = mRootHeight / 4 - FormatUtils.dpToPx(MapActivity.this, 24);
                                params.width = CoordinatorLayout.LayoutParams.WRAP_CONTENT;
                                params.height = FormatUtils.dpToPx(MapActivity.this, 48);
                                View view = snackbar.getView();
                                TextView textView = view.findViewById(R.id.snackbar_text);
                                textView.setTextSize(16f);
                                textView.setTypeface(ResourcesCompat.getFont(MapActivity.this, R.font.roboto_regular));
                                textView.setTextColor(Color.BLACK);
                                textView.setGravity(Gravity.CENTER);
                                textView.setPadding(12, 0, 12, 0);
                                textView.setText(Html.fromHtml("<b>" + response + "</b>" + getResources().getQuantityString(R.plurals.matching_users, i)));
                                view.setLayoutParams(params);
                                view.setBackgroundResource(R.drawable.snackbar_outline);
                                //view.setBackgroundColor(Color.WHITE);
                                snackbar.setDuration(TSnackbar.LENGTH_SHORT);
                                snackbar.show();
                            }
                        } catch (NumberFormatException ne) {

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();

            }
        });
        req.setTag(TAG_AUTO_MATCHING_COUNT);
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private void setupLocation() {
        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String latitude = "", longitude = "";
        boolean useLocation = preferences.getBoolean(ConfigData.PREF_USE_LOCATION, true);
        if (useLocation) {
            latitude = preferences.getString(ConfigData.PREF_LATITUDE, SettingsFragment.oldLatitude + "");
            longitude = preferences.getString(ConfigData.PREF_LONGITUDE, SettingsFragment.oldLongitude + "");
        } else {
            latitude = preferences.getString(ConfigData.ZIPCODE_LATITUDE, SettingsFragment.oldLatitude + "");
            longitude = preferences.getString(ConfigData.ZIPCODE_LONGITUDE, SettingsFragment.oldLongitude + "");
        }
        try {
            oldLatitude = Double.parseDouble(latitude);
            oldLongitude = Double.parseDouble(longitude);
            mMapCenter = new LatLng(oldLatitude, oldLongitude);
        } catch (NumberFormatException ne) {
            mMapCenter = new LatLng(SettingsFragment.oldLatitude, SettingsFragment.oldLongitude);
        }
    }

    // Style map according to relevant JSON file
    /*private void styleMap() {
        mMap.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(this, R.raw.alt_light_style)
        );
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults != null)
            if (grantResults.length > 0)
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        getLocation();
                    } else if (permissions[0].equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        showPicDialog();
                    }
                } else {
                    if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION))
                        Toast.makeText(UnoApplication.getAppContext(), "Please allow location access\nto use this app", Toast.LENGTH_SHORT).show();
                }
    }

    private void updateLocation(Location location) {
        float[] distance = new float[1];
        Location.distanceBetween(oldLatitude, oldLongitude, location.getLatitude(), location.getLongitude(), distance);
        if (distance[0] > 5.0) {
            requestLocationUpdate(location);
        } else {
            Toast.makeText(getApplicationContext(), R.string.updated_new_loc, Toast.LENGTH_SHORT).show();
        }
    }


    public void requestLocationUpdate(final Location location) {
        JSONObject jsonBody = null;
        String lat = "", longt = "";
        lat = location.getLatitude() + "";
        longt = location.getLongitude() + "";

        try {

            jsonBody = new JSONObject("{\"latitude\": \"" + lat + "\", \"longtitude\": \"" + longt + "\",\"userId\": \"" + ConfigData.USER_ID + "\"}");
        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST, URL_UPDATE_LOCATION, jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        SharedPreferences.Editor editor = preferences.edit();
                        if (response.trim().equalsIgnoreCase("true")) {
                            Toast.makeText(getApplicationContext(), R.string.updated_new_loc, Toast.LENGTH_SHORT).show();
                            editor.putBoolean(ConfigData.PREF_USE_LOCATION, true);
                            editor.putString(ConfigData.PREF_ZIPCODE, "");
                            editor.putString(ConfigData.PREF_LATITUDE, location.getLatitude() + "");
                            editor.putString(ConfigData.PREF_LONGITUDE, location.getLongitude() + "");
                            oldLatitude = location.getLatitude();
                            oldLongitude = location.getLongitude();
                            mMapCenter = new LatLng(location.getLatitude(), location.getLongitude());
                            mapFragment.setMapCenter(mMapCenter);
                            Intent pushNotification = new Intent(ConfigData.LOCATION_CHANGED);
                            LocalBroadcastManager.getInstance(UnoApplication.getAppContext()).sendBroadcast(pushNotification);
                            float zoomLevel = getZoomLevel(mRadiusMiles, mDiameter);
                            if (flagShowExtCircle)
                                updateCircle();
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mMapCenter, zoomLevel), MAP_ANIM_DURATION, null);
                        } else if (response.trim().equalsIgnoreCase("false")) {
                            //usually not called
                            // editor.putBoolean(ConfigData.PREF_USE_LOCATION, false);
                        }
                        editor.apply();
                        //RESPONSE IS true or false
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    int mAccuracyCount = 0;

    private void getLocation() {
        mAccuracyCount = 0;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                enableLoc();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_PERMISSION_LOC);
            }
        } else {
            enableLoc();
        }
    }

    private void processGalleryPic(Uri selectedImage) {
        try {
            int attribOrient = BitmapUtils.getPicOrientation(MapActivity.this, selectedImage);
            InputStream ims = getContentResolver().openInputStream(selectedImage);
            Bitmap bitmap = BitmapUtils.decodeSampledBitmapFromStream(ims, ConfigData.JPEG_REQUIRED_DIMENSION, ConfigData.JPEG_REQUIRED_DIMENSION);
            switch (attribOrient) {
                case 90:
                    bitmap = BitmapUtils.rotate(bitmap, 90);
                    break;
                case 270:
                    bitmap = BitmapUtils.rotate(bitmap, 270);
                    break;
                case 180:
                    bitmap = BitmapUtils.rotate(bitmap, 180);
                    break;
                default:
                    break;
            }
            mUploadPic.setImageBitmap(bitmap);
            FileOutputStream out = new FileOutputStream(mCurrentPhotoPath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, ConfigData.JPEG_QUALITY, out);
        } catch (NullPointerException | IOException e) {
            e.printStackTrace();
        }
    }

    private void enableLoc() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(ConfigData.LOCATION_REQ_ACCURACY);
        locationRequest.setInterval(2000);
        locationRequest.setExpirationDuration(ConfigData.LOCATION_REQ_EXPIRE);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());
        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    getCurrentLocation();
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                resolvable.startResolutionForResult(MapActivity.this, REQUEST_LOCATION);
                            } catch (IntentSender.SendIntentException e) {
                            } catch (ClassCastException e) {
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            }
        });
    }


    private void getCurrentLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                updateLocation(location);
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.fetching_loc, Toast.LENGTH_LONG).show();
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), R.string.failed_loc_fetch, Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    });
            mFusedLocationClient.requestLocationUpdates(locationRequest, new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            // to improve accuracy
                            updateLocation(locationResult.getLastLocation());
                            mFusedLocationClient.removeLocationUpdates(this);
                        }
                    },
                    Looper.myLooper());
        } catch (SecurityException err) {
            err.printStackTrace();
        }
    }

    private void hideSoftKeyboard() {
        if (isKeyboardShown)
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                WeakReference<View> tview = new WeakReference<>(getCurrentFocus());
                //Log.d("MISHU*7", tview.toString() + "   ");
                if (tview.get() == null) {
                    tview = new WeakReference<>(new View(MapActivity.this));
                }
                imm.hideSoftInputFromWindow(tview.get().getWindowToken(), 0);
            } catch (Exception e) {

            }
    }

    private void closeMapActivity() {
        //HIDE KEYBOARD
        hideSoftKeyboard();
        finish();
        overridePendingTransition(R.anim.slide_from_top, R.anim.slide_to_bottom);
    }

    private void showBigToast(int resid) {
        try {
            Toast toast = Toast.makeText(getApplicationContext(), resid, Toast.LENGTH_LONG);
            ViewGroup toastLayout = (ViewGroup) toast.getView();
            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(22);
            toastTV.setTypeface(ResourcesCompat.getFont(MapActivity.this, R.font.rubikreg));
            toast.show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), resid, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_top, R.anim.slide_to_bottom);
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (hasWindowFocus)
            setupPositionAdjustment();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        setupPositionAdjustment();
    }

    private void setupPositionAdjustment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            DisplayCutout displayCutout = getWindow().getDecorView().getRootWindowInsets().getDisplayCutout();
            if (displayCutout != null) {
                notchTopOffset = displayCutout.getSafeInsetTop();
                notchBottomOffset = displayCutout.getSafeInsetBottom();
            } else {
                notchTopOffset = getWindow().getDecorView().getRootWindowInsets().getStableInsetTop();
                notchBottomOffset = 0;
            }
            adjustUiForNotchDisplay(notchTopOffset, notchBottomOffset);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            notchTopOffset = getWindow().getDecorView().getRootWindowInsets().getStableInsetTop();
            notchBottomOffset = 0;
            adjustUiForNotchDisplay(notchTopOffset, notchBottomOffset);
        } else {
            notchBottomOffset = 0;
            int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                notchTopOffset = getResources().getDimensionPixelSize(resourceId);
            } else {
                notchTopOffset = FormatUtils.dpToPx(MapActivity.this, 24);
            }
            adjustUiForNotchDisplay(notchTopOffset, notchBottomOffset);
        }
    }

    private void adjustUiForNotchDisplay(int topMargin, int bottomMargin) {
        //Log.d("UNOLOG&**", "**  " + topMargin);
        ConstraintLayout topLayout = findViewById(R.id.constraintLayout);
        CoordinatorLayout.LayoutParams coor = (CoordinatorLayout.LayoutParams) topLayout.getLayoutParams();
        coor.topMargin = (-1) * (topMargin + bottomMargin);
        topLayout.setLayoutParams(coor);

        ConstraintLayout.LayoutParams clps = (ConstraintLayout.LayoutParams) cardView.getLayoutParams();
        int marginExtra = FormatUtils.dpToPx(MapActivity.this, 6);
        clps.topMargin = (topMargin + bottomMargin) + marginExtra;
        cardView.setLayoutParams(clps);
    }


    ///END///
}
