package com.obics.uno.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.obics.uno.MainActivity;
import com.obics.uno.R;
import com.obics.uno.UnoApplication;
import com.obics.uno.config.ConfigData;
import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
import com.obics.uno.utils.DatabaseMgr;
import com.obics.uno.utils.FormatUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;

import me.leolin.shortcutbadger.ShortcutBadger;


public class UnoFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = UnoFirebaseMessagingService.class.getSimpleName();
    public static int msgCount = 0;
    public static final int uniqueid = 1130;
    public static boolean isMainActBackground = true;
    public static boolean isMapActBackground = true;
    public static boolean isPicActBackground = true;

    public static HashSet<Integer> keyhash = new HashSet<>();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage == null)
            return;
        if (remoteMessage.getNotification() != null) {
            // Log.d(TAG, "Nofi** " + remoteMessage.getNotification().getBody() + "   " + remoteMessage.getNotification().getTitle());
        }
        if (remoteMessage.getData().size() > 0) {
            //Log.d(TAG, "Nofi=!!!!!!!**" + isMainActBackground + "  *   " + isMapActBackground+ "  *   " + isPicActBackground);
            boolean isInBackground = isMainActBackground && isMapActBackground && isPicActBackground;
            //Log.d(TAG, "Nofi=###= **" + isInBackground + "  *   " + remoteMessage.getData().toString());
            showNotif(remoteMessage.getData().toString(), isInBackground);
            /*if (FormatUtils.isAppIsInBackground(getApplicationContext()))
                showNotif(remoteMessage.getData().toString(),0);
            else {
                //todo update the notif kount
            }*/
        } else {
            //Log.d(TAG, "Nofi** " + "empty");
        }
    }


    private void showNotif(String cont, boolean isBackground) {
        Log.d(TAG, "FireNofi** "+isBackground+" --* " + cont);
        try {
            String content = FormatUtils.firebaseDataToJson(cont);
            if (content != null) {
                String dataType = "";
                String message_txt = "", subText = "";
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(content);
                int msg_master_id = 0;
                if (jsonObject != null) {
                    dataType = jsonObject.optString("dataType");
                    //SETUP THE MSG TXT
                    if (dataType.equalsIgnoreCase("SC") || dataType.equalsIgnoreCase("RC")) {
                        String body = jsonObject.optString("body");
                        String title = jsonObject.optString("title");
                        String chatuser = jsonObject.optString("chat_user_name");
                        String msgMasterId = jsonObject.optString("message_master_id");
                        try {
                            msg_master_id = Integer.parseInt(msgMasterId);
                        } catch (NumberFormatException e) {

                        }
                        message_txt = chatuser + " : " + body;
                        if (dataType.equalsIgnoreCase("SC"))
                            subText = "Channel- " + title;
                        else if (dataType.equalsIgnoreCase("RC"))
                            subText = "Broadcast- " + title;
                    } else if (dataType.equalsIgnoreCase("RH")) {
                        String userKeywordId = jsonObject.optString("userKeywordId");
                        try {
                            msg_master_id = Integer.parseInt(userKeywordId);
                        } catch (NumberFormatException e) {

                        }
                        String body = jsonObject.optString("keywordName");
                        String miles = jsonObject.optString("radius");
                        message_txt = "New Broadcast - " + body;
                        subText = "in " + miles + " miles ";
                    }
                    //SETUP NOTIFY TYPE
                    String message = "";
                    if (dataType.equalsIgnoreCase("SC") || dataType.equalsIgnoreCase("RH")) {
                        message = ConfigData.INTENT_DATA_RECV;
                    } else if (dataType.equalsIgnoreCase("RC")) {
                        message = ConfigData.INTENT_DATA_SEND;
                    }
                    int numbers = 0;
                    switch (message) {
                        case ConfigData.INTENT_DATA_RECV:
                            numbers = incrNotificationCount(ConfigData.TYPE_RECV);
                            break;
                        case ConfigData.INTENT_DATA_SEND:
                            if (!keyhash.contains(msg_master_id))
                                numbers = incrNotificationCount(ConfigData.TYPE_SEND);
                            break;
                    }

                    ShortcutBadger.applyCount(UnoApplication.getAppContext(), numbers);
                    if (isBackground) {
                        msgCount++;
                        if (msgCount == 1) {
                            Intent intent = new Intent(this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
                            intent.putExtra(ConfigData.NOTIFY_EXTRA_STR, content);
                            PendingIntent pendingIntent = PendingIntent.getActivity(this, uniqueid, intent,
                                    PendingIntent.FLAG_ONE_SHOT);
                            sendNotification("You have received a new message", pendingIntent, subText, numbers);
                            //sendNotification(message_txt, pendingIntent, subText);
                        } else {
                            Intent intent = new Intent(this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            //intent.putExtra(ConfigData.NOTIFY_EXTRA_STR, "22");
                            PendingIntent pendingIntent = PendingIntent.getActivity(this, uniqueid, intent,
                                    PendingIntent.FLAG_ONE_SHOT);
                            sendNotification("You have received new messages", pendingIntent, null, numbers);
                            //sendNotification("You have received " + msgCount + " messages", pendingIntent, null);
                        }
                    } else {
                        // Log.d("Nofi9", message);
                        if (!message.isEmpty()) {
                            Intent pushNotification = new Intent(ConfigData.NOTIFICATION_COUNT);
                            pushNotification.putExtra(ConfigData.NOTIFICATION_COUNT_TYPE, message);
                            /*if (!isMapActBackground || !isPicActBackground) {
                                String pushContent = "";
                                if (dataType.equalsIgnoreCase("RH"))
                                    pushContent = message_txt + " : " + subText;
                                else
                                    pushContent = subText + " : " + message_txt;
                                pushNotification.putExtra(ConfigData.NOTIFICATION_CONTENT_STR, pushContent);

                            }*/
                            pushNotification.putExtra(ConfigData.NOTIFICATION_MSG_MASTER, msg_master_id);
                            pushNotification.putExtra(ConfigData.NOTIFICATION_COUNT_VAL, 1);
                            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
                        }
                    }

                    /*if (dataType.equalsIgnoreCase("SC") || dataType.equalsIgnoreCase("RH")) {
                        //message = ;
                    } else if (dataType.equalsIgnoreCase("RC")) {
                        message = "Send";
                        incrNotificationCount(ConfigData.TYPE_SEND);
                    }*/
                }
            }
        } catch (JSONException | NumberFormatException | NullPointerException je) {
        }
    }

    private void sendNotification(String messageBody, PendingIntent pendingIntent, String subMessage, int numbers) {
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, ConfigData.CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_knob_round))
                        .setContentTitle("Knob")
                        .setTicker("Knob- " + messageBody)
                        .setContentText(messageBody)
                        .setAutoCancel(true)
                        .setNumber(numbers)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);
        //if (subMessage != null)
        //    notificationBuilder.setSubText(subMessage);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(uniqueid, notificationBuilder.build());
    }

    @Override
    public void onNewToken(String refreshedToken) {
        super.onNewToken(refreshedToken);
        Log.e("FireNEW_TOKEN", refreshedToken);
        storeRegIdInPref(refreshedToken);
        FirebaseMessaging.getInstance().subscribeToTopic("global");
    }

    private int incrNotificationCount(int type) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(UnoFirebaseMessagingService.this);
        SharedPreferences.Editor editor = preferences.edit();
        int recvCount = 0, sendCount = 0;
        recvCount = preferences.getInt(ConfigData.PREF_RECV_NOTIF_COUNT, 0);
        sendCount = preferences.getInt(ConfigData.PREF_SEND_NOTIF_COUNT, 0);
        switch (type) {
            case ConfigData.TYPE_RECV:
                recvCount++;
                editor.putInt(ConfigData.PREF_RECV_NOTIF_COUNT, recvCount);
                break;
            case ConfigData.TYPE_SEND:
                sendCount++;
                editor.putInt(ConfigData.PREF_SEND_NOTIF_COUNT, sendCount);
                break;
        }
        editor.apply();
        return recvCount + sendCount;
    }

    private void storeRegIdInPref(String token) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ConfigData.PREF_FCM_TOKEN, token);
        editor.apply();
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID) {
            requestFcmTokenUpdate(token);
        }
        /*SharedPreferences pref = getApplicationContext().getSharedPreferences(ConfigData.FCMTOKEN, 0);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("regId", token);
        editor.commit();*/
    }

    public void requestFcmTokenUpdate(final String token) {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);
            jsonBody.put("userAppId", token);
        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/notify/updateappid", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.trim().equalsIgnoreCase("true")) {
                            try {
                                //Log.d("Firebase**mmm**", "uploaded  " + token);
                                saveTokenUploaded(true);
                            } catch (Exception ex) {
                                saveTokenUploaded(false);
                            }
                        } else if (response.trim().equalsIgnoreCase("false")) {
                            //todo save the data
                            saveTokenUploaded(false);
                            //Log.d("Firebase**mmm**", "not******uploaded");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                saveTokenUploaded(false);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private void saveTokenUploaded(boolean saved) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(UnoFirebaseMessagingService.this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, saved);
        editor.apply();
    }

}
