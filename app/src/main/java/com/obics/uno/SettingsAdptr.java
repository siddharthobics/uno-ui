package com.obics.uno;

import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.obics.uno.receive.KeywordList;

import java.util.ArrayList;

public class SettingsAdptr extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<KeywordList> newsItemList;
    ReceiveHomeListener mListener;
    boolean showDelete = false;

    interface ReceiveHomeListener {
        void itemClicked(String itemname, int userKeywordId, String userRadius);

        void showDeleteIconState(boolean delete);

        void deleteItem(int id, String keyword);
    }

    public void toggleDelete() {
        showDelete = !showDelete;
    }

    public void setShowDelete(boolean showDelete) {
        this.showDelete = showDelete;
    }

    public class RecvHomeHolder extends RecyclerView.ViewHolder {
        public TextView username, uniq_username;
        AppCompatButton unblokBtn;

        public RecvHomeHolder(View view) {
            super(view);
            username = view.findViewById(R.id.user_name);
            uniq_username = view.findViewById(R.id.uniq_username);

            unblokBtn = view.findViewById(R.id.unblok_btn);
            unblokBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    //todo unblock here
                }
            });
        }
    }


    public SettingsAdptr() {
        //this.mContext = mContext;
        //this.newsItemList = newsItemList;
        //this.mListener = mListener;
    }

    public void setNewData(ArrayList<KeywordList> newsItemList) {
        this.newsItemList = newsItemList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.block_usr_card, parent, false);
        return new RecvHomeHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final RecvHomeHolder catItemHolder = (RecvHomeHolder) holder;
        catItemHolder.username.setText("Blocked User " + (position + 1));
        catItemHolder.uniq_username.setText("uniquename" + (position));

        /*final KeywordList newsItem = newsItemList.get(position);
        String keyname = newsItem.getKeyword().getKeywordName();
        if (keyname != null)
            catItemHolder.itemname = newsItem.getKeyword().getKeywordName();

        catItemHolder.username.setText(catItemHolder.itemname);
        catItemHolder.userKeywordId = newsItem.getUserKeywordId();
        catItemHolder.userRadius = newsItem.getKiloMeter() + "";
        String miles = String.format(Locale.US, "%.1f", newsItem.getKiloMeter() + 0.0f);
        catItemHolder.userradius.setText(miles);
        int count = newsItem.getMatchingMessageCount();
        if (count > 0) {
            catItemHolder.msgcount.setVisibility(View.VISIBLE);
            catItemHolder.msgcount.setText("" + count);
        } else {
            catItemHolder.msgcount.setVisibility(View.GONE);
        }
        if (showDelete) {
            catItemHolder.unblokBtn.setChecked(false);
            catItemHolder.unblokBtn.setVisibility(View.VISIBLE);
            mListener.showDeleteIconState(true);

        } else {
            catItemHolder.unblokBtn.setVisibility(View.INVISIBLE);
            mListener.showDeleteIconState(false);
        }
        catItemHolder.unblokBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //todo add ids on delete pressed
                    mListener.deleteItem(newsItem.getUserKeywordId(), catItemHolder.itemname);
                }
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return 20;
    }
///////////END OF CATEGORY ADAPTER/////////////////
}
