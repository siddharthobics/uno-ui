package com.obics.uno;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.chrisbanes.photoview.PhotoView;
import com.obics.uno.config.ConfigData;
import com.obics.uno.service.UnoFirebaseMessagingService;

public class PhotoViewActivity extends AppCompatActivity {
    private PhotoView mPhotoView;
    private FloatingActionButton mCloseBtn;
    private String mPicUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pic);
        mPicUrl = getIntent().getStringExtra(ConfigData.INTENT_EXTRA_DISP_PHOTO);
        mPhotoView = findViewById(R.id.photo_view);
        mCloseBtn = findViewById(R.id.close_btn);
        mCloseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mPhotoView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        mPhotoView.setZoomable(true);
        setupNotifyBroadcast();
        Glide.with(this)
                .asBitmap()
                .load(Uri.parse(mPicUrl))
                .apply(RequestOptions.fitCenterTransform().placeholder(R.drawable.ic_img_plchold).error(R.drawable.ic_img_error))
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        mPhotoView.setImageDrawable(ContextCompat.getDrawable(PhotoViewActivity.this, R.drawable.ic_img_error));
                        finish();
                    }

                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                        mPhotoView.setImageBitmap(resource);
                    }
                });
    }


    private BroadcastReceiver mRegistrationBroadcastReceiver = null;

    private void setupNotifyBroadcast() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ConfigData.NOTIFICATION_COUNT)) {
                    findViewById(R.id.new_message).setVisibility(ConstraintLayout.VISIBLE);
                }
            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRegistrationBroadcastReceiver != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mRegistrationBroadcastReceiver != null)
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(ConfigData.NOTIFICATION_COUNT));
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        try {
            notificationManager.cancel(UnoFirebaseMessagingService.uniqueid);
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
    }

}
