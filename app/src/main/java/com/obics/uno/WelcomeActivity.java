package com.obics.uno;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.text.Html;
import android.view.DisplayCutout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.obics.uno.config.ConfigData;
import com.obics.uno.utils.FormatUtils;
import com.obics.uno.utils.IntroPageTransformer;

import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;

public class WelcomeActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private LinearLayout dotsLayout;
    private static final int MAX_PAGES = 5;
    private int dotsSidePadding, dotsTopPadding;
    private AppEventsLogger logger;
    private FirebaseAnalytics mFirebaseAnalytics;
    private boolean introPageEvtArr[] = new boolean[MAX_PAGES];
    private AppCompatImageButton leftBtn, rightBtn;
    private boolean isAboveKitKat = false;
    public static final int OPTION_LOGIN = 1;
    public static final int OPTION_SIGNUP = 2;
    public static final String REQUEST_INTRO_PAGE = "intro_over";
    private boolean isDeviceSupported = false;
    public static final int TEXT_BTN_TOP_PADDING_DEFAULT = 15;
    public static final int DOTS_SIDE_PADDING = 4;

    private boolean checkCpuArchSupported(String arch) {
        boolean isDevSupported = false;
        switch (arch) {
            case "x86":
            case "x86_64":
            case "armeabi-v7a":
            case "arm64-v8a":
            case "armeabi":
                isDevSupported = true;
                break;
            default:
                break;
        }
        return isDevSupported;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(WelcomeActivity.this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(ConfigData.PREF_FIRST_TIME, false);
        editor.apply();
        // Making notification bar transparent
        //todo get device type for armeabi
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            isDeviceSupported = checkCpuArchSupported(Build.SUPPORTED_ABIS[0]);
        } else {
            isDeviceSupported = checkCpuArchSupported(Build.CPU_ABI);
        }
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        logger = AppEventsLogger.newLogger(getApplicationContext());
        isAboveKitKat = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT);
        setContentView(R.layout.activity_welcome);
        dotsSidePadding = FormatUtils.dpToPx(WelcomeActivity.this, DOTS_SIDE_PADDING);
        dotsTopPadding = FormatUtils.dpToPx(WelcomeActivity.this, TEXT_BTN_TOP_PADDING_DEFAULT);
        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.layoutDots);
        AppCompatButton btnSignup = findViewById(R.id.int_sgnup_btn);
        AppCompatButton btnLogin = findViewById(R.id.int_login_btn);
       /* getWindow().getDecorView().getRootView().addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {

            }

            @Override
            public void onViewDetachedFromWindow(View v) {
            }
        });*/
        leftBtn = findViewById(R.id.btn_left);
        rightBtn = findViewById(R.id.btn_right);
        // adding bottom dots
        addBottomDots(0);
        // making notification bar transparent
        changeStatusBarColor();
        viewPager.setAdapter(new IntroPagerAdapter());
        viewPager.setOffscreenPageLimit(MAX_PAGES);
        viewPager.setPageTransformer(false, new IntroPageTransformer());
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        leftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);

                //viewPager.arrowScroll(ViewPager.FOCUS_LEFT);
            }
        });
        rightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                //viewPager.scr
                //viewPager.arrowScroll(ViewPager.FOCUS_RIGHT);
            }
        });
        toggleArrowVisibility(true, false);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = getIntent().putExtra(REQUEST_INTRO_PAGE, OPTION_LOGIN);
                setResult(RESULT_OK, i);
                finish();
            }
        });
        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = getIntent().putExtra(REQUEST_INTRO_PAGE, OPTION_SIGNUP);
                setResult(RESULT_OK, i);
                finish();
            }
        });
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            DisplayCutout displayCutout = getWindow().getDecorView().getRootWindowInsets().getDisplayCutout();
            if (displayCutout != null) {
                adjustUiForNotchDisplay(displayCutout.getSafeInsetTop(), displayCutout.getSafeInsetBottom());
            }
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                DisplayCutout displayCutout = getWindow().getDecorView().getRootWindowInsets().getDisplayCutout();
                if (displayCutout != null) {
                    adjustUiForNotchDisplay(displayCutout.getSafeInsetTop(), displayCutout.getSafeInsetBottom());
                }
            }
    }

    private void addBottomDots(int currentPage) {
        TextView[] dots = new TextView[MAX_PAGES];
        dotsLayout.removeAllViews();
        for (int i = 0; i < MAX_PAGES; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(24);
            dots[i].setTypeface(ResourcesCompat.getFont(WelcomeActivity.this, R.font.roboto_regular));
            dots[i].setBackgroundColor(Color.TRANSPARENT);
            dots[i].setTextColor(Color.parseColor("#FFECEFF0"));
            dots[i].setPadding(dotsSidePadding, dotsTopPadding, dotsSidePadding, 0);
            dotsLayout.addView(dots[i]);
        }
        dots[currentPage].setTextColor(Color.parseColor("#ffb1b4bb"));
    }

    public void toggleArrowVisibility(boolean isAtZeroIndex, boolean isAtLastIndex) {
        if (isAtZeroIndex) {
            leftBtn.setVisibility(View.INVISIBLE);
        } else {
            leftBtn.setVisibility(View.VISIBLE);
        }
        if (isAtLastIndex)
            rightBtn.setVisibility(View.INVISIBLE);
        else
            rightBtn.setVisibility(View.VISIBLE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void sendIntroEvent(int position) {
        if (!introPageEvtArr[position]) {
            String pageno = "page " + position;
            Bundle bundle = new Bundle();
            bundle.putString(FirebaseAnalytics.Param.SUCCESS, "true");
            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, pageno);
            bundle.putString(FirebaseAnalytics.Param.CREATIVE_NAME, "Page Number");
            String tutEvent = FirebaseAnalytics.Event.TUTORIAL_BEGIN;
            if (position == MAX_PAGES - 1)
                tutEvent = FirebaseAnalytics.Event.TUTORIAL_COMPLETE;
            mFirebaseAnalytics.logEvent(tutEvent, bundle);
            Bundle params = new Bundle();
            params.putString(tutEvent, pageno);
            logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_TUTORIAL, params);
            introPageEvtArr[position] = true;
        }
    }


    private ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
            sendIntroEvent(position);
            toggleArrowVisibility(position == 0, position == MAX_PAGES - 1);
            if (position != 0) {
                View view = viewPager.getChildAt(position);
                if (view != null) {
                    if (isDeviceSupported) {
                        try {
                            GifImageView gifImageView = view.findViewById(R.id.computer);
                            if (gifImageView != null) {
                                GifDrawable gifDrawable = (GifDrawable) gifImageView.getDrawable();
                                if (gifDrawable != null) {
                                    if (gifDrawable.isPlaying()) {
                                        gifDrawable.reset();
                                    } else {
                                        gifDrawable.stop();
                                        gifDrawable.start();
                                        gifDrawable.reset();
                                    }
                                }
                            }
                        } catch (Exception e) {

                        }
                    } else {
                        WebView webView = view.findViewById(R.id.computer);
                        if (webView != null) {
                            //webView.freeMemory();
                            webView.clearCache(false);
                            webView.reload();
                        }
                    }
                }
            }
        }

        @Override
        public void onPageScrolled(int position, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * View pager adapter
     */
    public class IntroPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        IntroPagerAdapter() {
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = null;
            if (position == 0 && layoutInflater != null) {
                view = layoutInflater.inflate(R.layout.intro_fragment_layout_2, container, false);
                view.setTag(position);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPager.setCurrentItem(1, true);
                        //viewPager.arrowScroll(ViewPager.FOCUS_RIGHT);
                    }
                });

            /*} else if (position == 3 && layoutInflater != null) {
                view = layoutInflater.inflate(R.layout.intro_fragment_layout_3, container, false);
                TextView textView = view.findViewById(R.id.mid2_text);
                TextViewCompat.setAutoSizeTextTypeWithDefaults(textView, TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
                view.setTag(position);
                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewPager.arrowScroll(ViewPager.FOCUS_RIGHT);
                    }
                });*/
            } else if (layoutInflater != null) {
                if (isDeviceSupported) {
                    view = layoutInflater.inflate(R.layout.intro_fragment_layout_1, container, false);
                    GifImageView gifImageView = view.findViewById(R.id.computer);
                    TextView textView = view.findViewById(R.id.title);
                    view.setTag(position);
                    switch (position) {
                        case 1:
                            gifImageView.setImageResource(R.drawable.asset_2);
                            textView.setText(R.string.intro_1_title);
                            break;
                        case 2:
                            gifImageView.setImageResource(R.drawable.asset_3);
                            textView.setText(R.string.intro_2_title);
                            break;
                        case 3:
                            gifImageView.setImageResource(R.drawable.asset_4);
                            textView.setText(R.string.intro_3_title);
                            break;
                        case 4:
                            gifImageView.setImageResource(R.drawable.asset_5);
                            textView.setText(R.string.intro_4_title);
                            break;
                        default:
                            break;
                    }
                } else {
                    view = layoutInflater.inflate(R.layout.intro_fragment_layout_web, container, false);
                    WebView webView = view.findViewById(R.id.computer);
                    TextView textView = view.findViewById(R.id.title);
                    //webView.freeMemory();
                    webView.clearCache(false);
                    view.setTag(position);
                    switch (position) {
                        case 1:
                            webView.loadUrl("file:///android_res/drawable/asset_2.gif");
                            textView.setText(R.string.intro_1_title);
                            break;
                        case 2:
                            webView.loadUrl("file:///android_res/drawable/asset_3.gif");
                            textView.setText(R.string.intro_2_title);
                            break;
                        case 3:
                            webView.loadUrl("file:///android_res/drawable/asset_4.gif");
                            textView.setText(R.string.intro_3_title);
                            break;
                        case 4:
                            webView.loadUrl("file:///android_res/drawable/asset_5.gif");
                            textView.setText(R.string.intro_4_title);
                            break;
                        default:
                            break;
                    }
                    webView.setBackgroundColor(Color.TRANSPARENT);
                    webView.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
                    if (!isAboveKitKat) {
                        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
                    } else {
                        webView.getSettings().setLoadWithOverviewMode(true);
                        webView.getSettings().setUseWideViewPort(true);
                    }
                }
            }
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return MAX_PAGES;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
           /* View view = (View) object;
            if (view != null) {
                if (isDeviceSupported) {
                    GifImageView gifImageView = view.findViewById(R.id.computer);
                    if (gifImageView != null) {
                        GifDrawable gifDrawable = (GifDrawable) gifImageView.getDrawable();
                        if (gifDrawable != null) {
                            gifDrawable.recycle();
                        }
                    }
                } else {
                    WebView webView = view.findViewById(R.id.computer);
                    if (webView != null) {
                        //webView.freeMemory();
                        webView.clearCache(false);
                    }
                }
            }
            container.removeView(view);*/
        }
    }

    private void adjustUiForNotchDisplay(int topMargin, int bottomMargin) {
        ConstraintLayout.LayoutParams viewPagerLayoutParams = (ConstraintLayout.LayoutParams) viewPager.getLayoutParams();
        viewPagerLayoutParams.topMargin = (topMargin + bottomMargin);
        viewPagerLayoutParams.bottomMargin = (topMargin + bottomMargin);
        viewPager.setLayoutParams(viewPagerLayoutParams);
    }
}
