package com.obics.uno.config;

import com.google.android.gms.location.LocationRequest;

public class ConfigData {

    public static final int INVALID_USER_ID = -1;
    public static final int TYPE_RECV = 1;
    public static final int TYPE_SEND = 2;
    public static int DB_CATEG_RECV_HOME = 42;
    public static int DB_CATEG_SEND_HOME = 43;
    public static final String COLOR_DARK_TRANSPARENCY = "#77666666";
    public static final String ADMIN_KEYWORD = "Admin";

    public static int LARGE_VOLLEY_REQ_TIMEOUT = 60000;
    public static int MEDIUM_VOLLEY_REQ_TIMEOUT = 30000;

    public static int JPEG_QUALITY = 60;
    public static int JPEG_REQUIRED_DIMENSION = 500;
    public static int LOCATION_REQ_EXPIRE = 60000;
    public static int LOCATION_REQ_ACCURACY = LocationRequest.PRIORITY_HIGH_ACCURACY;
    //

    //HARDCODED KEYBOARD HEIGHT FOR MOST DEVICES??
    public static final int KBOARD_HEIGHT = 220;
    public static final int MAX_BROADCASTS = 10;
    public static final int MAX_CHANNELS = 10;

    //public static final int UNO_NEWS_TAG_ID = 965;
    public static final int UNO_NEWS_MSG_MASTER_ID = -1543;

    /*NOT WORKING

    //NOT WORKING*/

    //new production
    public static final String BASE_URL = "http://ec2-54-225-29-84.compute-1.amazonaws.com:8080/Business-core";

    //new qa
    //public static final String BASE_URL = "http://ec2-18-214-249-144.compute-1.amazonaws.com:8080/Business-core";

    //dev
    //public static final String BASE_URL = "http://52.205.211.251:8080/Business-core";


    public static final String CHANNEL_NEWS_NAME = "Knob News";

    public static final String INTENT_DATA_RECV = "Recv";
    public static final String INTENT_DATA_SEND = "Send";
    public static final String REDIRECT_DATA_RECV = "RecvRedirect";
    public static final String REDIRECT_DATA_SEND = "SendRedirect";
    public static final String INTENT_MAP_SCREEN_TYPE = "MapScreenType";
    public static final String NOTIFY_EXTRA_STR = "extrajson";
    public static final String PREF_IS_LOGGED = "is_logged_in";
    public static final String PREF_USER_ID = "user_id";
    public static final String PREF_PROFILE_PIC = "usr_prof_pic";
    public static final String PREF_LATITUDE = "usr_latitude";
    public static final String PREF_LONGITUDE = "usr_longitude";
    public static final String LOGIN_LATITUDE = "login_latitude";
    public static final String LOGIN_LONGITUDE = "login_longitude";
    public static final String ZIPCODE_LATITUDE = "zip_latitude";
    public static final String ZIPCODE_LONGITUDE = "zip_longitude";
    public static final String PREF_FCM_TOKEN = "pref_fcm_token";
    public static final String PREF_FIRST_TIME = "pref_first_launch";
    public static final String PREF_PHONE_NUMBER = "phone_number";
    public static final String NEWS_FIRST_TIME = "news_first_launch";

    public static final String PREF_USERNAME = "usr_name";
    public static final String PREF_DISPLAY_NAME = "usr_display_name";
    public static final String PREF_ABOUT_ME = "usr_about_me";
    public static final String PREF_WHERE_FROM = "usr_where_from";

    public static final String PREF_ZIPCODE = "usr_zipcode";
    public static final String PREF_USE_LOCATION = "usr_use_location";
    public static final String PREF_FCM_TOKEN_UPLOADED = "fcm_token_uploaded";

    public static final String PREF_RECV_NOTIF_COUNT = "pref_recv_count";
    public static final String PREF_SEND_NOTIF_COUNT = "pref_send_count";

    public static final String COMPARE_JPG = ".jpg";

    public static final String UNO_FILE_PROVIDER = "com.obics.uno.fileprovider";
    public static final String CHANNEL_ID = "unofire1234";

    public static int REFRESH_TIME = 15000;//15 sec refresh for other pages
    public static int REFRESH_TIME_CHAT = 12000;//12 second refresh for chat

    public static final int REDIRECT_HOME = -99;

    public static int USER_ID = INVALID_USER_ID;
    public static String PROFILE_PIC = "";
    public static String PROFILE_USERNAME = "";

    public static final String INTENT_EXTRA_LOC_ZIP = "ziplocation";

    public static final String INTENT_EXTRA_DISP_PHOTO = "photourl";
    //public static final String INTERNET_RECONNECTED = "internetReconnected";
    //public static final String INTERNET_DISCONNECTED = "internetDisconnected";

    public static final String NOTIFICATION_MSG_MASTER = "notifymessagemaster";
    public static final String LOCATION_CHANGED = "locChanged";

    public static final String NOTIFICATION_COUNT = "notifyKount";
    public static final String NOTIFICATION_COUNT_TYPE = "notifyKountType";
    public static final String NOTIFICATION_COUNT_VAL = "notifyKountValue";
    //public static final String NOTIFICATION_CONTENT_STR = "notifyKontent";

    public static final int LOC_HIGH_ACCURACY = 33;
    public static final int LOC_TURNED_OFF = 34;

    public static final String INTENT_ABOUT_ME = "aboutme";
    public static final String INTENT_WHERE_FROM = "wherefrom";


}
