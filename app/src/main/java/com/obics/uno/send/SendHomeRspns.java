package com.obics.uno.send;

import java.util.ArrayList;

public class SendHomeRspns {
    public class MessageMaster {
        private String chatDisplayName;

        public String getDisplayName() {
            return this.chatDisplayName;
        }

        public void setDisplayName(String displayName) {
            this.chatDisplayName = displayName;
        }

        private int chatUserId;

        public int getChatUserId() {
            return this.chatUserId;
        }

        public void setChatUserId(int chatUserId) {
            this.chatUserId = chatUserId;
        }

        private String chatUserName;

        public String getChatUserName() {
            return this.chatUserName;
        }

        public void setChatUserName(String chatUserName) {
            this.chatUserName = chatUserName;
        }

        private String chatWhereFrom;

        public String getChatWhereFrom() {
            return this.chatWhereFrom;
        }

        public void setChatWhereFrom(String chatWhereFrom) {
            this.chatWhereFrom = chatWhereFrom;
        }

        private String chatAboutMe;

        public String getChatAboutMe() {
            return this.chatAboutMe;
        }

        public void setChatAboutMe(String chatAboutMe) {
            this.chatAboutMe = chatAboutMe;
        }

        private String chatProfileImg;

        public String getChatProfileImg() {
            return this.chatProfileImg;
        }

        public void setChatProfileImg(String chatProfileImg) {
            this.chatProfileImg = chatProfileImg;
        }

        private String matchingMessageCount;

        public String getMatchingMessageCount() {
            return this.matchingMessageCount;
        }

        public void setMatchingMessageCount(String matchingMessageCount) {
            this.matchingMessageCount = matchingMessageCount;
        }

        private int chatMessageMasterId;

        public int getChatMessageMasterId() {
            return this.chatMessageMasterId;
        }

        public void setChatMessageMasterId(int chatMessageMasterId) {
            this.chatMessageMasterId = chatMessageMasterId;
        }

        private String chatInsertDate;

        public String getChatInsertDate() {
            return this.chatInsertDate;
        }

        public void setChatInsertDate(String chatInsertDate) {
            this.chatInsertDate = chatInsertDate;
        }
    }

    public class Keyword {
        private int keywordId;

        public int getKeywordId() {
            return this.keywordId;
        }

        public void setKeywordId(int keywordId) {
            this.keywordId = keywordId;
        }

        private String keywordName;

        public String getKeywordName() {
            return this.keywordName;
        }

        public void setKeywordName(String keywordName) {
            this.keywordName = keywordName;
        }

        private String createdBy;

        public String getCreatedBy() {
            return this.createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }
    }

    public class User {
        private String UserName;

        public String getUserName() {
            return this.UserName;
        }

        public void setUserName(String UserName) {
            this.UserName = UserName;
        }

        private int UserId;

        public int getUserId() {
            return this.UserId;
        }

        public void setUserId(int UserId) {
            this.UserId = UserId;
        }

        private String profileImg;

        public String getProfileImg() {
            return this.profileImg;
        }

        public void setProfileImg(String profileImg) {
            this.profileImg = profileImg;
        }
    }

    public class UserOutKeyword {
        private ArrayList<MessageMaster> messageMaster;

        public ArrayList<MessageMaster> getMessageMaster() {
            return this.messageMaster;
        }

        public void setMessageMaster(ArrayList<MessageMaster> messageMaster) {
            this.messageMaster = messageMaster;
        }

        private String fileName;

        public String getFileName() {
            return this.fileName;
        }

        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        private double kiloMeter;

        public double getKiloMeter() {
            return this.kiloMeter;
        }

        public void setKiloMeter(double kiloMeter) {
            this.kiloMeter = kiloMeter;
        }

        private String keywordDesc;

        public String getKeywordDesc() {
            return this.keywordDesc;
        }

        public void setKeywordDesc(String keywordDesc) {
            this.keywordDesc = keywordDesc;
        }

        private int userOutKeywordId;

        public int getUserOutKeywordId() {
            return this.userOutKeywordId;
        }

        public void setUserOutKeywordId(int userOutKeywordId) {
            this.userOutKeywordId = userOutKeywordId;
        }

        private ArrayList<Keyword> keyword;

        public ArrayList<Keyword> getKeyword() {
            return this.keyword;
        }

        public void setKeyword(ArrayList<Keyword> keyword) {
            this.keyword = keyword;
        }

        private ArrayList<User> user;

        public ArrayList<User> getUser() {
            return this.user;
        }

        public void setUser(ArrayList<User> user) {
            this.user = user;
        }

        private String insertedTime;

        public String getInsertedTime() {
            return this.insertedTime;
        }

        public void setInsertedTime(String insertedTime) {
            this.insertedTime = insertedTime;
        }
    }

    public class OutList {
        private ArrayList<UserOutKeyword> userOutKeyword;

        public ArrayList<UserOutKeyword> getUserOutKeyword() {
            return this.userOutKeyword;
        }

        public void setUserOutKeyword(ArrayList<UserOutKeyword> userOutKeyword) {
            this.userOutKeyword = userOutKeyword;
        }
    }

    private String serverCurrentTime;

    public String getServerCurrentTime() {
        return this.serverCurrentTime;
    }

    public void setServerCurrentTime(String serverCurrentTime) {
        this.serverCurrentTime = serverCurrentTime;
    }

    private String currentServerTime;

    public String getCurrentServerTime() {
        return this.currentServerTime;
    }

    public void setCurrentServerTime(String currentServerTime) {
        this.currentServerTime = currentServerTime;
    }

    private int notificationCount;

    public int getNotificationCount() {
        return this.notificationCount;
    }

    public void setNotificationCount(int totalNotification) {
        this.notificationCount = totalNotification;
    }

    private int todayKeywordCount;

    public int getTodayKeywordCount() {
        return this.todayKeywordCount;
    }

    public void setTodayKeywordCount(int todayKeywordCount) {
        this.todayKeywordCount = todayKeywordCount;
    }

    private String countUpdatedTime;

    public String getCountUpdatedTime() {
        return this.countUpdatedTime;
    }

    public void setCountUpdatedTime(String countUpdatedTime) {
        this.countUpdatedTime = countUpdatedTime;
    }


    private String userAppId;

    public String getUserAppId() {
        return this.userAppId;
    }

    public void setUserAppId(String userAppId) {
        this.userAppId = userAppId;
    }

    private ArrayList<OutList> outList;

    public ArrayList<OutList> getOutList() {
        return this.outList;
    }

    public void setOutList(ArrayList<OutList> outList) {
        this.outList = outList;
    }
}
