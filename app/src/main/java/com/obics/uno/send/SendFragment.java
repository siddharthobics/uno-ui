package com.obics.uno.send;

//import android.app.Activity;

import android.content.Context;
//import android.content.SharedPreferences;
import android.os.Bundle;
//import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
//import android.widget.Toast;

import com.obics.uno.R;
import com.obics.uno.config.ConfigData;
import com.obics.uno.interfaces.PagerFrgmntLstnr;
import com.obics.uno.receive.FragRecvHome;
//import com.obics.uno.utils.DatabaseMgr;

import org.json.JSONException;
import org.json.JSONObject;

public class SendFragment extends Fragment {
    /**
     * The send main container fragment
     */
    PagerFrgmntLstnr mListener;
    //boolean mVisible = false;
    private static final String SEND_FRAGMENT_HOME_TAG = "fragment_send_home_tag";
    private static final String SEND_FRAGMENT_HIST_TAG = "fragment_send_hist_tag";
    private static final String SEND_FRAGMENT_CHAT_TAG = "fragment_send_chat_tag";

    private static final String LAUNCH_FROM_NAVIGATION = "launchNavi";
    private static final String USER_KEYWORD_ID = "userKeywordId";
    private static final String USER_KEYWORD = "userKeyword";
    private static final String MSG_MASTER_ID = "msgMasterId";
    private static final String HISTORY_ID = "historyId";
    private static final String HISTORY_KEY = "historyKey";
    private static final String CHAT_WITH_USER = "chatwith";
    private static final String CHAT_USER_ID = "chat_user_id";
    private static final String CHAT_PROFILE_PIC = "chat_profile_pic";

    private String redirData = "";

    public SendFragment() {

    }

    public void setupRedirectData(String str) {
        redirData = str;
        if (isAdded())
            init();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (PagerFrgmntLstnr) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement PageFrgmntListener");
        }
        //this.mListener = context;

    }

    private void setupRedirect(String title) {
        if (!title.isEmpty()) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(title);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String datatype = jsonObject.optString("dataType");
            String message_master_id = jsonObject.optString("message_master_id");
            String userKeywordId = jsonObject.optString("userKeywordId");
            String chat_user_id = jsonObject.optString("chat_user_id");
            String keywordName = jsonObject.optString("keywordName");
            String chat_uniq_user = jsonObject.optString("chat_user_name");
            //String chat_user_name = "";
            String chat_user_name = jsonObject.optString("displayName");
            String aboutMe = jsonObject.optString("aboutMe");
            String whereFrom = jsonObject.optString("whereFrom");
            if (datatype.contains("RC")) {
                showSendChatFragment(Integer.parseInt(message_master_id), chat_user_name, ConfigData.REDIRECT_HOME, keywordName, Integer.parseInt(chat_user_id), false, "", aboutMe, whereFrom, chat_uniq_user);
            }
        }
    }


    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        try {
            getChildFragmentManager().findFragmentById(R.id.constraintLayout).setMenuVisibility(menuVisible);
        } catch (IllegalStateException | NullPointerException e) {

        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        try {
            getChildFragmentManager().findFragmentById(R.id.constraintLayout).setUserVisibleHint(isVisibleToUser);
        } catch (IllegalStateException | NullPointerException e) {
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_main_recv, container, false);
        init();
        return rootView;
    }

    private void init() {
        if (redirData.isEmpty())
            showSendHomeFragment(false);
        else if (redirData.equals(ConfigData.REDIRECT_DATA_SEND)) {
            FragSendHome fragSendChat = (FragSendHome) getChildFragmentManager().findFragmentByTag(SEND_FRAGMENT_HOME_TAG);
            if (fragSendChat == null) {
                showSendHomeFragment(false);
            }
            //FragSendChat fragSendChat = (FragSendChat) getChildFragmentManager().findFragmentByTag(SEND_FRAGMENT_CHAT_TAG);
            //if (fragSendChat != null) {
            // showSendHomeFragment(false);
            //}

        } else
            setupRedirect(redirData);
    }


    private void showSendChatFragment(int id, String str, int histId, String histKey, int chatUserId, boolean launchNavi, String chatpic, String aboutme, String wherefrom, String uniqusr) {
        FragSendChat fragSendChat = new FragSendChat();
        Bundle args = new Bundle();
        args.putInt(MSG_MASTER_ID, id);
        args.putInt(HISTORY_ID, histId);
        args.putString(HISTORY_KEY, histKey);
        args.putString(CHAT_WITH_USER, str);
        args.putString("chatwithuniqusr", uniqusr);

        args.putInt(CHAT_USER_ID, chatUserId);
        args.putBoolean(LAUNCH_FROM_NAVIGATION, launchNavi);
        args.putString(CHAT_PROFILE_PIC, chatpic);
        args.putString(ConfigData.INTENT_ABOUT_ME, aboutme);
        args.putString(ConfigData.INTENT_WHERE_FROM, wherefrom);

        fragSendChat.setArguments(args);
        fragSendChat.setSendFragListener(new FragSendChat.FragSendChatListener() {

            @Override
            public void showSendHomePage() {
                showSendHomeFragment(true);
            }

            @Override
            public void showHistPage(int id, String str) {
                //int count=getChildFragmentManager().getBackStackEntryCount()
                //getChildFragmentManager().popBackStackImmediate();
                showSendHistFragment(str, id);
            }

        });
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.//addToBackStack(SEND_FRAGMENT_HIST_TAG).
                replace(R.id.constraintLayout, fragSendChat, SEND_FRAGMENT_CHAT_TAG).commit();
    }


    private void showSendHistFragment(String keyword, int userKeywordId) {
        FragSendHistory fragSendHistory = new FragSendHistory();
        Bundle args = new Bundle();
        args.putInt(USER_KEYWORD_ID, userKeywordId);
        args.putString(USER_KEYWORD, keyword);
        fragSendHistory.setArguments(args);
        fragSendHistory.setSendFragListener(new FragSendHistory.FragSendHistListener() {

            @Override
            public void showChatPage(int id, String str, int histId, String histkey, int chatUserId, String chatpic, String aboutme, String wherefrom, String uniqusr) {
                showSendChatFragment(id, str, histId, histkey, chatUserId, true, chatpic, aboutme, wherefrom, uniqusr);
            }

            @Override
            public void showSendHome() {
                showSendHomeFragment(true);
            }

        });
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.constraintLayout, fragSendHistory, SEND_FRAGMENT_HIST_TAG).commit();
    }

    private void showSendHomeFragment(boolean launchFromNavigation) {
        FragSendHome fragSendHome = new FragSendHome();
        Bundle args = new Bundle();
        args.putBoolean(LAUNCH_FROM_NAVIGATION, launchFromNavigation);
        fragSendHome.setArguments(args);
        fragSendHome.setSendFragmentListener(new FragSendHome.FragSendHomeListener() {
            @Override
            public void showSendChatPage(int id, String str, int histid, String histkey, int chatUserId, String chatpic, String aboutme, String wherefrom, String uniqusr) {
                showSendChatFragment(id, str, histid, histkey, chatUserId, true, chatpic, aboutme, wherefrom, uniqusr);
            }

            @Override
            public void showSendHistPage(String str, int userKeywordId) {
                showSendHistFragment(str, userKeywordId);
            }
        });
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.constraintLayout, fragSendHome, SEND_FRAGMENT_HOME_TAG).commit();
        // if (!launchFromNavigation)
        //   fragSendHome.setUserVisibleHint(false);

    }

    ////END////
    public void updateSendHome(boolean showProgress, int message_masterid) {
        try {
            if (isAdded()) {
                if (showProgress)
                    setupRedirectData("");
                FragSendHome fragSendHome = (FragSendHome) getChildFragmentManager().findFragmentByTag(SEND_FRAGMENT_HOME_TAG);
                if (fragSendHome != null) {
                    fragSendHome.requestSendHomeList(true, false);
                }
                FragSendHistory fragSendHistory = (FragSendHistory) getChildFragmentManager().findFragmentByTag(SEND_FRAGMENT_HIST_TAG);
                if (fragSendHistory != null) {
                    fragSendHistory.refreshPage(false);
                }
                FragSendChat fragSendChat = (FragSendChat) getChildFragmentManager().findFragmentByTag(SEND_FRAGMENT_CHAT_TAG);
                if (fragSendChat != null) {
                    fragSendChat.refreshChat(message_masterid);
                }
            }
        } catch (IllegalStateException | NullPointerException e) {

        }
    }


}