package com.obics.uno.send;

import java.util.ArrayList;

public class SendHistRspns {
    public class MatchingUser {
        private int keywordId;

        public int getKeywordId() {
            return this.keywordId;
        }

        public void setKeywordId(int keywordId) {
            this.keywordId = keywordId;
        }

        private String displayName;

        public String getDisplayName() {
            return this.displayName;
        }

        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        private String keywordName;

        public String getKeywordName() {
            return this.keywordName;
        }

        public void setKeywordName(String keywordName) {
            this.keywordName = keywordName;
        }

        private int messageMasterId;

        public int getMessageMasterId() {
            return this.messageMasterId;
        }

        public void setMessageMasterId(int messageMasterId) {
            this.messageMasterId = messageMasterId;
        }

        private long insertDate;

        public long getInsertDate() {
            return this.insertDate;
        }

        public void setInsertDate(long insertDate) {
            this.insertDate = insertDate;
        }

        private String messageDescription;

        public String getMessageDescription() {
            return this.messageDescription;
        }

        public void setMessageDescription(String messageDescription) {
            this.messageDescription = messageDescription;
        }

        private String matchingMessageCount;

        public String getMatchingMessageCount() {
            return this.matchingMessageCount;
        }

        public void setMatchingMessageCount(String matchingMessageCount) {
            this.matchingMessageCount = matchingMessageCount;
        }

        private String userName;

        public String getUserName() {
            return this.userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        private String profileImg;

        public String getProfileImg() {
            return this.profileImg;
        }

        public void setProfileImg(String profileImg) {
            this.profileImg = profileImg;
        }

        private int userId;

        public int getUserId() {
            return this.userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        private boolean conversationStatus;

        public boolean getConversationStatus() {
            return this.conversationStatus;
        }

        public void setConversationStatus(boolean conversationStatus) {
            this.conversationStatus = conversationStatus;
        }

        private int keywordOutId;

        public int getKeywordOutId() {
            return this.keywordOutId;
        }

        public void setKeywordOutId(int keywordOutId) {
            this.keywordOutId = keywordOutId;
        }

        private String whereFrom;

        public String getWhereFrom() {
            return this.whereFrom;
        }

        public void setWhereFrom(String whereFrom) {
            this.whereFrom = whereFrom;
        }

        private String aboutMe;

        public String getAboutMe() {
            return this.aboutMe;
        }

        public void setAboutMe(String aboutMe) {
            this.aboutMe = aboutMe;
        }
    }


    private ArrayList<MatchingUser> matchingUsers;

    public ArrayList<MatchingUser> getMatchingUsers() {
        return this.matchingUsers;
    }

    public void setMatchingUsers(ArrayList<MatchingUser> matchingUsers) {
        this.matchingUsers = matchingUsers;
    }

    private String userAppId;

    public String getUserAppId() {
        return this.userAppId;
    }

    public void setUserAppId(String userAppId) {
        this.userAppId = userAppId;
    }

    private boolean status;

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}