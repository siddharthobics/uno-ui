package com.obics.uno.send;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;

import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
//import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

//import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
//import com.obics.uno.MainActivity;
import com.obics.uno.R;
import com.obics.uno.UnoApplication;
import com.obics.uno.config.ConfigData;
import com.obics.uno.dialog.ConfirmDeleteDialog;
import com.obics.uno.interfaces.PagerFrgmntLstnr;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
//import com.obics.uno.utils.DatabaseMgr;
import com.obics.uno.utils.ToolDotProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragSendHistory extends Fragment {
    /**
     * The send history fragment
     */
    private PagerFrgmntLstnr mMainListener;

    private RecyclerView mRecyclerView;
    private FragSendHistListener mListener;
    private ToolDotProgress mProgress;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SendHistAdapter mSendHistAdapter;
    private SendHistRspns mSendHistRspns = new SendHistRspns();
    private TextView mHeadingText, mErrorText;
    private int mUserKeywordId = -1;
    private String mKeyword = "";// mChatPic = "", mChatDesc = "";
    private static final String FRAGMENT_DIALOG_TAG = "fragment_send_hist";
    private boolean isVisible = true;
    private static final String USER_KEYWORD_ID = "userKeywordId";
    private static final String USER_KEYWORD = "userKeyword";
    private AppCompatImageButton mTopLeftBtn, mTopRightBtn;
    boolean fcmUploaded = false;
    private String fcmKey = "";
    private Handler handler;
    private Runnable runnable = new Runnable() {
        public void run() {
            requestSendHistList(mUserKeywordId, false, false);
            handler.postDelayed(this, ConfigData.REFRESH_TIME);
        }
    };

    @Override
    public void onStop() {
        handler.removeCallbacks(runnable);
        super.onStop();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //Log.d("UNOLOGK5", context + "  **  ");
        try {
            mMainListener = (PagerFrgmntLstnr) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement PageFrgmntListener");
        }
        //this.mListener = context;
    }

    interface FragSendHistListener {
        void showChatPage(int id, String str, int histid, String histkey, int chatUserId, String chatpic,String aboutme,String wherefrom, String uniqusr);

        void showSendHome();

        /*void updateReadCount(int readcount);

        void setPlaceholders();

        void logOut();*/

    }

    public FragSendHistory() {

    }

    private void setupExternalButtons() {
        mHeadingText.setText("#" + mKeyword);
        mTopLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null)
                    mListener.showSendHome();
            }
        });
        mTopRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleHomeDelete();
            }
        });
    }

    public void setSendFragListener(FragSendHistListener mListener) {
        this.mListener = mListener;
        if (mMainListener != null)
            mMainListener.setPlaceholderButtons(R.drawable.ic_left_arr, R.drawable.ic_del1);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (mSendHistAdapter != null)
            resetDelete();
        if (isVisibleToUser) {
            showTopButtons(true);
            if (!isVisible)
                refreshPage(false);
        } else {
            showTopButtons(false);
        }
        isVisible = isVisibleToUser;

    }

    private void showTopButtons(boolean show) {
        int visible = (show ? ConstraintLayout.VISIBLE : ConstraintLayout.GONE);
        if (mTopLeftBtn != null && mTopRightBtn != null) {
            mTopLeftBtn.setVisibility(visible);
            mTopRightBtn.setVisibility(visible);
            if (show)
                mTopRightBtn.setImageResource(R.drawable.ic_del1);
        }
        if (mRecyclerView != null) {
            if (show)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            if (mRecyclerView != null)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
            if (mMainListener != null)
                mMainListener.setPlaceholderButtons(R.drawable.ic_left_arr, R.drawable.ic_del1);
        } else {
            if (mRecyclerView != null)
                mRecyclerView.setBackgroundColor(Color.parseColor(ConfigData.COLOR_DARK_TRANSPARENCY));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.frag_send_hist, container, false);
        mUserKeywordId = getArguments().getInt(USER_KEYWORD_ID);
        mKeyword = getArguments().getString(USER_KEYWORD);
        mHeadingText = rootView.findViewById(R.id.topbar_toptext);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        mProgress = rootView.findViewById(R.id.progressBar);
        mProgress.setVisibility(View.GONE);
        mErrorText = rootView.findViewById(R.id.back_text);
        mErrorText.setVisibility(View.GONE);
        swipeRefreshLayout = rootView.findViewById(R.id.simpleSwipeRefreshLayout);
        mTopLeftBtn = rootView.findViewById(R.id.topbarleftbtn);
        mTopRightBtn = rootView.findViewById(R.id.topbarrightbtn);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        setupExternalButtons();
        if (isAdded()) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(UnoApplication.getAppContext());
            fcmUploaded = preferences.getBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, false);
            if (fcmUploaded)
                fcmKey = preferences.getString(ConfigData.PREF_FCM_TOKEN, "");
        }
        mSendHistRspns.setMatchingUsers(new ArrayList<SendHistRspns.MatchingUser>());
        mSendHistAdapter = new SendHistAdapter(getContext(), mSendHistRspns, new SendHistAdapter.SendHistAdptLstnr() {
            @Override
            public void itemClicked(int id, String itemname, int chatUserId, String chatpic,String aboutme,String wherefrom,String uniqusr) {
                if (mListener != null)
                    mListener.showChatPage(id, itemname, mUserKeywordId, mKeyword, chatUserId, chatpic, aboutme, wherefrom, uniqusr);
            }

            @Override
            public void showDeleteIconState(boolean delete) {
                if (getContext() != null && isVisible && mListener != null)
                    if (delete)
                        mTopRightBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_del2));
                    else
                        mTopRightBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_del1));
            }

            @Override
            public void deleteItem(int id, String keyword, int matchCount) {
                showConfirmDelete(id, keyword, matchCount);

            }


        });
        mRecyclerView.setAdapter(mSendHistAdapter);
        //requestSendHistList(mUserKeywordId, true);
        swipeRefreshLayout.setNestedScrollingEnabled(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestSendHistList(mUserKeywordId, false, true);
            }
        });
        handler = new Handler();
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        handler.postDelayed(runnable, ConfigData.REFRESH_TIME);
    }

    ////////
    public void toggleHomeDelete() {
        mSendHistAdapter.toggleDelete();
        mSendHistAdapter.notifyDataSetChanged();
    }

    private void showToast(int resid) {
        if (isAdded())
            Toast.makeText(UnoApplication.getAppContext(), resid, Toast.LENGTH_SHORT).show();

    }

    private void showConfirmDelete(final int id, final String keyword, final int matchCount) {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_DIALOG_TAG);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        ConfirmDeleteDialog deleteDialog = new ConfirmDeleteDialog();
        deleteDialog.setmConfrmDelLstnr(new ConfirmDeleteDialog.ConfirmDelDlgListener() {
            @Override
            public void clickPositive() {
                requestDeleteItem(id, keyword, matchCount);
            }

            @Override
            public void clickNegative() {
                resetDelete();
            }
        });
        Bundle args = new Bundle();
        args.putInt(ConfirmDeleteDialog.CONF_DEL_DLG_TYPE, ConfirmDeleteDialog.REPLY_TYPE);
        deleteDialog.setArguments(args);
        deleteDialog.show(manager, FRAGMENT_DIALOG_TAG);
    }

    private void resetDelete() {
        //mRecyclerView.scrollToPosition(0);
        if (mSendHistAdapter.getShowDelete()) {
            mSendHistAdapter.setShowDelete(false);
            mSendHistAdapter.notifyDataSetChanged();
        }
    }

    private void checkDuplicateLogin(String appid) {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID)
            if (fcmUploaded && appid != null)
                if (!fcmKey.isEmpty())
                    if (!appid.trim().equals(fcmKey.trim())) {
                        mRecyclerView.removeAllViews();
                        if (mMainListener != null)
                            mMainListener.logOut();
                    }
    }

    public void requestDeleteItem(int id, String keyword, final int matchcount) {
        mProgress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"messageMasterId\": \"" + keyword + "\", \"userKeywordId\": \"" + id + "\", \"userOutKeywordId\": \"" + id + "\", \"userId\": " + ConfigData.USER_ID + "}");
        } catch (JSONException e) {

        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/message/deleteConversation", jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        boolean status = response.optBoolean("status");
                        if (status) {
                            //Log.d("KISHUP", response.toString());
                            requestSendHistList(mUserKeywordId, true, true);
                            if (mMainListener != null)
                                mMainListener.decrSendCount(matchcount);
                            mProgress.setVisibility(View.GONE);
                        } else {
                            showToast(R.string.delete_failed);
                        }
                        swipeRefreshLayout.setRefreshing(false);
                        mProgress.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    showToast(R.string.no_internet_conn);
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    public void requestSendHistList(int userKeywordId, final boolean showProgress, final boolean resetDelete) {
        if (showProgress)
            mProgress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"userKeywordId\": \"" + userKeywordId + "\",\"userId\":" + ConfigData.USER_ID + "}");
        } catch (JSONException e) {

        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/message/getMatchingOutUser", jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mSendHistRspns = new Gson().fromJson(response.toString(), SendHistRspns.class);
                        checkDuplicateLogin(mSendHistRspns.getUserAppId());
                        if (resetDelete)
                            mSendHistAdapter.setShowDelete(false);
                        mSendHistAdapter.setNewData(mSendHistRspns);
                        mSendHistAdapter.notifyDataSetChanged();
                        mErrorText.setVisibility(View.GONE);

                        if (!mSendHistRspns.getStatus()) {
                            if (getContext() != null && isVisible && mListener != null)
                                mTopRightBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_del1));
                            mErrorText.setVisibility(View.VISIBLE);
                            if (isAdded())
                                mErrorText.setText(R.string.send_hist_error_1);
                        }
                        mProgress.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    showToast(R.string.no_internet_conn);
            }
        });

        VolleySingleton.getInstance().addToRequestQueue(req, true);
    }

    public void refreshPage(boolean resetDelete) {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID)
            if (mUserKeywordId != -1)
                requestSendHistList(mUserKeywordId, true, resetDelete);
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshPage(true);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (mListener != null)
                            mListener.showSendHome();
                        return true;
                    }
                }
                return false;
            }
        });


    }
}