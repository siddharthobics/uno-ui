package com.obics.uno.send;

import java.util.ArrayList;

public class HotTagRspns {

    public class HotTag {
        private int users;

        public int getUsers() {
            return this.users;
        }

        public void setUsers(int users) {
            this.users = users;
        }

        private String tag;

        public String getTag() {
            return this.tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }
    }

    private ArrayList<HotTag> hotTags;

    public ArrayList<HotTag> getHotTags() {
        return this.hotTags;
    }

    public void setHotTags(ArrayList<HotTag> hotTags) {
        this.hotTags = hotTags;
    }
}

