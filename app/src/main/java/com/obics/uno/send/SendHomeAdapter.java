package com.obics.uno.send;

import android.content.Context;
//import android.content.Intent;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
//import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.DownsampleStrategy;
import com.bumptech.glide.request.RequestOptions;
//import com.obics.uno.PhotoViewActivity;
import com.obics.uno.R;
import com.obics.uno.config.ConfigData;
import com.obics.uno.service.UnoFirebaseMessagingService;
import com.obics.uno.utils.FormatUtils;

import org.apache.commons.lang.StringEscapeUtils;

import java.util.ArrayList;
import java.util.Locale;

public class SendHomeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private SendHomeRspns mSendHomeItmLst;
    private SendHomeAdptLstnr mSendHomeLstnr;
    private boolean flagShowDelete = false;
    private Context mContext;
    int msgcount = 0;

    interface SendHomeAdptLstnr {
        void showMsgCount(int count);

        void itemChatClicked(int msgMstrId, String chatUsername, int histid, String histkey, int chatUserId, String chatpic, String aboutme, String wherefrom, String uniqusr);

        void itemClicked(String itemname, int userKeywordId);

        void showDeleteIconState(boolean delete);

        void deleteItem(int id, String keyword);
    }

    public void setNewData(SendHomeRspns newsItemList) {
        this.mSendHomeItmLst = newsItemList;
        msgcount = 0;

    }

    public void toggleDelete() {
        if (getItemCount() > 0) {
            flagShowDelete = !flagShowDelete;
            if (mSendHomeLstnr != null)
                mSendHomeLstnr.showDeleteIconState(flagShowDelete);
        }
    }

    public void setShowDelete(boolean showDelete) {
        if (getItemCount() > 0) {
            this.flagShowDelete = showDelete;
            if (mSendHomeLstnr != null)
                mSendHomeLstnr.showDeleteIconState(flagShowDelete);
        }
    }

    public boolean getShowDelete() {
        return flagShowDelete;
    }

    public class SendHomeHolder extends RecyclerView.ViewHolder {
        AppCompatImageView main_img;
        TextView heading_txt, desc_txt, time_txt, datetime_txt;
        TextView[] inlay = new TextView[5];
        AppCompatImageView[] img = new AppCompatImageView[5];
        int[] chatMsgMstrId = new int[5];
        int[] chatUserId = new int[5];
        String[] chatprofilepic = new String[5];
        String[] chatUserName = new String[5];
        String[] chatDispName = new String[5];
        String[] chatAboutMe = new String[5];
        String[] chatWhereFrom = new String[5];

        CheckBox radioButton;
        String itemname, itemfilename;
        int itemoutid;

        public SendHomeHolder(View view) {
            super(view);
            heading_txt = view.findViewById(R.id.primary_text);
            desc_txt = view.findViewById(R.id.sub_text);
            main_img = view.findViewById(R.id.media_image);
            radioButton = view.findViewById(R.id.delbtn);
            time_txt = view.findViewById(R.id.time_text);
            datetime_txt = view.findViewById(R.id.datetime_text);


            img[0] = view.findViewById(R.id.imgView);
            img[1] = view.findViewById(R.id.imgView2);
            img[2] = view.findViewById(R.id.imgView3);
            img[3] = view.findViewById(R.id.imgView4);
            img[4] = view.findViewById(R.id.imgView5);

            inlay[0] = view.findViewById(R.id.txt_inlay1);
            inlay[1] = view.findViewById(R.id.txt_inlay2);
            inlay[2] = view.findViewById(R.id.txt_inlay3);
            inlay[3] = view.findViewById(R.id.txt_inlay4);
            inlay[4] = view.findViewById(R.id.txt_inlay5);

            view.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    if (mSendHomeLstnr != null)
                        mSendHomeLstnr.itemClicked(itemname, itemoutid);
                }
            });

            main_img.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    /*if (itemfilename != null && (itemfilename.contains(ConfigData.COMPARE_JPG.toUpperCase()) || itemfilename.contains(ConfigData.COMPARE_JPG.toLowerCase()))) {
                        Intent i = new Intent(mContext, PhotoViewActivity.class);
                        i.putExtra(ConfigData.INTENT_EXTRA_DISP_PHOTO, itemfilename);
                        mContext.startActivity(i);
                    } else {*/
                    if (mSendHomeLstnr != null)
                        mSendHomeLstnr.itemClicked(itemname, itemoutid);
                    //}
                }
            });
            for (int in = 0; in < 4; in++) {
                final int i = in;
                img[i].setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (mSendHomeLstnr != null)
                            mSendHomeLstnr.itemChatClicked(chatMsgMstrId[i], chatDispName[i], ConfigData.REDIRECT_HOME, itemname, chatUserId[i], chatprofilepic[i], chatAboutMe[i], chatWhereFrom[i], chatUserName[i]);
                    }
                });
            }
        }
    }

    public SendHomeAdapter(Context con, SendHomeRspns newsItemList, SendHomeAdptLstnr mListener) {
        this.mContext = con;
        this.mSendHomeItmLst = newsItemList;
        this.mSendHomeLstnr = mListener;
        msgcount = 0;
//
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.send_home_card, parent, false);
        return new SendHomeHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final SendHomeHolder holder1 = (SendHomeHolder) holder;
        holder1.itemfilename = mSendHomeItmLst.getOutList().get(position).getUserOutKeyword().get(0).getFileName();
        String str = mSendHomeItmLst.getOutList().get(position).getUserOutKeyword().get(0).getKeyword().get(0).getCreatedBy();
        if (str != null) {
            holder1.time_txt.setVisibility(ConstraintLayout.GONE);
            holder1.desc_txt.setText(Html.fromHtml(mSendHomeItmLst.getOutList().get(position).getUserOutKeyword().get(0).getKeywordDesc()));
            holder1.desc_txt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSendHomeLstnr != null)
                        mSendHomeLstnr.itemClicked(holder1.itemname, holder1.itemoutid);
                }
            });
        } else {
            //String desc = (mSendHomeItmLst.getOutList().get(position).getUserOutKeyword().get(0).getKeywordDesc());

            String desc = StringEscapeUtils.unescapeJava(mSendHomeItmLst.getOutList().get(position).getUserOutKeyword().get(0).getKeywordDesc());
            holder1.desc_txt.setText(desc);
        }
        String time = mSendHomeItmLst.getOutList().get(position).getUserOutKeyword().get(0).getInsertedTime();
        double radius = mSendHomeItmLst.getOutList().get(position).getUserOutKeyword().get(0).getKiloMeter();
        holder1.datetime_txt.setText(FormatUtils.formatSendTime(time));
        //FormatUtils.formatSendTime(time) + "-" +
        holder1.time_txt.setText(String.format(Locale.US, "%.1f", Math.round(radius * 100.0) / 100.0) + " mi.");
        holder1.itemname = mSendHomeItmLst.getOutList().get(position).getUserOutKeyword().get(0).getKeyword().get(0).getKeywordName();
        //sendHomeHolder.chatUserId = mSendHomeItmLst.getOutList().get(position).getUserOutKeyword().get(0).getKeyword().get(0).;
        holder1.itemoutid = mSendHomeItmLst.getOutList().get(position).getUserOutKeyword().get(0).getUserOutKeywordId();
        //String userName = "";
        //if (sendHomeHolder.itemname != null)
        //    userName = FormatUtils.truncateToLength(sendHomeHolder.itemname, 15);
        holder1.heading_txt.setText(holder1.itemname);
        //holder1.desc_txt.setText(holder1.itemdesc);
        if (holder1.itemfilename != null)
            Glide
                    .with(mContext)
                    .load(Uri.parse(holder1.itemfilename))

                    .apply(RequestOptions.centerCropTransform().placeholder(R.drawable.ic_img_plchold).dontAnimate().diskCacheStrategy(DiskCacheStrategy.AUTOMATIC).downsample(DownsampleStrategy.AT_LEAST))

                    .into(holder1.main_img);
        ///messagemaster
        ArrayList<SendHomeRspns.MessageMaster> msgmasterarr = mSendHomeItmLst.getOutList().get(position).getUserOutKeyword().get(0).getMessageMaster();
        for (int i = 0; i < 5; i++) {
            holder1.inlay[i].setVisibility(View.INVISIBLE);
            holder1.img[i].setVisibility(View.INVISIBLE);
        }
        for (int i = 0; i < 4; i++) {
            if (i >= msgmasterarr.size())
                break;
            holder1.chatMsgMstrId[i] = msgmasterarr.get(i).getChatMessageMasterId();
            holder1.chatDispName[i] = msgmasterarr.get(i).getDisplayName();
            holder1.chatAboutMe[i] = msgmasterarr.get(i).getChatAboutMe();
            holder1.chatUserName[i] = msgmasterarr.get(i).getChatUserName();
            holder1.chatWhereFrom[i] = msgmasterarr.get(i).getChatWhereFrom();
            //holder1.chatDispName[i] = msgmasterarr.get(i).getChatUserName();
            holder1.chatUserId[i] = msgmasterarr.get(i).getChatUserId();
            holder1.chatprofilepic[i] = msgmasterarr.get(i).getChatProfileImg();

            loadMsgCount(holder1.chatMsgMstrId[i], holder1.inlay[i], holder1.heading_txt, msgmasterarr.get(i).getMatchingMessageCount(), holder1.img[i], msgmasterarr.get(i).getChatProfileImg());

        }
        if (msgmasterarr.size() > 5) {
            holder1.img[4].setVisibility(View.VISIBLE);
            holder1.img[4].setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_more));
            holder1.img[4].setPadding(0, 0, 0, 0);
            holder1.img[4].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSendHomeLstnr != null)
                        mSendHomeLstnr.itemClicked(holder1.itemname, holder1.itemoutid);
                }
            });
        } else if (msgmasterarr.size() == 5) {
            holder1.chatMsgMstrId[4] = msgmasterarr.get(4).getChatMessageMasterId();
            //holder1.chatDispName[4] = msgmasterarr.get(4).getChatUserName();
            holder1.chatUserName[4] = msgmasterarr.get(4).getChatUserName();
            holder1.chatAboutMe[4] = msgmasterarr.get(4).getChatAboutMe();
            holder1.chatWhereFrom[4] = msgmasterarr.get(4).getChatWhereFrom();
            holder1.chatprofilepic[4] = msgmasterarr.get(4).getChatProfileImg();
            holder1.chatDispName[4] = msgmasterarr.get(4).getDisplayName();
            holder1.chatUserId[4] = msgmasterarr.get(4).getChatUserId();
            loadMsgCount(holder1.chatMsgMstrId[4], holder1.inlay[4], holder1.heading_txt, msgmasterarr.get(4).getMatchingMessageCount(), holder1.img[4], msgmasterarr.get(4).getChatProfileImg());
            holder1.img[4].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mSendHomeLstnr != null)
                        mSendHomeLstnr.itemChatClicked(holder1.chatMsgMstrId[4], holder1.chatDispName[4], ConfigData.REDIRECT_HOME, holder1.itemname, holder1.chatUserId[4], holder1.chatprofilepic[4], holder1.chatAboutMe[4], holder1.chatWhereFrom[4], holder1.chatUserName[4]);
                }
            });
        }
        if (flagShowDelete) {
            holder1.radioButton.setChecked(false);
            holder1.radioButton.setVisibility(View.VISIBLE);
            /*if (mSendHomeLstnr != null)
                mSendHomeLstnr.showDeleteIconState(true);*/

        } else {
            holder1.radioButton.setVisibility(View.INVISIBLE);
            /*if (mSendHomeLstnr != null)
                mSendHomeLstnr.showDeleteIconState(false);*/
        }
        holder1.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //todo add ids on delete pressed
                    if (mSendHomeLstnr != null)
                        mSendHomeLstnr.deleteItem(holder1.itemoutid, holder1.itemname);
                }
            }
        });

    }

    private void loadMsgCount(int msgmasterid, TextView txtvw, TextView username, String msgcnt, AppCompatImageView imgvw, String imgurl) {
        if (!msgcnt.isEmpty()) {
            if (msgcnt.trim().equalsIgnoreCase("new"))
                UnoFirebaseMessagingService.keyhash.add(msgmasterid);
            txtvw.setVisibility(View.VISIBLE);
            txtvw.setText(msgcnt);
        }
        imgvw.setVisibility(View.VISIBLE);
        imgvw.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.color.transparent));

        if (!msgcnt.isEmpty()) {
            int pixel = FormatUtils.dpToPx(mContext, 2);
            imgvw.setPadding(pixel, pixel, pixel, pixel);
            imgvw.setBackgroundResource(R.drawable.picbg);
            //username.setTypeface(username.getTypeface(), Typeface.BOLD);
            imgvw.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_avatar));
        } else {
            //username.setTypeface(username.getTypeface(), Typeface.NORMAL);
            imgvw.setPadding(0, 0, 0, 0);
            imgvw.setBackgroundResource(R.drawable.ic_avatar);
            imgvw.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_avatar));
        }

        if (imgurl != null) {
            Glide
                    .with(mContext)
                    .load(Uri.parse(imgurl))
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(imgvw);
        }
    }

    @Override
    public int getItemCount() {
        if (mSendHomeItmLst.getOutList() != null) {
            int length = mSendHomeItmLst.getOutList().size();
            if (length > ConfigData.MAX_BROADCASTS)
                length = ConfigData.MAX_BROADCASTS;
            return length;
        } else
            return 0;

       /* if (mSendHomeItmLst.getOutList() != null)
            return mSendHomeItmLst.getOutList().size();
        else
            return 0;*/
    }
///////////END OF CATEGORY ADAPTER/////////////////
}
