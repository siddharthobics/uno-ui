package com.obics.uno.send;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.Guideline;
//import android.support.v4.content.ContextCompat;

import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
//import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.obics.uno.PhotoViewActivity;
import com.obics.uno.R;
import com.obics.uno.config.ConfigData;
import com.obics.uno.receive.ReqChatRspns;
//import com.obics.uno.service.UnoFirebaseMessagingService;
import com.obics.uno.utils.FormatUtils;
import com.obics.uno.utils.WrapWidthTextView;

import org.apache.commons.lang.StringEscapeUtils;

public class SendChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ReqChatRspns mSendChatList;
    //private SendChatAdptLstnr mListener;
    private Context mContext;
    private final static int TYPE_HEADER = 10;
    private final static int TYPE_ME = 11;
    private final static int TYPE_YOU = 12;
    private String mChatPic, mChatDesc;
    int paddin, paddingVertical;
    //private int unreadcnt = 0, myuserinitcnt = 0, chatuserinitcnt = 0, mMsgMasterId = 0, listcnt = 0;

    public void setNewData(ReqChatRspns reqChatRspns, int msgMasterId) {
        this.mSendChatList = reqChatRspns;

       /* unreadcnt = 0;
        myuserinitcnt = 0;
        chatuserinitcnt = 0;
        mMsgMasterId = msgMasterId;
        listcnt = mSendChatList.getConversations().size();*/
    }

    public interface SendChatAdptLstnr {
        void itemClicked(String itemname);

        //void readCountUpdate(int read);
    }

    public class SendChatHolder extends RecyclerView.ViewHolder {
        WrapWidthTextView userName;
        AppCompatImageView chatpic;
        ConstraintLayout container;
        Guideline guideline;


        public SendChatHolder(View view) {
            super(view);
            userName = view.findViewById(R.id.dialog_desc);
            chatpic = view.findViewById(R.id.media_image);
            container = view.findViewById(R.id.constraintLayout);
            guideline = view.findViewById(R.id.guideline1);

        }
    }

    public SendChatAdapter(Context con, ReqChatRspns reqChatRspns) {
        this.mContext = con;
        this.mSendChatList = reqChatRspns;
        paddingVertical = FormatUtils.dpToPx(mContext, 8);
        paddin = FormatUtils.dpToPx(mContext, 6);

        //this.mListener = mListener;
        //myuserinitcnt = 0;
        //unreadcnt = 0;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = null;
        switch (viewType) {
            case TYPE_HEADER:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_header, parent, false);
                break;
            case TYPE_YOU:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_in, parent, false);
                break;
            case TYPE_ME:
                itemView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.chat_out_send, parent, false);
                break;
            default:
                break;
        }
       /* if (viewType == TYPE_HEADER) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_header, parent, false);
        } else if (viewType == TYPE_YOU) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_in, parent, false);
        } else if (viewType == TYPE_ME) {
            itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_out_send, parent, false);
        }*/

        return new SendChatHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        final SendChatHolder sendChatHolder = (SendChatHolder) holder;
        if (position == 0) {
            mChatPic = mSendChatList.getConversations().get(position).getFileName();
            mChatDesc = mSendChatList.getConversations().get(position).getContent();
            if (mChatPic != null && (mChatPic.contains(ConfigData.COMPARE_JPG.toUpperCase()) || mChatPic.contains(ConfigData.COMPARE_JPG.toLowerCase()))) {
                sendChatHolder.chatpic.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent i = new Intent(mContext, PhotoViewActivity.class);
                        i.putExtra(ConfigData.INTENT_EXTRA_DISP_PHOTO, SendChatAdapter.this.mChatPic);
                        mContext.startActivity(i);
                    }
                });
                Glide
                        .with(mContext)
                        .load(Uri.parse(mChatPic))
                        .apply(RequestOptions.centerCropTransform().placeholder(R.drawable.ic_img_plchold)).into(sendChatHolder.chatpic);
            } else {
                sendChatHolder.chatpic.setVisibility(ConstraintLayout.GONE);
                sendChatHolder.guideline.setGuidelinePercent(0);
                ConstraintLayout.LayoutParams clp = (ConstraintLayout.LayoutParams) sendChatHolder.userName.getLayoutParams();
                clp.width = ConstraintLayout.LayoutParams.WRAP_CONTENT;
                clp.leftMargin = FormatUtils.dpToPx(mContext, 6);
                clp.rightMargin = 0;
                sendChatHolder.userName.setPadding(paddin * 3, paddingVertical, paddin * 2, paddingVertical);
                sendChatHolder.userName.setLayoutParams(clp);
                sendChatHolder.userName.setBackgroundResource(R.drawable.chatbg02);
                sendChatHolder.container.setBackgroundColor(Color.TRANSPARENT);
                RecyclerView.LayoutParams lp = (RecyclerView.LayoutParams) sendChatHolder.container.getLayoutParams();
                lp.leftMargin = FormatUtils.dpToPx(mContext, 80);
                sendChatHolder.container.setLayoutParams(lp);
                //sendChatHolder.chatpic.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_img_plchold));
            }
            if (mChatDesc != null) {
                String msg = StringEscapeUtils.unescapeJava(mChatDesc);//FormatUtils.unescapeJava(desc);

                sendChatHolder.userName.setText(Html.fromHtml(msg));
            }
        } else {
            String desc = mSendChatList.getConversations().get(position).getContent();
            if (desc != null) {
                String msg = StringEscapeUtils.unescapeJava(desc);//FormatUtils.unescapeJava(desc);
                //Log.d("MISHU**99", "  " + msg);
                sendChatHolder.userName.setText(msg);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (mSendChatList.getStatus())
            return mSendChatList.getConversations().size();
        else
            return 0;

    }

    @Override
    public int getItemViewType(int position) {
        int type;
        if (position == 0)
            type = TYPE_HEADER;
        else {
            int id = mSendChatList.getConversations().get(position).getUserId();
            if (id == ConfigData.USER_ID)
                type = TYPE_ME;
            else {
                type = TYPE_YOU;
            }
        }
        return type;
    }
///////////END OF CATEGORY ADAPTER/////////////////
}
