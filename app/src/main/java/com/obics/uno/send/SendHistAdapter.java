package com.obics.uno.send;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v4.content.ContextCompat;

import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
//import android.util.Log;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
//import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.obics.uno.R;
import com.obics.uno.config.ConfigData;
import com.obics.uno.service.UnoFirebaseMessagingService;
import com.obics.uno.utils.FormatUtils;

import org.apache.commons.lang.StringEscapeUtils;

public class SendHistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private SendHistRspns mSendHistList;
    private SendHistAdptLstnr mListener;
    private Context mContext;
    boolean flagShowDelete = false;

    interface SendHistAdptLstnr {
        void itemClicked(int id, String itemname, int chatUserId, String chatpic, String aboutme, String wherefrom, String uniqusr);

        void showDeleteIconState(boolean delete);

        void deleteItem(int id, String keyword, int matchCount);
    }

    public void setNewData(SendHistRspns newsItemList) {
        this.mSendHistList = newsItemList;
    }

    public void toggleDelete() {
        if (getItemCount() > 0) {
            flagShowDelete = !flagShowDelete;
            if (mListener != null)
                mListener.showDeleteIconState(flagShowDelete);
        }
    }

    public void setShowDelete(boolean showDelete) {
        if (getItemCount() > 0) {
            this.flagShowDelete = showDelete;
            if (mListener != null)
                mListener.showDeleteIconState(flagShowDelete);
        }
    }

    public boolean getShowDelete() {
        return flagShowDelete;
    }

    public class SendHistHolder extends RecyclerView.ViewHolder {
        public AppCompatImageView img1;
        String itemname = "", chatpic = "", aboutme = "", wherefrom = "", uniqusr = "";
        TextView userName, userDesc, timeTxt, inlay_txt;
        CheckBox radioButton;
        int mesgMasterId, chatUserId, matchCount;

        public SendHistHolder(View view) {
            super(view);
            img1 = view.findViewById(R.id.media_image);
            userName = view.findViewById(R.id.primary_text);
            userDesc = view.findViewById(R.id.sub_text);
            timeTxt = view.findViewById(R.id.time_text);
            inlay_txt = view.findViewById(R.id.inlay_txt);
            radioButton = view.findViewById(R.id.delbtn);
            view.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    //if (flagShowDelete)
                    // radioButton.toggle();
                    //else
                    if (mListener != null)
                        mListener.itemClicked(mesgMasterId, itemname, chatUserId, chatpic, aboutme, wherefrom, uniqusr);
                }
            });
        }
    }

    public SendHistAdapter(Context con, SendHistRspns newsItemList, SendHistAdptLstnr mListener) {
        this.mContext = con;
        this.mSendHistList = newsItemList;
        this.mListener = mListener;
        //setHasStableIds(true);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.history_itm_card, parent, false);
        return new SendHistHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final SendHistHolder sendHistHolder = (SendHistHolder) holder;
        String url = mSendHistList.getMatchingUsers().get(position).getProfileImg();
        String desc = mSendHistList.getMatchingUsers().get(position).getMessageDescription();
        //sendHistHolder.userDesc.setText(mSendHistList.getMatchingUsers().get(position).getMessageDescription());
        sendHistHolder.mesgMasterId = mSendHistList.getMatchingUsers().get(position).getMessageMasterId();
        String keyname = mSendHistList.getMatchingUsers().get(position).getKeywordName();
        if (keyname != null && (keyname.equalsIgnoreCase(ConfigData.CHANNEL_NEWS_NAME))) {
            if (desc != null)
                sendHistHolder.userDesc.setText(Html.fromHtml(desc));
            //sendHistHolder.radioButton.setEnabled(false);
        } else {
            if (desc != null)
                sendHistHolder.userDesc.setText(StringEscapeUtils.unescapeJava(desc));
        }


        sendHistHolder.uniqusr = mSendHistList.getMatchingUsers().get(position).getUserName();
        sendHistHolder.aboutme = mSendHistList.getMatchingUsers().get(position).getAboutMe();
        sendHistHolder.wherefrom = mSendHistList.getMatchingUsers().get(position).getWhereFrom();
        sendHistHolder.itemname = mSendHistList.getMatchingUsers().get(position).getDisplayName();
        sendHistHolder.chatpic = url;
        sendHistHolder.chatUserId = mSendHistList.getMatchingUsers().get(position).getUserId();
        sendHistHolder.userName.setText(StringEscapeUtils.unescapeJava(sendHistHolder.itemname));
        sendHistHolder.timeTxt.setText(FormatUtils.formatChatTime(mSendHistList.getMatchingUsers().get(position).getInsertDate() + ""));
        String msg = mSendHistList.getMatchingUsers().get(position).getMatchingMessageCount();
        sendHistHolder.img1.setImageDrawable(ContextCompat.getDrawable(mContext, android.R.color.transparent));
        sendHistHolder.matchCount = 0;
        if (!msg.isEmpty()) {
            if (msg.toLowerCase().contains("new")) {
                //if (msgcnt.trim().equalsIgnoreCase("new"))
                UnoFirebaseMessagingService.keyhash.add(sendHistHolder.mesgMasterId);
                sendHistHolder.matchCount = 1;
            } else {
                try {
                    int msgcnt = Integer.parseInt(msg);
                    if (msgcnt != 0)
                        sendHistHolder.matchCount = msgcnt;

                } catch (NumberFormatException pe) {
                    sendHistHolder.matchCount = 0;
                }
            }
            int pixel = FormatUtils.dpToPx(mContext, 2);
            sendHistHolder.img1.setPadding(pixel, pixel, pixel, pixel);
            sendHistHolder.img1.setBackgroundResource(R.drawable.picbg);
            sendHistHolder.img1.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_avatar));
            if (mContext != null)
                sendHistHolder.userName.setTypeface(ResourcesCompat.getFont(mContext, R.font.roboto_bold));
            //sendHistHolder.userName.setTypeface(sendHistHolder.userName.getTypeface(), Typeface.BOLD);
            sendHistHolder.userDesc.setTextColor(Color.BLACK);
            sendHistHolder.inlay_txt.setText(msg);
            sendHistHolder.inlay_txt.setVisibility(View.VISIBLE);
        } else {
            sendHistHolder.inlay_txt.setVisibility(View.GONE);
            sendHistHolder.img1.setPadding(0, 0, 0, 0);
            sendHistHolder.img1.setBackgroundResource(R.drawable.ic_avatar);
            if (mContext != null)
                sendHistHolder.userName.setTypeface(ResourcesCompat.getFont(mContext, R.font.roboto_regular));
            //sendHistHolder.userName.setTypeface(sendHistHolder.userName.getTypeface(), Typeface.NORMAL);
            sendHistHolder.userDesc.setTextColor(Color.parseColor("#b1b4bb"));

            //sendHistHolder.userDesc.setTypeface(sendHistHolder.userDesc.getTypeface(), Typeface.NORMAL);
        }
        //}
        // get
        if (url != null)
            Glide
                    .with(mContext)
                    .asBitmap()
                    .load(Uri.parse(url))
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(sendHistHolder.img1);

        if (flagShowDelete) {
            sendHistHolder.radioButton.setChecked(false);
            sendHistHolder.radioButton.setVisibility(View.VISIBLE);
          /*  if (mListener != null)
                mListener.showDeleteIconState(true);*/

        } else {
            sendHistHolder.radioButton.setVisibility(View.INVISIBLE);
           /* if (mListener != null)
                mListener.showDeleteIconState(false);*/
        }
        sendHistHolder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    if (mListener != null)
                        mListener.deleteItem(mSendHistList.getMatchingUsers().get(position).getKeywordId(), mSendHistList.getMatchingUsers().get(position).getMessageMasterId() + "", sendHistHolder.matchCount);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        if (mSendHistList.getStatus())
            return mSendHistList.getMatchingUsers().size();
        else
            return 0;

    }
///////////END OF CATEGORY ADAPTER/////////////////
}
