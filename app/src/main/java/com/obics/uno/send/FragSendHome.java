package com.obics.uno.send;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;


import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
//import android.util.Log;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

//import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
//import com.obics.uno.MainActivity;
import com.obics.uno.config.ConfigData;
import com.obics.uno.dialog.ConfirmDeleteDialog;
import com.obics.uno.MapActivity;
import com.obics.uno.R;
import com.obics.uno.interfaces.PagerFrgmntLstnr;
import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
import com.obics.uno.service.UnoFirebaseMessagingService;
import com.obics.uno.utils.DatabaseMgr;
import com.obics.uno.utils.FormatUtils;
import com.obics.uno.utils.ToolDotProgress;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class FragSendHome extends Fragment {

    private PagerFrgmntLstnr mMainListener;
    private RecyclerView mRecyclerView;
    private FragSendHomeListener mListener;
    private ToolDotProgress mProgress;
    private SendHomeAdapter sendHomeAdapter;
    private SendHomeRspns sendHomeRspns = new SendHomeRspns();
    private TextView mHeadingText, mFaabText, mLimCntTxt, mToolTxt;

    private AppCompatImageView mTooltip;
    private SwipeRefreshLayout swipeRefreshLayout;
    private boolean isVisible = true;
    private static final int SEND_MAP_REQUEST = 230;
    //private static final int SEND_MAP_RESULT = 485;
    private static final String FRAGMENT_DIALOG_TAG = "fragment_sen";
    private View fab;
    private AppCompatImageButton mTopLeftBtn, mTopRightBtn;
    boolean fcmUploaded = false;
    private String fcmKey = "";
    private static int mTagCount = 0;
    private DatabaseMgr datastore;
    private Handler handler;
    private Runnable runnable = new Runnable() {
        public void run() {
            requestSendHomeList(false, false);
            handler.postDelayed(this, ConfigData.REFRESH_TIME);
        }
    };

    @Override
    public void onStop() {
        handler.removeCallbacks(runnable);
        super.onStop();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //Log.d("UNOLOGK4", context + "  **  ");
        try {
            mMainListener = (PagerFrgmntLstnr) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement PageFrgmntListener");
        }
        //this.mListener = context;
    }

    public interface FragSendHomeListener {
        void showSendChatPage(int id, String str, int histid, String histkey, int chatUserId, String chatpic, String aboutme, String wherefrom, String uniqusr);

        void showSendHistPage(String str, int userKeywordId);

       /* void showLogin();

        void showSettings();

        void setPlaceholders();

        void showSendCount(int count);

        void logOut();*/

    }

    public FragSendHome() {

    }

    public void setSendFragmentListener(FragSendHomeListener mListener) {
        this.mListener = mListener;
        if (mMainListener != null)
            mMainListener.setPlaceholderButtons(R.drawable.ic_set1, R.drawable.ic_del1);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        datastore = new DatabaseMgr(getContext());
        View rootView = inflater.inflate(R.layout.fragment_send, container, false);
        mHeadingText = rootView.findViewById(R.id.topbartext);
        mHeadingText.setText(R.string.send_home_heading);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        mProgress = rootView.findViewById(R.id.progressBar);
        mProgress.setVisibility(View.GONE);
        swipeRefreshLayout = rootView.findViewById(R.id.simpleSwipeRefreshLayout);
        mTopLeftBtn = rootView.findViewById(R.id.topbarleftbtn);
        mTopRightBtn = rootView.findViewById(R.id.topbarrightbtn);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mTooltip = rootView.findViewById(R.id.setooltip);
        mToolTxt = rootView.findViewById(R.id.setooltiptxt);
        fab = rootView.findViewById(R.id.fabse);
        mFaabText = rootView.findViewById(R.id.fabsetxt);
        mLimCntTxt = rootView.findViewById(R.id.fabs_count_txt);
        fab.setOnTouchListener(btnlistner);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConfigData.USER_ID == ConfigData.INVALID_USER_ID) {
                    if (mMainListener != null)
                        mMainListener.showLogin();
                } else {
                    resetDelete();
                    fab.setEnabled(false);

                    Intent i = new Intent(getActivity(), MapActivity.class);
                    i.putExtra(ConfigData.INTENT_MAP_SCREEN_TYPE, 1);
                    startActivityForResult(i, SEND_MAP_REQUEST);

                    try {
                        getActivity().overridePendingTransition(R.anim.slide_from_bottom, R.anim.slide_to_top);//R.anim.slide_from_bottom
                    } catch (NullPointerException | IllegalStateException npe) {
                        npe.printStackTrace();
                    }
                }
            }
        });
        sendHomeRspns.setOutList(new ArrayList<SendHomeRspns.OutList>());
        sendHomeAdapter = new SendHomeAdapter(getContext(), sendHomeRspns, new SendHomeAdapter.SendHomeAdptLstnr() {

            @Override
            public void showMsgCount(int count) {

            }

            @Override
            public void itemChatClicked(int id, String itemname, int histid, String histkey, int chatUserId, String chatpic, String aboutme, String wherefrom, String uniqusr) {
                if (mListener != null)
                    mListener.showSendChatPage(id, itemname, histid, histkey, chatUserId, chatpic, aboutme, wherefrom, uniqusr);
            }

            @Override
            public void itemClicked(String itemname, int userKeywordId) {
                if (mListener != null)
                    mListener.showSendHistPage(itemname, userKeywordId);
            }

            @Override
            public void showDeleteIconState(boolean delete) {
                if (getContext() != null && isVisible && mListener != null)
                    if (delete)
                        mTopRightBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_del2));
                    else
                        mTopRightBtn.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_del1));
            }

            @Override
            public void deleteItem(int id, String keyword) {
                showConfirmDelete(id, keyword);

            }
        });
        mRecyclerView.setAdapter(sendHomeAdapter);
        setupExternalButtons();
        if (isAdded()) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());
            fcmUploaded = preferences.getBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, false);
            if (fcmUploaded)
                fcmKey = preferences.getString(ConfigData.PREF_FCM_TOKEN, "");
        }
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID) {
            String json = datastore.fetchCategory(ConfigData.DB_CATEG_SEND_HOME);
            populateSendHome(json, false, false);
            //requestSendHomeList(true);
        }
        swipeRefreshLayout.setNestedScrollingEnabled(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                requestSendHomeList(false, true);
            }
        });

        handler = new Handler();
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshPage(true);
        if (mTagCount < ConfigData.MAX_BROADCASTS - 1)
            fab.setEnabled(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        handler.postDelayed(runnable, ConfigData.REFRESH_TIME);
    }

    private void setupExternalButtons() {
        mTopLeftBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConfigData.USER_ID == ConfigData.INVALID_USER_ID) {
                    if (mMainListener != null)
                        mMainListener.showLogin();
                } else {
                    if (mMainListener != null)
                        mMainListener.showSettings();
                }
            }
        });
        mTopRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleHomeDelete();
            }
        });
    }


    private void showTopButtons(boolean show) {
       /* if(!show)
            Log.d("mailog", "getta39988844");
        else
            Log.d("mailog", "getta9999");*/

        int visible = (show ? ConstraintLayout.VISIBLE : ConstraintLayout.GONE);
        if (mTopLeftBtn != null && mTopRightBtn != null) {
            mTopLeftBtn.setVisibility(visible);
            mTopRightBtn.setVisibility(visible);
            if (show)
                mTopRightBtn.setImageResource(R.drawable.ic_del1);
        }
        if (mRecyclerView != null) {
            if (show)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);

        if (menuVisible) {
            if (mRecyclerView != null)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
            if (mMainListener != null)
                mMainListener.setPlaceholderButtons(R.drawable.ic_set1, R.drawable.ic_del1);
        } else {
            if (mRecyclerView != null)
                mRecyclerView.setBackgroundColor(Color.parseColor(ConfigData.COLOR_DARK_TRANSPARENCY));
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //hides the delete button everytime
        if (sendHomeAdapter != null) {
            resetDelete();
        }
        if (isVisibleToUser) {
            showTopButtons(true);
            if (!isVisible)
                refreshPage(false);
        } else {
            showTopButtons(false);
        }
        isVisible = isVisibleToUser;

    }

    private void showToast(int resid) {
        if (isAdded())
            Toast.makeText(getContext().getApplicationContext(), resid, Toast.LENGTH_SHORT).show();
    }

    private void checkDuplicateLogin(String appid) {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID)
            if (fcmUploaded && appid != null)
                if (!fcmKey.isEmpty())
                    if (!appid.trim().equals(fcmKey.trim())) {
                        mRecyclerView.removeAllViews();
                        if (mMainListener != null)
                            mMainListener.logOut();
                    }
    }

    ////////
    public void toggleHomeDelete() {
        sendHomeAdapter.toggleDelete();
        sendHomeAdapter.notifyDataSetChanged();
    }

    public void requestResetCounter(final long currentMillis) {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"userId\":" + ConfigData.USER_ID + "}");
        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/keyword/outKeywordCountUpdate", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.d("TAGLOGGED","LOGGEDR");
                        try {
                            JSONObject resp = new JSONObject(response);
                            if (resp.optBoolean("status")) {
                                //Log.d("TAGLOGGED", "LOGGEDR");
                                fab.setEnabled(true);
                                requestSendHomeList(true, false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    showToast(R.string.no_internet_conn);
            }
        });

        VolleySingleton.getInstance().addToRequestQueue(req, true);
    }

    private void populateSendHome(String json, boolean live, boolean resetDelete) {
        if (json != null) {
            if (live) {
                datastore.insertOrUpdate(ConfigData.DB_CATEG_SEND_HOME, json);
            }
            try {
                sendHomeRspns = new Gson().fromJson(json, SendHomeRspns.class);
            } catch (Exception e) {
                datastore.insertOrUpdate(ConfigData.DB_CATEG_SEND_HOME, "");
                return;
            }
            String time = sendHomeRspns.getCountUpdatedTime();
            //String time = "2018-09-24 11:26:27.0";

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            long currentMillis = 0;
            try {
                //currentMillis = sdf.parse(FormatUtils.convertToLATime(new Date(sendHomeRspns.getServerCurrentTime()))).getTime();
                Date datew = sdf.parse(FormatUtils.convertToLATime(new Date(sendHomeRspns.getServerCurrentTime())));
                //Date datew = sdf.parse(FormatUtils.convertToLATime(new Date(System.currentTimeMillis())));
                //Log.d("UNOLOGkool", " mydate***  " + datew.toString() + "   " + time);
                currentMillis = datew.getTime();
            } catch (ParseException | IllegalArgumentException | NullPointerException e) {
                e.printStackTrace();
                currentMillis = System.currentTimeMillis();
            }
            //if (sendHomeRspns.getOutList() != null)
            //    showTotalRemainingTags(sendHomeRspns.getOutList().size());
            checkDuplicateLogin(sendHomeRspns.getUserAppId());
            UnoFirebaseMessagingService.keyhash.clear();
            int lengthout = sendHomeRspns.getOutList().size();
            int todayCount = sendHomeRspns.getTodayKeywordCount();


            if (sendHomeRspns.getOutList() != null) {
                if (sendHomeRspns.getOutList().isEmpty()) {

                    showTimeRemainingTags(todayCount, currentMillis, time);
                    if (todayCount < ConfigData.MAX_BROADCASTS) {
                        mTooltip.setVisibility(ConstraintLayout.VISIBLE);
                        mToolTxt.setVisibility(ConstraintLayout.VISIBLE);
                        mLimCntTxt.setVisibility(ConstraintLayout.GONE);
                    } else {
                        mTooltip.setVisibility(ConstraintLayout.GONE);
                        mToolTxt.setVisibility(ConstraintLayout.GONE);
                    }
                } else {
                    mTooltip.setVisibility(ConstraintLayout.GONE);
                    mToolTxt.setVisibility(ConstraintLayout.GONE);
                    mLimCntTxt.setVisibility(ConstraintLayout.VISIBLE);
                    if (todayCount < ConfigData.MAX_BROADCASTS) {
                        if (lengthout < ConfigData.MAX_BROADCASTS) {
                            showTimeRemainingTags(todayCount, currentMillis, time);
                            fab.setEnabled(true);
                        } else {
                            showTotalRemainingTags(lengthout);
                        }
                    } else
                        showTimeRemainingTags(todayCount, currentMillis, time);
                }
            } else {
                mTooltip.setVisibility(ConstraintLayout.GONE);
                mToolTxt.setVisibility(ConstraintLayout.GONE);
                mLimCntTxt.setVisibility(ConstraintLayout.VISIBLE);
            }

            if (isAdded() && live) {
                int notificationCount = sendHomeRspns.getNotificationCount();
                if (mMainListener != null)
                    mMainListener.showSendCount(notificationCount);
                storeNotificationCount(notificationCount);
            }
            if (resetDelete)
                sendHomeAdapter.setShowDelete(false);

            sendHomeAdapter.setNewData(sendHomeRspns);
            sendHomeAdapter.notifyDataSetChanged();
            mProgress.setVisibility(View.GONE);
        }
    }

    private void showTotalRemainingTags(int tagCount) {
        if (tagCount > ConfigData.MAX_BROADCASTS)
            tagCount = ConfigData.MAX_BROADCASTS;
        mTagCount = tagCount;
        if (ConfigData.MAX_BROADCASTS - tagCount <= 0) {
            fab.setOnTouchListener(null);
            mLimCntTxt.setVisibility(ConstraintLayout.GONE);
            mFaabText.setText(R.string.add_new_msg_limit_reach);
            mFaabText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            fab.setBackgroundResource(R.color.settingDark);
            mFaabText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            fab.setEnabled(false);
        }
    }

    public void requestSendHomeList(final boolean showProgress, final boolean resetDelete) {
        if (showProgress)
            mProgress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"userId\":" + ConfigData.USER_ID + "}");
        } catch (JSONException e) {

        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/keyword/outList", jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        populateSendHome(response.toString(), true, resetDelete);
                        mProgress.setVisibility(View.GONE);
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    showToast(R.string.no_internet_conn);
            }
        });

        VolleySingleton.getInstance().addToRequestQueue(req, true);
    }

    //private static final int ConfigData.MAX_BROADCASTS =20;
    private void showTimeRemainingTags(int todayCount, long currentMillis, String time) {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID && time != null)
            if (FormatUtils.isWithinTimeLimit(time, currentMillis)) {
                showRemainingTags(todayCount, currentMillis);
            } else {
                requestResetCounter(currentMillis);
                //mLimCntTxt.setVisibility(ConstraintLayout.VISIBLE);
            }
    }


    private void showRemainingTags(int tagCount, long basetime) {
        if (tagCount < ConfigData.MAX_BROADCASTS) {
            mFaabText.setText(R.string.send_broadcast_btn);
            fab.setBackgroundResource(R.drawable.out_btm_btn_bg);
            fab.setOnTouchListener(btnlistner);
            mFaabText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);
            mFaabText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_small_arr_white_up, 0);
            fab.setEnabled(true);
            mLimCntTxt.setVisibility(ConstraintLayout.VISIBLE);
            int difcount = ConfigData.MAX_BROADCASTS - tagCount;
            if (isAdded())
                mLimCntTxt.setText(Html.fromHtml("You can send <b>" + difcount + "</b>" + getResources().getQuantityString(R.plurals.send_broadcast_limit, difcount)));
        } else {

            int diff[] = FormatUtils.getHourDifference(basetime);
            if (isAdded()) {
                mTooltip.setVisibility(ConstraintLayout.GONE);
                mToolTxt.setVisibility(ConstraintLayout.GONE);
                String time_diff = "";
                if (diff[0] > 1)
                    time_diff = getString(R.string.send_broadcast_limit_reach) + (diff[0] + 1) + " hours";
                else if (diff[0] == 1) {
                    String appens = "";
                    if (diff[1] > 0) {
                        appens = (diff[0] + 1) + " hours";
                    } else
                        appens = "1 hour ";
                    time_diff = getString(R.string.send_broadcast_limit_reach) + appens;
                } else if (diff[0] < 1) {
                    String appens = " minute";
                    if (diff[1] > 1)
                        appens = " minutes";
                    time_diff = getString(R.string.send_broadcast_limit_reach) + diff[1] + appens;
                }
                mFaabText.setText(time_diff);
            }
            fab.setBackgroundResource(R.color.settingDark);
            fab.setOnTouchListener(null);
            mFaabText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            mFaabText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            fab.setEnabled(false);
            mLimCntTxt.setVisibility(ConstraintLayout.GONE);
        }
    }

    private void requestDeleteItem(int id, String keyword) {
        mProgress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"id\": \"" + id + "\", \"keyword\": \"" + keyword + "\",\"userId\":" + ConfigData.USER_ID + "}");
        } catch (JSONException e) {

        }
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/keyword/deleteOut", jsonBody,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        populateSendHome(response.toString(), true, true);
                        mProgress.setVisibility(View.GONE);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mProgress.setVisibility(View.GONE);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    showToast(R.string.no_internet_conn);
            }
        });

        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private void showConfirmDelete(final int id, final String keyword) {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_DIALOG_TAG);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        ConfirmDeleteDialog deleteDialog = new ConfirmDeleteDialog();
        deleteDialog.setmConfrmDelLstnr(new ConfirmDeleteDialog.ConfirmDelDlgListener() {
            @Override
            public void clickPositive() {
                requestDeleteItem(id, keyword);
            }

            @Override
            public void clickNegative() {
                resetDelete();
            }
        });
        deleteDialog.show(manager, FRAGMENT_DIALOG_TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mProgress.setVisibility(View.GONE);
        if (requestCode == SEND_MAP_REQUEST && resultCode == MapActivity.SEND_MAP_RESULT) {
            try {
                String json = datastore.fetchCategory(ConfigData.DB_CATEG_SEND_HOME);
                populateSendHome(json, false, true);
            } catch (Exception ree) {
                requestSendHomeList(true, true);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }

    ////END////
    private void resetDelete() {
        //mRecyclerView.scrollToPosition(0);
        if (sendHomeAdapter.getShowDelete()) {
            sendHomeAdapter.setShowDelete(false);
            sendHomeAdapter.notifyDataSetChanged();
        }
    }

    private void storeNotificationCount(int sendCount) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(ConfigData.PREF_SEND_NOTIF_COUNT, sendCount);
        editor.apply();
    }

    private void refreshPage(boolean resetDelete) {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID)
            requestSendHomeList(true, resetDelete);
    }

    private View.OnTouchListener btnlistner = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    mFaabText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_small_arr_black_up, 0);
                    break;
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP:
                    mFaabText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_small_arr_white_up, 0);

                    break;
            }
            return false;
        }
    };
}
