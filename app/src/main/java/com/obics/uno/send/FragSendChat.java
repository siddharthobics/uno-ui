package com.obics.uno.send;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
//import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
//import android.util.Log;
//import android.util.Log;
//import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
//import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

//import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
//import com.obics.uno.MainActivity;
import com.obics.uno.R;
import com.obics.uno.UnoApplication;
import com.obics.uno.config.ConfigData;
import com.obics.uno.dialog.BlockConfirmDialog;
import com.obics.uno.dialog.EnterFeedbackDlg;
import com.obics.uno.dialog.ProfileDialogFragment;
import com.obics.uno.interfaces.PagerFrgmntLstnr;
//import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
import com.obics.uno.receive.RecvHistRspns;
import com.obics.uno.receive.ReqChatRspns;
import com.obics.uno.service.UnoFirebaseMessagingService;
//import com.obics.uno.utils.DatabaseMgr;
import com.obics.uno.utils.FormatUtils;
import com.obics.uno.utils.ToolDotProgress;
import com.obics.uno.utils.UnicodeLimitTextWatcher;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
//import java.lang.reflect.Field;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class FragSendChat extends Fragment {
    /**
     * The send chat fragment
     */
    private PagerFrgmntLstnr mMainListener;
    private RecyclerView mRecyclerView;
    private FragSendChatListener mListener;
    private ToolDotProgress mProgress;
    private AppCompatImageButton mTopLeftBtn, mTopRightBtn, topimg;
    private SendChatAdapter mRecvMsgAdapter;
    private ReqChatRspns mRecvChatRspns = new ReqChatRspns();
    private TextView mHeadingText, mNameText, mTimeTxt;
    private EditText mChatInput;
    private AppCompatImageView mSendButton;
    private String mHistoryKey = "", mChatWithUser = "", mChatPic = "", mAboutme = "", mWherefrom = "", mChatUniqUsr = "";
    private int mHistoryId = ConfigData.REDIRECT_HOME, mMsgMasterId, mChatUserId, mRootHeight = 0;
    private View rootView;
    private static final String FRAGMENT_REPORT = "fragment_report";
    private static final String FRAGMENT_BLOCK = "fragment_block";

    boolean fcmUploaded = false;
    private String fcmKey = "", msg = "";
    private static final String MSG_MASTER_ID = "msgMasterId";
    private static final String HISTORY_ID = "historyId";
    private static final String HISTORY_KEY = "historyKey";
    private static final String CHAT_WITH_USER = "chatwith";
    private static final String CHAT_USR_ID = "chat_user_id";
    private static final String REQ_TAG_SEND_CHAT = "req_tag_send_chat";
    private static final String CHAT_PROFILE_PIC = "chat_profile_pic";
    private static final String FRAGMENT_BOTTOM_DLG_TAG = "bottomse_dialog_tag";


    private Handler handler;
    private boolean isVisible = true, isBlocked = true;

    private Runnable runnable = new Runnable() {
        public void run() {
            requestSendChatContent(mMsgMasterId, false, false);
            handler.postDelayed(this, ConfigData.REFRESH_TIME_CHAT);
        }
    };

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mMainListener = (PagerFrgmntLstnr) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement PageFrgmntListener");
        }
    }

    @Override
    public void onStop() {
        //hide keyboard
        WeakReference<View> tview = new WeakReference<>(getView());
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(tview.get().getWindowToken(), 0);
        handler.removeCallbacks(runnable);
        super.onStop();
    }

    private TextWatcher mChatTxtWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            mSendButton.setEnabled(s.length() > 0);
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };

    public interface FragSendChatListener {
        void showSendHomePage();

        void showHistPage(int id, String str);

    }

    public FragSendChat() {

    }

    public void setSendFragListener(FragSendChatListener mListener) {
        this.mListener = mListener;
        if (mMainListener != null)
            mMainListener.setPlaceholderButtons(R.drawable.ic_left_arr, R.drawable.ic_menu_dot);
    }

    private void setupExternalButtons() {
        mHeadingText.setText("#" + mHistoryKey);
        if (mHistoryId != ConfigData.REDIRECT_HOME) {
            mTopLeftBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isAdded())
                        hideSoftKeyboard(getContext(), mChatInput);
                    if (mListener != null)
                        mListener.showHistPage(mHistoryId, mHistoryKey);
                }
            });
        } else {
            mTopLeftBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isAdded())
                        hideSoftKeyboard(getContext(), mChatInput);
                    if (mListener != null)
                        mListener.showSendHomePage();
                }
            });
        }
        mTopRightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopupMenu(v, "hello");
            }
        });
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            if (mRecyclerView != null)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
            if (mMainListener != null)
                mMainListener.setPlaceholderButtons(R.drawable.ic_left_arr, R.drawable.ic_menu_dot);
        } else {
            if (mRecyclerView != null)
                mRecyclerView.setBackgroundColor(Color.parseColor(ConfigData.COLOR_DARK_TRANSPARENCY));
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            showTopButtons(true);
            if (!isVisible)
                requestSendChatContent(mMsgMasterId, true, true);
        } else {
            showTopButtons(false);
        }
        isVisible = isVisibleToUser;

    }

    private void showTopButtons(boolean show) {
        int visible = (show ? ConstraintLayout.VISIBLE : ConstraintLayout.GONE);
        if (mTopLeftBtn != null && mTopRightBtn != null) {
            mTopLeftBtn.setVisibility(visible);
            mTopRightBtn.setVisibility(visible);
        }
        if (mRecyclerView != null) {
            if (show)
                mRecyclerView.setBackgroundColor(Color.TRANSPARENT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_chat, container, false);
        topimg = rootView.findViewById(R.id.media_image);
        rootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                //r will be populated with the coordinates of your view that area still visible.
                rootView.getWindowVisibleDisplayFrame(r);
                int rootHeight = rootView.getHeight() + FormatUtils.dpToPx(UnoApplication.getAppContext(), 41) + 2;
                if (rootHeight > mRootHeight)
                    mRootHeight = rootHeight;
                int heightDiff = rootView.getRootView().getHeight() - (r.bottom - r.top);
                if (heightDiff > ConfigData.KBOARD_HEIGHT) {
                    //topimg.setVisibility(ConstraintLayout.GONE);
                    if (mRecvChatRspns != null)
                        if (mRecvChatRspns.getConversations() != null)
                            mRecyclerView.smoothScrollToPosition(mRecvChatRspns.getConversations().size());
                } else {
                    //topimg.setVisibility(ConstraintLayout.VISIBLE);
                }
            }
        });

        mChatPic = getArguments().getString(CHAT_PROFILE_PIC);
        if (mChatPic == null)
            mChatPic = "";
        mHistoryKey = getArguments().getString(HISTORY_KEY);
        mChatWithUser = getArguments().getString(CHAT_WITH_USER);
        mAboutme = getArguments().getString(ConfigData.INTENT_ABOUT_ME);
        mWherefrom = getArguments().getString(ConfigData.INTENT_WHERE_FROM);
        mChatUniqUsr = getArguments().getString("chatwithuniqusr");
        mHistoryId = getArguments().getInt(HISTORY_ID);
        mMsgMasterId = getArguments().getInt(MSG_MASTER_ID);
        mChatUserId = getArguments().getInt(CHAT_USR_ID);
        mMsgMasterId = getArguments().getInt(MSG_MASTER_ID);
        mHeadingText = rootView.findViewById(R.id.topbartext);
        mNameText = rootView.findViewById(R.id.chat_top_name);
        mTimeTxt = rootView.findViewById(R.id.chat_top_txt);
        mTopLeftBtn = rootView.findViewById(R.id.topbarleftbtn);
        mTopRightBtn = rootView.findViewById(R.id.topbarrightbtn);
        mRecyclerView = rootView.findViewById(R.id.recycler_view);
        mProgress = rootView.findViewById(R.id.progressBar);
        mProgress.changeStartColor(ContextCompat.getColor(getContext(), R.color.outboxDark));
        mProgress.changeEndColor(ContextCompat.getColor(getContext(), R.color.outBtnDarker));
        mProgress.setVisibility(View.GONE);
        View topbg = rootView.findViewById(R.id.topbarbase);
        topbg.setBackgroundResource(R.drawable.topbaroutbox);
        //mHeadingText
        if (!TextUtils.isEmpty(mChatWithUser))
            mNameText.setText(StringEscapeUtils.unescapeJava(mChatWithUser));
        //else
        //  requestDisplayName(mChatUserId);
        //mNameText.setText(mChatWithUser);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mChatInput = rootView.findViewById(R.id.chatInput);
        mChatInput.addTextChangedListener(mChatTxtWatcher);
        mChatInput.addTextChangedListener(new UnicodeLimitTextWatcher(mChatInput, 1000));
        mSendButton = rootView.findViewById(R.id.send_save_btn);
        if (isAdded())
            mSendButton.setImageDrawable(ContextCompat.getDrawable(getContext(), R.drawable.ic_chat_send));
        topimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAdded() && !isBlocked) {
                    FragmentManager manager = getActivity().getSupportFragmentManager();
                    Fragment frag = manager.findFragmentByTag(FRAGMENT_BOTTOM_DLG_TAG);
                    if (frag != null) {
                        manager.beginTransaction().remove(frag).commitAllowingStateLoss();
                    }
                    ProfileDialogFragment bottomSheetDialog = new ProfileDialogFragment();
                    Bundle args = new Bundle();
                    args.putInt("screenheight", mRootHeight);
                    args.putString("profilepic", mChatPic);
                    args.putString("dispname", mChatWithUser);
                    args.putString("chatwithuniqusr", mChatUniqUsr);
                    args.putString(ConfigData.INTENT_ABOUT_ME, mAboutme);
                    args.putString(ConfigData.INTENT_WHERE_FROM, mWherefrom);
                    bottomSheetDialog.setArguments(args);
                    bottomSheetDialog.show(manager, FRAGMENT_BOTTOM_DLG_TAG);
                }
            }
        });
        mSendButton.setEnabled(false);
        if (!mChatPic.isEmpty())
            Glide
                    .with(getContext())
                    .asBitmap()
                    .load(Uri.parse(mChatPic))
                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(topimg);
        setupExternalButtons();
        if (isAdded()) {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext().getApplicationContext());
            fcmUploaded = preferences.getBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, false);
            if (fcmUploaded)
                fcmKey = preferences.getString(ConfigData.PREF_FCM_TOKEN, "");
        }
        mRecvMsgAdapter = new SendChatAdapter(getContext(), mRecvChatRspns);
        mRecyclerView.setAdapter(mRecvMsgAdapter);
        requestSendChatContent(mMsgMasterId, true, true);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSendButton.setEnabled(false);
                requestChatUpdate(mMsgMasterId);
            }
        });
        handler = new Handler();
        return rootView;
    }

    public void refreshChat(int msgMasterId) {
        if (mMsgMasterId == msgMasterId)
            requestSendChatContent(mMsgMasterId, false, true);

    }

    protected void hideSoftKeyboard(final Context context, EditText input) {
        WeakReference<EditText> tedit = new WeakReference<>(input);
        tedit.get().setInputType(0);
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        try {
            imm.hideSoftInputFromWindow(tedit.get().getWindowToken(), 0);
        } catch (NullPointerException npe) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        handler.postDelayed(runnable, ConfigData.REFRESH_TIME_CHAT);
    }

    private void enableInput(boolean enable) {
        mSendButton.setEnabled(enable);

    }

    public void requestSendChatContent(int msgMasterId, final boolean showProgress, final boolean scroll) {
        if (!isVisible && !showProgress)
            return;
        if (isAdded())
            VolleySingleton.getInstance().cancelAll(REQ_TAG_SEND_CHAT);


        if (showProgress)
            mProgress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put("messageMasterId", msgMasterId);
            jsonBody.put("userId", ConfigData.USER_ID);

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                    ConfigData.BASE_URL + "/message/getConversations", jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            mProgress.setVisibility(View.GONE);
                            mRecvChatRspns = new Gson().fromJson(response.toString(), ReqChatRspns.class);
                            checkDuplicateLogin(mRecvChatRspns.getUserAppId());
                            isBlocked = mRecvChatRspns.getIsBlocked();
                            if (isBlocked) {
                                //Toast.makeText(getContext().getApplicationContext(), "User blocked", Toast.LENGTH_SHORT).show();
                                mChatInput.setEnabled(false);
                                mTimeTxt.setVisibility(View.VISIBLE);
                                mTimeTxt.setText("You have blocked this user");
                                mTimeTxt.setPadding(12, 32, 12, 2);
                            } else {
                                enableInput(true);
                                mChatInput.setEnabled(true);
                                int unreadcnt = 0;
                                int listsize = mRecvChatRspns.getConversations().size();
                                for (int u = listsize - 1; u >= 0; u--) {
                                    String str = mRecvChatRspns.getConversations().get(u).getReadStatus();
                                    int userid = mRecvChatRspns.getConversations().get(u).getUserId();
                                    if (str.contains("unRead") && userid != ConfigData.USER_ID) {
                                        unreadcnt++;
                                    }
                                }
                                if (unreadcnt == listsize - 1) {
                                    if (mMainListener != null)
                                        mMainListener.decrSendCount(1);
                                    if (UnoFirebaseMessagingService.keyhash.contains(mMsgMasterId))
                                        UnoFirebaseMessagingService.keyhash.remove(mMsgMasterId);
                                } else {
                                    if (mMainListener != null)
                                        mMainListener.decrSendCount(unreadcnt);
                                }
                                if (mChatPic.isEmpty() && listsize >= 2) {
                                    //mNameText.setText(mRecvChatRspns.getConversations().get(1).getUserName());
                                    String url = mRecvChatRspns.getConversations().get(1).getProfileImg();
                                    if (isAdded())
                                        if (url != null)
                                            Glide
                                                    .with(getContext())
                                                    .asBitmap()
                                                    .load(Uri.parse(url))
                                                    .apply(RequestOptions.circleCropTransform().placeholder(R.drawable.ic_avatar)).into(topimg);
                                }
                                mTimeTxt.setVisibility(View.VISIBLE);
                                mTimeTxt.setPadding(12, 2, 12, 2);

                                mTimeTxt.setText(FormatUtils.formatChatTime(mRecvChatRspns.getConversations().get(0).getInsertDate()));
                                mRecvMsgAdapter.setNewData(mRecvChatRspns, mMsgMasterId);
                                mRecvMsgAdapter.notifyDataSetChanged();
                                if (scroll)
                                    mRecyclerView.scrollToPosition(listsize - 1);
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgress.setVisibility(View.GONE);
                    String errString = NetUtil.getMessage(error);
                    if (!errString.isEmpty())
                        showToast(R.string.no_internet_conn);
                }
            });
            req.setTag(REQ_TAG_SEND_CHAT);

            VolleySingleton.getInstance().addToRequestQueue(req, true);
        } catch (JSONException | IllegalStateException | NullPointerException e) {

        }
    }


    public void requestChatUpdate(final int msgMasterId) {
        msg = "";
        msg = FormatUtils.getTruncatedStr(mChatInput.getText().toString().trim(), 1000);
        /*msg = StringEscapeUtils.escapeJava();
        if (msg.length() > 1000)
            msg = msg.substring(0, 1000);*/
        //final String msg = FormatUtils.escapeJavaString(mChatInput.getText().toString().trim());
        //Log.d("MISHU**",msgMasterId+"  **  "+FormatUtils.escapeJavaString(msg));
        mChatInput.setText("");
        if (msg.isEmpty() || isBlocked)
            return;

        mProgress.setVisibility(View.VISIBLE);
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);
            jsonBody.put("messageMasterId", msgMasterId);
            jsonBody.put("message", msg);

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                    ConfigData.BASE_URL + "/message/addConversation", jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            enableInput(true);
                            mProgress.setVisibility(View.GONE);
                            String stat = response.optString("status");
                            if (stat.equals("true")) {
                                //todo update text here
                                ReqChatRspns.Conversation conversation = new ReqChatRspns().new Conversation();
                                conversation.setContent(msg);
                                conversation.setFileName("");
                                conversation.setConversationStatus(true);
                                conversation.setUserId(ConfigData.USER_ID);
                                if (mRecvChatRspns.getConversations() != null)
                                    mRecvChatRspns.getConversations().add(conversation);
                                mRecvMsgAdapter.setNewData(mRecvChatRspns, mMsgMasterId);
                                mRecvMsgAdapter.notifyDataSetChanged();
                                mChatInput.setText("");
                                requestSendChatContent(msgMasterId, true, true);
                            } else {
                                if (isAdded())
                                    Toast.makeText(getContext().getApplicationContext(), "  \"" + mChatWithUser + "\" is not available  ", Toast.LENGTH_SHORT).show();
                            }

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    mProgress.setVisibility(View.GONE);
                    enableInput(true);

                    String errString = NetUtil.getMessage(error);
                    if (!errString.isEmpty())
                        showToast(R.string.no_internet_conn);
                }
            });

            VolleySingleton.getInstance().addToRequestQueue(req, true);
        } catch (JSONException | IllegalStateException | NullPointerException e) {

        }
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
    private void showPopupMenu(View view, String link) {
        // inflate menu
        Context wrapper = new ContextThemeWrapper(getContext(), R.style.CustomPopup);
        PopupMenu popup = new PopupMenu(wrapper, view);
        MenuInflater inflater = popup.getMenuInflater();
        if (isBlocked) {
            inflater.inflate(R.menu.menu_unblok, popup.getMenu());
        } else {
            inflater.inflate(R.menu.menu_main, popup.getMenu());
        }
        popup.setOnMenuItemClickListener(new CategoryMenuClickListener(link));
        popup.show();
    }

    /**
     * Click listener for popup menu items
     */
    class CategoryMenuClickListener implements PopupMenu.OnMenuItemClickListener {
        String strLink = "";

        public CategoryMenuClickListener(String link) {
            strLink = link;
        }

        @Override
        public boolean onMenuItemClick(MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.block_user:
                    showBlockDlg();
                    return true;
                case R.id.report:
                    showFeedbackDlg();
                    //requestReportUser();
                    return true;
                default:
            }
            return false;
        }
    }

 /*   PopupWindow popupWindow;

    private void showPopupWind() {
        LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View customView = layoutInflater.inflate(R.layout.popup_chat, null);


        //instantiate popup window
        popupWindow = new PopupWindow(customView, ConstraintLayout.LayoutParams.WRAP_CONTENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);

        //display the popup window
        popupWindow.showAtLocation(mHeadingText, Gravity.END | Gravity.TOP, 0, 0);

    }*/

    ////END////
    /*public void requestReportUser() {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);
            jsonBody.put("reportedUserId", mChatUserId);
            jsonBody.put("userDescription", "I want to block this user.");

        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/admin/reportUser", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.trim().equalsIgnoreCase("true")) {

                            Handler handler = new Handler(Looper.getMainLooper());
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getContext().getApplicationContext(), "  Reported User   " + mChatWithUser + "   ", Toast.LENGTH_SHORT).show();
                                }
                            });
                        } else {
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                        showToast(R.string.no_internet_conn);
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req);
    }*/
    private void showBlockDlg() {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_BLOCK);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        BlockConfirmDialog blockConfirmDialog = new BlockConfirmDialog();
        blockConfirmDialog.setBlockLstnr(new BlockConfirmDialog.BlockListener() {
            @Override
            public void userBlocked() {
                isBlocked = true;
                mChatInput.setEnabled(false);
                mRecyclerView.setAdapter(null);
                mRecyclerView.removeAllViewsInLayout();
                mTimeTxt.setVisibility(View.VISIBLE);
                mTimeTxt.setText("You have blocked this user");
                mTimeTxt.setPadding(12, 32, 12, 2);
                // refreshChat(mMsgMasterId);

            }
        });
        Bundle args = new Bundle();
        args.putInt(BlockConfirmDialog.USER_ID, mChatUserId);
        args.putString(BlockConfirmDialog.USER_NAME, mChatUniqUsr);
        blockConfirmDialog.setArguments(args);
        blockConfirmDialog.show(manager, FRAGMENT_BLOCK);
    }

    private void showFeedbackDlg() {
        FragmentManager manager = getFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_REPORT);
        if (frag != null) {
            manager.beginTransaction().remove(frag).commit();
        }
        EnterFeedbackDlg feedbackDlg = new EnterFeedbackDlg();
        Bundle args = new Bundle();
        args.putInt(EnterFeedbackDlg.USER_ID, mChatUserId);
        feedbackDlg.setArguments(args);
        feedbackDlg.show(manager, FRAGMENT_REPORT);
    }

    private void checkDuplicateLogin(String appid) {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID)
            if (fcmUploaded && appid != null)
                if (!fcmKey.isEmpty())
                    if (!appid.trim().equals(fcmKey.trim())) {
                        mRecyclerView.removeAllViews();
                        if (mMainListener != null)
                            mMainListener.logOut();
                    }
    }

    private void showToast(int resid) {
        if (isAdded())
            Toast.makeText(getContext().getApplicationContext(), resid, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onResume() {
        super.onResume();
        mChatInput.setFocusableInTouchMode(true);
        mChatInput.requestFocus();
        mChatInput.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (mHistoryId != ConfigData.REDIRECT_HOME) {
                            if (mListener != null)
                                mListener.showHistPage(mHistoryId, mHistoryKey);
                        } else {
                            if (mListener != null)
                                mListener.showSendHomePage();
                        }
                        return true;
                    }
                }
                return false;
            }
        });
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        if (mHistoryId != ConfigData.REDIRECT_HOME) {
                            if (mListener != null)
                                mListener.showHistPage(mHistoryId, mHistoryKey);
                        } else {
                            if (mListener != null)
                                mListener.showSendHomePage();
                        }
                        return true;
                    }
                }
                return false;
            }
        });

    }


    /*public void requestDisplayName(int userId) {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"userKeywordId\": \"-100\",\"userId\":" + userId + "}");

            JsonObjectRequest req = new JsonObjectRequest(Request.Method.POST,
                    ConfigData.BASE_URL + "/message/getMatchingInUser", jsonBody,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            RecvHistRspns mRecvHistRspns = new Gson().fromJson(response.toString(), RecvHistRspns.class);
                            mNameText.setText(mRecvHistRspns.getDisplayName());
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    String errString = NetUtil.getMessage(error);
                    if (!errString.isEmpty())
                        showToast(R.string.no_internet_conn);
                }
            });

            VolleySingleton.getInstance().addToRequestQueue(req, true);
        } catch (JSONException | IllegalStateException | NullPointerException e) {

        }
    }*/
}