package com.obics.uno;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
//import android.net.Uri;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
//import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;

//import com.facebook.FacebookSdk;

import android.support.v7.widget.AppCompatImageView;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
//import android.util.Log;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.DisplayCutout;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.crashlytics.android.Crashlytics;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
//import com.facebook.internal.Logger;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
//import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.obics.uno.config.ConfigData;
import com.obics.uno.dialog.EnterPasswordDlg;
import com.obics.uno.dialog.EnterPhoneDlg;
import com.obics.uno.dialog.EnterUniqUsernameDlg;
import com.obics.uno.dialog.LoginDlg;
import com.obics.uno.dialog.LoginSignupDlg;
import com.obics.uno.dialog.NotificationEnableDlg;
import com.obics.uno.dialog.SmsVerifyDlg;
import com.obics.uno.interfaces.PagerFrgmntLstnr;
import com.obics.uno.netutils.JsonToStringRequest;
import com.obics.uno.netutils.NetUtil;
import com.obics.uno.netutils.VolleySingleton;
import com.obics.uno.receive.ReceiveFragment;
import com.obics.uno.send.SendFragment;
import com.obics.uno.service.UnoFirebaseMessagingService;
import com.obics.uno.utils.DatabaseMgr;
import com.obics.uno.utils.FormatUtils;
//import com.obics.uno.utils.TextViewDrawableSize;
//import com.obics.uno.utils.ToolDotProgress;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import me.leolin.shortcutbadger.ShortcutBadger;

public class MainActivity extends AppCompatActivity implements PagerFrgmntLstnr {

    private FirebaseAnalytics mFirebaseAnalytics;
    private ReceiveFragment mChannelFragment;
    private SendFragment mBroadcastFragment;
    private ViewPager mViewPager;
    private TabLayout mTabLayout;
    private static final int TAB_INBOX_POS = 0;
    private static final int TAB_OUTBOX_POS = 1;
    private static final int SCREEN_SETTINGS = 19;
    private static final int SMS_TIMEOUT_SEC = 60;
    private static final int SCREEN_HOME = 17;
    private static int mCurrentScreen = SCREEN_HOME;
    private static final int INTRO_SHOWN = 197;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private boolean update;
    private View settings_placeholder;
    private static final String FRAGMENT_SETTINGS_TAG = "fragment_settings";
    private static final String FRAGMENT_LOGIN_SIGNUP_TAG = "fragment_login_signup";
    private static final String FRAGMENT_ENABLE_NOTIF_TAG = "fragment_login_signup";
    private static final String FRAGMENT_ENTER_PHONE_TAG = "fragment_enter_phone";
    private static final String FRAGMENT_LOGIN_TAG = "fragment_login";
    private static final String FRAGMENT_SMS_VERIFY_TAG = "frag_sms_verify";
    private static final String FRAGMENT_UNIQUE_USERNAME_TAG = "frag_unique_username";
    private static final String FRAGMENT_PASSWORD_TAG = "fragment_password";
    private String countryCodeValue = "";
    private AppCompatImageView mLeftButton, mRightButton;
    private String mtempphone = "", mPhoneNumber = "", mUsername = "", mDisplayName = "", mPassword = "", mLatitude = "", mLongitude = "";
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private static int verifyType = 0;
    private LocationRequest locationRequest;
    private SharedPreferences preferences;
    private BroadcastReceiver mRegistrationBroadcastReceiver = null;
    private final static int REQUEST_LOCATION = 189;
    private final static int REQUEST_CODE_LOCATION = 123;
    private float oldPagerOffset = 0;
    private FragmentManager mFragmentManager;
    AppEventsLogger logger;
    //private View.OnAttachStateChangeListener mAttachListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.UnoTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        mFragmentManager = getSupportFragmentManager();
        getCountryCode();
        Fabric.with(this, new Crashlytics());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        initFirebase();
        createNotificationChannel();
        logger = AppEventsLogger.newLogger(getApplicationContext());
        preferences = PreferenceManager.getDefaultSharedPreferences(UnoApplication.getAppContext());
        boolean first_time = preferences.getBoolean(ConfigData.PREF_FIRST_TIME, true);
        if (first_time)
            showIntroScreen();
        //ask for login
        checkLogin();
        init();
        ////
        setupLocalBroadcastRecvr();
        ////
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (update) {
                    updateLocation(locationResult.getLastLocation());
                } else {
                    mLatitude = "" + locationResult.getLastLocation().getLatitude();
                    mLongitude = "" + locationResult.getLastLocation().getLongitude();
                    //Toast.makeText(getApplicationContext(), "Signuplocfound2", Toast.LENGTH_SHORT).show();

                }
                //showLocProgress(false);
                mFusedLocationClient.removeLocationUpdates(this);
            }
        };
        // ATTENTION: This was auto-generated to handle app links.
        //Intent appLinkIntent = getIntent();
        //String appLinkAction = appLinkIntent.getAction();
        //Uri appLinkData = appLinkIntent.getData();
    }

    private void setupInboxConfig(boolean showInbox) {
        mChannelFragment.setUserVisibleHint(showInbox);
        mBroadcastFragment.setUserVisibleHint(!showInbox);
    }

    private void hideFragmentBtns() {
        mChannelFragment.setUserVisibleHint(false);
        mBroadcastFragment.setUserVisibleHint(false);
    }

    private void init() {
        //mSectionsPagerAdapter = ;
        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.vpager);
        mViewPager.setAdapter(new SectionsPagerAdapter(getSupportFragmentManager()));
        mViewPager.setSaveFromParentEnabled(false);
        mViewPager.setOffscreenPageLimit(1);
        settings_placeholder = findViewById(R.id.setting_plcholder);
        mTabLayout = findViewById(R.id.tabs);
        ViewTreeObserver treeObserver = mTabLayout.getViewTreeObserver();
        treeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @SuppressLint("NewApi")
            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {
                Rect rect = new Rect();
                //mTabLayout.getWindowVisibleDisplayFrame(rect);
                setupPositionAdjustment();
                int screenWidth = mTabLayout.getWidth();
                if (screenWidth < 700)
                    setTabTxtSize();
                //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN)
                mTabLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                // else
                //   mTabLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);

            }
        });
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            boolean isNotIdle = true;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                if (positionOffset != 0.00) {
                    isNotIdle = true;
                    if (positionOffset > 0.5) {
                        mChannelFragment.setMenuVisibility(false);
                        mBroadcastFragment.setMenuVisibility(true);
                    } else {
                        mChannelFragment.setMenuVisibility(true);
                        mBroadcastFragment.setMenuVisibility(false);
                    }
                    hideFragmentBtns();
                    mLeftButton.setVisibility(ConstraintLayout.VISIBLE);
                    mRightButton.setVisibility(ConstraintLayout.VISIBLE);
                } else {
                    isNotIdle = false;
                    if (oldPagerOffset != 0.0f) {
                        if (position == TAB_INBOX_POS) {
                            setupInboxConfig(true);
                        } else {
                            setupInboxConfig(false);
                        }
                    }
                }
                oldPagerOffset = positionOffset;
            }

            @Override
            public void onPageSelected(int position) {
                hideSoftKeyboard();
            }

            @Override
            public void onPageScrollStateChanged(int state) {//HIDE  BUTTONS
                switch (state) {
                    case ViewPager.SCROLL_STATE_IDLE:
                        mLeftButton.setVisibility(ConstraintLayout.GONE);
                        mRightButton.setVisibility(ConstraintLayout.GONE);
                        break;
                    case ViewPager.SCROLL_STATE_SETTLING:
                        mLeftButton.setVisibility(ConstraintLayout.VISIBLE);
                        mRightButton.setVisibility(ConstraintLayout.VISIBLE);
                        break;
                    case ViewPager.SCROLL_STATE_DRAGGING:
                        if (isNotIdle)
                            hideFragmentBtns();
                        break;
                    default:
                        break;
                }
               /* if (state == ViewPager.SCROLL_STATE_IDLE) {
                    mLeftButton.setVisibility(ConstraintLayout.GONE);
                    mRightButton.setVisibility(ConstraintLayout.GONE);
                } else if (state == ViewPager.SCROLL_STATE_SETTLING) {
                    mLeftButton.setVisibility(ConstraintLayout.VISIBLE);
                    mRightButton.setVisibility(ConstraintLayout.VISIBLE);
                } else if (state == ViewPager.SCROLL_STATE_DRAGGING) {
                    if (isNotIdle)
                        hideFragmentBtns();
                }*/
            }
        });
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        // backView = findViewById(R.id.backvw);
        // backView
        //backView.setBackgroundResource(R.drawable.splash_bg);
        //backView.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {//nougat and up
            //CoordinatorLayout coor = findViewById(R.id.coordlay);
            //coor.setBackgroundResource(R.drawable.splash_bg);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        } else {//FOR ALL BELOW NOUGAT

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                if (Build.VERSION.SDK_INT == Build.VERSION_CODES.M) { //FOR MARSHMALLOW
                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                    window.setStatusBarColor(Color.TRANSPARENT);
                } else {//FOR LOLLIPOP
                    window.setStatusBarColor(Color.parseColor(ConfigData.COLOR_DARK_TRANSPARENCY));
                }
            }
        }
        mChannelFragment = new ReceiveFragment();
        mBroadcastFragment = new SendFragment();
        initTabView();
        showHomePage();
        mLeftButton = findViewById(R.id.topbarleftbtn);
        mRightButton = findViewById(R.id.topbarrightbtn);
    }

    /**
     */
    class SectionsPagerAdapter extends FragmentStatePagerAdapter {
        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case TAB_INBOX_POS:
                    return mChannelFragment;
                case TAB_OUTBOX_POS:
                    return mBroadcastFragment;
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

    public void actRecreate() {
        checkLogin();
        mChannelFragment.updateReceiveHome(true, 0);
        mBroadcastFragment.updateSendHome(true, 0);
        showTabNotifyCount(TAB_INBOX_POS, 0);
        showTabNotifyCount(TAB_OUTBOX_POS, 0);
    }

    private void setupTopCount() {
        int recvCount = preferences.getInt(ConfigData.PREF_RECV_NOTIF_COUNT, 0);
        int sendCount = preferences.getInt(ConfigData.PREF_SEND_NOTIF_COUNT, 0);
        showTabNotifyCount(TAB_INBOX_POS, recvCount);
        showTabNotifyCount(TAB_OUTBOX_POS, sendCount);
        ShortcutBadger.applyCount(UnoApplication.getAppContext(), recvCount + sendCount);

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // getWindow().getDecorView().getRootView().addOnAttachStateChangeListener(mAttachListener);
        if (mRegistrationBroadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(ConfigData.NOTIFICATION_COUNT));
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(ConfigData.LOCATION_CHANGED));
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;
        try {
            notificationManager.cancel(UnoFirebaseMessagingService.uniqueid);
        } catch (NullPointerException npe) {
            npe.printStackTrace();
        }
        setupTopCount();
        final int count = UnoFirebaseMessagingService.msgCount;
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID && count > 0) {
            if ((getIntent().getExtras() != null)) {
                try {
                    String notifyRedir = "";
                    notifyRedir = getIntent().getExtras().getString(ConfigData.NOTIFY_EXTRA_STR);
                    getIntent().putExtra(ConfigData.NOTIFY_EXTRA_STR, "");
                    if (count > 1) {
                        if (notifyRedir != null) {
                            if (!notifyRedir.trim().isEmpty()) {
                                mChannelFragment.setupRedirectData("");
                                mBroadcastFragment.setupRedirectData("");
                            }
                        }

                    } else {
                        if (notifyRedir != null) {
                            if (!notifyRedir.trim().isEmpty()) {
                                JSONObject jobj = new JSONObject(notifyRedir);
                                String type = jobj.optString("dataType");
                                switch (type) {
                                    case "RC":
                                        mViewPager.setCurrentItem(TAB_OUTBOX_POS, false);
                                        mBroadcastFragment.setupRedirectData(notifyRedir);
                                        break;
                                    case "SC":
                                    case "RH":
                                        mViewPager.setCurrentItem(TAB_INBOX_POS, false);
                                        mChannelFragment.setupRedirectData(notifyRedir);
                                        break;
                                }
                            }
                        }
                    }
                } catch (Exception e) {

                }
            }
            UnoFirebaseMessagingService.msgCount = 0;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (NotificationManagerCompat.from(getApplicationContext()).areNotificationsEnabled()) {
                List<NotificationChannel> notificationChannels = notificationManager.getNotificationChannels();
                if (notificationChannels.get(0).getImportance() == NotificationManager.IMPORTANCE_NONE) {
                    showNotificationDlg(ConfigData.CHANNEL_ID);
                }
            } else
                showNotificationDlg("");
        } else {
            if (!NotificationManagerCompat.from(getApplicationContext()).areNotificationsEnabled())
                showNotificationDlg("");
        }

    }


    private void setupLocalBroadcastRecvr() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(ConfigData.NOTIFICATION_COUNT)) {
                    String str = intent.getStringExtra(ConfigData.NOTIFICATION_COUNT_TYPE);
                    int msg_master_id = intent.getIntExtra(ConfigData.NOTIFICATION_MSG_MASTER, 0);
                    if (str.equalsIgnoreCase(ConfigData.INTENT_DATA_SEND)) {
                        mBroadcastFragment.updateSendHome(false, msg_master_id);
                    } else if (str.equalsIgnoreCase(ConfigData.INTENT_DATA_RECV)) {
                        mChannelFragment.updateReceiveHome(false, msg_master_id);
                    }
                    setupTopCount();
                } else if (intent.getAction().equals(ConfigData.LOCATION_CHANGED)) {
                    //update map location here
                    if (!intent.getBooleanExtra(ConfigData.INTENT_EXTRA_LOC_ZIP, false))
                        upDateSettings(true);
                }
            }
        };
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRegistrationBroadcastReceiver != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        // getWindow().getDecorView().getRootView().removeOnAttachStateChangeListener(mAttachListener);

    }

    private void initTabView() {
        setupTabBackground();
        final ViewGroup tabStrip = (ViewGroup) mTabLayout.getChildAt(0);
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                View tabView = tabStrip.getChildAt(tab.getPosition());
                switch (tab.getPosition()) {
                    case TAB_INBOX_POS:
                        tabView.setBackgroundResource(R.drawable.selectedintab);
                        break;
                    case TAB_OUTBOX_POS:
                        tabView.setBackgroundResource(R.drawable.selectedouttab);
                        break;
                }
                if (mCurrentScreen == SCREEN_SETTINGS) {
                    showHomePage();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                View tabView = tabStrip.getChildAt(tab.getPosition());
                tabView.setBackgroundResource(R.drawable.unselectedtab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (mCurrentScreen == SCREEN_SETTINGS) {
                    View tabView = tabStrip.getChildAt(tab.getPosition());
                    switch (tab.getPosition()) {
                        case TAB_INBOX_POS:
                            tabView.setBackgroundResource(R.drawable.selectedintab);
                            break;
                        case TAB_OUTBOX_POS:
                            tabView.setBackgroundResource(R.drawable.selectedouttab);
                            break;
                    }
                    showHomePage();
                } else {
                    hideSoftKeyboard();
                    switch (tab.getPosition()) {
                        case TAB_INBOX_POS:
                            mChannelFragment.setupRedirectData(ConfigData.REDIRECT_DATA_RECV);
                            break;
                        case TAB_OUTBOX_POS:
                            mBroadcastFragment.setupRedirectData(ConfigData.REDIRECT_DATA_SEND);
                            break;
                    }
                }
            }
        });
    }

    private void showSettingsPage() {
        mCurrentScreen = SCREEN_SETTINGS;
        settings_placeholder.setVisibility(View.VISIBLE);
        final ViewGroup tabStrip = (ViewGroup) mTabLayout.getChildAt(0);
        unSelectAllTabs(tabStrip);
        SettingsFragment frgmntSettings = new SettingsFragment();
        frgmntSettings.setSettingsFragmentListener(new SettingsFragment.SettingsFragmentListener() {
            @Override
            public void enableLocation() {
                enableLoc(true);
            }

            @Override
            public void removeLocationUpdates() {
                removeLocationUpdateCb();
                //showLocProgress(false);
            }

            @Override
            public void closeFragment() {
                showHomePage();
            }
        });
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_from_top,
                R.anim.slide_to_top);
        transaction.replace(R.id.container, frgmntSettings, FRAGMENT_SETTINGS_TAG).commit();
    }

    private void showHomePage() {
        mCurrentScreen = SCREEN_HOME;
        removeSettingsFrag();
        settings_placeholder.setVisibility(ConstraintLayout.GONE);
        setupTabBackground();
    }

    private void hideSoftKeyboard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            WeakReference<View> tview = new WeakReference<>(getCurrentFocus());
            if (tview.get() == null) {
                tview = new WeakReference<>(new View(MainActivity.this));
            }
            imm.hideSoftInputFromWindow(tview.get().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    private void removeSettingsFrag() {
        FragmentManager manager = getSupportFragmentManager();
        Fragment frag = manager.findFragmentByTag(FRAGMENT_SETTINGS_TAG);
        if (frag != null) {
            hideSoftKeyboard();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(R.anim.slide_from_top,
                    R.anim.slide_to_top);
            transaction.remove(frag).commit();
        }
    }

    @Override
    public void onBackPressed() {
        switch (mCurrentScreen) {
            case SCREEN_SETTINGS:
                showHomePage();
                break;
            case SCREEN_HOME:
            default:
                super.onBackPressed();
                break;
        }
    }

    private void setTabTxtSize() {
        ViewGroup tabStrip = (ViewGroup) mTabLayout.getChildAt(0);
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            View tabView = tabStrip.getChildAt(i);
            if (tabView != null) {
                View vb = mTabLayout.getTabAt(i).getCustomView();
                if (vb != null) {
                    TextView tv = tabView.findViewById(R.id.tabtxt);
                    AppCompatImageView tv2 = tabView.findViewById(R.id.tabicontxt);
                    TextView tv3 = tabView.findViewById(R.id.tabext);
                    tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14.5f);
                    ConstraintLayout.LayoutParams clp = (ConstraintLayout.LayoutParams) tv2.getLayoutParams();
                    clp.height = FormatUtils.spToPx(MainActivity.this, 14f);
                    clp.width = ConstraintLayout.LayoutParams.WRAP_CONTENT;
                    tv2.setLayoutParams(clp);
                    ConstraintLayout.LayoutParams clps = (ConstraintLayout.LayoutParams) tv3.getLayoutParams();
                    clps.rightMargin = FormatUtils.dpToPx(MainActivity.this, 5);
                    tv3.setLayoutParams(clps);
                }
            }
        }
    }

    private void unSelectAllTabs(ViewGroup tabStrip) {
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            View tabView = tabStrip.getChildAt(i);
            if (tabView != null) {
                mTabLayout.getTabAt(i).setCustomView(R.layout.custom_tab);
                tabView.setBackgroundResource(R.drawable.unselectedtab);
                TextView tabText = tabView.findViewById(R.id.tabtxt);
                AppCompatImageView tabImg = tabView.findViewById(R.id.tabicontxt);
                if (i == TAB_INBOX_POS) {
                    tabText.setText(R.string.tab_text_1);
                    tabImg.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_channel_tab));
                } else if (i == TAB_OUTBOX_POS) {
                    tabText.setText(R.string.tab_text_2);
                    tabImg.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_broadcast_tab));
                }
            }
        }
    }

    private void setupTabBackground() {
        final ViewGroup tabStrip = (ViewGroup) mTabLayout.getChildAt(0);
        unSelectAllTabs(tabStrip);
        int position = mTabLayout.getSelectedTabPosition();
        View tabView = tabStrip.getChildAt(position);
        if (position == TAB_INBOX_POS) {
            tabView.setBackgroundResource(R.drawable.selectedintab);
            mChannelFragment.setUserVisibleHint(true);
            mBroadcastFragment.setUserVisibleHint(false);
        } else {
            tabView.setBackgroundResource(R.drawable.selectedouttab);
            mChannelFragment.setUserVisibleHint(false);
            mBroadcastFragment.setUserVisibleHint(true);
        }
    }

    private void showTabNotifyCount(int index, int count) {
        ViewGroup tabStrip = (ViewGroup) mTabLayout.getChildAt(0);
        View tabView = tabStrip.getChildAt(index);
        if (tabView != null) {
            TextView tv = tabView.findViewById(R.id.tabext);
            if (count > 0) {
                tv.setVisibility(ConstraintLayout.VISIBLE);
                tv.setText("" + count);
            } else {
                tv.setVisibility(ConstraintLayout.GONE);
            }
        }
    }

    private void storeNotificationCount(int type, int newcount) {
        SharedPreferences.Editor editor = preferences.edit();
        switch (type) {
            case ConfigData.TYPE_RECV:
                editor.putInt(ConfigData.PREF_RECV_NOTIF_COUNT, newcount);
                break;
            case ConfigData.TYPE_SEND:
                editor.putInt(ConfigData.PREF_SEND_NOTIF_COUNT, newcount);
                break;
        }
        editor.apply();
        setupTopCount();
    }

    ///listener methods
    @Override
    public void decrReceiveCount(int count) {
        int kount = preferences.getInt(ConfigData.PREF_RECV_NOTIF_COUNT, 0);
        int dcount = kount - count;
        showTabNotifyCount(TAB_INBOX_POS, dcount);
        storeNotificationCount(ConfigData.TYPE_RECV, dcount);
    }

    @Override
    public void decrSendCount(int count) {
        int kount = preferences.getInt(ConfigData.PREF_SEND_NOTIF_COUNT, 0);
        int dcount = kount - count;
        showTabNotifyCount(TAB_OUTBOX_POS, dcount);
        storeNotificationCount(ConfigData.TYPE_SEND, dcount);

    }

    @Override
    public void showReceiveCount(int count) {
        showTabNotifyCount(TAB_INBOX_POS, count);
        storeNotificationCount(ConfigData.TYPE_RECV, count);
    }

    @Override
    public void showSendCount(int count) {
        showTabNotifyCount(TAB_OUTBOX_POS, count);
        storeNotificationCount(ConfigData.TYPE_SEND, count);
    }

    @Override
    public void setPlaceholderButtons(int leftResource, int rightResource) {
        if (mLeftButton != null && mRightButton != null) {
            mLeftButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, leftResource));
            mRightButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, rightResource));
        }
    }

    @Override
    public void showLogin() {
        showLoginSignupDlg();
    }

    @Override
    public void showSettings() {
        if (ConfigData.USER_ID != ConfigData.INVALID_USER_ID) {
            FragmentManager manager = getSupportFragmentManager();
            Fragment frag = manager.findFragmentByTag(FRAGMENT_SETTINGS_TAG);
            if (frag == null)
                showSettingsPage();
        }
    }

    @Override
    public void logOut() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(ConfigData.PREF_IS_LOGGED, false);
        editor.putBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, false);
        editor.putInt(ConfigData.PREF_USER_ID, ConfigData.INVALID_USER_ID);
        editor.putString(ConfigData.PREF_PROFILE_PIC, "");
        editor.putString(ConfigData.PREF_USERNAME, "");
        editor.putString(ConfigData.PREF_PHONE_NUMBER, "");
        editor.putString(ConfigData.PREF_DISPLAY_NAME, "");
        editor.putString(ConfigData.LOGIN_LATITUDE, "");
        editor.putString(ConfigData.LOGIN_LONGITUDE, "");
        editor.putString(ConfigData.PREF_ZIPCODE, "");
        editor.putString(ConfigData.ZIPCODE_LATITUDE, "");
        editor.putString(ConfigData.ZIPCODE_LONGITUDE, "");
        editor.putString(ConfigData.PREF_LATITUDE, "");
        editor.putString(ConfigData.PREF_LONGITUDE, "");
        editor.putString(ConfigData.PREF_WHERE_FROM, "");
        editor.putString(ConfigData.PREF_ABOUT_ME, "");
        editor.putInt(ConfigData.PREF_RECV_NOTIF_COUNT, 0);
        editor.putInt(ConfigData.PREF_SEND_NOTIF_COUNT, 0);
        editor.putBoolean(ConfigData.NEWS_FIRST_TIME, true);
        editor.apply();
        ShortcutBadger.applyCount(UnoApplication.getAppContext(), 0);
        if (mCurrentScreen == SCREEN_SETTINGS) {
            showHomePage();
        }
        showReceiveCount(0);
        showSendCount(0);
        ConfigData.USER_ID = ConfigData.INVALID_USER_ID;
        DatabaseMgr datastore = new DatabaseMgr(MainActivity.this);
        datastore.deleteAll();
        Toast.makeText(getApplicationContext(), R.string.force_logout, Toast.LENGTH_LONG).show();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                actRecreate();
            }
        });
    }

    private void checkLogin() {
        boolean logged = preferences.getBoolean(ConfigData.PREF_IS_LOGGED, false);
        if (!logged) {
            showLoginSignupDlg();
        } else {
            ConfigData.USER_ID = preferences.getInt(ConfigData.PREF_USER_ID, 0);
            ConfigData.PROFILE_PIC = preferences.getString(ConfigData.PREF_PROFILE_PIC, "");
            ConfigData.PROFILE_USERNAME = preferences.getString(ConfigData.PREF_USERNAME, "");

            if (!preferences.getBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, false))
                requestFcmTokenUpdate();

            //todo check for location update
            if (preferences.getBoolean(ConfigData.PREF_USE_LOCATION, true)) {
                try {
                    double oldLatitude = Double.parseDouble(preferences.getString(ConfigData.PREF_LATITUDE, ""));
                    double oldLongitude = Double.parseDouble(preferences.getString(ConfigData.PREF_LONGITUDE, ""));
                    if (oldLatitude == 0 && oldLongitude == 0)
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                getLocation(true);
                            }
                        });
                } catch (Exception e) {

                }
            }

        }
    }

    public void requestFcmTokenUpdate() {
        JSONObject jsonBody = null;
        final String token = preferences.getString(ConfigData.PREF_FCM_TOKEN, "");
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userId", ConfigData.USER_ID);
            jsonBody.put("userAppId", token);
        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/notify/updateappid", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.trim().equalsIgnoreCase("true")) {
                            saveTokenUploaded(true);
                            // Log.d("Firebase****", "uploaded  " + token);
                        } else if (response.trim().equalsIgnoreCase("false")) {
                            //todo save the data
                            saveTokenUploaded(false);
                            //Log.d("Firebase****", "not******uploaded");
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                saveTokenUploaded(false);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private void removeFragmentByTag(String tag) {
        Fragment frag = mFragmentManager.findFragmentByTag(tag);
        if (frag != null) {
            mFragmentManager.beginTransaction().remove(frag).commitAllowingStateLoss();
        }
    }

    private void showPinonVerify(String pin) {
        Fragment frag = mFragmentManager.findFragmentByTag(FRAGMENT_SMS_VERIFY_TAG);
        if (frag != null) {
            SmsVerifyDlg dlg = (SmsVerifyDlg) frag;
            dlg.setPinTxt(pin);
        }
    }

    private void showPinInvalid(boolean valid) {
        Fragment frag = mFragmentManager.findFragmentByTag(FRAGMENT_SMS_VERIFY_TAG);
        if (frag != null) {
            SmsVerifyDlg dlg = (SmsVerifyDlg) frag;
            dlg.setInvalidtxt(valid);
        }
    }

    //FIRST LOGIN DLG
    private void showLoginSignupDlg() {
        removeFragmentByTag(FRAGMENT_LOGIN_SIGNUP_TAG);
        LoginSignupDlg loginSignupDlg = new LoginSignupDlg();
        loginSignupDlg.setLoginSignupLstnr(new LoginSignupDlg.LoginSignupLstnr() {
            @Override
            public void clickLogin() {
                /////////////
                showEnterLoginDlg();
            }

            @Override
            public void clickSignup() {
                startSignupProcess();
            }
        });
        loginSignupDlg.show(mFragmentManager, FRAGMENT_LOGIN_SIGNUP_TAG);
    }

    //second login dialog
    private void showEnterLoginDlg() {
        //remove the previous fragment???
        removeFragmentByTag(FRAGMENT_LOGIN_SIGNUP_TAG);
        removeFragmentByTag(FRAGMENT_LOGIN_TAG);
        LoginDlg loginDlg = new LoginDlg();
        Bundle args = new Bundle();
        args.putString(LoginDlg.COUNTRY_CODE, countryCodeValue);
        loginDlg.setArguments(args);
        loginDlg.setLoginLstnr(new LoginDlg.LoginLstnr() {
            @Override
            public void clickLogin() {
            }

            @Override
            public void clickForgotPassword() {
                showEnterPhoneDlg(EnterPhoneDlg.TYPE_RESET);
            }

            @Override
            public void goBack() {
                hideSoftKeyboard();
                showLoginSignupDlg();
            }
        });
        loginDlg.show(mFragmentManager, FRAGMENT_LOGIN_TAG);
    }

    //second phone dialog
    private void showEnterPhoneDlg(final int verifyType) {
        //remove the previous fragment???
        MainActivity.verifyType = verifyType;
        removeFragmentByTag(FRAGMENT_LOGIN_TAG);
        removeFragmentByTag(FRAGMENT_LOGIN_SIGNUP_TAG);
        removeFragmentByTag(FRAGMENT_ENTER_PHONE_TAG);
        EnterPhoneDlg enterPhoneDlg = new EnterPhoneDlg();
        Bundle args = new Bundle();
        args.putInt(EnterPhoneDlg.ARG_DIALOG_TYPE, verifyType);
        args.putString(EnterPhoneDlg.COUNTRY_CODE, countryCodeValue);

        enterPhoneDlg.setArguments(args);
        enterPhoneDlg.setEnterPhnDlgLstnr(new EnterPhoneDlg.EnterPhnDlgLstnr() {
            @Override
            public void clickSendSMS(String phoneno) {
                MainActivity.verifyType = EnterPhoneDlg.TYPE_SIGNUP;
                startPhoneNumberVerification(phoneno);
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.SUCCESS, "true");
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, phoneno);
                bundle.putString(FirebaseAnalytics.Param.CREATIVE_NAME, "Phone Entered");

                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);

                Bundle params = new Bundle();
                params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, "phone_entered");
                logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);


                showSmsVerifyDlg(phoneno, "", verifyType);
            }

            @Override
            public void goBack() {
                showLoginSignupDlg();
            }

            @Override
            public void clickResetPasswd(String phoneno) {
                MainActivity.verifyType = EnterPhoneDlg.TYPE_RESET;
                startPhoneNumberVerification(phoneno);
                showSmsVerifyDlg(phoneno, "", EnterPhoneDlg.TYPE_RESET);
            }

            /*@Override
            public void clickCall() {
                //TODO CALL PHONE TO VERIFY
            }*/
        });
        enterPhoneDlg.show(mFragmentManager, FRAGMENT_ENTER_PHONE_TAG);
    }

    private void showSmsVerifyDlg(String phoneno, String smscode, final int verifyType) {
        removeFragmentByTag(FRAGMENT_ENTER_PHONE_TAG);
        removeFragmentByTag(FRAGMENT_SMS_VERIFY_TAG);
        mtempphone = phoneno;
        SmsVerifyDlg smsVerifyDlg = new SmsVerifyDlg();
        Bundle args = new Bundle();
        args.putString(SmsVerifyDlg.ARG_PHONE_NUMBER, phoneno);
        args.putString(SmsVerifyDlg.ARG_SMS_CODE, smscode);
        smsVerifyDlg.setArguments(args);
        smsVerifyDlg.setSmsVerifyDlgLstnr(new SmsVerifyDlg.SmsVerifyDlgLstnr() {
            @Override
            public void otpEntered(String str) {
                //START SMS VERIFY
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.SUCCESS, "true");
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Otp Received");

                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);

                Bundle params = new Bundle();
                params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, "otp received");
                logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);


                try {
                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, str);
                    signInWithPhoneAuthCredential(credential, verifyType);
                } catch (IllegalArgumentException er) {
                    Toast.makeText(getApplicationContext(), R.string.phone_auth_err, Toast.LENGTH_SHORT).show();
                    showPinInvalid(false);
                }
            }

            @Override
            public void resendOtp(String phoneNumber) {
                resendVerificationCode(phoneNumber, mResendToken);
            }
        });
        smsVerifyDlg.show(mFragmentManager, FRAGMENT_SMS_VERIFY_TAG);
    }

    private void showEnterUsernameDlg() {
        removeFragmentByTag(FRAGMENT_UNIQUE_USERNAME_TAG);
        EnterUniqUsernameDlg enterUniqUsernameDlg = new EnterUniqUsernameDlg();
        enterUniqUsernameDlg.setEnterUniqUsernameLstnr(new EnterUniqUsernameDlg.EnterUniqUsernameLstnr() {
            @Override
            public void userNameVerified(String username) {
                mUsername = username;
                mDisplayName = username;
                showEnterPasswordDlg();
            }
        });
        enterUniqUsernameDlg.show(mFragmentManager, FRAGMENT_UNIQUE_USERNAME_TAG);
    }

    private void showEnterPasswordDlg() {
        removeFragmentByTag(FRAGMENT_PASSWORD_TAG);
        EnterPasswordDlg enterPasswordDlg = new EnterPasswordDlg();
        Bundle args = new Bundle();
        if (verifyType == EnterPhoneDlg.TYPE_SIGNUP) {
            args.putInt(EnterPasswordDlg.ARG_DLG_TYPE, EnterPasswordDlg.TYPE_SET_PASSWD);
        } else if (verifyType == EnterPhoneDlg.TYPE_RESET) {
            args.putInt(EnterPasswordDlg.ARG_DLG_TYPE, EnterPasswordDlg.TYPE_RESET_PASSWD);
        }
        enterPasswordDlg.setArguments(args);
        enterPasswordDlg.setEnterPasswdLstnr(new EnterPasswordDlg.EnterPasswdLstnr() {
            @Override
            public void clickPositive(String pass) {
                if (verifyType == EnterPhoneDlg.TYPE_SIGNUP) {
                    mPassword = pass;


                    requestSignup();
                } else if (verifyType == EnterPhoneDlg.TYPE_RESET) {
                    //TODO requestPASSWORD UPDATE()
                    requestPasswordReset(mPhoneNumber, pass);
                }
            }
        });
        enterPasswordDlg.show(mFragmentManager, FRAGMENT_PASSWORD_TAG);
    }


    public void requestSignup() {
        JSONObject jsonBody = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        View view = getLayoutInflater().inflate(R.layout.common_info_dlg, null);
        TextView tv = view.findViewById(R.id.bottom_txt);
        //ToolDotProgress mProgress = view.findViewById(R.id.progressBar);
        tv.setText(R.string.signing_up);
        builder.setView(view);
        //requestLogin(autouser, autopass);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        try {
            jsonBody = new JSONObject();
            jsonBody.put("userName", mUsername);
            jsonBody.put("displayName", mDisplayName);
            jsonBody.put("password", mPassword);
            jsonBody.put("latitude", mLatitude);
            jsonBody.put("longtitude", mLongitude);
            jsonBody.put("phoneNumber", mPhoneNumber);
            jsonBody.put("userAppId", preferences.getString(ConfigData.PREF_FCM_TOKEN, ""));

        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/user/signup", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        if (response.trim().equalsIgnoreCase("true")) {
                            Bundle bundle = new Bundle();
                            bundle.putString(FirebaseAnalytics.Param.SUCCESS, "true");
                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, mUsername);
                            Bundle params = new Bundle();
                            params.putString(AppEventsConstants.EVENT_PARAM_REGISTRATION_METHOD, "sign_up");
                            logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION, params);
                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SIGN_UP, bundle);
                            Toast.makeText(getApplicationContext(), R.string.signup_success, Toast.LENGTH_SHORT).show();
                            autoLoginRequest(mUsername, mPassword);
                        } else if (response.trim().equalsIgnoreCase("false")) {
                            //todo save the data
                            Toast.makeText(getApplicationContext(), R.string.signup_failed, Toast.LENGTH_SHORT).show();
                            actRecreate();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
                //actRecreate();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private void autoLoginRequest(String phone, String passwd) {
        removeFragmentByTag(FRAGMENT_LOGIN_TAG);
        LoginDlg loginDlg = new LoginDlg();
        Bundle args = new Bundle();
        args.putString(LoginDlg.AUTOLOG_USER, phone);
        args.putString(LoginDlg.AUTOLOG_PASS, passwd);
        //args.putString(LoginDlg.COUNTRY_CODE, countryCodeValue);
        loginDlg.setArguments(args);
        loginDlg.show(mFragmentManager, FRAGMENT_LOGIN_TAG);
    }

    public void requestPasswordReset(final String phone, final String passwd) {
        JSONObject jsonBody = null;
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        View view = getLayoutInflater().inflate(R.layout.common_info_dlg, null);
        TextView tv = view.findViewById(R.id.bottom_txt);
        //ToolDotProgress mProgress = view.findViewById(R.id.progressBar);
        tv.setText(R.string.resetting);
        builder.setView(view);
        //requestLogin(autouser, autopass);
        final Dialog dialog = builder.create();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        try {
            jsonBody = new JSONObject();
            jsonBody.put("password", passwd);
            jsonBody.put("phoneNumber", phone);
        } catch (JSONException e) {
        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/user/changePassword", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        if (response.trim().equalsIgnoreCase("true")) {
                            Toast.makeText(getApplicationContext(), R.string.passwd_success, Toast.LENGTH_SHORT).show();
                            autoLoginRequest(phone, passwd);
                        } else if (response.trim().equalsIgnoreCase("false")) {
                            //todo save the data
                            Toast.makeText(getApplicationContext(), R.string.passwd_failed, Toast.LENGTH_SHORT).show();
                            actRecreate();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
                //actRecreate();
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    /*private void showNetworkUnavailable() {
        if (!isFinishing()) {
            removeFragmentByTag("netdlg");
            NetworkConnDlg networkConnDlg = new NetworkConnDlg();
            networkConnDlg.show(mFragmentManager, "netdlg");
        }
    }*/
    private void startSignupProcess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                getLocation(false);
            }
        });
        mPhoneNumber = "";
        mUsername = "";
        mDisplayName = "";
        mPassword = "";
        mLatitude = "0.0";
        mLongitude = "0.0";
        showEnterPhoneDlg(EnterPhoneDlg.TYPE_SIGNUP);
    }

    private void checkLocAccuracyUpdate(final boolean fromSettings) {
        int locationState = getLocAccuracy();
        if (locationState != ConfigData.LOC_TURNED_OFF) {
            if (locationState == ConfigData.LOC_HIGH_ACCURACY) {
                getLocation(true);
            } else {
                if (mAccuracyCount == 0) {
                    enableLoc(fromSettings);
                    Toast.makeText(getApplicationContext(), R.string.loc_error1, Toast.LENGTH_SHORT).show();
                    mAccuracyCount++;
                } else {
                    Toast.makeText(getApplicationContext(), R.string.loc_error2, Toast.LENGTH_SHORT).show();
                    mAccuracyCount = 0;
                }
            }
        } else {
            Toast.makeText(getApplicationContext(), R.string.loc_error3, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SettingsFragment.REQUEST_LOCATION) {
            FragmentManager manager = getSupportFragmentManager();
            Fragment frag = manager.findFragmentByTag(FRAGMENT_SETTINGS_TAG);
            if (frag != null)
                frag.onActivityResult(requestCode, resultCode, data);
            else {
                checkLocAccuracyUpdate(true);
            }
        } else {
            switch (requestCode) {
                case REQUEST_LOCATION:
                    checkLocAccuracyUpdate(false);
                    break;
                case INTRO_SHOWN:
                    if (resultCode == RESULT_OK) {
                        int res = data.getIntExtra(WelcomeActivity.REQUEST_INTRO_PAGE, 0);
                        switch (res) {
                            case WelcomeActivity.OPTION_LOGIN:
                                showEnterLoginDlg();
                                break;
                            case WelcomeActivity.OPTION_SIGNUP:
                                startSignupProcess();
                                break;
                        }

                    }
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /////////////////FIREBASE///////////////////////

    private void initFirebase() {
        FirebaseApp.initializeApp(UnoApplication.getAppContext());
        mAuth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // 1 - Instant verification.  2 - Auto-retrieval.
                if (!TextUtils.isEmpty(credential.getSmsCode()))
                    Toast.makeText(getApplicationContext(), R.string.sms_detected, Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getApplicationContext(), R.string.sms_auto_verified, Toast.LENGTH_SHORT).show();
                showPinonVerify(credential.getSmsCode());
                showPinInvalid(false);
                signInWithPhoneAuthCredential(credential, verifyType);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                //Toast.makeText(getApplicationContext(), e.getMessage()+"", Toast.LENGTH_SHORT).show();
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    Toast.makeText(getApplicationContext(), "   Verification Failed! Invalid Phone or SMS Code   ", Toast.LENGTH_SHORT).show();
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Toast.makeText(getApplicationContext(), "   Verification Failed! Too many requests. Try after some time.   ", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                Toast.makeText(getApplicationContext(), R.string.verify_req_sent, Toast.LENGTH_SHORT).show();
                showPinInvalid(false);
                mVerificationId = verificationId;
                mResendToken = token;
            }
        };
    }


    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential, final int verifytype) {
        showPinInvalid(false);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), R.string.pin_verify_success, Toast.LENGTH_SHORT).show();
                            removeFragmentByTag(FRAGMENT_SMS_VERIFY_TAG);
                            mPhoneNumber = mtempphone;
                            if (verifytype == EnterPhoneDlg.TYPE_SIGNUP)
                                showEnterUsernameDlg();
                            else if (verifytype == EnterPhoneDlg.TYPE_RESET)
                                showEnterPasswordDlg();
                            //FirebaseUser user = task.getResult().getUser();
                        } else {
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                showPinInvalid(true);
                                //Toast.makeText(getApplicationContext(), "   Pin Failed   ", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        if (!TextUtils.isEmpty(phoneNumber))
            PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, SMS_TIMEOUT_SEC, TimeUnit.SECONDS, MainActivity.this, mCallbacks);
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        if (!TextUtils.isEmpty(phoneNumber))
            PhoneAuthProvider.getInstance().verifyPhoneNumber(phoneNumber, SMS_TIMEOUT_SEC, TimeUnit.SECONDS, MainActivity.this, mCallbacks, token);             // ForceResendingToken from callbacks
    }

    ////////////////////////////////////////////////

    //Location code
    private void getCurrentLocation(boolean update_loc) {
        update = update_loc;
        try {
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                if (update) {
                                    updateLocation(location);
                                } else {
                                    mLatitude = "" + location.getLatitude();
                                    mLongitude = location.getLongitude() + "";
                                    //Toast.makeText(getApplicationContext(), "Signuplocfound", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), R.string.fetching_loc, Toast.LENGTH_LONG).show();
                                //showLocProgress(true);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), R.string.failed_loc_fetch, Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    });
            mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback,
                    Looper.myLooper());
        } catch (SecurityException err) {

        }
    }

    private void removeLocationUpdateCb() {
        try {
            if (mFusedLocationClient != null)
                mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        } catch (Exception e) {

        }
    }

    private void updateLocation(Location location) {
        float[] distance = new float[1];
        Location.distanceBetween(SettingsFragment.oldLatitude, SettingsFragment.oldLongitude, location.getLatitude(), location.getLongitude(), distance);
        if (distance[0] > 5.0) {
            requestLocationUpdate(location);
        } else {
            Toast.makeText(getApplicationContext(), R.string.updated_new_loc, Toast.LENGTH_SHORT).show();
        }
    }

    /*private void showLocProgress(boolean show) {
        FragmentManager manager = getSupportFragmentManager();
        SettingsFragment frag = (SettingsFragment) manager.findFragmentByTag(FRAGMENT_SETTINGS_TAG);
        if (frag != null)
            frag.showLocProgress(show);
    }*/

    private void upDateSettings(boolean updated) {
        FragmentManager manager = getSupportFragmentManager();
        SettingsFragment frag = (SettingsFragment) manager.findFragmentByTag(FRAGMENT_SETTINGS_TAG);
        if (frag != null)
            frag.updateLocUi(updated);
    }

    public void requestLocationUpdate(final Location location) {
        JSONObject jsonBody = null;
        try {
            jsonBody = new JSONObject("{\"latitude\": \"" + location.getLatitude() + "\", \"longtitude\": \"" + location.getLongitude() + "\",\"userId\": \"" + ConfigData.USER_ID + "\"}");
        } catch (JSONException e) {

        }
        JsonToStringRequest req = new JsonToStringRequest(Request.Method.POST,
                ConfigData.BASE_URL + "/user/updateUserLocation", jsonBody,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.trim().equalsIgnoreCase("true")) {
                            Toast.makeText(getApplicationContext(), R.string.updated_new_loc, Toast.LENGTH_SHORT).show();
                            //mZipcode = "-99";
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.putBoolean(ConfigData.PREF_USE_LOCATION, true);
                            editor.putString(ConfigData.PREF_LATITUDE, location.getLatitude() + "");
                            editor.putString(ConfigData.PREF_LONGITUDE, location.getLongitude() + "");
                            editor.putString(ConfigData.PREF_ZIPCODE, "");
                            editor.apply();
                            SettingsFragment.oldLatitude = location.getLatitude();
                            SettingsFragment.oldLongitude = location.getLongitude();
                            upDateSettings(true);
                            Intent pushNotification = new Intent(ConfigData.LOCATION_CHANGED);
                            LocalBroadcastManager.getInstance(UnoApplication.getAppContext()).sendBroadcast(pushNotification);
                        } else if (response.trim().equalsIgnoreCase("false")) {
                            //SharedPreferences.Editor editor = preferences.edit();
                            //editor.putBoolean(ConfigData.PREF_USE_LOCATION, false);
                            //editor.apply();
                            upDateSettings(false);
                        }
                        //RESPONSE IS true or false
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                upDateSettings(false);
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
                //todo check here
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private int getLocAccuracy() {
        int locationMode = 0;
        String locationProviders;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }
            if (locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY)
                locationMode = ConfigData.LOC_HIGH_ACCURACY;
            else if (locationMode == Settings.Secure.LOCATION_MODE_OFF)
                locationMode = ConfigData.LOC_TURNED_OFF;

        } else {
            locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            if (locationProviders.contains(LocationManager.GPS_PROVIDER) && locationProviders.contains(LocationManager.NETWORK_PROVIDER)) {
                locationMode = ConfigData.LOC_HIGH_ACCURACY;
            } else if (TextUtils.isEmpty(locationProviders)) {
                locationMode = ConfigData.LOC_TURNED_OFF;
            }
        }
        return locationMode;
    }

    int mAccuracyCount = 0;

    private void getLocation(boolean requestPermission) {
        mAccuracyCount = 0;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                enableLoc(false);
            } else if (requestPermission) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
            }
        } else {
            enableLoc(false);
        }
    }

    private void enableLoc(final boolean fromSettings) {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(ConfigData.LOCATION_REQ_ACCURACY);
        locationRequest.setInterval(2000);
        locationRequest.setExpirationDuration(ConfigData.LOCATION_REQ_EXPIRE);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(MainActivity.this).checkLocationSettings(builder.build());
        task.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    task.getResult(ApiException.class);
                    getCurrentLocation(fromSettings);
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                int requestCode = REQUEST_LOCATION;
                                if (fromSettings) {
                                    requestCode = SettingsFragment.REQUEST_LOCATION;
                                    ResolvableApiException resolvable = (ResolvableApiException) exception;
                                    resolvable.startResolutionForResult(MainActivity.this, requestCode);
                                }
                            } catch (IntentSender.SendIntentException e) {
                            } catch (ClassCastException e) {
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            break;
                    }
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //for fragment settings
        if (mCurrentScreen == SCREEN_SETTINGS) {
            Fragment frag = mFragmentManager.findFragmentByTag(FRAGMENT_SETTINGS_TAG);
            if (frag != null) {
                frag.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        } else if (grantResults != null)
            if (grantResults.length > 0)
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
                        getLocation(true);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please allow location access\nto use this app", Toast.LENGTH_SHORT).show();
                }
    }

    ///////////////////////////////
    /*@SuppressLint("NewApi")
    private void unRegisterConnMonitorLollipop() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }
        ConnectivityManager.NetworkCallback netcb = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
            }

            @Override
            public void onLost(Network network) {
                showNetworkUnavailable();
                Toast.makeText(getApplicationContext(), "  Please check your internet connection !  ", Toast.LENGTH_SHORT).show();
            }
        };
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        connectivityManager.unregisterNetworkCallback(netcb);
    }
    @SuppressLint("NewApi")
    private void registerConnMonitorLollipop() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkRequest.Builder builder = new NetworkRequest.Builder();
        connectivityManager.registerNetworkCallback(builder.build(), netcb);
    }

    @SuppressLint("NewApi")
    private ConnectivityManager.NetworkCallback netcb = new ConnectivityManager.NetworkCallback() {
        @Override
        public void onAvailable(Network network) {
        }

        @Override
        public void onLost(Network network) {
            showNetworkUnavailable();
            Toast.makeText(getApplicationContext(), "  Please check your internet connection !  ", Toast.LENGTH_SHORT).show();
        }
    };*/

    /////////
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(ConfigData.CHANNEL_ID, "Knob Messages", importance);
            channel.setDescription("Knob Notifications");
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void getCountryCode() {
        TelephonyManager tm = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
        countryCodeValue = tm.getNetworkCountryIso();
        StringRequest req = new StringRequest(Request.Method.GET,
                "http://ip2c.org/s",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        String[] resparr = response.split(";");
                        if (!resparr[1].equalsIgnoreCase("ZZ"))
                            countryCodeValue = resparr[1];
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String errString = NetUtil.getMessage(error);
                if (!errString.isEmpty())
                    Toast.makeText(getApplicationContext(), errString, Toast.LENGTH_SHORT).show();
                //todo check here
            }
        });
        VolleySingleton.getInstance().addToRequestQueue(req, false);
    }

    private void saveTokenUploaded(boolean saved) {
        // SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(UnoFirebaseMessagingService.this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(ConfigData.PREF_FCM_TOKEN_UPLOADED, saved);
        editor.apply();
    }

    /*private void showBigToast(int resid) {
        try {
            Toast toast = Toast.makeText(getApplicationContext(), resid, Toast.LENGTH_LONG);
            ViewGroup toastLayout = (ViewGroup) toast.getView();
            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            toastTV.setTextSize(18);
            toastTV.setTypeface(ResourcesCompat.getFont(MainActivity.this, R.font.rubikreg));
            toast.show();
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), resid, Toast.LENGTH_LONG).show();
        }
    }*/


    private void goToNotificationSettings(String channel) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (channel != null) {
                if (!channel.isEmpty()) {
                    intent.setAction(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
                    intent.putExtra(Settings.EXTRA_CHANNEL_ID, channel);
                } else
                    intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            } else {
                intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            }
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName());
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("app_package", getPackageName());
            intent.putExtra("app_uid", getApplicationInfo().uid);
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH_MR1) {
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + getPackageName()));
        }
        startActivity(intent);
    }


    //FIRST LOGIN DLG
    private void showNotificationDlg(final String channelId) {
        int notifType = NotificationEnableDlg.NOTIF_APP;
        if (channelId != null)
            if (!channelId.isEmpty())
                notifType = NotificationEnableDlg.NOTIF_CATEG;
        removeFragmentByTag(FRAGMENT_ENABLE_NOTIF_TAG);
        NotificationEnableDlg enableDlg = new NotificationEnableDlg();
        Bundle args = new Bundle();
        args.putInt(NotificationEnableDlg.NOTIF_DLG_TYPE, notifType);
        enableDlg.setArguments(args);
        enableDlg.setEnableNotifnListener(new NotificationEnableDlg.EnableNotifnListener() {
            @Override
            public void enableNotifications() {
                goToNotificationSettings(channelId);
            }
        });
        enableDlg.show(mFragmentManager, FRAGMENT_ENABLE_NOTIF_TAG);
    }

    private void showIntroScreen() {
        Intent intent = new Intent(MainActivity.this, WelcomeActivity.class);
        startActivityForResult(intent, INTRO_SHOWN);
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (hasWindowFocus)
            setupPositionAdjustment();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        setupPositionAdjustment();
    }

    private void setupPositionAdjustment() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            DisplayCutout displayCutout = getWindow().getDecorView().getRootWindowInsets().getDisplayCutout();
            if (displayCutout != null) {
                adjustUiForNotchDisplay(displayCutout.getSafeInsetTop(), displayCutout.getSafeInsetBottom());
            } else {
                int top = getWindow().getDecorView().getRootWindowInsets().getStableInsetTop();
                adjustUiForNotchDisplay(top, 0);
            }
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int top = getWindow().getDecorView().getRootWindowInsets().getStableInsetTop();
            adjustUiForNotchDisplay(top, 0);
        } else {
            int result = 0;
            int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = getResources().getDimensionPixelSize(resourceId);
            } else {
                result = FormatUtils.dpToPx(MainActivity.this, 24);
            }
            adjustUiForNotchDisplay(result, 0);
        }
    }

    private void adjustUiForNotchDisplay(int topMargin, int bottomMargin) {
        // Log.d("UNOLOG", "SGSFFS" + topMargin);
        ConstraintLayout topLayout = findViewById(R.id.constraintLayout);
        CoordinatorLayout.LayoutParams coor = (CoordinatorLayout.LayoutParams) topLayout.getLayoutParams();
        coor.topMargin = (-1) * (topMargin + bottomMargin);
        topLayout.setLayoutParams(coor);
        Space topSpace = findViewById(R.id.topbartext);

        ConstraintLayout.LayoutParams clps = (ConstraintLayout.LayoutParams) topSpace.getLayoutParams();
        int marginExtra = FormatUtils.dpToPx(MainActivity.this, 41);
        clps.topMargin = (topMargin + bottomMargin) + 2 + marginExtra;
        topSpace.setLayoutParams(clps);

        ConstraintLayout.LayoutParams clp = (ConstraintLayout.LayoutParams) mTabLayout.getLayoutParams();
        clp.topMargin = (topMargin + bottomMargin) + 2;
        mTabLayout.setLayoutParams(clp);

    }
}
